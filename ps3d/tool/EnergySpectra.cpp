#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>
#include <cmath>

#include "fftw3.h"

#include "Field.h"
#include "Utils.h"

#define NX 48
#define NY 48
#define NZ 48

using namespace std;
using std::string;

int main( int argc, char** argv ) {
	int		step		= atoi( argv[1] );
	Field*		velx		= new Field( "velx", NX, NY, NZ );
	Field*		vely		= new Field( "vely", NX, NY, NZ );
	Field*		velz		= new Field( "velz", NX, NY, NZ );
	double*		KE		= new double[velx->nf];
	int*		k2		= new int[velx->nf];
	int		k[3];
	char		filename[20];
	ofstream	file;

	cout << "step: " << step << endl;
	velx->Read( step );
	vely->Read( step );
	velz->Read( step );
	velx->Forward();
	vely->Forward();
	velz->Forward();

	/* kinetic energy for the x-wavenumbers */
	for( int ii = 0; ii < velx->nf; ii++ ) {
		velx->IndexToMode( ii, k );
		k2[ii] = k[0]*k[0] + k[1]*k[1] + k[2]*k[2];
		KE[ii] = velx->kVals[ii][0]*velx->kVals[ii][0] + velx->kVals[ii][1]*velx->kVals[ii][1] +
			 vely->kVals[ii][0]*vely->kVals[ii][0] + vely->kVals[ii][1]*vely->kVals[ii][1] +
			 velz->kVals[ii][0]*velz->kVals[ii][0] + velz->kVals[ii][1]*velz->kVals[ii][1];
	}

	sprintf( filename, "kinetic_energy.%.5u.en", step );
	file.open( filename );
	for( int ii = 0; ii < velx->nf; ii++ ) {
		file << sqrt(k2[ii]) << "\t" << KE[ii] << endl;
	}
	file.close();

	delete[] KE;
	delete[] k2;
	delete velx;
	delete vely;
	delete velz;

	return EXIT_SUCCESS;
}
