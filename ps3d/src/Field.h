class Field {
	public:
		Field( std::string _name, int _nx, int _ny, int _nz );
		~Field();
		std::string 	name;
		int		nx;				/* no. nodes in x */
		int		ny;				/* no. nodes in y */
		int		nz;				/* no. nodes in z */
		int		nr;				/* total nodes in real space */
		int		nf;				/* total modes in fourier space */
		double* 	xVals;				/* real space data */
		fftw_complex*	kVals;				/* fourier space data */
		fftw_plan	forward;			/* real to fourier transform */
		fftw_plan	backward;			/* fourier to real transform */
		void		IndexToMode( int i, int* m );	/* mode index to {k_x, k_y, k_z} indices */
		void		IndexToNode( int i, int* n );	/* node index to {x,y,z} indices */
		void		ModeToIndex( int* m, int* i );	/* mode coordinate { k_x, k_y, k_z } to index */
		void		Forward();
		void		Backward();
		void		Copy( Field* field );
		void		Add( Field* field, double a );
		Field*		MapToBuffer();
		void		MapFromBuffer( Field* psiBuf );
		void		Read( int timeStep );
		void		Save( int timeStep );
		Field*		Deriv( int dim, bool trans );		/* returns the field derivative in dim */
	private:
		fftw_complex*	kBuff;
};
