#include <cmath>
#include <cstdlib>
#include <string>
#include <hdf5.h>
#include <fftw3.h>

#include "Field.h"
#include "Backscatter.h"

using namespace std;
using std::string;

/* 
stochastic backscatter with k^4 power spectrum
reference:
	Leith, (1990) Stochastic backscatter in a subgrid-scale model: Plane shear mixing layer
	Phys. Fluids A, vol. 2 297-299
 */

Backscatter::Backscatter( Field* _ax, Field* _ay, Field* _az, Field* _velx, Field* _vely, Field* _velz, 
			  double _dx, double _dt, double _Cb ) 
{
	dx	= _dx;
	dt	= _dt;
	Cb	= _Cb;

	ax	= _ax;
	ay	= _ay;
	az	= _az;
	velx	= _velx;
	vely	= _vely;
	velz	= _velz;

	srand( time( NULL ) );
}

Backscatter::~Backscatter() {}

double Backscatter::GenRand() {
	double x = (2.0*rand())/RAND_MAX - 1.0;

	return exp(-0.5*x*x)/(sqrt(2.0*M_PI));
}

int Backscatter::EtaIJK( int i, int j, int k ) {
	if( i == j || j == k || k == i ) {
		return 0;
	}
	else if( (i == 0 && j == 1 && k == 2) || 
		 (i == 2 && j == 0 && k == 1) || 
		 (i == 1 && j == 2 && k == 0) ) {
		return +1;
	}
	else {
		return -1;
	}
}

void Backscatter::ResetVector() {
	for( int i = 0; i < ax->nr; i++ ) {
		ax->xVals[i] = 0.0;
		ay->xVals[i] = 0.0;
		az->xVals[i] = 0.0;
	}
	for( int i = 0; i < ax->nf; i++ ) {
		ax->kVals[i][0] = ax->kVals[i][1] = 0.0;
		ay->kVals[i][0] = ay->kVals[i][1] = 0.0;
		az->kVals[i][0] = az->kVals[i][1] = 0.0;
	}
}

void Backscatter::Eval() {
	double	Sik[9], S1p5, S2, fac, twoThirds = 2.0/3.0;
	Field*	u[9];
	Field*	phi[3];
	Field*	a_b[3];
	Field*	dPjDk[9];
	Field*	velx_b	= velx->MapToBuffer();
	Field*	vely_b	= vely->MapToBuffer();
	Field*	velz_b	= velz->MapToBuffer();

	a_b[0]	= new Field( "ax_b", velx_b->nx, velx_b->ny, velx_b->nz );
	a_b[1]	= new Field( "ay_b", vely_b->nx, vely_b->ny, vely_b->nz );
	a_b[2]	= new Field( "az_b", velz_b->nx, velz_b->ny, velz_b->nz );
	phi[0]	= new Field( "phi-i", velx_b->nx, velx_b->ny, velx_b->nz );
	phi[1]	= new Field( "phi-j", velx_b->nx, velx_b->ny, velx_b->nz );
	phi[2]	= new Field( "phi-k", velx_b->nx, velx_b->ny, velx_b->nz );

	for( int ii = 0; ii < 3; ii++ ) {
		u[ii+0] = velx_b->Deriv( ii, false );
		u[ii+3] = vely_b->Deriv( ii, false );
		u[ii+6] = velz_b->Deriv( ii, false );
	}

	fac = Cb*pow(dt,1.5)*pow(dx/dt,2);

	for( int i = 0; i < velx->nr; i++ ) {
		/* calculate the deviatoric strain rate tensor */
		for( int ii = 0; ii < 3; ii++ ) {
			for( int kk = 0; kk < 3; kk++ ) {
				Sik[ii*3+kk] = u[ii*3+kk]->xVals[i] + u[kk*3+ii]->xVals[i];
				if( ii == kk ) {
					Sik[ii*3+kk] -= twoThirds*u[ii*3+kk]->xVals[i];
				}
			}
		}

		/* calculate the potentials */
		for( int kk = 0; kk < 3; kk++ ) {
			S2 = 0.0;
			for( int ii = 0; ii < 3; ii++ ) {
				S2 += Sik[ii*3+kk]*Sik[ii*3+kk];
			}
			S1p5 = pow(S2,0.75);

			phi[kk]->xVals[i] = fac*S1p5*GenRand();
		}
	}

	/* differentiate the potentials */
	for( int jj = 0; jj < 3; jj++ ) {
		for( int kk = 0; kk < 3; kk++ ) {
			dPjDk[jj*3+kk] = phi[jj]->Deriv( kk, true );
		}
	}

	ResetVector();

	/* calcualte the acceleration vector */
	for( int jj = 0; jj < 3; jj++ ) {
		for( int kk = 0; kk < 3; kk++ ) {
			a_b[0]->Add( dPjDk[jj*3+kk], EtaIJK( 0, jj, kk ) );
			a_b[1]->Add( dPjDk[jj*3+kk], EtaIJK( 1, jj, kk ) );
			a_b[2]->Add( dPjDk[jj*3+kk], EtaIJK( 2, jj, kk ) );
		}
	}

	ax->MapFromBuffer( a_b[0] );
	ay->MapFromBuffer( a_b[1] );
	az->MapFromBuffer( a_b[2] );

	for( int i = 0; i < 9; i++ ) {
		delete u[i];
		delete dPjDk[i];
	}
	for( int i = 0; i < 3; i++ ) {
		delete phi[i];
		delete a_b[i];
	}
	delete velx_b;
	delete vely_b;
	delete velz_b;
}
