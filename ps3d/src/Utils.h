double CalcViscosity( double l, double u, int n );
void   MeshSave( int nx, int ny, int nz );
void   WriteVTK( Field** fields, int nFields, int step, double time, bool fromHDF5File );
