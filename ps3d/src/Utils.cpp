#include <iostream>
#include <fstream>
#include <cmath>
#include <hdf5.h>
#include <fftw3.h>

#include "Field.h"
#include "Utils.h"

using namespace std;
using std::string;

/*
Calculate the viscosity for the corresponding minimum wave length of two grid points
Referece:	
	Davidson, pp. 582
*/
double CalcViscosity( double l, double u, int n ) {
	double	eta	= 2.0*l/n;	/* 2D Kolmogorov microscale */
	double	nu	= u*eta*eta/l;	/* viscosity */

	return nu;
}

void MeshSave( int nx, int ny, int nz ) {
	hid_t 		file, attrib_id, group_id, attribData_id, fileSpace, fileData, fileSpace2, fileData2, memSpace;
	hsize_t 	a_dims, start[2], count[2], size[2];
	int		attribData;
	char 		filename[40];
	int 		nEls[3];
	int 		nDims		= 3;
	int		nodesPerEl	= 8;
	double		min[3]		= { 0.0, 0.0, 0.0 };
	double		max[3]		= { 2.0*M_PI, 2.0*M_PI, 2.0*M_PI };

	sprintf( filename, "mesh.h5" );
	file = H5Fcreate( filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

	/* mesh dimensionality */
	a_dims = 1;
	attribData = 3; /* mesh dimensionality */
	attribData_id = H5Screate_simple( 1, &a_dims, NULL );
	group_id = H5Gopen( file, "/"/*, H5P_DEFAULT*/ );
	attrib_id = H5Acreate( group_id, "dimensions", H5T_STD_I32BE, attribData_id, H5P_DEFAULT/*, H5P_DEFAULT*/ );
	H5Awrite( attrib_id, H5T_NATIVE_INT, &attribData );
	H5Aclose( attrib_id );
	H5Gclose( group_id );
	H5Sclose( attribData_id );
	
	/* mesh resolution */
	nEls[0] = nx - 1;
	nEls[1] = ny - 1;
	nEls[2] = nz - 1;
	a_dims = 2;
	attribData_id = H5Screate_simple( 1, &a_dims, NULL );
	group_id = H5Gopen( file, "/"/*, H5P_DEFAULT*/ );
	attrib_id = H5Acreate( group_id, "mesh resolution", H5T_STD_I32BE, attribData_id, H5P_DEFAULT/*, H5P_DEFAULT*/ );
	H5Awrite( attrib_id, H5T_NATIVE_INT, nEls );
	H5Aclose( attrib_id );
	H5Gclose( group_id );
	H5Sclose( attribData_id );

	/* max and min coords of mesh */
	count[0] = (hsize_t)nDims;
	fileSpace = H5Screate_simple( 1, count, NULL );
	fileData = H5Dcreate( file, "/min", H5T_NATIVE_DOUBLE, fileSpace, H5P_DEFAULT/*, H5P_DEFAULT, H5P_DEFAULT*/ );
	H5Dwrite( fileData, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, min );
	H5Dclose( fileData );
	H5Sclose( fileSpace );
	fileSpace = H5Screate_simple( 1, count, NULL );
	fileData = H5Dcreate( file, "/max", H5T_NATIVE_DOUBLE, fileSpace, H5P_DEFAULT/*, H5P_DEFAULT, H5P_DEFAULT*/ );
	H5Dwrite( fileData, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, max );
	H5Dclose( fileData );
	H5Sclose( fileSpace );

	/* write the vertices */
 	size[0] = (hsize_t)(nx*ny*nz);
	size[1] = (hsize_t)nDims;
	fileSpace = H5Screate_simple( 2, size, NULL );
	fileData = H5Dcreate( file, "/vertices", H5T_NATIVE_DOUBLE, fileSpace, H5P_DEFAULT/*, H5P_DEFAULT, H5P_DEFAULT*/ );
	size[0] = (hsize_t)nEls[0]*nEls[1]*nEls[2]; /* use linear quads for writing file */
	size[1] = (hsize_t)nodesPerEl;
	fileSpace2 = H5Screate_simple( 2, size, NULL );
	fileData2 = H5Dcreate( file, "/connectivity", H5T_NATIVE_INT, fileSpace2, H5P_DEFAULT/*, H5P_DEFAULT, H5P_DEFAULT*/ );

	count[0] = 1;
	count[1] = nDims;
	memSpace = H5Screate_simple( 2, count, NULL );
	H5Sselect_all( memSpace );

	/* assume a domain of {0,2*pi} in all dimensions */
	for( int node_i = 0; node_i < nx*ny*nz; node_i++ ) {
		int 	xi[3];
		double 	vert[3];
		xi[0] = node_i/(ny*nz);
		xi[1] = (node_i-xi[0]*(ny*nz))/nz;
		xi[2] = (node_i-xi[0]*(ny*nz))%nz;
		vert[0] = 2.0*M_PI*((double)(xi[0]))/nx;
		vert[1] = 2.0*M_PI*((double)(xi[1]))/ny;
		vert[2] = 2.0*M_PI*((double)(xi[2]))/nz;
		start[1] = 0;
		start[0] = node_i;
		H5Sselect_hyperslab( fileSpace, H5S_SELECT_SET, start, NULL, count, NULL );
		H5Dwrite( fileData, H5T_NATIVE_DOUBLE, memSpace, fileSpace, H5P_DEFAULT, vert );
	}
	H5Sclose( memSpace );
	H5Dclose( fileData );
	H5Sclose( fileSpace );

	H5Sget_simple_extent_dims( fileSpace2, size, NULL );
	count[0] = 1;
	count[1] = size[1];
	memSpace = H5Screate_simple( 2, count, NULL );
	H5Sselect_all( memSpace );
	
	for( int el_i = 0; el_i < nEls[0]; el_i++ ) {
		for( int el_j = 0; el_j < nEls[1]; el_j++ ) {
			for( int el_k = 0; el_k < nEls[2]; el_k++ ) {
				int elNodes[8];

				elNodes[0] = el_i*(nEls[1]+1)*(nEls[2]+1) + el_j*(nEls[2]+1) + el_k;
				elNodes[1] = elNodes[0]+1;
				elNodes[2] = elNodes[1]+nz;
				elNodes[3] = elNodes[2]-1;
				elNodes[4] = elNodes[0]+ny*nz;
				elNodes[5] = elNodes[4]+1;
				elNodes[6] = elNodes[5]+nz;
				elNodes[7] = elNodes[6]-1;

				start[1] = 0;
				start[0] = el_i*nEls[1]*nEls[2] + el_j*nEls[2] + el_k;
				H5Sselect_hyperslab( fileSpace2, H5S_SELECT_SET, start, NULL, count, NULL );
				H5Dwrite( fileData2, H5T_NATIVE_INT, memSpace, fileSpace2, H5P_DEFAULT, elNodes );
			}
		}
	}

	H5Sclose( memSpace );
	H5Dclose( fileData2 );
	H5Sclose( fileSpace2 );
	H5Fclose( file );
}

/* sourman's revised vtk file header writing, see:
   https://www.underworldproject.org/hg/StgFEM/file/5a295377c216/plugins/VTKRegularFeVariableOutput/VTKRegularFeVariableOutput.c */
void WriteVTK( Field** fields, int nFields, int step, double time, bool fromHDF5File ) {
	char 		filename[50];
	ofstream 	file;
	char		fieldname[50];
	hid_t		h5file, fileData, fileSpace, memSpace;
	hsize_t		size[2], maxSize[2], start[2], count[2];
	double*		buf[1];

	cout << "writing output for step: " << step << endl;

	sprintf( filename, "Fields.%05d.vtk", step );
	file.open( filename );
	file << "# vtk DataFile Version 2.0" << endl;
	file << "NSModel Field: mesh fields, time=" << time << endl;
	file << "ASCII" << endl;
	file << "DATASET STRUCTURED_POINTS" << endl;
	file << "DIMENSIONS " << fields[0]->nx << " " << fields[0]->ny << " " << fields[0]->nz << endl;
	file << "ORIGIN " << M_PI << " " << M_PI << " " << M_PI << endl;
	file << "SPACING " << 2.0*M_PI/fields[0]->nx << " " << 2.0*M_PI/fields[0]->ny << " " << 2.0*M_PI/fields[0]->nz << endl;
	file << "POINT_DATA " << fields[0]->nx*fields[0]->ny*fields[0]->nz << endl;
	for( int field_i = 0; field_i < nFields; field_i++ ) {
		if( fromHDF5File ) {
			sprintf( fieldname, "%s.%.5u.h5", fields[field_i]->name.c_str(), step );
			h5file 		= H5Fopen( fieldname, H5F_ACC_RDONLY, H5P_DEFAULT );
			fileData 	= H5Dopen( h5file, "/data" );
			fileSpace 	= H5Dget_space( fileData );

			H5Sget_simple_extent_dims( fileSpace, size, maxSize );
			start[1] = 0;
			count[0] = 1;
			count[1] = 1;
			memSpace = H5Screate_simple( 2, count, NULL );
		}

		file << "SCALARS " << fields[field_i]->name << " double 1" << endl;
		file << "LOOKUP_TABLE default" << endl;

		if( fromHDF5File ) {
			for( int node_i = 0; node_i < fields[field_i]->nr; node_i++ ) {
				start[0] = node_i;
				H5Sselect_hyperslab( fileSpace, H5S_SELECT_SET, start, NULL, count, NULL );
				H5Sselect_all( memSpace );
				H5Dread( fileData, H5T_NATIVE_DOUBLE, memSpace, fileSpace, H5P_DEFAULT, buf );
				file << buf[0] << endl;
			}
			H5Dclose( fileData );
			H5Sclose( memSpace );
			H5Sclose( fileSpace );
			H5Fclose( h5file );
		}
		else {
			for( int node_i = 0; node_i < fields[field_i]->nr; node_i++ ) {
				file << fields[field_i]->xVals[node_i] << endl;
			}
		}
	}
	file.close();
}
