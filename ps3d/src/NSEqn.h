class NSEqn {
	public:
		NSEqn( Field* _velx, Field* _vely, Field* _velz, Field* _pres, double _nu, double _dt );
		~NSEqn();
		Field*	velx;
		Field*	vely;
		Field*	velz;
		Field*	pres;
		double	nu;
		double	dt;
		Field*	fx;
		Field*	fy;
		Field*	fz;
		Field*	Convolve( Field* psi, Field* us, Field* uy, Field* uz );
		void	Solve( Field* velxPrev, Field* velyPrev, Field* velzPrev );
	private:
		void	FirstOrder();
		void	SecondOrder( Field* velxPrev, Field* velyPrev, Field* velzPrev );
};
