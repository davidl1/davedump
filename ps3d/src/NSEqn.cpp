#include <string>
#include <fstream>
#include <iostream>
#include <cmath>
#include <fftw3.h>
#include "Field.h"
#include "Utils.h"
#include "NSEqn.h"

/*
Solve the incompressible Navier Stokes equations to second order in time
Reference:
	Karniadakis, Isreali and Orszag (1991) High-Order Splitting Methods for 
	the Incompressible Navier-Stokes Equations, J. Comp. Phys. 97, 414-443
*/

using namespace std;
using std::string;

NSEqn::NSEqn( Field* _velx, Field* _vely, Field* _velz, Field* _pres, double _nu, double _dt ) {
	velx	= _velx;
	vely	= _vely;
	velz	= _velz;
	pres	= _pres;

	nu	= _nu;
	dt	= _dt;

	fx	= NULL;
	fy	= NULL;
	fz	= NULL;
}

NSEqn::~NSEqn() {}

Field* NSEqn::Convolve( Field* psi, Field* ux, Field* uy, Field* uz ) {
	char	fname[50];
	Field*	psiBuf 		= psi->MapToBuffer();
	Field*	convBuf		= new Field( "convBuf", psiBuf->nx, psiBuf->ny, psiBuf->nz );
	Field*	dPsiDx;
	Field*	dPsiDy;
	Field*	dPsiDz;
	Field*	uxBuf  		= ux->MapToBuffer();
	Field*	uyBuf  		= uy->MapToBuffer();
	Field*	uzBuf  		= uz->MapToBuffer();
	Field*	conv;

	sprintf( fname, "%s-conv", psi->name.c_str() );
	conv = new Field( fname, psi->nx, psi->ny, psi->nz );

	dPsiDx = psiBuf->Deriv( 0, false );
	dPsiDy = psiBuf->Deriv( 1, false );
	dPsiDz = psiBuf->Deriv( 2, false );

	for( int i = 0; i < convBuf->nr; i++ ) {
		convBuf->xVals[i] = uxBuf->xVals[i]*dPsiDx->xVals[i] + uyBuf->xVals[i]*dPsiDy->xVals[i] + uzBuf->xVals[i]*dPsiDz->xVals[i];
	}
	conv->MapFromBuffer( convBuf );

	delete psiBuf;
	delete dPsiDx;
	delete dPsiDy;
	delete dPsiDz;
	delete convBuf;
	delete uxBuf;
	delete uyBuf;
	delete uzBuf;

	return conv;
}

void NSEqn::Solve( Field* velxPrev, Field* velyPrev, Field* velzPrev ) {
	Field*	velxTemp	= NULL;
	Field*	velyTemp	= NULL;
	Field*	velzTemp	= NULL;

	if( velxPrev && velyPrev && velzPrev ) {
		velxTemp = new Field( "velx-temp", velx->nx, velx->ny, velx->nz );
		velyTemp = new Field( "vely-temp", vely->nx, vely->ny, vely->nz );
		velzTemp = new Field( "velz-temp", velz->nx, velz->ny, velz->nz );
		velxTemp->Copy( velx );
		velyTemp->Copy( vely );
		velzTemp->Copy( velz );

		SecondOrder( velxPrev, velyPrev, velzPrev );

		velxPrev->Copy( velxTemp );
		velyPrev->Copy( velyTemp );
		velzPrev->Copy( velzTemp );
		delete velxTemp;
		delete velyTemp;
		delete velzTemp;
	}
	else {
		FirstOrder();
	}
}

void NSEqn::FirstOrder() {
	int	k[3], k2;
	double	inv;
	Field*	convx		= Convolve( velx, velx, vely, velz );
	Field*	convy		= Convolve( vely, velx, vely, velz );
	Field*	convz		= Convolve( velz, velx, vely, velz );
	Field*	uxHat		= new Field( "uxHat", velx->nx, velx->ny, velx->nz );
	Field*	uyHat		= new Field( "uyHat", vely->nx, vely->ny, vely->nz );
	Field*	uzHat		= new Field( "uzHat", velz->nx, velz->ny, velz->nz );
	Field*	uxHatHat	= new Field( "uxHatHat", velx->nx, velx->ny, velx->nz );
	Field*	uyHatHat	= new Field( "uyHatHat", vely->nx, vely->ny, vely->nz );
	Field*	uzHatHat	= new Field( "uzHatHat", velz->nx, velz->ny, velz->nz );

	/* 1. calculate the intermediate velocity fields */
	for( int i = 1; i < pres->nf; i++ ) {
		uxHat->kVals[i][0] = velx->kVals[i][0] - dt*convx->kVals[i][0];
		uxHat->kVals[i][1] = velx->kVals[i][1] - dt*convx->kVals[i][1];

		uyHat->kVals[i][0] = vely->kVals[i][0] - dt*convy->kVals[i][0];
		uyHat->kVals[i][1] = vely->kVals[i][1] - dt*convy->kVals[i][1];

		uzHat->kVals[i][0] = velz->kVals[i][0] - dt*convz->kVals[i][0];
		uzHat->kVals[i][1] = velz->kVals[i][1] - dt*convz->kVals[i][1];
	}

	/* 2. calculate the change in pressure */
	Field* uxHatDx = uxHat->Deriv( 0, false );
	Field* uyHatDy = uyHat->Deriv( 1, false );
	Field* uzHatDz = uzHat->Deriv( 2, false );

	for( int i = 1; i < pres->nf; i++ ) {
		pres->IndexToMode( i, k );
		k2 = -(k[0]*k[0] + k[1]*k[1] + k[2]*k[2]);
		inv = 1.0/dt/k2;
		pres->kVals[i][0] = inv*(uxHatDx->kVals[i][0] + uyHatDy->kVals[i][0] + uzHatDz->kVals[i][0]);
		pres->kVals[i][1] = inv*(uxHatDx->kVals[i][1] + uyHatDy->kVals[i][1] + uzHatDz->kVals[i][1]);
	}

	/* 3. add the pressure correction to the velocities */
	Field* dPdx = pres->Deriv( 0, false );
	Field* dPdy = pres->Deriv( 1, false );
	Field* dPdz = pres->Deriv( 2, false );

	for( int i = 1; i < pres->nf; i++ ) {
		uxHatHat->kVals[i][0] = uxHat->kVals[i][0] - dt*dPdx->kVals[i][0];
		uxHatHat->kVals[i][1] = uxHat->kVals[i][1] - dt*dPdx->kVals[i][1];

		uyHatHat->kVals[i][0] = uyHat->kVals[i][0] - dt*dPdy->kVals[i][0];
		uyHatHat->kVals[i][1] = uyHat->kVals[i][1] - dt*dPdy->kVals[i][1];

		uzHatHat->kVals[i][0] = uzHat->kVals[i][0] - dt*dPdz->kVals[i][0];
		uzHatHat->kVals[i][1] = uzHat->kVals[i][1] - dt*dPdz->kVals[i][1];
	}

	/* 4. apply the viscosity */
	for( int i = 1; i < pres->nf; i++ ) {
		pres->IndexToMode( i, k );
		k2 = -(k[0]*k[0] + k[1]*k[1] + k[2]*k[2]);
		inv = 1.0/(1.0 + dt*nu*k2*k2);

		velx->kVals[i][0] = inv*uxHatHat->kVals[i][0];
		velx->kVals[i][1] = inv*uxHatHat->kVals[i][1];

		vely->kVals[i][0] = inv*uyHatHat->kVals[i][0];
		vely->kVals[i][1] = inv*uyHatHat->kVals[i][1];

		velz->kVals[i][0] = inv*uzHatHat->kVals[i][0];
		velz->kVals[i][1] = inv*uzHatHat->kVals[i][1];
	}

	velx->Backward();
	vely->Backward();
	velz->Backward();
	pres->Backward();

	delete convx;		delete convy;		delete convz;
	delete uxHat;		delete uyHat;		delete uzHat;
	delete uxHatHat;	delete uyHatHat;	delete uzHatHat;
	delete uxHatDx;		delete uyHatDy;		delete uzHatDz;
	delete dPdx;		delete dPdy;		delete dPdz;
}

void NSEqn::SecondOrder( Field* velxPrev, Field* velyPrev, Field* velzPrev ) {
	int	k[3], k2;
	double	inv;
	Field*	convx		= Convolve( velx, velx, vely, velz );
	Field*	convy		= Convolve( vely, velx, vely, velz );
	Field*	convz		= Convolve( velz, velx, vely, velz );
	Field*	convxp		= Convolve( velxPrev, velxPrev, velyPrev, velzPrev );
	Field*	convyp		= Convolve( velyPrev, velxPrev, velyPrev, velzPrev );
	Field*	convzp		= Convolve( velzPrev, velxPrev, velyPrev, velzPrev );
	Field*	uxHat		= new Field( "uxHat", velx->nx, velx->ny, velx->nz );
	Field*	uyHat		= new Field( "uyHat", vely->nx, vely->ny, vely->nz );
	Field*	uzHat		= new Field( "uzHat", velz->nx, velz->ny, velz->nz );
	Field*	uxHatHat	= new Field( "uxHatHat", velx->nx, velx->ny, velx->nz );
	Field*	uyHatHat	= new Field( "uyHatHat", vely->nx, vely->ny, vely->nz );
	Field*	uzHatHat	= new Field( "uzHatHat", velz->nx, velz->ny, velz->nz );

	/* 1. calculate the intermediate velocity fields */
	for( int i = 1; i < pres->nf; i++ ) {
		uxHat->kVals[i][0] = 2.0*velx->kVals[i][0] - 0.5*velxPrev->kVals[i][0] - 2.0*dt*convx->kVals[i][0] + dt*convxp->kVals[i][0];
		uxHat->kVals[i][1] = 2.0*velx->kVals[i][1] - 0.5*velxPrev->kVals[i][1] - 2.0*dt*convx->kVals[i][1] + dt*convxp->kVals[i][1];

		uyHat->kVals[i][0] = 2.0*vely->kVals[i][0] - 0.5*velyPrev->kVals[i][0] - 2.0*dt*convy->kVals[i][0] + dt*convyp->kVals[i][0];
		uyHat->kVals[i][1] = 2.0*vely->kVals[i][1] - 0.5*velyPrev->kVals[i][1] - 2.0*dt*convy->kVals[i][1] + dt*convyp->kVals[i][1];

		uzHat->kVals[i][0] = 2.0*velz->kVals[i][0] - 0.5*velzPrev->kVals[i][0] - 2.0*dt*convz->kVals[i][0] + dt*convzp->kVals[i][0];
		uzHat->kVals[i][1] = 2.0*velz->kVals[i][1] - 0.5*velzPrev->kVals[i][1] - 2.0*dt*convz->kVals[i][1] + dt*convzp->kVals[i][1];
	}

	/* 2. calculate the change in pressure */
	Field* uxHatDx = uxHat->Deriv( 0, false );
	Field* uyHatDy = uyHat->Deriv( 1, false );
	Field* uzHatDz = uzHat->Deriv( 2, false );

	for( int i = 1; i < pres->nf; i++ ) {
		pres->IndexToMode( i, k );
		k2 = -(k[0]*k[0] + k[1]*k[1] + k[2]*k[2]);
		inv = 1.0/dt/k2;
		pres->kVals[i][0] = inv*(uxHatDx->kVals[i][0] + uyHatDy->kVals[i][0] + uzHatDz->kVals[i][0]);
		pres->kVals[i][1] = inv*(uxHatDx->kVals[i][1] + uyHatDy->kVals[i][1] + uzHatDz->kVals[i][1]);
	}

	/* 3. add the pressure correction to the velocities */
	Field* dPdx = pres->Deriv( 0, false );
	Field* dPdy = pres->Deriv( 1, false );
	Field* dPdz = pres->Deriv( 2, false );

	for( int i = 1; i < pres->nf; i++ ) {
		uxHatHat->kVals[i][0] = uxHat->kVals[i][0] - dt*dPdx->kVals[i][0];
		uxHatHat->kVals[i][1] = uxHat->kVals[i][1] - dt*dPdx->kVals[i][1];

		uyHatHat->kVals[i][0] = uyHat->kVals[i][0] - dt*dPdy->kVals[i][0];
		uyHatHat->kVals[i][1] = uyHat->kVals[i][1] - dt*dPdy->kVals[i][1];

		uzHatHat->kVals[i][0] = uzHat->kVals[i][0] - dt*dPdz->kVals[i][0];
		uzHatHat->kVals[i][1] = uzHat->kVals[i][1] - dt*dPdz->kVals[i][1];
	}

	/* 4. apply the viscosity */
	for( int i = 1; i < pres->nf; i++ ) {
		pres->IndexToMode( i, k );
		k2 = -(k[0]*k[0] + k[1]*k[1] + k[2]*k[2]);
		inv = 1.0/(1.5 + dt*nu*k2*k2);

		velx->kVals[i][0] = inv*uxHatHat->kVals[i][0];
		velx->kVals[i][1] = inv*uxHatHat->kVals[i][1];

		vely->kVals[i][0] = inv*uyHatHat->kVals[i][0];
		vely->kVals[i][1] = inv*uyHatHat->kVals[i][1];

		velz->kVals[i][0] = inv*uzHatHat->kVals[i][0];
		velz->kVals[i][1] = inv*uzHatHat->kVals[i][1];
	}

	velx->Backward();
	vely->Backward();
	velz->Backward();
	pres->Backward();

	delete convx;		delete convy;		delete convz;
	delete convxp;		delete convyp;		delete convzp;
	delete uxHat;		delete uyHat;		delete uzHat;
	delete uxHatHat;	delete uyHatHat;	delete uzHatHat;
	delete uxHatDx;		delete uyHatDy;		delete uzHatDz;
	delete dPdx;		delete dPdy;		delete dPdz;
}
