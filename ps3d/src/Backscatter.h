class Backscatter {
	public:
		Backscatter( Field* _ax,Field* _ay, Field* _az, Field* _velx, Field* _vely, Field* _velz, 
			     double _dx, double _dt, double _Cb );
		~Backscatter();
		double	dx;		/* resolution length scale - TODO?? */
		double	dt;		/* resolution time scale - TODO?? */
		double	Cb;		/* Smagorinsky constant */
		Field* 	ax;
		Field* 	ay;
		Field* 	az;
		Field*	velx;
		Field*	vely;
		Field*	velz;
		void	Eval();
	private:
		double	GenRand();
		int	EtaIJK( int i, int j, int k );
		void	ResetVector();
};
