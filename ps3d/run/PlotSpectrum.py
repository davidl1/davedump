#!/usr/bin/env python

import sys
import numpy as np
import matplotlib.pyplot as plt
import pylab

a = int(sys.argv[1])
buf = "%.5u" % a

A=np.loadtxt('kinetic_energy.' + buf + '.en')
A=A[1:,:]
k=A[:,0]
e=A[:,1]

inds = np.argsort(k)
k = k[inds]
e = e[inds]

ii    = 0
k2    = np.zeros(len(k))
e2    = np.zeros(len(k))
n2    = np.zeros(len(k))
count = 0
while True:
	jj = ii

	ei = np.abs(e[ii])
	while True:
		jj = jj + 1
		if jj >= len(k):
			break

		if np.abs(k[jj]-k[ii]) > 1.0e-4:
			break

		ei = ei + np.abs(e[jj])
	
	k2[count] = k[ii]
	e2[count] = ei
	n2[count] = jj - ii

	ii = jj

	if ii >= len(k):
		break

	count = count + 1

k2 = k2[:count]
e2 = e2[:count]
n2 = n2[:count]

e2 = e2/n2
k  = k2
e  = e2

plt.loglog(k,e,'.')
plt.loglog(k,np.power(e[0],0.92)*np.power(k,-5.0/3.0))
plt.show()
