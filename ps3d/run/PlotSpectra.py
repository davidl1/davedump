#!/usr/bin/env python

import sys
import numpy as np
import matplotlib.pyplot as plt

a = int(sys.argv[1])
buf = "%.5u" % a

A=np.loadtxt('kinetic_energy.' + buf + '.en')
A=A[1:,:]
K=A[:,0]
E=A[:,1]

plt.loglog(K,E,'.')
plt.loglog(K,np.power(E[0],0.92)*np.power(K,-5.0/3.0))
plt.show()
