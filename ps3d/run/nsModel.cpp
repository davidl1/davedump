#include <string>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <hdf5.h>
#include <fftw3.h>

#include "Field.h"
#include "Utils.h"
#include "NSEqn.h"

#define NX 48
#define NY 48
#define NZ 48

using namespace std;
using std::string;

void InitPhi( Field* phi ) {
	int k[3], ki;

	srand(101);
	for( int l = 0; l < 4; l++ ) {
		do {
			k[0] = rand()%8;
			k[1] = rand()%8;
			k[2] = rand()%8;
		} while( k[0] == 0 || k[1] == 0 || k[2] == 0 );
		phi->ModeToIndex(k,&ki);
		phi->kVals[ki][0] = 0.1;
		cout << "kx[" << l << "]: " << k[0] << "\tky[" << l << "]: " << k[1] << "\tkz[" << l << "]:" << k[2] << endl;
	}
	phi->Backward();
}

int main( int argc, char** argv ) {
	Field*		pres		= new Field( "pres", NX, NY, NZ );
	Field*		velx		= new Field( "velx", NX, NY, NZ );
	Field*		vely		= new Field( "vely", NX, NY, NZ );
	Field*		velz		= new Field( "velz", NX, NY, NZ );
	Field*		velxPrev	= new Field( "velx-prev", NX, NY, NZ );
	Field*		velyPrev	= new Field( "vely-prev", NX, NY, NZ );
	Field*		velzPrev	= new Field( "velz-prev", NX, NY, NZ );
	Field*		fields[4];
	double		dt		= 0.125*M_PI/NX;
	int		dumpEvery	= 1;
	double		nu		= 20.0/pow(NX,4);
	double		time		= 0.0;
	NSEqn*		ns		= new NSEqn( velx, vely, velz, pres, nu, dt );

	cout << "Time step (dt):\t" << dt << endl;
	cout << "Viscosity (nu):\t" << nu << endl;

	fields[0] = pres;
	fields[1] = velx;
	fields[2] = vely;
	fields[3] = velz;

	InitPhi( pres );
	InitPhi( velx );
	InitPhi( vely );
	InitPhi( velz );
	WriteVTK( fields, 4, 0, time, false );

	time += dt;
	velxPrev->Copy( velx );
	velyPrev->Copy( vely );
	velzPrev->Copy( velz );

	ns->Solve( NULL, NULL, NULL );
	WriteVTK( fields, 4, 1, time, false );

	for( int s = 2; s <= 4; s++ ) {
		time += dt;
		ns->Solve( velxPrev, velyPrev, velzPrev );
		if( s%dumpEvery == 0 ) {
			WriteVTK( fields, 4, s, time, false );
		}
	}

	delete pres;
	delete velx;
	delete vely;
	delete velz;
	delete velxPrev;
	delete velyPrev;
	delete velzPrev;
	delete ns;

	return EXIT_SUCCESS;
}
