#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

A=np.loadtxt('buffer.test')
NX=A[:,0]
ET=A[:,1]
EF=A[:,2]

plt.semilogy(NX,ET,'-o')
plt.semilogy(NX,EF,'-x')
plt.legend(('to-err','from-err'),loc=0)
plt.show()
