#include <string>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>
#include <hdf5.h>
#include <fftw3.h>

#include "Field.h"
#include "Utils.h"

using namespace std;
using std::string;

double Sech( double x, double a ) { return 1.0/cosh(a*(x-M_PI)); }

double CalcErr( Field* phiA, Field* phiN ) {
	double errSq = 0.0, normSq = 0.0;

	for( int i = 0; i < phiA->nr; i++ ) {
		errSq  += (phiA->xVals[i] - phiN->xVals[i])*(phiA->xVals[i] - phiN->xVals[i]);
		normSq += phiA->xVals[i]*phiA->xVals[i];
	}

	return sqrt( errSq/normSq );
}

void TestBuffering( int nx, double* toErr, double* fromErr ) {
	Field*	phi	= new Field( "phi", nx, nx, nx );
	Field*	phi_b	= new Field( "phi_b", nx+nx/2, nx+nx/2, nx+nx/2 );
	Field*	phi_f	= new Field( "phi_f", nx, nx, nx );
	Field*	phi_t	= NULL;
	int 	n[3];
	double	x[3];
	double	a = 4.0, b = 4.0, c = 4.0;

	for( int i = 0; i < phi->nr; i++ ) {
		phi->IndexToNode( i, n );
		x[0] = (2.0*M_PI*n[0])/nx;
		x[1] = (2.0*M_PI*n[1])/nx;
		x[2] = (2.0*M_PI*n[2])/nx;

		phi->xVals[i] = Sech(x[0],a)*Sech(x[1],b)*Sech(x[2],c);
	}
	phi_t = phi->MapToBuffer();

	for( int i = 0; i < phi_b->nr; i++ ) {
		phi_b->IndexToNode( i, n );
		x[0] = (2.0*M_PI*n[0])/(nx+nx/2);
		x[1] = (2.0*M_PI*n[1])/(nx+nx/2);
		x[2] = (2.0*M_PI*n[2])/(nx+nx/2);

		phi_b->xVals[i] = Sech(x[0],a)*Sech(x[1],b)*Sech(x[2],c);
	}
	phi_f->MapFromBuffer( phi_b );

	*toErr   = CalcErr( phi_b, phi_t );
	*fromErr = CalcErr( phi, phi_f );

	cout << "nx: " << nx << "\tTo Err: " << *toErr << "\tFrom Err: " << *fromErr << endl;

	delete phi;
	delete phi_b;
	delete phi_t;
	delete phi_f;
}

int main( int argc, char** argv ) {
	int		nx[8]	= { 16, 32, 48, 64, 80, 96, 112, 128 };
	double		toErr[8], fromErr[8];
	ofstream	file;
	
	file.open( "buffer.test" );
	for( int res = 0; res < 8; res++ ) {
		TestBuffering( nx[res], &toErr[res], &fromErr[res] );
		file << nx[res] << "\t" << toErr[res] << "\t" << fromErr[res] << endl;
	}
	file.close();

	return EXIT_SUCCESS;
}
