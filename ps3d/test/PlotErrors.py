#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

A=np.loadtxt('deriv.test')
NX=A[:,0]
EX=A[:,1]
EY=A[:,2]
EZ=A[:,3]

plt.semilogy(NX,EX,'-o')
plt.semilogy(NX,EY,'-x')
plt.semilogy(NX,EZ,'-.')
plt.legend(('x-err','y-err','z-err'),loc=0)
plt.show()
