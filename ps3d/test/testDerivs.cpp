#include <string>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>
#include <hdf5.h>
#include <fftw3.h>

#include "Field.h"
#include "Utils.h"

using namespace std;
using std::string;

double Sech( double x, double a ) { return 1.0/cosh(a*(x-M_PI)); }

double CalcErr( Field* phiA, Field* phiN ) {
	double errSq = 0.0, normSq = 0.0;

	for( int i = 0; i < phiA->nr; i++ ) {
		errSq  += (phiA->xVals[i] - phiN->xVals[i])*(phiA->xVals[i] - phiN->xVals[i]);
		normSq += phiA->xVals[i]*phiA->xVals[i];
	}

	return sqrt( errSq/normSq );
}

void TestDeriv( int nx, double* xErr, double* yErr, double* zErr ) {
	Field*	phi	= new Field( "phi", nx, nx, nx );
	Field*	dPhiDxA	= new Field( "dPhiDxA", nx, nx, nx );
	Field*	dPhiDyA	= new Field( "dPhiDyA", nx, nx, nx );
	Field*	dPhiDzA	= new Field( "dPhiDzA", nx, nx, nx );
	Field*	dPhiDxN = NULL;
	Field*	dPhiDyN = NULL;
	Field*	dPhiDzN = NULL;
	int	n[3];
	double	x[3];
	double	a = 4.0, b = 4.0, c = 4.0;

	for( int i = 0; i < phi->nr; i++ ) {
		phi->IndexToNode( i, n );
		x[0] = (2.0*M_PI*n[0])/nx;
		x[1] = (2.0*M_PI*n[1])/nx;
		x[2] = (2.0*M_PI*n[2])/nx;

		phi->xVals[i] = Sech(x[0],a)*Sech(x[1],b)*Sech(x[2],c);
		dPhiDxA->xVals[i] = -a*tanh(a*(x[0]-M_PI))*phi->xVals[i];
		dPhiDyA->xVals[i] = -b*tanh(b*(x[1]-M_PI))*phi->xVals[i];
		dPhiDzA->xVals[i] = -c*tanh(c*(x[2]-M_PI))*phi->xVals[i];
	}

	dPhiDxN = phi->Deriv( 0, true );
	dPhiDyN = phi->Deriv( 1, true );
	dPhiDzN = phi->Deriv( 2, true );

	*xErr = CalcErr( dPhiDxA, dPhiDxN );
	*yErr = CalcErr( dPhiDyA, dPhiDyN );
	*zErr = CalcErr( dPhiDzA, dPhiDzN );

	cout << "nx: " << nx << "\tdPhiDx Err: " << *xErr << endl;
	cout << "ny: " << nx << "\tdPhiDy Err: " << *yErr << endl;
	cout << "nz: " << nx << "\tdPhiDz Err: " << *zErr << endl;

	delete phi;
	delete dPhiDxA;	delete dPhiDyA;	delete dPhiDzA;
	delete dPhiDxN;	delete dPhiDyN;	delete dPhiDzN;
}

int main( int argc, char** argv ) {
	int		nx[8]	= { 16, 32, 48, 64, 80, 96, 112, 128 };
	double		xErr[8], yErr[8], zErr[8];
	ofstream	file;
	
	file.open( "deriv.test" );
	for( int res = 0; res < 8; res++ ) {
		TestDeriv( nx[res], &xErr[res], &yErr[res], &zErr[res] );
		file << nx[res] << "\t" << xErr[res] << "\t" << yErr[res] << "\t" << zErr[res] << endl;
	}
	file.close();

	return EXIT_SUCCESS;
}
