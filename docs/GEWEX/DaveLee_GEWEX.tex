\documentclass[landscape,a0paper,fontscale=0.285]{baposter} % Adjust the font scale/size here

\usepackage{graphicx} % Required for including images
%\graphicspath{{figures/}} % Directory in which figures are stored

\usepackage{amsmath} % For typesetting math
\usepackage{amssymb} % Adds new symbols to be used in math mode

\usepackage{booktabs} % Top and bottom rules for tables
\usepackage{enumitem} % Used to reduce itemize/enumerate spacing
\usepackage{palatino} % Use the Palatino font
\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures

\usepackage{multicol} % Required for multiple columns
\setlength{\columnsep}{1.5em} % Slightly increase the space between columns
\setlength{\columnseprule}{0mm} % No horizontal rule between columns

\usepackage{tikz} % Required for flow chart
\usetikzlibrary{shapes,arrows} % Tikz libraries required for the flow chart in the template

\newcommand{\compresslist}{ % Define a command to reduce spacing within itemize/enumerate environments, this is used right after \begin{itemize} or \begin{enumerate}
\setlength{\itemsep}{1pt}
\setlength{\parskip}{0pt}
\setlength{\parsep}{0pt}
}

\definecolor{lightblue}{rgb}{0.145,0.6666,1} % Defines the color used for content box headers

\begin{document}

\begin{poster}
{
headerborder=closed, % Adds a border around the header of content boxes
colspacing=1em, % Column spacing
bgColorOne=white, % Background color for the gradient on the left side of the poster
bgColorTwo=white, % Background color for the gradient on the right side of the poster
borderColor=lightblue, % Border color
headerColorOne=black, % Background color for the header in the content boxes (left side)
headerColorTwo=lightblue, % Background color for the header in the content boxes (right side)
headerFontColor=white, % Text color for the header text in the content boxes
boxColorOne=white, % Background color of the content boxes
textborder=roundedleft, % Format of the border around content boxes, can be: none, bars, coils, triangles, rectangle, rounded, roundedsmall, roundedright or faded
eyecatcher=true, % Set to false for ignoring the left logo in the title and move the title left
headerheight=0.1\textheight, % Height of the header
headershape=roundedright, % Specify the rounded corner in the content box headers, can be: rectangle, small-rounded, roundedright, roundedleft or rounded
headerfont=\Large\bf\textsc, % Large, bold and sans serif font in the headers of content boxes
%textfont={\setlength{\parindent}{1.5em}}, % Uncomment for paragraph indentation
linewidth=2pt % Width of the border lines around content boxes
}
%----------------------------------------------------------------------------------------
%       TITLE SECTION 
%----------------------------------------------------------------------------------------
%
{\includegraphics[height=0.06\textwidth]{coecss-logo-2.jpg}}
{\bf\textsc{Convection Permitting Models of the Maritime Continent}\vspace{0.0em}}
{\textsc{David Lee$^1$, Todd Lane$^1$, Muhammad Hassim$^1$ \& Stuart Webster$^2$ \hspace{12pt}
1: ARC CoE Climate System Science, University of Melbourne, 2: UK Met Office}}
{\includegraphics[height=0.06\textwidth]{melb-uni-logo.jpg}}

\headerbox{Background}{name=background,column=0,row=0}{
The Maritime Continent is a particularly challenging region for weather and climate models owing to the complex orogrophy of the
Indonesian Archipelago, the diurnal cycle and sea breezes induced by these islands and the interaction between these convective 
systems and larger scale structures such as the Madden-Julian Oscillation (MJO) and the trade winds.
\newline
\newline
High resolution ($\Delta x = 1.33km$) nested models with different configurations have been used to investigate tropical convection
in the Maritime Continent during a period of suppressed MJO (early February 2010) using the Unified Model (MetUM, version 8.5). Deep 
convection occurs at scales of $\mathcal{O}(10^4m)$, such that resolutions of $\mathcal{O}(10^2m)$ are required to properly resolve 
these structures. The results presented here can therefore be regarded only as \emph{convection permitting}, in the sense that 
convection is represented explicitly at grid scales but not properly resolved\cite{BryanEtAl_MWR_03}.
}

\headerbox{Wet Bias of Tropical Convection}{name=introduction,column=0,row=1,below=background}{
Comparisons against the CPOL radar at Darwin and the TRMM satellite product over the south western Maritime Continent
show a significant wet bias in accumulated rainfall over a six day period.

\vspace{-1.0em}
\begin{center}
\includegraphics[height=0.36\textwidth,width=0.32\textwidth]{accum_rain/accum_rain_cpol_2p5_6days.png}
\includegraphics[height=0.36\textwidth,width=0.32\textwidth]{accum_rain/accum_rain_wrf_6days.png}
\includegraphics[height=0.36\textwidth,width=0.32\textwidth]{accum_rain/accum_rain_darwin_6days.png}\\
\vspace{-1.0em}
\includegraphics[height=0.32\textwidth,width=0.40\textwidth]{accum_rain/accum_rain_trmm_6days.png}
\includegraphics[height=0.32\textwidth,width=0.40\textwidth]{accum_rain/accum_rain_6days.png}
\end{center}

\vspace{-1.0em}
Representations of the diurnal cycle over Darwin are improved with the omission of sub-grid convection and the use of 
conservative interpolation of moisture in the semi-Lagrangian advection scheme.

\vspace{-1.0em}
\begin{center}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{diurnal_cycle/avg_diurnal_cycle_cpol_6days.png}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{diurnal_cycle/avg_diurnal_cycle_wrf_darwin_6days.png}
%\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{diurnal_cycle/avg_diurnal_cycle_dt15_6days.png}
%\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{diurnal_cycle/avg_diurnal_cycle_dt15_NoConv_6days.png}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{diurnal_cycle/avg_diurnal_cycle_dt15_NoConv_SL02_6days.png}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{diurnal_cycle/avg_diurnal_cycle_dt15_NoConv_SL02_3DNoBlend_6days.png}
\end{center}
}

\headerbox{Moisture Budget}{name=moisturebudget,column=1,row=0}{
The moisture budget for the regional domain is determined as:
\begin{equation*}
\frac{1}{\Omega}\int_\Omega\frac{\mathrm{d}CWT}{\mathrm{dt}}=  \frac{E}{L_v} - P - \int_\mathrm{z}\rho\Big(
\mathbf{u}\cdot\frac{\partial q}{\partial\mathbf{x}} +
\mathbf{u}\cdot\frac{\partial q_{cl}}{\partial\mathbf{x}} +
\mathbf{u}\cdot\frac{\partial q_{cf}}{\partial\mathbf{x}}
\Big)\mathrm{dz} + \mathrm{Rd}\Omega
\end{equation*}

\begin{indent}
\begin{tabular}{c l|c l}
$CWT$        & Total column moisture            & $E$          & Evaporation\\
$q$          & Water vapor                      & $\rho$       & Density\\
$q_{cl}$     & Cloud (liquid)                   & $P$          & Precipitation\\
$q_{cf}$     & Cloud (ice)                      & $\mathrm{R}$ & Residual\\
$L_v$        & Latent heat of vaporisation      & $\Omega$     & Regional domain\\
\end{tabular}
\end{indent}
\\

Moisture conservation is improved by using high order interpolants in the semi-Lagrangian advection scheme, 
and correcting for the negative moisture thus produced.
Average residual values from day 5 onwards are 0.078{mm}, 0.079{mm} and 0.047{mm} respectively for the 
base case, the no convection scheme case and the no convection-high order interpolation case.

\vspace{-1.0em}
\begin{center}
\includegraphics[height=0.28\textwidth,width=0.32\textwidth]{moisture_budget/moisture_budget_dt15_2.png}
\includegraphics[height=0.28\textwidth,width=0.32\textwidth]{moisture_budget/moisture_budget_NoConv_2.png}
\includegraphics[height=0.28\textwidth,width=0.32\textwidth]{moisture_budget/moisture_budget_NoConv_SL02_2.png}
\end{center}
}

\headerbox{Kinetic Energy Spectra}{name=kespectra,column=1,row=1,below=moisturebudget}{
Despite not being truly within the inertial range, owing to the presence of convection at grid scales,
the kinetic energy spectra still appear to follow well the -5/3 energy cascade. The vertically and meridionally
averaged spectra~\cite{Skamarock_MWR_04} show reduced energy transfer at mesoscales when the time step is decreased 
and the convection scheme removed. 
\newline
\newline
Use of the 3D Smagorinsky turbulence scheme in place of the blended scheme~\cite{BoutleEtAl_MWR_14} 
in the vertical results in increased dissipation at resolved scales ($>8\Delta x$) beyond that predicted by the 5/3 law, 
but improved energy transfer rates at small scales.

\vspace{-1.0em}
\begin{center}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{ke_spectra/ke_spectra_darwin_dt50.png}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{ke_spectra/ke_spectra_darwin_dt15_NoConv.png}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{ke_spectra/ke_spectra_darwin_dt15_NoConv_SL02_3DNoBlend.png}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{ke_spectra/ke_spectra_darwin_dt15_NoConv_SL02_3pm.png}
\end{center}

\vspace{-1.0em}
At 3pm during the peak of the diurnal cycle, energy is injected into the system at convective scales, disrupting the
5/3 cascade.
}

\headerbox{Vertical Structure}{name=verticalstructure,column=2,row=0}{ 
Comparisons to radiosonde measurements at Darwin and a WRF realisation at the same resolution shows a distinct wet bias in
the UM around the freezing level ($\sim 5000m$). Previous studies of precipitation in the Darwin region using WRF have shown
the clouds that are too high and narrow~\cite{CaineEtAl_MWR_13}. This problem is exacerbated in the UM simulations.

\vspace{-1.0em}
\begin{center}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{vertical_structure/thz_darwin_local.png}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{vertical_structure/qvz_darwin_local.png}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{vertical_structure/wuz_cpol_blended.png}
%\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{vertical_structure/qiz_cpol_wrf.png}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{vertical_structure/qiz_wrf_cloud.png}
\end{center}

The vertical diffusion coefficients are larger in the blended sub grid scheme than the 3D Smagorinsky scheme, which 
suppresses the vertical updraft of moisture and the concentration of ice above the freezing level.

\vspace{-1.0em}
\begin{center}
%\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{vertical_structure/smz_cpol_box.png}
%\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{vertical_structure/smz_cpol_blended.png}
%\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{vertical_structure/qiz_cpol_box.png}
%\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{vertical_structure/qiz_cpol_blended.png}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{vertical_structure/smz_smag3d_cloud.png}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{vertical_structure/smz_blend_cloud.png}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{vertical_structure/qiz_smag3d_cloud.png}
\includegraphics[height=0.22\textwidth,width=0.24\textwidth]{vertical_structure/qiz_blend_cloud.png}
\end{center}
}

\headerbox{Conclusions}{name=conclusions,column=2,row=1,below=verticalstructure}{
There appears to be a positive bias in water vapor around the freezing level in the MetUM. Convective
updrafts are therefore producing too much cloud above the freezing level which is causing a strong
positive rainfall bias. The problem is exacerbated with the use of higher resolution in time and space, 
and somewhat mitigated by increased horizontal diffusion to suppress the updrafts of moisture in the 
3D Smagorinsky scheme. 
}

\headerbox{References}{name=references,column=2,row=2,below=conclusions}{
\renewcommand{\section}[2]{\vskip 0.05em} % Get rid of the default "References" section title
%\nocite{*} % Insert publications even if they are not cited in the poster
\small{ 
\bibliographystyle{unsrt}
\bibliography{DaveLee_GEWEX} 
}
}

\end{poster}

\end{document}
