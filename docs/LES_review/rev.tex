\documentclass{article}
\begin{document}

\noindent{\bf Commutative filtering}\\
Second order accurate filtering for nonuniform grids.~\cite{GM95}\\
Extended by description of a class of filters with arbitrary order accuracy. Difference between the filtered and
unfiltered boundary conditions is the same order as the filter error and so the original boundary conditions still
apply.~\cite{VLM98}\\

\noindent{\bf Stochastic backscatter}\\
Backscatter with a $k^4$ spectrum for 3D isotropic turbulence. Spatial and temporal scales of the
subgrid modes are small with respect to the cut off length scale and time step of the filter, such that
the backscattered modes are decorrelated in space and time.~\cite{Leith90}\\
Scheme refined for boundary layers where the subgrid scales are no longer in an intertial range.~\cite{MT92}\\

\noindent{\bf Dynamic SGS closure}\\
Second, wider filter intruduced so that comparison of stress tensors may be used to determine Smagorinsky constant
C dynamically. Better models near wall turbulence and allows for negative viscosity, "backscatter", but not with
$k^4$ spectrum.~\cite{Ger91}\\
Modification for least squares determination of C to remove potential singularity.~\cite{Lilly92}\\
Persistent negative viscosity backscatter may lead to numerical instability. This was removed in a constrained
version of the dynamic localization model.~\cite{Gho95}\\

\noindent{\bf Relation to Convection}\\
For LES to work consistently, the scales of motion must be much larger than the grid spacing, which in turn must
be much larger than the dissipative scale, and most of the energy must reside in the resolved scales. However 
deep convection occurs on a scale of $\mathcal{O}(10^4 m)$ and so these scales are not properly resolved unless a 
grid spacing of $\mathcal{O}(10^2 m)$ is applied.~\cite{BWF03}

\begin{thebibliography}{99}
\bibitem{GM95} S. Ghosal and P. Moin (1995) The Basic Equations for the Large Eddy Simulation of Turbulent Flows in
Complex Geometry \emph{J. Comp. Phys.} $\mathbf{118}$ 24--37

\bibitem{VLM98} O. V. Vasilyev, T. S. Lund and P. Moin (1998) A General Class of Cummutative Filters for LES in Complex 
Geometries \emph{J. Comp. Phys.} $\mathbf{146}$ 82--104

\bibitem{Leith90} C. E. Leith (1990) Stochastic backscatter in a subgrid-scale model: Plane shear mixing layer
\emph{Phys. Fluids A} $\mathbf{2}$ 297--299

\bibitem{MT92} P. J. Mason and D. J. Thomson (1992) Stochastic backscatter in large-eddy simulations of boundary layers
\emph{J. Fluid Mech.} $\mathbf{242}$ 51--78

\bibitem{Ger91} M. Germano, U. Piomelli, P. Moin and W. H. Cabot (1991) A dynamic subgrid-scale eddy viscosity model
\emph{Phys. Fluids A} $\mathbf{3}$ 1760--1765

\bibitem{Lilly92} D. K. Lilly (1992) A proposed modification of the Germano subgrid-scale closure method 
\emph{Phys. Fluids A} $\mathbf{4}$ 633--635

\bibitem{Gho95} S. Ghosal, T. S. Lund, P. Moin and K. Akselvoll (1995) A dynamic localization model for large-eddy
simulation of turbulent flows \emph{J. Fluid Mech.} $\mathbf{286}$ 229--255

\bibitem{BWF03} G. H. Bryan, J. C. Wyngaard and J. M. Fritsch (2003) Resolution Requirements for the Simulation of
Deep Moist Convection \emph{Mon. Wea. Rev.} $\mathbf{131}$ 2394--2416

\end{thebibliography}

\end{document}
