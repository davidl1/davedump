#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>

#include "fftw3.h"

#include "Field.h"
#include "Utils.h"

#define NX 256
#define NY 256

using namespace std;
using std::string;

int main( int argc, char** argv ) {
	int		step		= atoi( argv[1] );
	int		maxStep		= atoi( argv[2] );
	double		pr, pi;
	double*		Ex		= new double[NY/2+1];
	double*		Ey		= new double[NX/2+1];
	Field*		phi		= new Field( "phi", NX, NY );
	char		filename[20];
	ofstream	file;

	for( int s = step; s <= maxStep; s += step ) {
		cout << "step: " << s << endl;
		phi->Read( s );
		phi->Forward();

		/* kinetic energy for the x-wavenumbers */
		for( int ky = 0; ky < NY/2+1; ky++ ) {
			pr = phi->kVals[ky][0];
			pi = phi->kVals[ky][1];
			Ex[ky] = ky*ky*( pr*pr + pi*pi );
		}
		/* kinetic energy of the y-wavenumbers */
		for( int kx = 0; kx < NX/2+1; kx ++ ) {
			pr = phi->kVals[kx*(NY/2+1)][0];
			pi = phi->kVals[kx*(NY/2+1)][1];
			Ey[kx] = kx*kx*( pr*pr + pi*pi );
		}

		sprintf( filename, "phi.%.5u.en", s );
		file.open( filename );
		for( int k = 0; k < NX/2+1; k++ ) {
			file << k << "\t" << Ex[k] << "\t" << Ey[k] << endl;
		}
		file.close();
	}

	delete[] Ex;
	delete[] Ey;
	delete phi;

	return EXIT_SUCCESS;
}
