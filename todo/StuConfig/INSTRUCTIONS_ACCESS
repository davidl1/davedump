
BUILDING THE UPDATED LAM EXECUTABLE
===================================

(1) copy ./umui/fcm-mkpatch-out_extra_70338.tar.gz onto accesscollab.

(2) you need to gzip -d this file and then tar -xf the resulting file.

(3) Probably best if Scott installs this on the FCM repository.
    This should be used instead of the vn8.5_ukv_p_extra branch already 
    on your FCM. Perhaps call it vn8.5_ukv_p_extra2 ?

(4) Once this has been installed, then on the  UMUI, Scott should 
    take a copy of the EXEC build job you have been using up until now. 
    Point this EXEC job at the new branch and submit it to create the new 
    executable.

(5) For the LAM conservation code, repeat steps 1 to 3 for 
    ./umui/fcm-mkpatch-out_lam_conservation2_70546.tar.gz.
    Take a copy of the exec job used in step 4 and add this lam_conservation branch
    as a second one to the list of branches used in the FCM compilation table.


UPLOADING THE UPDATED UMUI JOBS
===============================

(1) Dave, copy the contents of ./umui/fcast_basis_files_for_ACCESS onto Accesscollab.

(2) Scott, please take a copy of the standard sacb experiment.

(3) Open up the jobs in the new experiment and then upload the basis files 
    into the new jobs for E,F N and O. Jobs A and B are fine as they are (though see (6) below)

(4) The only changes you should then need to make to these jobs are to make sure 
    that the "User Information and submission Method" ->
    "Job submission method" is the same as in sacb, and that the follow
    on "PBSpro" window has qsub set in the top entry.

(5) Actually, you need to make sure that the compilation window is pointing at the
    updated EXEC job for jobs E, F, N and O.

(6) Also, update the global RCF job A by simply turning on soil moisture stress as follows:-
Difference in window subindep_Recon_Gen
 -> Model Selection
   -> Reconfiguration
     -> General Reconfiguration Options
Check box: Using soil moisture stress for interpolating soil moisture
 Entry is set to 'ON'    (it was 'OFF') . 

DAVE, NOTE THIS WILL AFFECT THE SOIL MOISTURE IN YOUR EXPERIMENTS, SO ITS WORTH TESTING THE IMPACT OF
THIS SINGLE SWITCH IN ISOLATION.  

UPLOADING CLIMATOLOGICAL AEROSOLS SOURCE DATA
=============================================

(1) Dave, copy the contents of /data/nwp/app/hadsw/NS/clim_aerosols/split_tar_gz
    onto raijin. 
(2) Catenate the gzipped tar file back together by typing cat * > aero.tar.gz

(3) Scott, could you then put them somewhere central.

(4) Scott, gzip -d the file and then tar -xf the resulting file. The resulting
    data should be about 11Gb in size.

(5) Note the location of your data - this will be needed for the updated
    setup scripts (the sites/access/env file as described below).


UPDATING THE HPC SCRIPTS
========================

(1) Dave, copy hpc/hpc/tar.gz onto raijin. Scott, could you copy this 
    centrally into something like a /NS/vn8.5/vna subdirectory?.

(2) Scott, gzip -d this file and tar -xf the resulting file.

(3) Scott, edit the set_up/sites/access/env file to set the directory for the climatological aerosols
    to the location noted in point (5) above. See set_up/sites/mss/env file, line 26 for what needs to be set.

(4) Your copies of the setup_nesting scripts (both those used already and the ones for the future) then need to 
    be updated to point at 
    export   TOP_BOT_DIR=...../NS/vn8.5/vna/start_end  (line 225 ish)
    export ns_set_up_dir=...../NS/vn8.5/vna/set_up (line 250ish)


