#!/usr/bin/env python

# Generate the diurnal cycle for the CPOL region from the TRMM data

import numpy as np
from scipy.io import netcdf
from matplotlib import pyplot as plt
from matplotlib import colors as cls
from scipy.interpolate import *
import pylab

def ReadField(filename,fieldname):
        print 'from: ',filename,'\treading: ',fieldname
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
        return field[:]

def ReadScalar(filename,varname):
        print 'from: ',filename,'\treading: ',varname
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[varname]
        infile.close()
        return field.getValue()

directory = '/mnt/coe0/davidl1/NewGuinea/TRMM/'
filename  = '3B42.201002'
days      = [4,5,6,7,9,11]

lat_trmm  = ReadField(directory+filename+'02.00.7.nc','latitude')
lon_trmm  = ReadField(directory+filename+'02.00.7.nc','longitude')

#xinds = np.arange(48)+1219
#yinds = np.arange(48)+126
xinds = np.arange(49)+1219
yinds = np.arange(49)+126
lon_trmm   = lon_trmm[xinds]
lat_trmm   = lat_trmm[yinds]
ny    = len(lat_trmm)
nx    = len(lon_trmm)
print nx,ny

#get the land sea mask
lat_um = ReadField('../020112/lsm.nc','latitude')
lon_um = ReadField('../020112/lsm.nc','longitude')
lsm_um = ReadField('../020112/lsm.nc','lsm')
lsm_um = lsm_um[0,0,:,:]

totrain = np.zeros((ny,nx))
for day in days:
	for time in 3*np.arange(8):
		rain    = ReadField(directory+filename+'%.2u'%day+'.'+'%.2u'%time+'.7.nc','pcp')
		rain2   = rain[0,yinds[0]:yinds[-1]+1,xinds[0]:xinds[-1]+1]
		full    = rain2>0.0
		#totrain = totrain + 3.0*full*rain2
		totrain = totrain + full*rain2

# get the cpol domain mask
directory_cpol = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km_2/'
filename_cpol  = 'cpol_2d_rainrate_2010'

Re   = 6378100.0
lon_cpol = ReadScalar(directory_cpol+filename_cpol+'0201_0000.nc','radar_longitude')
lat_cpol = ReadScalar(directory_cpol+filename_cpol+'0201_0000.nc','radar_latitude')
x    = ReadField(directory_cpol+filename_cpol+'0201_0000.nc','x')
y    = ReadField(directory_cpol+filename_cpol+'0201_0000.nc','y')
x    = 1000.0*x
y    = 1000.0*y
dx   = (x[-1]-x[0])/len(x)
dy   = (y[-1]-y[0])/len(y)
lon_cpol  = lon_cpol + x*360.0/2.0/np.pi/Re
lat_cpol  = lat_cpol + y*360.0/2.0/np.pi/Re
pcp = ReadField(directory_cpol+filename_cpol+'0202_0000.nc','rr')
empty_cpol = pcp[0,:,:]<-10.0
full_cpol  = (empty_cpol+1)%2

# interpolate the mask to the trmm and cpol points
pts_trmm=np.transpose([np.tile(lon_trmm,len(lat_trmm)),np.repeat(lat_trmm,len(lon_trmm))])
pts_cpol=np.transpose([np.tile(lon_cpol,len(lat_cpol)),np.repeat(lat_cpol,len(lon_cpol))])
pts_um  =np.transpose([np.tile(lon_um  ,len(lat_um  )),np.repeat(lat_um  ,len(lon_um  ))])
lsmr=np.ravel(lsm_um)
fullr=np.ravel(full_cpol)

lsm_trmm=griddata(pts_um,lsmr,pts_trmm,method='nearest')
lsm_trmm=lsm_trmm.reshape(len(lat_trmm),len(lon_trmm))
slm_trmm=(lsm_trmm+1)%2

lsm_cpol=griddata(pts_um,lsmr,pts_cpol,method='nearest')
lsm_cpol=lsm_cpol.reshape(len(lat_cpol),len(lon_cpol))

full_trmm=griddata(pts_cpol,fullr,pts_trmm,method='nearest')
full_trmm=full_trmm.reshape(len(lat_trmm),len(lon_trmm))

nt    = np.sum(full_trmm)
nl    = np.sum(full_trmm*lsm_trmm)
ns    = np.sum(full_trmm*slm_trmm)

lrain = np.zeros(8)
srain = np.zeros(8)
days  = [4,5,6,7,9,11]
mins  = [0]

for day in days:
        for hr in np.arange(8):
		rain    = ReadField(directory+filename+'%.2u'%day+'.'+'%.2u'%(3*hr)+'.7.nc','pcp')
		rain2   = rain[0,yinds[0]:yinds[-1]+1,xinds[0]:xinds[-1]+1]
		full    = rain2>0.0
		#rain3   = full*rain2
		rain3   = full*rain2/3.0

		lrain[hr] = lrain[hr] + np.sum(full_trmm*lsm_trmm*rain3)/nl
		srain[hr] = srain[hr] + np.sum(full_trmm*slm_trmm*rain3)/ns

lrain  = lrain/len(days)
srain  = srain/len(days)
lrain2 = np.zeros(8)
srain2 = np.zeros(8)
GMT=9
#time   = 3*np.arange(8)+GMT
#time   = time%24
time    = 3*np.arange(8)
#for ii in np.arange(8):
#        ind        = (ii-3)%8
#        lrain2[ii] = lrain[ind]
#        srain2[ii] = srain[ind]

time = (time + GMT - 1.5)%24.0
inds = np.argsort(time)
print time
print inds
time = time[inds]
print time
lrain2 = lrain[inds]
srain2 = srain[inds]

time3 = np.zeros(10)
lrain3 = np.zeros(10)
srain3 = np.zeros(10)
time3[0]  =-1.5
time3[1:9]=time
time3[9]  =25.5
lrain3[0]  =lrain2[-1]
lrain3[1:9]=lrain2
lrain3[9]  =lrain2[0]
srain3[0]  =srain2[-1]
srain3[1:9]=srain2
srain3[9]  =srain2[0]

fig=plt.figure()
ax=fig.add_subplot(111)
plt.plot(time3,lrain3,'-o')
plt.plot(time3,srain3,'-o')
plt.legend(('land','sea'))
plt.xlim([0,24])
plt.ylim([0.0,1.0])
plt.xlabel('Local time (hrs)')
plt.ylabel('Precipitation rate (mm)')
plt.title('Avg. diurnal cycle (TRMM)')
pylab.savefig('avg_diurnal_cycle_darwin_trmm_2.png')
plt.show()

np.savetxt('diurnal_darwin_trmm_time.txt',time3)
np.savetxt('diurnal_darwin_trmm_land.txt',lrain3)
np.savetxt('diurnal_darwin_trmm_sea.txt', srain3)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon_cpol,lat_cpol,lsm_cpol,colors='k')
#co = ax.contourf(lon,lat,totrain)
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
#levs=[0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
#levs=[0.,2.,5.,10.,20.,40.,80.,120.,160.,200.]
levs=[0.,5.,10.,20.,40.,80.,120.,240.,320.,400.]
cmap=cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm=cls.BoundaryNorm(levs,cmap.N)
co = ax.contourf(lon_trmm,lat_trmm,full_trmm*totrain,levels=levs,cmap=cmap,norm=norm)
plt.colorbar(co,orientation='vertical')
#plt.title('Accumulated rainfall for 144 hours - TRMM')
#plt.show()

