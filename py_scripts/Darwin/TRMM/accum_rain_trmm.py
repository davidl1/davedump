#!/usr/bin/env python

# Generate an accumulated rainfall plot from the TRMM data

import numpy as np
from scipy.io import netcdf
from matplotlib import pyplot as plt
from matplotlib import colors as cls
import pylab
from Utils import *

directory = '/mnt/coe0/davidl1/NewGuinea/TRMM/'
filename  = '3B42.201002'
days      = [2,4,5,7,9]

lat  = ReadField(directory+filename+'02.00.7.nc','latitude')
lon  = ReadField(directory+filename+'02.00.7.nc','longitude')

xinds = np.arange(48)+1219
yinds = np.arange(48)+126
lon   = lon[xinds]
lat   = lat[yinds]
ny    = len(lat)
nx    = len(lon)
print nx,ny

#get the land sea mask
lat2 = ReadField('../020112/lsm.nc','latitude')
lon2 = ReadField('../020112/lsm.nc','longitude')
lsm2 = ReadField('../020112/lsm.nc','lsm')
lsm2 = lsm2[0,0,:,:]

totrain = np.zeros((ny,nx))
for day in days:
	for time in 3*np.arange(8):
		rain    = ReadField(directory+filename+'%.2u'%day+'.'+'%.2u'%time+'.7.nc','pcp')
		rain2   = rain[0,yinds[0]:yinds[-1]+1,xinds[0]:xinds[-1]+1]
		full    = rain2>0.0
		totrain = totrain + 3.0*full*rain2

# interpolate the mask to the trmm points
pts0=np.transpose([np.tile(lon2,len(lat2)),np.repeat(lat2,len(lon2))])
pts1=np.transpose([np.tile(lon,ny),np.repeat(lat,nx)])
lsmr=np.ravel(lsm2)
lsm1=griddata(pts0,lsmr,pts1,method='nearest')
lsm =lsm1.reshape(ny,nx)
slm =(lsm+1)%2

# calculate the area for the grid points
Re = 6378100.0
dx = 2.0*np.pi*Re*(lon[-1]-lon[0])/(len(lon)-1)/360.0
dx = dx*np.ones(len(lon))
dy = np.zeros(len(lat))
for ii in np.arange(len(dy))-1:
        dy[ii] = 2.0*np.pi*Re*(lat[ii+1]-lat[ii])/360.0
dy[-1] = dy[-2]
area = np.outer(dy,dx)

# calculate averages - full domain
totland = np.sum(lsm*area)
totsea  = np.sum(slm*area)
totlandrain = np.sum(lsm*area*totrain)
totsearain  = np.sum(slm*area*totrain)
print 'accumulated rainfall averages - full domain'
print 'avg. precip. land:  ', totlandrain/totland
print 'avg. precip. sea:   ', totsearain/totsea
print 'avg. precip. total: ', np.sum(totrain*area)/np.sum(area)


fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon2,lat2,lsm2,colors='k')
#co = ax.contourf(lon,lat,totrain)
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
levs=[0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
cmap=cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm=cls.BoundaryNorm(levs,cmap.N)
co = ax.contourf(lon,lat,totrain,levels=levs,cmap=cmap,norm=norm)
plt.xlim([lon2[0],lon2[-1]])
plt.ylim([lat2[0],lat2[-1]])
plt.colorbar(co,orientation='vertical')
plt.title('Accumulated rainfall for 120 hours - TRMM')
pylab.savefig('accum_rain_120_trmm.png')
plt.show()

lsm = InterpGrid(lon2,lat2,lon,lat,lsm2,'nearest')
slm = (lsm+1)%2

np.savetxt('lon_trmm.txt',lon)
np.savetxt('lat_trmm.txt',lat)
np.savetxt('pcp_trmm_full.txt',totrain)
np.savetxt('pcp_trmm_land.txt',lsm*totrain)
np.savetxt('pcp_trmm_sea.txt', slm*totrain)
