#!/usr/bin/env python

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
from matplotlib import colors as cls
import numpy as np
import pylab

def ReadFile(filename,fieldname,scaled):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	if scaled:
		m = field.scale_factor
		c = field.add_offset
		infile.close()
		return m*field[:,:,:]+c

	else:
		infile.close()
		return field[:]

def InterpGrid(xin,yin,xout,yout,A,interpType):
	print 'interp type: ', interpType
	pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
	pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
	Ar      = np.ravel(A)
	Aor     = griddata(pts_in,Ar,pts_out,method=interpType)
	Ao      = Aor.reshape(len(yout),len(xout))
	return Ao

def GenMask(lon,lat,x,y,r,prcp,pmin):
        mask = np.zeros((len(lat),len(lon)))
        pnts = np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
        pnts[:,0] = pnts[:,0] - x
        pnts[:,1] = pnts[:,1] - y
        radi = np.sqrt(pnts[:,0]*pnts[:,0]+pnts[:,1]*pnts[:,1])
        radi = radi.reshape(len(lat),len(lon))
        mask = np.zeros(radi.shape)
        for ii in np.arange(radi.shape[0]):
                for jj in np.arange(radi.shape[1]):
                        if radi[ii,jj] < r and prcp[ii,jj] > pmin:
                                mask[ii,jj] = 1

	return mask

def GenMaskBox(lon,lat,xi,xf,yi,yf,precip,pmin):
        mask = np.zeros((len(lat),len(lon)))
        for ii in np.arange(mask.shape[0]):
                for jj in np.arange(mask.shape[1]):
			if lat[ii] > yi and lat[ii] < yf and lon[jj] > xi and lon[jj] < xf:
				if precip[ii,jj] > pmin:
					mask[ii,jj] = 1

	return mask

def InterpZ(phi,hgt,htz,mask):
	phiz = np.zeros((len(htz),phi.shape[1],phi.shape[2]))
	for ii in np.arange(phi.shape[1]):
		for jj in np.arange(phi.shape[2]):
			if mask[ii,jj]:
				phiz[:,ii,jj]=griddata(hgt[:,ii,jj],phi[:,ii,jj],htz,method='cubic')

	return phiz

xi=233
xf=467
yi=473
yf=707

lat  = ReadFile('data/XLAT_d03_NewGuinea.nc','XLAT',False)
lon  = ReadFile('data/XLONG_d03_NewGuinea.nc','XLONG',False)
lsm  = ReadFile('data/LANDMASK_d03_NewGuinea.nc','LANDMASK',False)

lat  = lat[0,yi:yf,0]
lon  = lon[0,0,xi:xf]
lsm  = lsm[0,yi:yf,xi:xf]
slm  = (lsm+1)%2
nx   = len(lon)
ny   = len(lat)

rain_c1  = ReadFile('data/prcp_d03_2010-02-02_08','RAINC',False)
rain_nc1 = ReadFile('data/prcp_d03_2010-02-02_08','RAINNC',False)
rain_c0  = ReadFile('data/prcp_d03_2010-02-02_04','RAINC',False)
rain_nc0 = ReadFile('data/prcp_d03_2010-02-02_04','RAINNC',False)
rain     = rain_c1[0,yi:yf,xi:xf] + rain_nc1[0,yi:yf,xi:xf] - rain_c0[0,yi:yf,xi:xf] - rain_nc0[0,yi:yf,xi:xf]

#mask      = GenMask(lon,lat,131.14,-13.26,0.5,rain,0.01)
mask     = GenMaskBox(lon,lat,130.0,131.6,-12.0,-11.1,rain,10.0)

#npInv = 1.0/lsm.shape[0]/lsm.shape[1]
npInv    = 1.0/np.sum(mask)
print 'num pts: ',np.sum(mask)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon[0:300],lat[100:400],lsm[100:400,0:300],colors='k')
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
#levs = [0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
#levs = [0.,2.,5.,10.,20.,40.,80.,120.,160.,200.]
cmap = cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
#norm = cls.BoundaryNorm(levs,cmap.N)
#co   = ax.contourf(lon[xi:xf],lat[yi:yf],rain[yi:yf,xi:xf],levels=levs,cmap=cmap,norm=norm)
co   = ax.contourf(lon[0:300],lat[100:400],mask[100:400,0:300]*rain[100:400,0:300],cmap=cmap)
plt.colorbar(co,orientation='vertical')
plt.title('Accumulated rainfall for 4 hours (mm) - (WRF)')
pylab.savefig('wrf_hector.png')
plt.show()

th = ReadFile('data/T_020205.nc','T',True)[0,:,yi:yf,xi:xf]
wwz = np.zeros(th.shape[0])
wuz = np.zeros(th.shape[0])
wtz = np.zeros(th.shape[0])
qvz = np.zeros(th.shape[0])
qrz = np.zeros(th.shape[0])
qiz = np.zeros(th.shape[0])
qcz = np.zeros(th.shape[0])
qsz = np.zeros(th.shape[0])
qgz = np.zeros(th.shape[0])
thz = np.zeros(th.shape[0])

times = [5,6,7,8]
for time in times:
	th = ReadFile('data/T_0202'+'%.2d'%time+'.nc','T',True)[0,:,yi:yf,xi:xf]
	ux = ReadFile('data/U_0202'+'%.2d'%time+'.nc','U',True)[0,:,yi:yf,xi:xf]
	uy = ReadFile('data/V_0202'+'%.2d'%time+'.nc','V',True)[0,:,yi:yf,xi:xf]
	uz = ReadFile('data/W_0202'+'%.2d'%time+'.nc','W',True)[0,:,yi:yf,xi:xf]
	qv = ReadFile('data/QV_0202'+'%.2d'%time+'.nc','QVAPOR',True)[0,:,yi:yf,xi:xf]
	qr = ReadFile('data/QR_0202'+'%.2d'%time+'.nc','QRAIN',True)[0,:,yi:yf,xi:xf]
	qi = ReadFile('data/QI_0202'+'%.2d'%time+'.nc','QICE',True)[0,:,yi:yf,xi:xf]
	qc = ReadFile('data/QC_0202'+'%.2d'%time+'.nc','QCLOUD',True)[0,:,yi:yf,xi:xf]
	qs = ReadFile('data/QS_0202'+'%.2d'%time+'.nc','QSNOW',True)[0,:,yi:yf,xi:xf]
	qg = ReadFile('data/QG_0202'+'%.2d'%time+'.nc','QGRAUP',True)[0,:,yi:yf,xi:xf]

	phi  = ReadFile('data/PH_0202'+'%.2d'%time+'.nc','PH',True)[0,:,yi:yf,xi:xf]
	phi0 = ReadFile('data/PHB_0202'+'%.2d'%time+'.nc','PHB',True)[0,:,yi:yf,xi:xf]

	# generate the height fields on rho and theta levels
	hgt1 = np.zeros((phi.shape))
	gInv = 1.0/9.8
	print 'generating height field... (1)'
	hgt1[:,:,:] = gInv*(phi[:,:,:] + phi0[:,:,:])

	print 'generating height field... (2)'
	hgt2 = np.zeros(qv.shape)
	for ii in np.arange(qv.shape[0]):
		hgt2[ii,:,:] = 0.5*(hgt1[ii,:,:] + hgt1[ii+1,:,:])

	imax=0
	jmax=0
	hmax=-1.0e+99
	for ii in np.arange(hgt1.shape[1]):
		for jj in np.arange(hgt1.shape[2]):
			if mask[ii,jj]:
				if hgt1[0,ii,jj] > hmax:
					hmax = hgt1[0,ii,jj]
					imax = ii
					jmax = jj

	htz1 = hgt1[:,imax,jmax]
	htz2 = hgt2[:,imax,jmax]

	# interpolate to consistent height levels
	uzi = InterpZ(uz,hgt1,htz2,mask)
	uxi = InterpZ(ux,hgt2,htz2,mask)
	uyi = InterpZ(uy,hgt2,htz2,mask)
	qvi = InterpZ(qv,hgt2,htz2,mask)
	qri = InterpZ(qr,hgt2,htz2,mask)
	qii = InterpZ(qi,hgt2,htz2,mask)
	qci = InterpZ(qc,hgt2,htz2,mask)
	qsi = InterpZ(qs,hgt2,htz2,mask)
	qgi = InterpZ(qg,hgt2,htz2,mask)
	thi = InterpZ(th,hgt2,htz2,mask)

	# remove mean flow components
	thj = np.zeros(np.shape(thi))
	for ii in np.arange(len(htz2)):
		uxi[ii,:,:] = uxi[ii,:,:] - npInv*np.sum(uxi[ii,:,:])
		uyi[ii,:,:] = uyi[ii,:,:] - npInv*np.sum(uyi[ii,:,:])
		thj[ii,:,:] = thi[ii,:,:] - npInv*np.sum(thi[ii,:,:])

	for ii in np.arange(len(htz2)):
		wwz[ii] = wwz[ii] + npInv*np.sum(uzi[ii,:,:]*uzi[ii,:,:])
		wuz[ii] = wuz[ii] + npInv*np.sum(uzi[ii,:,:]*uxi[ii,:,:] + uzi[ii,:,:]*uyi[ii,:,:])
		wtz[ii] = wtz[ii] + npInv*np.sum(uzi[ii,:,:]*thj[ii,:,:])
		qvz[ii] = qvz[ii] + npInv*np.sum(qvi[ii,:,:])
		qrz[ii] = qrz[ii] + npInv*np.sum(qri[ii,:,:])
		qiz[ii] = qiz[ii] + npInv*np.sum(qii[ii,:,:])
		qcz[ii] = qcz[ii] + npInv*np.sum(qci[ii,:,:])
		qsz[ii] = qsz[ii] + npInv*np.sum(qsi[ii,:,:])
		qgz[ii] = qgz[ii] + npInv*np.sum(qgi[ii,:,:])
		thz[ii] = thz[ii] + npInv*np.sum(thi[ii,:,:])

wwz = wwz/len(times)
wuz = wuz/len(times)
wtz = wtz/len(times)
qvz = qvz/len(times)
qrz = qrz/len(times)
qiz = qiz/len(times)
qcz = qcz/len(times)
qsz = qsz/len(times)
qgz = qgz/len(times)
thz = thz/len(times)
	
fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(wuz, htz2,'-')
plt.plot(wwz, htz2,'-')
plt.xlabel('<$wu$> (m/s)')
plt.ylabel('height (m)')
plt.legend(('<$wu$>','<$ww$>'),loc=3,prop={'size':10})
plt.title('Perturbed momentum flux vertical profile')
pylab.savefig('wuz_hector_wrf_2.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(wtz, htz2,'-')
plt.xlabel('<$w\Theta$> (mK/s)')
plt.ylabel('height (m)')
plt.title('Potential temperature flux vertical profile')
pylab.savefig('wtz_hector_wrf_2.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qvz, htz2,'-')
plt.xlabel('<$q_{vapor}$> (kg/kg)')
plt.ylabel('height (m)')
plt.title('Water vapor vertical profile')
pylab.savefig('qvz_hector_wrf_2.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(thz, htz2,'-')
plt.xlabel('$<\Theta>$ (K)')
plt.ylabel('height (m)')
plt.title('Potential temperature vertical profile')
pylab.savefig('thz_hector_wrf_2.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qrz, htz2,'-')
plt.plot(qiz, htz2,'-')
plt.plot(qcz, htz2,'-')
plt.plot(qsz, htz2,'-')
plt.plot(qgz, htz2,'-')
plt.xlabel('<q> (kg/kg)')
plt.legend(('rain','ice','cloud','snow','graupel'),loc=1,prop={'size':10})
plt.ylabel('height (m)')
plt.title('Water species vertical profile')
pylab.savefig('qiz_hector_wrf_2.png')

plt.show()

np.savetxt('hgt_hector_2.wrf',htz2)
np.savetxt('wwz_hector_2.wrf',wwz)
np.savetxt('wuz_hector_2.wrf',wuz)
np.savetxt('wtz_hector_2.wrf',wtz)
np.savetxt('thz_hector_2.wrf',thz)
np.savetxt('qvz_hector_2.wrf',qvz)
np.savetxt('qrz_hector_2.wrf',qrz)
np.savetxt('qiz_hector_2.wrf',qiz)
np.savetxt('qcz_hector_2.wrf',qcz)
np.savetxt('qsz_hector_2.wrf',qsz)
np.savetxt('qgz_hector_2.wrf',qgz)
