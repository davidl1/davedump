#!/usr/bin/env python

# Generate the diurnal cycle for the full UM domain from the WRF output

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
import numpy as np
import pylab

def ReadField(filename,fieldname):
        print 'from: ',filename,'\treading: ',fieldname
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
        return field[:]

def ReadScalar(filename,varname):
        print 'from: ',filename,'\treading: ',varname
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[varname]
        infile.close()
        return field.getValue()

def InterpGrid(xin,yin,xout,yout,A,interpType):
	print 'interp type: ', interpType
	pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
	pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
	Ar      = np.ravel(A)
	Aor     = griddata(pts_in,Ar,pts_out,method=interpType)
	Ao      = Aor.reshape(len(yout),len(xout))
	return Ao

directory = '/mnt/coe0/davidl1/WRF/d02/'
filename  = 'prcp_d02_2010-02-'

lat  = ReadField(directory+'XLAT_d02_NewGuinea.nc','XLAT')
lon  = ReadField(directory+'XLONG_d02_NewGuinea.nc','XLONG')
lsm  = ReadField(directory+'LANDMASK_d02_NewGuinea.nc','LANDMASK')

lat  = lat[0,:,0]
lon  = lon[0,0,:]
lsm  = lsm[0,:,:]
slm  = (lsm+1)%2
nx   = len(lon)
ny   = len(lat)

# get the cpol domain mask
directory_cpol = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km_2/'
filename_cpol  = 'cpol_2d_rainrate_2010'

Re   = 6378100.0
lon_cpol = ReadScalar(directory_cpol+filename_cpol+'0201_0000.nc','radar_longitude')
lat_cpol = ReadScalar(directory_cpol+filename_cpol+'0201_0000.nc','radar_latitude')
x    = ReadField(directory_cpol+filename_cpol+'0201_0000.nc','x')
y    = ReadField(directory_cpol+filename_cpol+'0201_0000.nc','y')
x    = 1000.0*x
y    = 1000.0*y
dx   = (x[-1]-x[0])/len(x)
dy   = (y[-1]-y[0])/len(y)
lon_cpol  = lon_cpol + x*360.0/2.0/np.pi/Re
lat_cpol  = lat_cpol + y*360.0/2.0/np.pi/Re
pcp = ReadField(directory_cpol+filename_cpol+'0202_0000.nc','rr')
empty_cpol = pcp[0,:,:]<-10.0
full_cpol  = (empty_cpol+1)%2

# interpolate the mask to the trmm and cpol points
pts=np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
pts_cpol=np.transpose([np.tile(lon_cpol,len(lat_cpol)),np.repeat(lat_cpol,len(lon_cpol))])
fullr=np.ravel(full_cpol)
full=griddata(pts_cpol,fullr,pts,method='nearest')
mask=full.reshape(len(lat),len(lon))

nl    = np.sum(mask*lsm)
ns    = np.sum(mask*slm)
lrain = np.zeros(24)
srain = np.zeros(24)

days = [2,4,5,7,9,11]
for day in days:
	for hr in np.arange(24):
		if hr == 0:
			day_p = day - 1
			hr_p  = 23
		else:
			day_p = day
			hr_p  = hr - 1

		rain_c1  = ReadField(directory+filename+'%.2u'%day+'_'+'%.2u'%hr+':00:00','RAINC')
		rain_nc1 = ReadField(directory+filename+'%.2u'%day+'_'+'%.2u'%hr+':00:00','RAINNC')
		rain_c0  = ReadField(directory+filename+'%.2u'%day_p+'_'+'%.2u'%hr_p+':00:00','RAINC')
		rain_nc0 = ReadField(directory+filename+'%.2u'%day_p+'_'+'%.2u'%hr_p+':00:00','RAINNC')

		rain     = rain_c1[0,:,:] + rain_nc1[0,:,:] - rain_c0[0,:,:] - rain_nc0[0,:,:]
		rain     = mask*rain

		lrain[hr] = lrain[hr] + np.sum(rain*lsm)
		srain[hr] = srain[hr] + np.sum(rain*slm)

avg_lrain = lrain/len(days)/nl
avg_srain = srain/len(days)/ns

fig=plt.figure()
ax=fig.add_subplot(111)
time=np.arange(24)
avg_lrain_2=np.zeros(24)
avg_srain_2=np.zeros(24)
GMT=9
for ii in np.arange(24):
	ind=(ii-GMT)%24
	avg_lrain_2[ii]=avg_lrain[ind]
	avg_srain_2[ii]=avg_srain[ind]

plt.plot(time,avg_lrain_2,'-o',time,avg_srain_2,'-o')
plt.legend(('land','sea'),loc=0)
plt.xlabel('Local time (hrs)')
plt.ylabel('Precipitation rate (mm)')
plt.xlim([0,23])
plt.ylim([0,1.0])
plt.title('Precipitation vs. local time for land and sea (WRF, $\Delta x=4.0km$)')
pylab.savefig('avg_diurnal_cycle_wrf4k_darwin.png')
plt.show()

time3 =np.zeros(25)
lrain3=np.zeros(25)
srain3=np.zeros(25)
time3[:24]=time
time3[24] =24
lrain3[:24]=avg_lrain_2
lrain3[24] =avg_lrain_2[0]
srain3[:24]=avg_srain_2
srain3[24] =avg_srain_2[0]

np.savetxt('diurnal_darwin_wrf4k_time.txt',time3)
np.savetxt('diurnal_darwin_wrf4k_land.txt',lrain3)
np.savetxt('diurnal_darwin_wrf4k_sea.txt', srain3)

