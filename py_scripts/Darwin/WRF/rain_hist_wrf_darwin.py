#!/usr/bin/env python

# redundant version

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
import numpy as np
import pylab

def ReadFile(filename,fieldname):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

def InterpGrid(xin,yin,xout,yout,A,interpType):
	print 'interp type: ', interpType
	pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
	pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
	Ar      = np.ravel(A)
	Aor     = griddata(pts_in,Ar,pts_out,method=interpType)
	Ao      = Aor.reshape(len(yout),len(xout))
	return Ao

directory = '/scratch/hassimme/WRF/New_Guinea/d03/PRCP/'
filename  = 'prcp_d03_2010-02-'

lat  = ReadFile(directory+'XLAT_d03_NewGuinea.nc','XLAT')
lon  = ReadFile(directory+'XLONG_d03_NewGuinea.nc','XLONG')
lsm  = ReadFile(directory+'LANDMASK_d03_NewGuinea.nc','LANDMASK')

lat  = lat[0,:,0]
lon  = lon[0,0,:]
lsm  = lsm[0,:,:]
slm  = (lsm+1)%2
nx   = len(lon)
ny   = len(lat)

lon_cpol  = np.loadtxt('lon_cpol.txt')
lat_cpol  = np.loadtxt('lat_cpol.txt')
mask_cpol = np.loadtxt('mask_cpol.txt')
mask      = InterpGrid(lon_cpol,lat_cpol,lon,lat,mask_cpol,'nearest')

nl    = np.sum(mask*lsm)
ns    = np.sum(mask*slm)

prange = 1
nbins  = 250

shist = np.zeros(nbins)
lhist = np.zeros(nbins)
thist = np.zeros(nbins)

days = [2,4,5,7,9]
mask = mask.flatten()
lsm  = lsm.flatten()
for day in days:
        for hr in np.arange(24):
                if hr == 0:
                        day_p = day - 1
                        hr_p  = 23
                else:
                        day_p = day
                        hr_p  = hr - 1

                rain_c1  = ReadFile(directory+filename+'%.2u'%day+'_'+'%.2u'%hr+':00:00','RAINC')
                rain_nc1 = ReadFile(directory+filename+'%.2u'%day+'_'+'%.2u'%hr+':00:00','RAINNC')
                rain_c0  = ReadFile(directory+filename+'%.2u'%day_p+'_'+'%.2u'%hr_p+':00:00','RAINC')
                rain_nc0 = ReadFile(directory+filename+'%.2u'%day_p+'_'+'%.2u'%hr_p+':00:00','RAINNC')

                rain     = rain_c1[0,:,:] + rain_nc1[0,:,:] - rain_c0[0,:,:] - rain_nc0[0,:,:]
		rain     = rain.flatten()
		rain     = rain/prange
		for ii in np.arange(len(rain)):
			if mask[ii] > 0.5:
				rbin = np.int(rain[ii])
				thist[rbin] = thist[rbin] + 1
				if lsm[ii]:
					lhist[rbin] = lhist[rbin] + 1
				else:
					shist[rbin] = shist[rbin] + 1


lhist = 100*lhist/5/24/nl
shist = 100*shist/5/24/ns
thist = 100*thist/5/24/np.sum(mask)

print np.sum(lhist)
print np.sum(shist)
print np.sum(thist)

fig=plt.figure()		
ax=fig.add_subplot(111)
rfall = prange*np.arange(nbins)
plt.semilogy(rfall,lhist,'-o',rfall,shist,'-o',rfall,thist,'-o')
plt.legend(('land','sea','total'),loc=0)
plt.xlabel('Precipitation (mm)')
plt.ylabel('Percent of domain area')
plt.title('Rainfall Histogram (WRF)')
pylab.savefig('rain_histogram_wrf_darwin.png')
plt.show()
