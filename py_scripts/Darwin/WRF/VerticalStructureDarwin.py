#!/usr/bin/env python

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
from matplotlib import colors as cls
import numpy as np
import pylab

def ReadFile(filename,fieldname):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

def InterpGrid(xin,yin,xout,yout,A,interpType):
	print 'interp type: ', interpType
	pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
	pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
	Ar      = np.ravel(A)
	Aor     = griddata(pts_in,Ar,pts_out,method=interpType)
	Ao      = Aor.reshape(len(yout),len(xout))
	return Ao

def GenMask(lon,lat,x,y,r,prcp,pmin):
        mask = np.zeros((len(lat),len(lon)))
        pnts = np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
        pnts[:,0] = pnts[:,0] - x
        pnts[:,1] = pnts[:,1] - y
        radi = np.sqrt(pnts[:,0]*pnts[:,0]+pnts[:,1]*pnts[:,1])
        radi = radi.reshape(len(lat),len(lon))
        mask = np.zeros(radi.shape)
        for ii in np.arange(radi.shape[0]):
                for jj in np.arange(radi.shape[1]):
                        if radi[ii,jj] < r and prcp[ii,jj] > pmin:
                                mask[ii,jj] = 1

	return mask

def GenMaskBox(lon,lat,xi,xf,yi,yf,precip,pmin):
        mask = np.zeros((len(lat),len(lon)))
        for ii in np.arange(mask.shape[0]):
                for jj in np.arange(mask.shape[1]):
			if lat[ii] > yi and lat[ii] < yf and lon[jj] > xi and lon[jj] < xf:
				if precip[ii,jj] > pmin:
					mask[ii,jj] = 1

	return mask

def InterpZ(phi,hgt,htz,mask):
	phiz = np.zeros((len(htz),phi.shape[1],phi.shape[2]))
	for ii in np.arange(phi.shape[1]):
		for jj in np.arange(phi.shape[2]):
			if mask[ii,jj]:
				phiz[:,ii,jj]=griddata(hgt[:,ii,jj],phi[:,ii,jj],htz,method='cubic')

	return phiz

xi=233
xf=467
yi=473
yf=707

lat  = ReadFile('data/XLAT_d03_NewGuinea.nc','XLAT')
lon  = ReadFile('data/XLONG_d03_NewGuinea.nc','XLONG')
lsm  = ReadFile('data/LANDMASK_d03_NewGuinea.nc','LANDMASK')

lat  = lat[0,yi:yf,0]
lon  = lon[0,0,xi:xf]
lsm  = lsm[0,yi:yf,xi:xf]
slm  = (lsm+1)%2
nx   = len(lon)
ny   = len(lat)

rain = np.zeros(lsm.shape)
for day in np.arange(7)+2:
	rain_c1  = ReadFile('data/prcp_d03_2010-02-'+'%.2d'%day+'_08','RAINC')
	rain_nc1 = ReadFile('data/prcp_d03_2010-02-'+'%.2d'%day+'_08','RAINNC')
	rain_c0  = ReadFile('data/prcp_d03_2010-02-'+'%.2d'%day+'_04','RAINC')
	rain_nc0 = ReadFile('data/prcp_d03_2010-02-'+'%.2d'%day+'_04','RAINNC')
	rain     = rain + rain_c1[0,yi:yf,xi:xf] + rain_nc1[0,yi:yf,xi:xf] - rain_c0[0,yi:yf,xi:xf] - rain_nc0[0,yi:yf,xi:xf]

#mask      = GenMask(lon,lat,131.14,-13.26,0.5,rain,0.01)
mask     = GenMaskBox(lon,lat,130.0,131.6,-12.0,-11.1,rain,10.0)
#mask = np.ones(lsm.shape)

#npInv = 1.0/lsm.shape[0]/lsm.shape[1]
npInv    = 1.0/np.sum(mask)
print 'num pts: ',np.sum(mask)

fig = plt.figure()
ax = fig.add_subplot(111)
#ax.contour(lon[0:300],lat[100:400],lsm[100:400,0:300],colors='k')
ax.contour(lon,lat,lsm,colors='k')
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
#levs = [0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
#levs = [0.,2.,5.,10.,20.,40.,80.,120.,160.,200.]
cmap = cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
#norm = cls.BoundaryNorm(levs,cmap.N)
#co   = ax.contourf(lon[xi:xf],lat[yi:yf],rain[yi:yf,xi:xf],levels=levs,cmap=cmap,norm=norm)
#co   = ax.contourf(lon[0:300],lat[100:400],mask[100:400,0:300]*rain[100:400,0:300],cmap=cmap)
co   = ax.contourf(lon,lat,mask*rain,cmap=cmap)
plt.colorbar(co,orientation='vertical')
plt.title('Accumulated rainfall for 28 hours (mm) - (WRF)')
pylab.savefig('wrf_darwin.png')
plt.show()

day   = 2
time  = 5
phi   = ReadFile('data/PH_02'+'%.2d'%day+'%.2d'%time+'.nc','PH')
phi0  = ReadFile('data/PHB_02'+'%.2d'%day+'%.2d'%time+'.nc','PHB')
qv    = ReadFile('data/QVAPOR_02'+'%.2d'%day+'%.2d'%time+'.nc','QVAPOR')

# generate the height fields on rho and theta levels
gInv = 1.0/9.8
print 'generating height field... (1)',phi.shape
hgt1 = np.zeros(phi.shape)
hgt1[:,:,:] = gInv*(phi[:,:,:] + phi0[:,:,:])

print 'generating height field... (2)',qv.shape
hgt2 = np.zeros(qv.shape)
for ii in np.arange(qv.shape[0]):
	hgt2[ii,:,:] = 0.5*(hgt1[ii,:,:] + hgt1[ii+1,:,:])

imax=0
jmax=0
hmax=-1.0e+99
for ii in np.arange(hgt1.shape[1]):
	for jj in np.arange(hgt1.shape[2]):
		if mask[ii,jj]:
			if hgt1[0,ii,jj] > hmax:
				hmax = hgt1[0,ii,jj]
				imax = ii
				jmax = jj

htz1 = hgt1[:,imax,jmax]
htz2 = hgt2[:,imax,jmax]

print phi.shape
print htz2.shape

wwz = np.zeros(qv.shape[0])
wuz = np.zeros(qv.shape[0])
wtz = np.zeros(qv.shape[0])
qvz = np.zeros(qv.shape[0])
qrz = np.zeros(qv.shape[0])
qiz = np.zeros(qv.shape[0])
qcz = np.zeros(qv.shape[0])
qsz = np.zeros(qv.shape[0])
qgz = np.zeros(qv.shape[0])
thz = np.zeros(qv.shape[0])

times = [5,6,7,8]
days  = [2,3,4,5,6,7,8]
for day in days:
	for time in times:
		th = ReadFile('data/T_02'+'%.2d'%day+'%.2d'%time+'.nc','T') + 300.0
		ux = ReadFile('data/U_02'+'%.2d'%day+'%.2d'%time+'.nc','U')
		uy = ReadFile('data/V_02'+'%.2d'%day+'%.2d'%time+'.nc','V')
		uz = ReadFile('data/W_02'+'%.2d'%day+'%.2d'%time+'.nc','W')
		qv = ReadFile('data/QVAPOR_02'+'%.2d'%day+'%.2d'%time+'.nc','QVAPOR')
		qr = ReadFile('data/QRAIN_02'+'%.2d'%day+'%.2d'%time+'.nc','QRAIN')
		qi = ReadFile('data/QICE_02'+'%.2d'%day+'%.2d'%time+'.nc','QICE')
		qc = ReadFile('data/QCLOUD_02'+'%.2d'%day+'%.2d'%time+'.nc','QCLOUD')
		qs = ReadFile('data/QSNOW_02'+'%.2d'%day+'%.2d'%time+'.nc','QSNOW')
		qg = ReadFile('data/QGRAUP_02'+'%.2d'%day+'%.2d'%time+'.nc','QGRAUP')

		phi  = ReadFile('data/PH_02'+'%.2d'%day+'%.2d'%time+'.nc','PH')
		phi0 = ReadFile('data/PHB_02'+'%.2d'%day+'%.2d'%time+'.nc','PHB')

		# generate the height fields on rho and theta levels
		gInv = 1.0/9.8
		print 'generating height field... (1)'
		hgt1 = np.zeros(phi.shape)
		hgt1[:,:,:] = gInv*(phi[:,:,:] + phi0[:,:,:])

		print 'generating height field... (2)'
		hgt2 = np.zeros(qv.shape)
		for ii in np.arange(qv.shape[0]):
			hgt2[ii,:,:] = 0.5*(hgt1[ii,:,:] + hgt1[ii+1,:,:])

		# interpolate to consistent height levels
		uzi = InterpZ(uz,hgt1,htz2,mask)
		uxi = InterpZ(ux,hgt2,htz2,mask)
		uyi = InterpZ(uy,hgt2,htz2,mask)
		qvi = InterpZ(qv,hgt2,htz2,mask)
		qri = InterpZ(qr,hgt2,htz2,mask)
		qii = InterpZ(qi,hgt2,htz2,mask)
		qci = InterpZ(qc,hgt2,htz2,mask)
		qsi = InterpZ(qs,hgt2,htz2,mask)
		qgi = InterpZ(qg,hgt2,htz2,mask)
		thi = InterpZ(th,hgt2,htz2,mask)

		# remove mean flow components
		thj = np.zeros(np.shape(thi))
		for ii in np.arange(len(htz2)):
			uxi[ii,:,:] = uxi[ii,:,:] - npInv*np.sum(uxi[ii,:,:])
			uyi[ii,:,:] = uyi[ii,:,:] - npInv*np.sum(uyi[ii,:,:])
			thj[ii,:,:] = thi[ii,:,:] - npInv*np.sum(thi[ii,:,:])

		for ii in np.arange(len(htz2)):
			wwz[ii] = wwz[ii] + npInv*np.sum(uzi[ii,:,:]*uzi[ii,:,:])
			wuz[ii] = wuz[ii] + npInv*np.sum(uzi[ii,:,:]*uxi[ii,:,:] + uzi[ii,:,:]*uyi[ii,:,:])
			wtz[ii] = wtz[ii] + npInv*np.sum(uzi[ii,:,:]*thj[ii,:,:])
			qvz[ii] = qvz[ii] + npInv*np.sum(qvi[ii,:,:])
			qrz[ii] = qrz[ii] + npInv*np.sum(qri[ii,:,:])
			qiz[ii] = qiz[ii] + npInv*np.sum(qii[ii,:,:])
			qcz[ii] = qcz[ii] + npInv*np.sum(qci[ii,:,:])
			qsz[ii] = qsz[ii] + npInv*np.sum(qsi[ii,:,:])
			qgz[ii] = qgz[ii] + npInv*np.sum(qgi[ii,:,:])
			thz[ii] = thz[ii] + npInv*np.sum(thi[ii,:,:])

wwz = wwz/len(times)/len(days)
wuz = wuz/len(times)/len(days)
wtz = wtz/len(times)/len(days)
qvz = qvz/len(times)/len(days)
qrz = qrz/len(times)/len(days)
qiz = qiz/len(times)/len(days)
qcz = qcz/len(times)/len(days)
qsz = qsz/len(times)/len(days)
qgz = qgz/len(times)/len(days)
thz = thz/len(times)/len(days)
	
np.savetxt('hgt_darwin.wrf',htz2)
np.savetxt('wwz_darwin.wrf',wwz)
np.savetxt('wuz_darwin.wrf',wuz)
np.savetxt('wtz_darwin.wrf',wtz)
np.savetxt('thz_darwin.wrf',thz)
np.savetxt('qvz_darwin.wrf',qvz)
np.savetxt('qrz_darwin.wrf',qrz)
np.savetxt('qiz_darwin.wrf',qiz)
np.savetxt('qcz_darwin.wrf',qcz)
np.savetxt('qsz_darwin.wrf',qsz)
np.savetxt('qgz_darwin.wrf',qgz)

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(wuz, htz2,'-')
plt.plot(wwz, htz2,'-')
plt.xlabel('<$wu$> (m/s)')
plt.ylabel('height (m)')
plt.legend(('<$wu$>','<$ww$>'),loc=3,prop={'size':10})
plt.title('Perturbed momentum flux vertical profile')
pylab.savefig('wuz_darwin_wrf.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(wtz, htz2,'-')
plt.xlabel('<$w\Theta$> (mK/s)')
plt.ylabel('height (m)')
plt.title('Potential temperature flux vertical profile')
pylab.savefig('wtz_darwin_wrf.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qvz, htz2,'-')
plt.xlabel('<$q_{vapor}$> (kg/kg)')
plt.ylabel('height (m)')
plt.title('Water vapor vertical profile')
pylab.savefig('qvz_darwin_wrf.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(thz, htz2,'-')
plt.xlabel('$<\Theta>$ (K)')
plt.ylabel('height (m)')
plt.title('Potential temperature vertical profile')
pylab.savefig('thz_darwin_wrf.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qrz, htz2,'-')
plt.plot(qiz, htz2,'-')
plt.plot(qcz, htz2,'-')
plt.plot(qsz, htz2,'-')
plt.plot(qgz, htz2,'-')
plt.xlabel('<q> (kg/kg)')
plt.legend(('rain','ice','cloud','snow','graupel'),loc=1,prop={'size':10})
plt.ylabel('height (m)')
plt.title('Water species vertical profile')
pylab.savefig('qiz_darwin_wrf.png')

plt.show()
