This directory contains post processing scripts to be applied to netCDF WRF output files. 
These scripts perform the following:

*) Generate a plot of the accumulated rainfall, and calculate the total rainfall over land, sea
   and the full domain for specified days
	plot_rain_cpol_wrf.py

*) Generate the daily averaged diurnal cycle over the CPOL radar domain for land and sea
	diurnal_cycle_wrf_darwin.py

*) Generate the daily averaged diurnal cycle over the UM LAM domain for land and sea
	diurnal_cycle_wrf_full.py

*) Calculate the fractional skill score (FSS) for rainfall data against reference 
   at varying scales and precipitation intensities over the CPOL radar domain
	FSS_Darwin_WRF.py

*) Calculate the meridionally and verically averaged kinetic energy spectrum of the zonal wave
   numbers
	KE_Spectrum_Fourier_WRF.py

*) Calculate the temporally and horizontally averaged vertical profiles within clouds (as defined by
   a critical hydrometeor concentration) over the Tiwi islands between 15.00 and 18.00 local time
	VerticalStructureCloud.py

*) Calculate a histogram of rainfall intensity for land, sea and full area for the CPOL radar domain
	RainfallHistogram_WRF.py
