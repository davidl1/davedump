#!/usr/bin/env python

# Generate a histogram of rainfall intensite for the CPOL domain from the WRF output

import numpy as np
from scipy.io import netcdf
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import pylab

def ReadFile(filename,fieldname,isscalar):
        print 'from: ' + filename + '\tloading: ' + fieldname
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
        if isscalar:
                return field.getValue()
        else:
                return field[:]


def InterpGrid(xin,yin,xout,yout,A,interpType):
        print 'interp type: ',interpType
        pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
        pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
        Ar      = np.ravel(A)
        Aor     = griddata(pts_in,Ar,pts_out,method=interpType)
        Ao      = Aor.reshape(len(yout),len(xout))
        return Ao

directory = '/mnt/coe0/davidl1/WRF/d03/'
filename  = 'prcp_d03_2010-02-'

lat  = ReadFile(directory+'XLAT_d03_NewGuinea.nc','XLAT',False)
lon  = ReadFile(directory+'XLONG_d03_NewGuinea.nc','XLONG',False)
lsm  = ReadFile(directory+'LANDMASK_d03_NewGuinea.nc','LANDMASK',False)

lat = lat[0,:,0]
lon = lon[0,0,:]
lsm = lsm[0,:,:]
nx  = len(lon)
ny  = len(lat)

# get the radar data to determine coverage...
dir_cpol  = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km/'
file_cpol = 'cpol_2d_rainrate_2010'
pcp_cpol  = ReadFile(dir_cpol+file_cpol+'0202_0000.nc','rr',False)
empty     = pcp_cpol[0,:,:]<-10
full      = (empty+1)%2
lon0_cpol = ReadFile(dir_cpol+file_cpol+'0201_0000.nc','radar_longitude',True)
lat0_cpol = ReadFile(dir_cpol+file_cpol+'0201_0000.nc','radar_latitude',True)
x         = ReadFile(dir_cpol+file_cpol+'0201_0000.nc','x',False)
y         = ReadFile(dir_cpol+file_cpol+'0201_0000.nc','y',False)
x         = 1000.0*x
y         = 1000.0*y
Re        = 6378100.0
lon_cpol  = lon0_cpol + x*360.0/2.0/np.pi/Re
lat_cpol  = lat0_cpol + y*360.0/2.0/np.pi/Re
# interpolate
pts0      = np.transpose([np.tile(lon_cpol,len(lat_cpol)),np.repeat(lat_cpol,len(lon_cpol))])
pts1      = np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
full      = np.ravel(full)
full      = griddata(pts0,full,pts1,method='nearest')
full      = full.reshape(ny,nx)

slm = (lsm+1)%2
nl  = np.sum(full*lsm)
ns  = np.sum(full*slm)

prange = 2
nbins  = 160

shist = np.zeros(nbins)
lhist = np.zeros(nbins)
thist = np.zeros(nbins)

#for tt in np.arange(len(times)):
rain = np.zeros((ny,nx))
days = [4,5,6,7,9,11]
full = full.flatten()
lsm  = lsm.flatten()
for day in days:
        for hr in np.arange(24):
                if hr == 0:
                        day_p = day - 1
                        hr_p  = 23
                else:
                        day_p = day
                        hr_p  = hr - 1

                rain_c1  = ReadFile(directory+filename+'%.2u'%day+'_'+'%.2u'%hr+':00:00','RAINC',False)
                rain_nc1 = ReadFile(directory+filename+'%.2u'%day+'_'+'%.2u'%hr+':00:00','RAINNC',False)
                rain_c0  = ReadFile(directory+filename+'%.2u'%day_p+'_'+'%.2u'%hr_p+':00:00','RAINC',False)
                rain_nc0 = ReadFile(directory+filename+'%.2u'%day_p+'_'+'%.2u'%hr_p+':00:00','RAINNC',False)

                rain     = rain_c1[0,:,:] + rain_nc1[0,:,:] - rain_c0[0,:,:] - rain_nc0[0,:,:]

                rain2 = rain.flatten()
                rain2 = rain2/prange
                for ii in np.arange(nx*ny):
                        if full[ii]:
                                rbin = np.int(rain2[ii])
                                thist[rbin] = thist[rbin] + 1
                                if lsm[ii]:
                                        lhist[rbin] = lhist[rbin] + 1
                                else:
                                        shist[rbin] = shist[rbin] + 1


lhist = 100*lhist/6/24/nl
shist = 100*shist/6/24/ns
thist = 100*thist/6/24/np.sum(full)
rfall = prange*np.arange(nbins)

print np.sum(lhist)
print np.sum(shist)
print np.sum(thist)

#np.savetxt('rain_hist_l_wrf_dscale.txt',lhist)
#np.savetxt('rain_hist_s_wrf_dscale.txt',shist)
#np.savetxt('rain_hist_t_wrf_dscale.txt',thist)

print nbins
print rfall.shape
print thist.shape
print lhist.shape
print shist.shape

fig=plt.figure()		
ax=fig.add_subplot(111)
plt.semilogy(rfall,thist,'-o',rfall,lhist,'-o',rfall,shist,'-o')
plt.legend(('total','land','sea'),loc=0)
plt.xlabel('precipitation (mm)')
plt.ylabel('% of domain area')
plt.title('rainfall histogram (WRF)')
pylab.savefig('rain_histogram_wrf.png')
plt.show()
