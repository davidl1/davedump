#!/usr/bin/env python

# calculate the fractional skill score for rainfall data against reference 
# at varying scales
# reference:
#	Roberts and Lean (2008) MWR: 136, 78-97

import numpy as np
from scipy.io import *
from scipy.interpolate import *
from matplotlib import pyplot as plt
import matplotlib.colors as cls
import pylab

def LoadField(filename,fieldname,isscalar):
        print 'from: ' + filename + '\tloading: ' + fieldname
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
	if isscalar:
		return field.getValue()
	else:
        	return field[:]

def InterpGrid( xin, yin, xout, yout, A ):
	pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
	pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
	Ar      = np.ravel(A)
	Aor     = griddata(pts_in,Ar,pts_out,method='cubic')
	Ao      = Aor.reshape(len(yout),len(xout))
	return Ao

def GenFSS( lon, lat, pcp_m, pcp_o, pmax, full ):
	mod = pcp_m>pmax
	obs = pcp_o>pmax

	n = np.min([len(lon),len(lat)])
	n = n/2
	n = np.arange(n)
	n = 2*n+1

	FSS = np.zeros(len(n))

	k = 0
	for ni in n:
		x = np.arange(len(lon)-ni)+ni/2
		y = np.arange(len(lat)-ni)+ni/2
		Mn = np.zeros((len(y),len(x)))
		On = np.zeros((len(y),len(x)))
		i  = 0

		nx = 0
		ny = 0
		for yi in y:
			j = 0
			for xi in x:
				xr      = xi + np.arange(ni)-ni/2
				yr      = yi + np.arange(ni)-ni/2

                                if np.sum(full[yr[0]:yr[-1]+1,xr[0]:xr[-1]+1]):
					Mn[i,j] = np.sum(mod[yr[0]:yr[-1]+1,xr[0]:xr[-1]+1])
					On[i,j] = np.sum(obs[yr[0]:yr[-1]+1,xr[0]:xr[-1]+1])

				nx = nx + 1
				ny = ny + 1

				j = j + 1

			i = i + 1;

		#MSE_ref = (np.sum(On*On) + np.sum(Mn*Mn))/len(x)/len(y)
		#MSE     = np.sum((On-Mn)*(On-Mn))/len(x)/len(y)
		MSE_ref = (np.sum(On*On) + np.sum(Mn*Mn))/nx/ny
		MSE     = np.sum((On-Mn)*(On-Mn))/nx/ny
		FSS[k]  = 1.0 - MSE/MSE_ref

		print 'n: ',ni,'\tMSE: ',MSE,'\tref: ',MSE_ref,'\tFSS: ',FSS[k]
		k = k + 1

	return FSS

def GenF_o( pcp_o, pmax, full ):
        mask = pcp_o > pmax
        obs  = 1.0*np.sum(full*mask)
        tot  = 1.0*np.sum(full)
        return obs/tot

#lon_w = np.loadtxt('lon_wrf_darwin.txt')
#lat_w = np.loadtxt('lat_wrf_darwin.txt')
#pcp_w = np.loadtxt('pcp_wrf_darwin_full.txt')
directory = '/mnt/coe0/davidl1/WRF/d03/'
filename  = 'prcp_d03_2010-02-'

lat_w  = LoadField(directory+'XLAT_d03_NewGuinea.nc','XLAT',False)
lon_w  = LoadField(directory+'XLONG_d03_NewGuinea.nc','XLONG',False)
lsm_w  = LoadField(directory+'LANDMASK_d03_NewGuinea.nc','LANDMASK',False)

lat_w  = lat_w[0,:,0]
lon_w  = lon_w[0,0,:]
lsm_w  = lsm_w[0,:,:]
pcp_w  = np.zeros(lsm_w.shape)

days = [4,5,6,7,9,11]
for day in days:
        for hr in np.arange(24):
                if hr == 0:
                        day_p = day - 1
                        hr_p  = 23
                else:
                        day_p = day
                        hr_p  = hr - 1

                rain_c1  = LoadField(directory+filename+'%.2u'%day+'_'+'%.2u'%hr+':00:00','RAINC',False)
                rain_nc1 = LoadField(directory+filename+'%.2u'%day+'_'+'%.2u'%hr+':00:00','RAINNC',False)
                rain_c0  = LoadField(directory+filename+'%.2u'%day_p+'_'+'%.2u'%hr_p+':00:00','RAINC',False)
                rain_nc0 = LoadField(directory+filename+'%.2u'%day_p+'_'+'%.2u'%hr_p+':00:00','RAINNC',False)

                rain_i   = rain_c1[0,:,:] + rain_nc1[0,:,:] - rain_c0[0,:,:] - rain_nc0[0,:,:]
                pcp_w    = pcp_w + rain_i

# get the radar data to determine coverage...
#dir_o  = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km/'
dir_o  = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km_2/'
file_o = 'cpol_2d_rainrate_2010'
pcp_o  = LoadField(dir_o+file_o+'0202_1200.nc','rr',False)
pcp_o  = pcp_o[0,:,:]
empty  = pcp_o[:,:]<-10
full   = (empty+1)%2
pcp_o  = full*pcp_o
lon0_o = LoadField(dir_o+file_o+'0201_0000.nc','radar_longitude',True)
lat0_o = LoadField(dir_o+file_o+'0201_0000.nc','radar_latitude',True)
x      = LoadField(dir_o+file_o+'0201_0000.nc','x',False)
y      = LoadField(dir_o+file_o+'0201_0000.nc','y',False)
x      = 1000.0*x
y      = 1000.0*y
Re     = 6378100.0
lon_o  = lon0_o + x*360.0/2.0/np.pi/Re
lat_o  = lat0_o + y*360.0/2.0/np.pi/Re
nx     = len(lon_o)
ny     = len(lat_o)

lon       = lon_o
lat       = lat_o
pts0      = np.transpose([np.tile(lon_w,len(lat_w)),np.repeat(lat_w,len(lon_w))])
pts1      = np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
full_r    = np.ravel(full)
full_w    = griddata(pts1,full_r,pts0,method='nearest')
full_w    = full_w.reshape(len(lat_w),len(lon_w))

pcp_o = np.zeros((ny,nx))
for day in days:
	for hr in np.arange(24):
		pcp = LoadField(dir_o+file_o+'02' + '%.2u'%day + '_'+ '%.2u' % hr+'00.nc','rr',False)
		#pcp_o = pcp_o + full*pcp[0,:,:]/6.0
		pcp_o = pcp_o + full*pcp[0,:,:]

# interpolate model data to observation grid
pcp_w_r   = np.ravel(pcp_w)
pcp_w_o   = griddata(pts0,pcp_w_r,pts1,method='cubic')
pcp_w     = pcp_w_o.reshape(ny,nx)

# binary field for threshold
n = np.min([len(lon),len(lat)])/2
pmax = np.zeros(4);
pmax[0] =  12.0
pmax[1] =  25.0
pmax[2] =  50.0
pmax[3] = 100.0
FSS = np.zeros((n,4))
f_o = np.zeros(4)
for ii in np.arange(4):
	FSS[:,ii] = GenFSS(lon,lat,pcp_w,pcp_o,pmax[ii],full)
	f_o[ii]   = GenF_o(pcp_o,pmax[ii],full)

n   = np.arange(n)
n   = 2*n+1
dx  = Re*2.0*np.pi*(lon[-1]-lon[0])/360.0/len(lon)
dxn = dx*n/1000.0
print 'dx:',dx
plt.xlabel('Scale (km)')
plt.ylabel('FSS')
plt.title('Fractions Skill Score (WRF)')
plt.plot(dxn,FSS[:,0],'-',color='b')
plt.plot(dxn,FSS[:,1],'-',color='g')
plt.plot(dxn,FSS[:,2],'-',color='r')
plt.plot(dxn,FSS[:,3],'-',color='c')
plt.legend(('12mm','25mm','50mm','100mm'),loc=4)
plt.plot(dxn,f_o[0]*np.ones(len(dxn)),'--',color='b')
plt.plot(dxn,f_o[1]*np.ones(len(dxn)),'--',color='g')
plt.plot(dxn,f_o[2]*np.ones(len(dxn)),'--',color='r')
plt.plot(dxn,f_o[3]*np.ones(len(dxn)),'--',color='c')
plt.plot(dxn,0.5+0.5*f_o[0]*np.ones(len(dxn)),'-.',color='b')
plt.plot(dxn,0.5+0.5*f_o[1]*np.ones(len(dxn)),'-.',color='g')
plt.plot(dxn,0.5+0.5*f_o[2]*np.ones(len(dxn)),'-.',color='r')
plt.plot(dxn,0.5+0.5*f_o[3]*np.ones(len(dxn)),'-.',color='c')

plt.ylim([0.0,1.0])
plt.legend(('12mm','25mm','50mm','100mm'),loc=4)
pylab.savefig('FSS_WRF.png')
plt.show()
