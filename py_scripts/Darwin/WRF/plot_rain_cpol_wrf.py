#!/usr/bin/env python

# Plot the accumulated rainfall from the WRF output over the CPOL domain

import numpy as np
from scipy.io import netcdf
from scipy.interpolate import griddata
from matplotlib import pyplot as plt
from matplotlib import colors as cls
import pylab

def ReadFile(filename,fieldname):
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

def InterpGrid(xin,yin,xout,yout,A,interpType):
        print 'interp type: ', interpType
        pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
        pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
        Ar      = np.ravel(A)
        Aor     = griddata(pts_in,Ar,pts_out,method=interpType)
        Ao      = Aor.reshape(len(yout),len(xout))
        return Ao

directory = '/scratch/hassimme/WRF/New_Guinea/d03/PRCP/'
filename  = 'prcp_d03_2010-02-'

lat  = ReadFile(directory+'XLAT_d03_NewGuinea.nc','XLAT')
lon  = ReadFile(directory+'XLONG_d03_NewGuinea.nc','XLONG')
hgt  = ReadFile(directory+'TERRAIN_HGT_d03_NewGuinea.nc','HGT')
lsm  = ReadFile(directory+'LANDMASK_d03_NewGuinea.nc','LANDMASK')

lat  = lat[0,:,0]
lon  = lon[0,0,:]
hgt  = hgt[0,:,:]
lsm  = lsm[0,:,:]
ny   = hgt.shape[0]
nx   = hgt.shape[1]
slm  = (lsm+1)%2

totrain  = np.zeros((ny,nx))
days = [2,4,5,7,9]
for day in days:
	for hr in np.arange(24):
		if hr == 0:
			day_p = day - 1
			hr_p  = 23
		else:
			day_p = day
			hr_p  = hr - 1

		rain_c1  = ReadFile(directory+filename+'%.2u'%day+'_'+'%.2u'%hr+':00:00','RAINC')
		rain_nc1 = ReadFile(directory+filename+'%.2u'%day+'_'+'%.2u'%hr+':00:00','RAINNC')
		rain_c0  = ReadFile(directory+filename+'%.2u'%day_p+'_'+'%.2u'%hr_p+':00:00','RAINC')
		rain_nc0 = ReadFile(directory+filename+'%.2u'%day_p+'_'+'%.2u'%hr_p+':00:00','RAINNC')

		totrain  = totrain + rain_c1[0,:,:] + rain_nc1[0,:,:] - rain_c0[0,:,:] - rain_nc0[0,:,:]

lon_c  = np.loadtxt('lon_cpol.txt')
lat_c  = np.loadtxt('lat_cpol.txt')
mask_c = np.loadtxt('mask_cpol.txt')
mask_w = InterpGrid(lon_c,lat_c,lon,lat,mask_c,'nearest')
pcp    = mask_w*totrain

#xi=237
#xf=463
#yi=477
#yf=704
xi=233
xf=467
yi=473
yf=707

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon[xi:xf],lat[yi:yf],lsm[yi:yf,xi:xf],colors='k')
#ax.contour(lon,lat,lsm,colors='k')
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
#levs = [0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
levs = [0.,2.,5.,10.,20.,40.,80.,120.,160.,200.]
cmap = cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm = cls.BoundaryNorm(levs,cmap.N)
co   = ax.contourf(lon[xi:xf],lat[yi:yf],pcp[yi:yf,xi:xf],levels=levs,cmap=cmap,norm=norm)
#co   = ax.contourf(lon,lat,pcp,levels=levs,cmap=cmap,norm=norm)
plt.colorbar(co,orientation='vertical')
plt.title('Accumulated rainfall for 120 hours (mm) - WRF')
pylab.savefig('accum_rain_120_wrf_rescale.png')
plt.show()

np.savetxt('lon_wrf_darwin.txt',lon[xi:xf])
np.savetxt('lat_wrf_darwin.txt',lat[yi:yf])
np.savetxt('pcp_wrf_darwin_full.txt',pcp[yi:yf,xi:xf])
np.savetxt('pcp_wrf_darwin_land.txt',lsm[yi:yf,xi:xf]*pcp[yi:yf,xi:xf])
np.savetxt('pcp_wrf_darwin_sea.txt' ,slm[yi:yf,xi:xf]*pcp[yi:yf,xi:xf])
