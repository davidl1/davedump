#!/usr/bin/env python

# Calculate the temporally and horizontally averaged vertical profiles within clouds (as defined by
# a critical hydrometeor concentration) over the Tiwi islands between 15.00 and 18.00 local time
# inclusive (UTZ + 9) for the WRF output for:
#       potential temperature                           (theta)
#       vertical momentum flux                          (w'w')
#       horiztonal momentum flux                        (w'u' + w'v')
#       potential temperature flux                      (w'theta')
#       vapor                                           (q)
#       liquid                                          (ql)
#       ice                                             (qi)
#       rain                                            (qr)
#       graupel                                         (qg)
#       cloud                                           (qc)
#       snow                                            (qs)

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
from matplotlib import colors as cls
import numpy as np
import pylab

def ReadFile(filename,fieldname):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

def InterpGrid(xin,yin,xout,yout,A,interpType):
	print 'interp type: ', interpType
	pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
	pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
	Ar      = np.ravel(A)
	Aor     = griddata(pts_in,Ar,pts_out,method=interpType)
	Ao      = Aor.reshape(len(yout),len(xout))
	return Ao

def GenMaskBox(lon,lat,xi,xf,yi,yf,precip,pmin):
        mask = np.zeros((len(lat),len(lon)))
        for ii in np.arange(mask.shape[0]):
                for jj in np.arange(mask.shape[1]):
			if lat[ii] > yi and lat[ii] < yf and lon[jj] > xi and lon[jj] < xf:
				#if precip[ii,jj] > pmin:
				mask[ii,jj] = 1

	return mask

def InterpZ(phi,hgt,htz,mask):
	phiz = np.zeros((len(htz),phi.shape[1],phi.shape[2]))
	for ii in np.arange(phi.shape[1]):
		for jj in np.arange(phi.shape[2]):
			if mask[ii,jj]:
				phiz[:,ii,jj]=griddata(hgt[:,ii,jj],phi[:,ii,jj],htz,method='cubic')

	return phiz

xi=233
xf=467
yi=473
yf=707

lat  = ReadFile('data/XLAT_d03_NewGuinea.nc','XLAT')
lon  = ReadFile('data/XLONG_d03_NewGuinea.nc','XLONG')
lsm  = ReadFile('data/LANDMASK_d03_NewGuinea.nc','LANDMASK')

lat  = lat[0,yi:yf,0]
lon  = lon[0,0,xi:xf]
lsm  = lsm[0,yi:yf,xi:xf]
slm  = (lsm+1)%2
nx   = len(lon)
ny   = len(lat)

rain = np.zeros(lsm.shape)
for day in np.arange(7)+2:
	rain_c1  = ReadFile('data/prcp_d03_2010-02-'+'%.2d'%day+'_08','RAINC')
	rain_nc1 = ReadFile('data/prcp_d03_2010-02-'+'%.2d'%day+'_08','RAINNC')
	rain_c0  = ReadFile('data/prcp_d03_2010-02-'+'%.2d'%day+'_04','RAINC')
	rain_nc0 = ReadFile('data/prcp_d03_2010-02-'+'%.2d'%day+'_04','RAINNC')
	rain     = rain + rain_c1[0,yi:yf,xi:xf] + rain_nc1[0,yi:yf,xi:xf] - rain_c0[0,yi:yf,xi:xf] - rain_nc0[0,yi:yf,xi:xf]

mbox = GenMaskBox(lon,lat,130.0,131.6,-12.0,-11.1,rain,10.0)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon,lat,lsm,colors='k')
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
#levs = [0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
#levs = [0.,2.,5.,10.,20.,40.,80.,120.,160.,200.]
cmap = cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
#norm = cls.BoundaryNorm(levs,cmap.N)
co   = ax.contourf(lon,lat,mbox*rain,cmap=cmap)
plt.colorbar(co,orientation='vertical')
plt.title('Accumulated rainfall for 28 hours (mm) - (WRF)')
pylab.savefig('wrf_darwin.png')
plt.show()

day   = 2
time  = 5
phi   = ReadFile('data/PH_02'+'%.2d'%day+'%.2d'%time+'.nc','PH')
phi0  = ReadFile('data/PHB_02'+'%.2d'%day+'%.2d'%time+'.nc','PHB')
qv0   = ReadFile('data/QVAPOR_02'+'%.2d'%day+'%.2d'%time+'.nc','QVAPOR')

# generate the height fields on rho and theta levels
gInv = 1.0/9.8
print 'generating height field... (1)',phi.shape
hgt1 = np.zeros(phi.shape)
hgt1[:,:,:] = gInv*(phi[:,:,:] + phi0[:,:,:])

print 'generating height field... (2)',qv0.shape
hgt2 = np.zeros(qv0.shape)
for ii in np.arange(qv0.shape[0]):
	hgt2[ii,:,:] = 0.5*(hgt1[ii,:,:] + hgt1[ii+1,:,:])

imax=0
jmax=0
hmax=-1.0e+99
for ii in np.arange(hgt1.shape[1]):
	for jj in np.arange(hgt1.shape[2]):
		if hgt1[0,ii,jj] > hmax:
			hmax = hgt1[0,ii,jj]
			imax = ii
			jmax = jj

htz1 = hgt1[:,imax,jmax]
htz2 = hgt2[:,imax,jmax]

print phi.shape
print htz2.shape

counter = np.zeros(qv0.shape)
th  = np.zeros(qv0.shape)
qv  = np.zeros(qv0.shape)
qr  = np.zeros(qv0.shape)
qi  = np.zeros(qv0.shape)
qc  = np.zeros(qv0.shape)
qs  = np.zeros(qv0.shape)
qg  = np.zeros(qv0.shape)
wu  = np.zeros(qv0.shape)
ww  = np.zeros(qv0.shape)
wt  = np.zeros(qv0.shape)

times = [5,6,7,8]
days  = [2,3,4,5,6,7,8]
for day in days:
	for time in times:
		thf = ReadFile('data/T_02'+'%.2d'%day+'%.2d'%time+'.nc','T') + 300.0
		uxf = ReadFile('data/U_02'+'%.2d'%day+'%.2d'%time+'.nc','U')
		uyf = ReadFile('data/V_02'+'%.2d'%day+'%.2d'%time+'.nc','V')
		uzf = ReadFile('data/W_02'+'%.2d'%day+'%.2d'%time+'.nc','W')
		qvf = ReadFile('data/QVAPOR_02'+'%.2d'%day+'%.2d'%time+'.nc','QVAPOR')
		qrf = ReadFile('data/QRAIN_02'+'%.2d'%day+'%.2d'%time+'.nc','QRAIN')
		qif = ReadFile('data/QICE_02'+'%.2d'%day+'%.2d'%time+'.nc','QICE')
		qcf = ReadFile('data/QCLOUD_02'+'%.2d'%day+'%.2d'%time+'.nc','QCLOUD')
		qsf = ReadFile('data/QSNOW_02'+'%.2d'%day+'%.2d'%time+'.nc','QSNOW')
		qgf = ReadFile('data/QGRAUP_02'+'%.2d'%day+'%.2d'%time+'.nc','QGRAUP')

		phi  = ReadFile('data/PH_02'+'%.2d'%day+'%.2d'%time+'.nc','PH')
		phi0 = ReadFile('data/PHB_02'+'%.2d'%day+'%.2d'%time+'.nc','PHB')

		m2  = qrf + qif + qcf + qsf + qgf > 1.0e-4
		mask = np.zeros((m2.shape[1],m2.shape[2]))
		for ii in np.arange(m2.shape[1]):
			for jj in np.arange(m2.shape[2]):
				if mbox[ii,jj] > 0.1:
					mask[ii,jj] = np.sum(m2[:,ii,jj])

		# generate the height fields on rho and theta levels
		gInv = 1.0/9.8
		print 'generating height field... (1)'
		hgt1 = np.zeros(phi.shape)
		hgt1[:,:,:] = gInv*(phi[:,:,:] + phi0[:,:,:])

		print 'generating height field... (2)'
		hgt2 = np.zeros(qv.shape)
		for ii in np.arange(qv.shape[0]):
			hgt2[ii,:,:] = 0.5*(hgt1[ii,:,:] + hgt1[ii+1,:,:])

		# interpolate to consistent height levels
		uzi = InterpZ(uzf,hgt1,htz2,mask)
		qvi = InterpZ(qvf,hgt2,htz2,mask)
		qri = InterpZ(qrf,hgt2,htz2,mask)
		qii = InterpZ(qif,hgt2,htz2,mask)
		qci = InterpZ(qcf,hgt2,htz2,mask)
		qsi = InterpZ(qsf,hgt2,htz2,mask)
		qgi = InterpZ(qgf,hgt2,htz2,mask)
		uxi = InterpZ(uxf,hgt2,htz2,mask)
		uyi = InterpZ(uyf,hgt2,htz2,mask)
		thi = InterpZ(thf,hgt2,htz2,mask)

		m2 = qri + qii + qci + qsi + qgi > 1.0e-4
		for ii in np.arange(th.shape[0]):
			for jj in np.arange(th.shape[1]):
				for kk in np.arange(th.shape[2]):
					if m2[ii,jj,kk]:
						counter[ii,jj,kk] = counter[ii,jj,kk] + 1.0

		th = th + m2*thi
		qv = qv + m2*qvi
		qr = qr + m2*qri
		qi = qi + m2*qii
		qc = qc + m2*qci
		qs = qs + m2*qsi
		qg = qg + m2*qgi

		um  = np.zeros(qv0.shape)
		vm  = np.zeros(qv0.shape)
		wm  = np.zeros(qv0.shape)
		tm  = np.zeros(qv0.shape)
		# remove mean flow components
		for ii in np.arange(len(htz2)):
			npts = np.sum(m2[ii,:,:])
			if npts > 0.1:
				um[ii,:,:] = m2[ii,:,:]*uxi[ii,:,:] - np.sum(m2[ii,:,:]*uxi[ii,:,:])/npts
				vm[ii,:,:] = m2[ii,:,:]*uyi[ii,:,:] - np.sum(m2[ii,:,:]*uyi[ii,:,:])/npts
				wm[ii,:,:] = m2[ii,:,:]*uzi[ii,:,:] - np.sum(m2[ii,:,:]*uzi[ii,:,:])/npts
				tm[ii,:,:] = m2[ii,:,:]*thi[ii,:,:] - np.sum(m2[ii,:,:]*thi[ii,:,:])/npts

		wu = wu + m2*wm*(um + vm)
		ww = ww + m2*wm*wm
		wt = wt + m2*wm*tm

wwz = np.zeros(len(htz2))
wuz = np.zeros(len(htz2))
wtz = np.zeros(len(htz2))
thz = np.zeros(len(htz2))
qvz = np.zeros(len(htz2))
qrz = np.zeros(len(htz2))
qiz = np.zeros(len(htz2))
qcz = np.zeros(len(htz2))
qsz = np.zeros(len(htz2))
qgz = np.zeros(len(htz2))

for ii in np.arange(len(htz2)):
	wwz[ii] = np.sum(ww[ii,:,:])/np.sum(counter[ii,:,:])
	wuz[ii] = np.sum(wu[ii,:,:])/np.sum(counter[ii,:,:])
	wtz[ii] = np.sum(wt[ii,:,:])/np.sum(counter[ii,:,:])
	thz[ii] = np.sum(th[ii,:,:])/np.sum(counter[ii,:,:])
	qvz[ii] = np.sum(qv[ii,:,:])/np.sum(counter[ii,:,:])
	qrz[ii] = np.sum(qr[ii,:,:])/np.sum(counter[ii,:,:])
	qiz[ii] = np.sum(qi[ii,:,:])/np.sum(counter[ii,:,:])
	qcz[ii] = np.sum(qc[ii,:,:])/np.sum(counter[ii,:,:])
	qsz[ii] = np.sum(qs[ii,:,:])/np.sum(counter[ii,:,:])
	qgz[ii] = np.sum(qg[ii,:,:])/np.sum(counter[ii,:,:])
	
np.savetxt('hgt_darwin_cloud.wrf',htz2)
np.savetxt('wwz_darwin_cloud.wrf',wwz)
np.savetxt('wuz_darwin_cloud.wrf',wuz)
np.savetxt('wtz_darwin_cloud.wrf',wtz)
np.savetxt('thz_darwin_cloud.wrf',thz)
np.savetxt('qvz_darwin_cloud.wrf',qvz)
np.savetxt('qrz_darwin_cloud.wrf',qrz)
np.savetxt('qiz_darwin_cloud.wrf',qiz)
np.savetxt('qcz_darwin_cloud.wrf',qcz)
np.savetxt('qsz_darwin_cloud.wrf',qsz)
np.savetxt('qgz_darwin_cloud.wrf',qgz)

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(wuz, htz2,'-')
plt.plot(wwz, htz2,'-')
plt.xlabel('<$wu$> (m/s)')
plt.ylabel('height (m)')
plt.legend(('<$wu$>','<$ww$>'),loc=3,prop={'size':10})
plt.title('Perturbed momentum flux vertical profile')
pylab.savefig('wuz_darwin_wrf_cloud.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(wtz, htz2,'-')
plt.xlabel('<$w\Theta$> (mK/s)')
plt.ylabel('height (m)')
plt.title('Potential temperature flux vertical profile')
pylab.savefig('wtz_darwin_wrf_cloud.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qvz, htz2,'-')
plt.xlabel('<$q_{vapor}$> (kg/kg)')
plt.ylabel('height (m)')
plt.title('Water vapor vertical profile')
pylab.savefig('qvz_darwin_wrf_cloud.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(thz, htz2,'-')
plt.xlabel('$<\Theta>$ (K)')
plt.ylabel('height (m)')
plt.title('Potential temperature vertical profile')
pylab.savefig('thz_darwin_wrf_cloud.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qrz, htz2,'-')
plt.plot(qiz, htz2,'-')
plt.plot(qcz, htz2,'-')
plt.plot(qsz, htz2,'-')
plt.plot(qgz, htz2,'-')
plt.xlabel('<q> (kg/kg)')
plt.legend(('rain','ice','cloud','snow','graupel'),loc=1,prop={'size':10})
plt.ylabel('height (m)')
plt.title('Water species vertical profile')
pylab.savefig('qiz_darwin_wrf_cloud.png')

plt.show()
