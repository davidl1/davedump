#!/usr/bin/env python

import numpy as np
from scipy.io import netcdf
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import pylab
import os.path

def ReadFile(filename,fieldname,isscalar):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	if isscalar:
		return field.getValue()
	else:
		return field[:]

def GenFilename(day,hr,mi):
	return '02' + '%.2u' % day + '_' + '%.2u' % hr + '%u' % mi + '0.nc'

directory = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km/'
filename  = 'cpol_2d_rainrate_2010'
#directory = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_72_2.5km/'
#filename  = 'cpol_2d_rainrate_72_2p5km_2010'

Re   = 6378100.0
lon0 = ReadFile(directory+filename+'0201_0000.nc','radar_longitude',True)
lat0 = ReadFile(directory+filename+'0201_0000.nc','radar_latitude',True)
x    = ReadFile(directory+filename+'0201_0000.nc','x',False)
y    = ReadFile(directory+filename+'0201_0000.nc','y',False)
x    = 1000.0*x
y    = 1000.0*y
nx   = x.shape[0]
ny   = y.shape[0]
dx   = (x[-1]-x[0])/len(x)
dy   = (y[-1]-y[0])/len(y)
lon  = lon0 + x*360.0/2.0/np.pi/Re
lat  = lat0 + y*360.0/2.0/np.pi/Re

# get the land sea mask
ht     = ReadFile('/mnt/coe0/davidl1/Darwin_50/020112/lsm.nc','lsm',False)
lon_ht = ReadFile('/mnt/coe0/davidl1/Darwin_50/020112/lsm.nc','longitude',False)
lat_ht = ReadFile('/mnt/coe0/davidl1/Darwin_50/020112/lsm.nc','latitude',False)
ht     = ht[0,0,:,:]

lsm0=ht
pts0=np.transpose([np.tile(lon_ht,len(lat_ht)),np.repeat(lat_ht,len(lon_ht))])
pts1=np.transpose([np.tile(lon,ny),np.repeat(lat,nx)])
lsmr=np.ravel(lsm0)
lsm1=griddata(pts0,lsmr,pts1,method='nearest')
lsm =lsm1.reshape(ny,nx)
slm =(lsm+1)%2

pcp_i = ReadFile(directory+filename+'0202_0000.nc','rr',False)
empty = pcp_i[0,:,:]<-10
full  = (empty+1)%2
nt    = np.sum(full)
nl    = np.sum(full*lsm)
ns    = np.sum(full*slm)

lrain = np.zeros(24)
srain = np.zeros(24)
lrain = np.zeros(24)
train = np.zeros(24)
ndays = 0
#days  = [4,5,6,7,9,11]
#days  = np.arange(28)+1
#days  = [2,4,5,6,7,9]
days  = [2,4,5,6,7,9]
mins  = [0]

hrcount = np.zeros(24*len(mins))

for day in days:
	nhrs  = np.zeros(24)
	for hr in np.arange(24):
		for mi in mins:
			ext   = GenFilename(day,hr,mi)
			if os.path.isfile(directory+filename+ext):
				pcp_i = ReadFile(directory+filename+ext,'rr',False)
				vals  = pcp_i[0,:,:]>0.0
				pcp   = vals*pcp_i[0,:,:]

				lrain[hr] = lrain[hr] + np.sum(full*lsm*pcp)/nl
				srain[hr] = srain[hr] + np.sum(full*slm*pcp)/ns
				train[hr] = train[hr] + np.sum(full*pcp)/nt
				hrcount[hr] = hrcount[hr] + 1
				nhrs[hr] = 1
				continue

			if hr == 0:
				ext_m1 = GenFilename(day-1,23,5)
			else:
				ext_m1 = GenFilename(day,hr-1,5)

			ext_p1 = GenFilename(day,hr,1)

			if os.path.isfile(directory+filename+ext_m1) and os.path.isfile(directory+filename+ext_p1):
				pcp_m1 = ReadFile(directory+filename+ext_m1,'rr',False)
				pcp_p1 = ReadFile(directory+filename+ext_p1,'rr',False)
				vals   = pcp_m1[0,:,:]>0.0
				pcp    = vals*(0.5*(pcp_m1+pcp_p1))

				lrain[hr]   = lrain[hr] + np.sum(full*lsm*pcp)/nl
				srain[hr]   = srain[hr] + np.sum(full*slm*pcp)/ns
				train[hr]   = train[hr] + np.sum(full*pcp)/nt
				hrcount[hr] = hrcount[hr] + 1
				nhrs[hr] = 1
				continue
			elif os.path.isfile(directory+filename+ext_m1):
				pcp_m1 = ReadFile(directory+filename+ext_m1,'rr',False)
				vals   = pcp_m1[0,:,:]>0.0
				pcp    = vals*pcp_m1

				lrain[hr]   = lrain[hr] + np.sum(full*lsm*pcp)/nl
				srain[hr]   = srain[hr] + np.sum(full*slm*pcp)/ns
				train[hr]   = train[hr] + np.sum(full*pcp)/nt
				hrcount[hr] = hrcount[hr] + 1
				nhrs[hr] = 1
				continue
			elif os.path.isfile(directory+filename+ext_p1):
				pcp_m1 = ReadFile(directory+filename+ext_p1,'rr',False)
				vals   = pcp_m1[0,:,:]>0.0
				pcp    = vals*pcp_p1

				lrain[hr]   = lrain[hr] + np.sum(full*lsm*pcp)/nl
				srain[hr]   = srain[hr] + np.sum(full*slm*pcp)/ns
				train[hr]   = train[hr] + np.sum(full*pcp)/nt
				hrcount[hr] = hrcount[hr] + 1
				nhrs[hr] = 1
				continue

			if hr == 0:
				ext_m1 = GenFilename(day-1,23,mi)
			else:
				ext_m1 = GenFilename(day,hr-1,mi)
			
			if hr == 23:
				ext_p1 = GenFilename(day+1,0,mi)
			else:
				ext_p1 = GenFilename(day,hr+1,mi)

			if os.path.isfile(directory+filename+ext_m1) and os.path.isfile(directory+filename+ext_p1):
				pcp_m1 = ReadFile(directory+filename+ext_m1,'rr',False)
				pcp_p1 = ReadFile(directory+filename+ext_p1,'rr',False)
				vals   = pcp_m1[0,:,:]>0.0
				pcp    = vals*(0.5*(pcp_m1+pcp_p1))

				lrain[hr]   = lrain[hr] + np.sum(full*lsm*pcp)/nl
				srain[hr]   = srain[hr] + np.sum(full*slm*pcp)/ns
				train[hr]   = train[hr] + np.sum(full*pcp)/nt
				hrcount[hr] = hrcount[hr] + 1
				nhrs[hr] = 1

	print nhrs
	print np.sum(nhrs)
	if np.sum(nhrs) > 23.5:
		ndays   = ndays + 1
				
print hrcount
print 'ndays:',ndays

lrain  = lrain/len(days)
srain  = srain/len(days)
train  = train/len(days)
#lrain  = lrain/hrcount
#srain  = srain/hrcount
#train  = train/hrcount
lrain2 = np.zeros(24)
srain2 = np.zeros(24)
train2 = np.zeros(24)
time   = np.arange(24)
GMT=9
for ii in np.arange(24):
        ind        = (ii-9)%24
        lrain2[ii] = lrain[ind]
        srain2[ii] = srain[ind]
        train2[ii] = train[ind]

fig=plt.figure()
ax=fig.add_subplot(111)
plt.plot(time,lrain2,'-o')
plt.plot(time,srain2,'-o')
plt.legend(('land','sea'))
plt.xlim([0,23])
plt.ylim([0.0,1.0])
plt.xlabel('Local time (hrs)')
plt.ylabel('Precipitation rate (mm)')
plt.title('Avg. diurnal cycle (CPOL)')
#pylab.savefig('avg_diurnal_cycle_cpol_6days_2.png')
plt.show()

time3 =np.zeros(25)
lrain3=np.zeros(25)
srain3=np.zeros(25)
train3=np.zeros(25)
time3[:24]=time
time3[24] =24
lrain3[:24]=lrain2
lrain3[24] =lrain2[0]
srain3[:24]=srain2
srain3[24] =srain2[0]
train3[:24]=train2
train3[24] =train2[0]

np.savetxt('diurnal_darwin_cpol_6days_time.txt',time3)
np.savetxt('diurnal_darwin_cpol_6days_land.txt',lrain3)
np.savetxt('diurnal_darwin_cpol_6days_sea.txt', srain3)
np.savetxt('diurnal_darwin_cpol_6days_total.txt', train3)
