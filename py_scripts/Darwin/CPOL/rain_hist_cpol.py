#!/usr/bin/env python

# script to generate the diurnal cycle from the CPOL data

import numpy as np
from scipy.io import netcdf
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import pylab

def ReadFile(filename,fieldname,isscalar):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	if isscalar:
		return field.getValue()
	else:
		return field[:]

directory = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km/'
filename  = 'cpol_2d_rainrate_2010'

Re   = 6378100.0
lon0 = ReadFile(directory+filename+'0201_0000.nc','radar_longitude',True)
lat0 = ReadFile(directory+filename+'0201_0000.nc','radar_latitude',True)
x    = ReadFile(directory+filename+'0201_0000.nc','x',False)
y    = ReadFile(directory+filename+'0201_0000.nc','y',False)
x    = 1000.0*x
y    = 1000.0*y
nx   = x.shape[0]
ny   = y.shape[0]
print (x[-1]-x[0])/len(x)
print (y[-1]-y[0])/len(y)
lon  = lon0 + x*360.0/2.0/np.pi/Re
lat  = lat0 + y*360.0/2.0/np.pi/Re

# get the land sea mask
ht     = ReadFile('/mnt/coe0/davidl1/Darwin_50/020112/lsm.nc','lsm',False)
lon_ht = ReadFile('/mnt/coe0/davidl1/Darwin_50/020112/lsm.nc','longitude',False)
lat_ht = ReadFile('/mnt/coe0/davidl1/Darwin_50/020112/lsm.nc','latitude',False)
ht     = ht[0,0,:,:]

lsm0=ht
pts0=np.transpose([np.tile(lon_ht,len(lat_ht)),np.repeat(lat_ht,len(lon_ht))])
pts1=np.transpose([np.tile(lon,ny),np.repeat(lat,nx)])
lsmr=np.ravel(lsm0)
lsm1=griddata(pts0,lsmr,pts1,method='nearest')
lsm =lsm1.reshape(ny,nx)
slm =(lsm+1)%2

print lon[-1]-lon[0]
print lat[-1]-lat[0]

days = [2,4,5,7,9]

prange = 1
nbins  = 250
shist  = np.zeros(nbins)
lhist  = np.zeros(nbins)
thist  = np.zeros(nbins)

pcp_i = ReadFile(directory+filename+'0202_0000.nc','rr',False)
empty = pcp_i[0,:,:]<-10
full  = (empty+1)%2
nt    = np.sum(full)
nl    = np.sum(full*lsm)
ns    = np.sum(full*slm)

lsm  = lsm.flatten()
full = full.flatten()
for day in days:
	for hr in np.arange(24):
		ext   = '02' + '%.2u' % day + '_' + '%.2u' % hr + '00.nc'
		pcp_i = ReadFile(directory+filename+ext,'rr',False)
		vals  = pcp_i[0,:,:]>0.0
		pcp   = vals*pcp_i[0,:,:]

		pcp = pcp.flatten()
		pcp = pcp/prange

		for ii in np.arange(len(pcp)):
			if full[ii]:
				rbin = np.int(pcp[ii])
				thist[rbin] = thist[rbin] + 1
				if lsm[ii]:
					lhist[rbin] = lhist[rbin] + 1
				else:
					shist[rbin] = shist[rbin] + 1

thist = 100*thist/24/len(days)/nt
lhist = 100*lhist/24/len(days)/nl
shist = 100*shist/24/len(days)/ns
fall = prange*np.arange(nbins)

print np.sum(thist)
print np.sum(lhist)
print np.sum(shist)

fig=plt.figure()
ax=fig.add_subplot(111)
plt.semilogy(fall,lhist,'-o')
plt.semilogy(fall,shist,'-o')
plt.semilogy(fall,thist,'-o')
plt.legend(('land','sea','total'))
plt.xlabel('precipitation (mm)')
plt.ylabel('% of domain area')
plt.title('Rainfall Histogram (CPOL)')
pylab.savefig('rain_histogram_cpol.png')
plt.show()
