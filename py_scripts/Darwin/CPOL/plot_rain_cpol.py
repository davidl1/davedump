#!/usr/bin/env python

import numpy as np
from scipy.io import netcdf
from scipy.interpolate import griddata
from matplotlib import pyplot as plt
from matplotlib import colors as cls
import pylab

def ReadFile(filename,fieldname,isscalar):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	if isscalar:
		return field.getValue()
	else:
		return field[:]

directory = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km/'
filename  = 'cpol_2d_rainrate_2010'
#directory = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_72_2.5km/'
#filename  = 'cpol_2d_rainrate_72_2p5km_2010'

Re   = 6378100.0
lon0 = ReadFile(directory+filename+'0201_0000.nc','radar_longitude',True)
lat0 = ReadFile(directory+filename+'0201_0000.nc','radar_latitude',True)
x    = ReadFile(directory+filename+'0201_0000.nc','x',False)
y    = ReadFile(directory+filename+'0201_0000.nc','y',False)
x    = 1000.0*x # in km, not m
y    = 1000.0*y # in km, not m
nx   = x.shape[0]
ny   = y.shape[0]
print nx,ny
lon  = lon0 + x*360.0/2.0/np.pi/Re
lat  = lat0 + y*360.0/2.0/np.pi/Re
print 'lon: ',lon[0],'-',lon[-1]
print 'lat: ',lat[0],'-',lat[-1]

# get the land sea mask
ht     = ReadFile('/mnt/coe0/davidl1/Darwin_50/020112/lsm.nc','lsm',False)
lon_ht = ReadFile('/mnt/coe0/davidl1/Darwin_50/020112/lsm.nc','longitude',False)
lat_ht = ReadFile('/mnt/coe0/davidl1/Darwin_50/020112/lsm.nc','latitude',False)
ht     = ht[0,0,:,:]

lsm0=ht
pts0=np.transpose([np.tile(lon_ht,len(lat_ht)),np.repeat(lat_ht,len(lon_ht))])
pts1=np.transpose([np.tile(lon,ny),np.repeat(lat,nx)])
lsmr=np.ravel(lsm0)
lsm1=griddata(pts0,lsmr,pts1,method='nearest')
lsm =lsm1.reshape(ny,nx)
slm =(lsm+1)%2

print lon[-1]-lon[0]
print lat[-1]-lat[0]

pcp_i = ReadFile(directory+filename+'0202_0000.nc','rr',False)
pcp_i = pcp_i[0,:,:]
mask  = pcp_i<-10.0
mask  = (mask+1)%2

days = [2,4,5,7,9]
pcp  = np.zeros((len(lat),len(lon)))
for day in days:
	for hr in  np.arange(24):
		ext   = '02' + '%.2u' % day + '_' + '%.2u' % hr + '00.nc'
		pcp_i = ReadFile(directory+filename+ext,'rr',False)
		vals  = pcp_i[0,:,:]>0.0
		pcp   = pcp + vals*pcp_i[0,:,:]

# calculate averages - full domain
area = np.outer(2500.0*np.ones(ny),2500.0*np.ones(nx))
totland = np.sum(mask*lsm*area)
totsea  = np.sum(mask*slm*area)
totlandrain = np.sum(mask*lsm*area*pcp)
totsearain  = np.sum(mask*slm*area*pcp)
print 'accumulated rainfall averages - full domain'
print 'avg. precip. land:  ', totlandrain/totland
print 'avg. precip. sea:   ', totsearain/totsea
print 'avg. precip. total: ', np.sum(mask*pcp*area)/np.sum(mask*area)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon,lat,lsm,colors='k')
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
levs=[0.,2.,5.,10.,20.,40.,80.,120.,160.,200.]
cmap=cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm=cls.BoundaryNorm(levs,cmap.N)
co = ax.contourf(lon,lat,pcp,levels=levs,cmap=cmap,norm=norm)
plt.colorbar(co,orientation='horizontal')
plt.title('Accumulated rainfall for 120 hours (mm) - (CPOL)')
pylab.savefig('accum_rain_cpol_2p5.png')
plt.show()

np.savetxt('lon_cpol.txt',lon)
np.savetxt('lat_cpol.txt',lat)
np.savetxt('pcp_cpol_full.txt',pcp)
np.savetxt('pcp_cpol_land.txt',lsm*pcp)
np.savetxt('pcp_cpol_sea.txt', slm*pcp)
np.savetxt('mask_cpol.txt',mask)
