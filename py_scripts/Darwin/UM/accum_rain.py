#!/usr/bin/env python

# Generates a plot of accumulated rainfall accross the darwin CPOL radar domain and calculate 
# rainfall amounts over land, sea and the total CPOL domain

from scipy.interpolate import *
import matplotlib.pyplot as plt
import matplotlib.colors as cls
import numpy as np
import pylab
from Utils import *

times = ['020200','020212','020400','020412','020500','020512','020700','020712','020900','020912']

filename = '../' + '020112' + '/lsm_4km.nc'
#filename = '../' + '020112' + '/lsm.nc'
lsm = ReadField(filename,'lsm')
lon = ReadField(filename,'longitude')
lat = ReadField(filename,'latitude')

precip_t = np.zeros((lsm.shape[2],lsm.shape[3]))
dt = 1.0*60*60
Re = 6378100.0
dx = 2.0*np.pi*Re*(lon[-1]-lon[0])/(len(lon)-1)/360.0
dx = dx*np.ones(len(lon))
dy = np.zeros(len(lat))
for ii in np.arange(len(dy))-1:
	dy[ii] = 2.0*np.pi*Re*(lat[ii+1]-lat[ii])/360.0
dy[-1] = dy[-2]
area = np.outer(dy,dx)

# precip_2 is the hourly averaged dump, _1 is instantaneous
for time in times:
	filename = '../' + time + '/precip_4km.nc'
	#filename = '../' + time + '/precip.nc'
	precip_i = ReadField(filename,'precip')
	for time_i in np.arange(precip_i.shape[0]):
		precip_t = precip_t + precip_i[time_i,0,:,:]

# rescale to mm (10^-3 m)
rho = 1.0e+3
m2mm = 1.0e+3
precip_t = (dt/rho*m2mm)*precip_t

# calculate averages - full domain
lsm = lsm[0,0,:,:]
slm = (lsm+1)%2
totland = np.sum(lsm*area)
totsea  = np.sum(slm*area)
totlandrain = np.sum(lsm*area*precip_t)
totsearain  = np.sum(slm*area*precip_t)
print 'accumulated rainfall averages - full domain'
print 'avg. precip. land:  ', totlandrain/totland
print 'avg. precip. sea:   ', totsearain/totsea
print 'avg. precip. total: ', np.sum(precip_t*area)/np.sum(area)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon,lat,lsm,colors='k')
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
levs=[0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
#levs=[0.,2.,5.,10.,20.,40.,80.,120.,160.,200.]
cmap=cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm=cls.BoundaryNorm(levs,cmap.N)
co = ax.contourf(lon,lat,precip_t,levels=levs,cmap=cmap,norm=norm)
plt.title('Accumulated rainfall for 120 hours (mm) - (UM)')
plt.colorbar(co,orientation='horizontal')
pylab.savefig('accum_rain_120_4km.png')

# interpolate radar mask and plot that space only
dir_o     = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km/'
file_o    = 'cpol_2d_rainrate_2010'
pcp_o     = ReadField(dir_o+file_o+'0202_1200.nc','rr')
pcp_o     = pcp_o[0,:,:]
empty     = pcp_o[:,:]<-10
full      = (empty+1)%2
lon0_o    = ReadScalar(dir_o+file_o+'0201_0000.nc','radar_longitude')
lat0_o    = ReadScalar(dir_o+file_o+'0201_0000.nc','radar_latitude')
x         = ReadField(dir_o+file_o+'0201_0000.nc','x')
y         = ReadField(dir_o+file_o+'0201_0000.nc','y')
x         = 1000.0*x
y         = 1000.0*y
Re        = 6378100.0
lon_o     = lon0_o + x*360.0/2.0/np.pi/Re
lat_o     = lat0_o + y*360.0/2.0/np.pi/Re
pts0      = np.transpose([np.tile(lon_o,len(lat_o)),np.repeat(lat_o,len(lon_o))])
pts1      = np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
full_r    = np.ravel(full)
full_m    = griddata(pts0,full_r,pts1,method='nearest')
full_m    = full_m.reshape(len(lat),len(lon))
precip_m  = full_m*precip_t

# calcualtes averages - darwin
totland = np.sum(full_m*lsm*area)
totsea  = np.sum(full_m*slm*area)
totlandrain = np.sum(full_m*lsm*area*precip_t)
totsearain  = np.sum(full_m*slm*area*precip_t)
print 'accumulated rainfall averages - darwin'
print 'avg. precip. land:  ', totlandrain/totland
print 'avg. precip. sea:   ', totsearain/totsea
print 'avg. precip. total: ', np.sum(full_m*precip_t*area)/np.sum(full_m*area)

fig = plt.figure()
ax = fig.add_subplot(111)
#ax.contour(lon[400:-369],lat[400:-369],lsm[400:-369,400:-369],colors='k')
#co = ax.contourf(lon[400:-369],lat[400:-369],precip_m[400:-369,400:-369],levels=levs,cmap=cmap,norm=norm)
ax.contour(lon[270:-249],lat[265:-254],lsm[265:-254,270:-249],colors='k')
co = ax.contourf(lon[270:-249],lat[265:-254],precip_m[265:-254,270:-249],levels=levs,cmap=cmap,norm=norm)
plt.title('Accumulated rainfall for 120 hours (mm) - (UM)')
plt.colorbar(co,orientation='horizontal')
pylab.savefig('accum_rain_120_darwin_4km.png')
plt.show()

#np.savetxt('lon_um_darwin.txt',lon[390:-359])
#np.savetxt('lat_um_darwin.txt',lat[390:-359])
np.savetxt('lon_um_region_4km.txt',lon)
np.savetxt('lat_um_region_4km.txt',lat)
np.savetxt('pcp_um_region_full_4km.txt',precip_t)
np.savetxt('pcp_um_region_land_4km.txt',lsm*precip_t)
np.savetxt('pcp_um_region_sea_4km.txt', slm*precip_t)
np.savetxt('pcp_um_darwin_full_4km.txt',precip_m)
np.savetxt('pcp_um_darwin_land_4km.txt',lsm*precip_m)
np.savetxt('pcp_um_darwin_sea_4km.txt', slm*precip_m)
#np.savetxt('pcp_um_darwin_full.txt',precip_m[390:-359,390:-359])
#np.savetxt('pcp_um_darwin_land.txt',lsm[390:-359,390:-359]*precip_m[390:-359,390:-359])
#np.savetxt('pcp_um_darwin_sea.txt', slm[390:-359,390:-359]*precip_m[390:-359,390:-359])

#pcp_m_dar_full = InterpGrid(lon,lat,lon_o,lat_o,precip_m,'cubic')
#pcp_m_dar_land = InterpGrid(lon,lat,lon_o,lat_o,lsm*precip_m,'cubic')
#pcp_m_dar_sea  = InterpGrid(lon,lat,lon_o,lat_o,slm*precip_m,'cubic')
#np.savetxt('pcp_um_darwin_full_cpol_pts.txt',pcp_m_dar_full)
#np.savetxt('pcp_um_darwin_land_cpol_pts.txt',pcp_m_dar_land)
#np.savetxt('pcp_um_darwin_sea_cpol_pts.txt',pcp_m_dar_sea)
