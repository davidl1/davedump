#!/usr/bin/env python

# Calculate the instantaneous horizontally averaged vertical energy profile for a cloud
# decomposed into kinetic, potential, internal and moist energy

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
from matplotlib import colors as cls
import numpy as np
import pylab

def ReadFile(filename,fieldname):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

def GenMaskBox(lon,lat,xi,xf,yi,yf):
        mask = np.zeros((len(lat),len(lon)))
        for ii in np.arange(mask.shape[0]):
                for jj in np.arange(mask.shape[1]):
			if lat[ii] > yi and lat[ii] < yf and lon[jj] > xi and lon[jj] < xf:
				mask[ii,jj] = 1

	return mask

def GetIndices(lon,lat,xi,xf,yi,yf):
	xmin = 0
	xmax = 0
	ymin = 0
	ymax = 0

	for ii in np.arange(len(lon)):
		if lon[ii] > xi:
			xmin = ii
			break

	for ii in np.arange(len(lon)):
		if lon[ii] > xf:
			xmax = ii
			break

	for ii in np.arange(len(lat)):
		if lat[ii] > yi:
			ymin = ii
			break

	for ii in np.arange(len(lat)):
		if lat[ii] > yf:
			ymax = ii
			break

	return [xmin,xmax,ymin,ymax]


def InterpZ(phi,hgt,htz,mask):
	print 'interpolating field...'

	phiz = np.zeros((len(htz),phi.shape[1],phi.shape[2]))
	for ii in np.arange(phi.shape[1]):
		for jj in np.arange(phi.shape[2]):
			if mask[ii,jj]:
				phiz[:,ii,jj]=griddata(hgt[:,ii,jj],phi[:,ii,jj],htz,method='cubic')

	return phiz

path     = '/mnt/coe0/davidl1/Darwin_15/020112/'
lsm      = ReadFile(path+'lsm.nc','lsm')[0,0,:,:]
lon      = ReadFile(path+'lsm.nc','longitude')
lat      = ReadFile(path+'lsm.nc','latitude')
ht_rho   = ReadFile(path+'ht_rho.nc','ht')[0,:,:,:]
ht_theta = ReadFile(path+'ht_theta.nc','ht')[0,:,:,:]
orog     = ReadFile(path+'orog.nc','ht')[0,0,:,:]

slm  = (lsm+1)%2
nx   = len(lon)
ny   = len(lat)

#mbox = GenMaskBox(lon,lat,130.0,131.6,-12.0,-11.1)
[xi,xf,yi,yf] = GetIndices(lon,lat,130.0,131.6,-12.0,-11.1)

ht_rho   = ht_rho[:,yi:yf,xi:xf]
ht_theta = ht_theta[:,yi:yf,xi:xf]
orog     = orog[yi:yf,xi:xf]

print orog.shape

iimax = 0
jjmax = 0
htmax = 0.0
for ii in np.arange(orog.shape[0]):
	for jj in np.arange(orog.shape[1]):
		if orog[ii,jj] > htmax:
			iimax = ii
			jjmax = jj
			htmax = orog[ii,jj]

print 'max indices:',iimax,jjmax,htmax
nz = ht_rho.shape[0]

z_rho   = ht_rho[:nz-1,iimax,jjmax]
z_theta = ht_theta[:nz-1,iimax,jjmax]

RdInv = 1.0/287.05
c_v   = 1005.0 - 287.05
g     = 9.8
#Re    = 6387100.0
L_c   = 2.501e+6
L_f   = 0.334e+6

path  = '/mnt/coe0/davidl1/Darwin_15_NoConv_SL02_3DNoBlend/'

day   = '020700'
hour  = 6
hrmod = hour%3
hrdiv = hour-hrmod

KE  = np.zeros(nz-1)	# kinetic
PE  = np.zeros(nz-1)	# potetial
IE  = np.zeros(nz-1)	# internal
#CE  = np.zeros(nz-1)	# liquid
#FE  = np.zeros(nz-1)	# ice
ME  = np.zeros(nz-1)	# moist

velx  = ReadFile(path+day+'/velx_'+'%d'%hrdiv+'.nc','x-wind')[hrmod,:,yi:yf,xi:xf]
vely  = ReadFile(path+day+'/vely_'+'%d'%hrdiv+'.nc','y-wind')[hrmod,:,yi:yf,xi:xf]
velz  = ReadFile(path+day+'/velz_'+'%d'%hrdiv+'.nc','dz_dt')[hrmod,:,yi:yf,xi:xf]
qvap  = ReadFile(path+day+'/hum_'+'%d'%hrdiv+'.nc','q')[hrmod,:,yi:yf,xi:xf]
qliq  = ReadFile(path+day+'/qcl_'+'%d'%hrdiv+'.nc','QCL')[hrmod,:,yi:yf,xi:xf]
qice  = ReadFile(path+day+'/qcf_'+'%d'%hrdiv+'.nc','QCF')[hrmod,:,yi:yf,xi:xf]
qrai  = ReadFile(path+day+'/rain_'+'%d'%hrdiv+'.nc','field1681')[hrmod,:,yi:yf,xi:xf]
qgra  = ReadFile(path+day+'/graup_'+'%d'%hrdiv+'.nc','field1921')[hrmod,:,yi:yf,xi:xf]
theta = ReadFile(path+day+'/theta_'+'%d'%hrdiv+'.nc','theta')[hrmod,:,yi:yf,xi:xf]
exner = ReadFile(path+day+'/exner_'+'%d'%hrdiv+'.nc','field7')[hrmod,:,yi:yf,xi:xf]
press = ReadFile(path+day+'/pressure_'+'%d'%hrdiv+'.nc','p')[hrmod,:,yi:yf,xi:xf]

rho = RdInv*press/theta/exner

# mask out points with insufficient concentration to be considered part of the cloud
m2 = qliq + qice + qrai + qgra > 1.0e-4
mask = np.zeros((m2.shape[1],m2.shape[2]))
for ii in np.arange(m2.shape[1]):
	for jj in np.arange(m2.shape[2]):
		mask[ii,jj] = np.sum(m2[:,ii,jj])

mz = mask > 0.1
print 'num pts: ', np.sum(mz)

# interpolate to consistent height levels
velxi  = InterpZ(velx, ht_rho,  z_theta,mz)
velyi  = InterpZ(vely, ht_rho,  z_theta,mz)
velzi  = InterpZ(velz, ht_theta,z_theta,mz)
qvapi  = InterpZ(qvap, ht_theta,z_theta,mz)
qliqi  = InterpZ(qliq, ht_theta,z_theta,mz)
qicei  = InterpZ(qice, ht_theta,z_theta,mz)
qraii  = InterpZ(qrai, ht_theta,z_theta,mz)
qgrai  = InterpZ(qgra, ht_theta,z_theta,mz)
thetai = InterpZ(theta,ht_theta,z_theta,mz)
exneri = InterpZ(exner,ht_theta,z_theta,mz)
pressi = InterpZ(press,ht_theta,z_theta,mz)
rhoi   = InterpZ(rho  ,ht_theta,z_theta,mz)

m2 = qliqi + qicei + qraii + qgrai > 1.0e-4
counter = np.zeros(m2.shape)
for ii in np.arange(m2.shape[0]):
	for jj in np.arange(m2.shape[1]):
		for kk in np.arange(m2.shape[2]):
			if m2[ii,jj,kk]:
				counter[ii,jj,kk] = counter[ii,jj,kk] + 1.0

# internal energy
ie = c_v*rhoi*thetai*exneri

# kinetic energy
ke = 0.5*rhoi*(velxi*velxi + velyi*velyi + velzi*velzi)

zf = np.zeros(rhoi.shape)
for ii in np.arange(rhoi.shape[1]):
	for jj in np.arange(rhoi.shape[2]):
		zf[:,ii,jj] = z_theta
#		zf[:,ii,jj] = z_theta + Re

#tempi = thetai*exneri
#L_c   = 1000.0*(2500.8 - 2.36*tempi + 0.0016*tempi*tempi - 0.00006*tempi*tempi*tempi)

# potential energy
pe = g*rhoi*zf
#ce = -rhoi*L_c*(qliqi + qraii)
#fe = -rhoi*(L_c + L_f)*(qicei + qgrai)

# moist energy
me = -rhoi*(L_c*(qliqi + qraii) + (L_c + L_f)*(qicei + qgrai))

ke = m2*ke
pe = m2*pe
ie = m2*ie
me = m2*me

# vertically average
for ii in np.arange(len(z_theta)):
	npts = np.sum(counter[ii,:,:])
	if npts > 0.1:
		KE[ii]  = np.sum(ke[ii,:,:])/npts
		PE[ii]  = np.sum(pe[ii,:,:])/npts
		IE[ii]  = np.sum(ie[ii,:,:])/npts
		#CE[ii]  = np.sum(ce[ii,:,:])/npts
		#FE[ii]  = np.sum(fe[ii,:,:])/npts
		ME[ii]  = np.sum(me[ii,:,:])/npts

np.savetxt('ht_darwin_cloud.energybudget',z_theta)
np.savetxt('ke_darwin_cloud.energybudget',KE)
np.savetxt('pe_darwin_cloud.energybudget',PE)
np.savetxt('ie_darwin_cloud.energybudget',IE)
np.savetxt('me_darwin_cloud.energybudget',ME)
#np.savetxt('ce_darwin.cloud',CE)
#np.savetxt('fe_darwin.cloud',FE)

countz = np.zeros(len(z_theta))
for ii in np.arange(len(z_theta)):
	countz[ii] = np.sum(counter[ii,:,:])

np.savetxt('count_darwin.cloud.energybudget',countz)
