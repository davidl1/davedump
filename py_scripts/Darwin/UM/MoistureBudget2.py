#!/usr/bin/env python

# Calculate the time varying moisture budget for the regional model, including change in total column 
# moisture, evaporation, precipitation, boundary fluxes and residual
# Reference:
#       UM Scientific ducomentaition, Large-scale precipitation,
#       https://nf.nci.org.au/facilities/software/UM/8.2/umdoc_system/UM_docs/scientific.html

import numpy as np
from scipy.io import netcdf
from scipy.interpolate import *
import matplotlib.pyplot as plt
import pylab

def ReadFile(filename,fieldname):
        print 'reading file: ', filename
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
        return field[:]

def VertInt(phi3d,hgt):
	nz    = phi3d.shape[0]
	phi2d = np.zeros((phi3d.shape[1],phi3d.shape[2]))
	phi2d = phi2d + 0.5*(hgt[1,:,:]-hgt[0,:,:])*phi3d[0,:,:]
	phi2d = phi2d + 0.5*(hgt[nz-1,:,:]-hgt[nz-2,:,:])*phi3d[nz-1,:,:]
	for ii in np.arange(nz-2)+1:
		phi2d = phi2d + 0.5*(hgt[ii+1,:,:]-hgt[ii-1,:,:])*phi3d[ii,:,:]

	return phi2d

def InterpZ( phi0, hgt0, hgt1 ):
	phi1 = np.zeros(hgt1.shape)
	for ii in np.arange(phi0.shape[1]):
		for jj in np.arange(phi0.shape[2]):
			phi1[:,ii,jj] = griddata(hgt0[:,ii,jj],phi0[:,ii,jj],hgt1[:,ii,jj],method='cubic')

	return phi1

def IntegrateZ(phi2d,hgt2d):
	nz    = phi2d.shape[0]
	phi1d = np.zeros(phi2d.shape[1])
	phi1d = phi1d + 0.5*(hgt2d[1,:]-hgt2d[0,:])*phi2d[0,:]
	phi1d = phi1d + 0.5*(hgt2d[nz-1,:]-hgt2d[nz-2,:])*phi2d[nz-1,:]
	for ii in np.arange(nz-2)+1:
		phi1d = phi1d + 0.5*(hgt2d[ii+1,:]-hgt2d[ii-1,:])*phi2d[ii,:]

	return phi1d

def GetIndices(lon,lat,xi,xf,yi,yf):
        xmin = 0
        xmax = 0
        ymin = 0
        ymax = 0

        for ii in np.arange(len(lon)):
                if lon[ii] > xi:
                        xmin = ii
                        break

        for ii in np.arange(len(lon)):
                if lon[ii] > xf:
                        xmax = ii
                        break

        for ii in np.arange(len(lat)):
                if lat[ii] > yi:
                        ymin = ii
                        break

        for ii in np.arange(len(lat)):
                if lat[ii] > yf:
                        ymax = ii
                        break

        return [xmin,xmax,ymin,ymax]

times = ['020200','020212','020300','020312','020400','020412','020500','020512','020600','020612','020700','020712','020800','020812','020900','020912','021000','021012','021100','021112']

path = '../../Darwin_15/020112/'
xi = +20
xf = -20
yi = +20
yf = -20
#lon = ReadFile(path+'lsm.nc','longitude')
#lat = ReadFile(path+'lsm.nc','latitude')
#[xi,xf,yi,yf] = GetIndices(lon,lat,126.0,129,-13.0,-10.0)

lsm = ReadFile(path+'lsm.nc','lsm')[0,0,yi:yf,xi:xf]
lon = ReadFile(path+'lsm.nc','longitude')[xi:xf]
lat = ReadFile(path+'lsm.nc','latitude')[yi:yf]
hgt = ReadFile(path+'ht_theta.nc','ht')[0,:,yi:yf,xi:xf]
htr = ReadFile(path+'ht_rho.nc','ht')[0,:,yi:yf,xi:xf]
nz  = hgt.shape[0]

print hgt.shape
print htr.shape
print 'xrange: ',lon[0],lon[-1]
print 'yrange: ',lat[0],lat[-1]

T      = np.arange(12*len(times))/24.0 + 2.0
dCWTdt = np.zeros(12*len(times))
E      = np.zeros(12*len(times))
P      = np.zeros(12*len(times))
adv_qt = np.zeros(12*len(times))
R      = np.zeros(12*len(times))

tsInv  = 1.0/15.0
dtInv  = 1.0/60.0/60.0
LvInv  = 1.0/(2.501e+6)
RdInv  = 1.0/287.05 # gas constant for dry air, J/kg/K

path = '../' + times[0] + '/'
cwt_wet_curr = ReadFile(path+'column_wet_mass.nc','unspecified')[:,0,yi:yf,xi:xf]
cwt_dry_curr = ReadFile(path+'column_dry_mass.nc','unspecified')[:,0,yi:yf,xi:xf]
cwt_qcl_curr = ReadFile(path+'column_qcl.nc','unspecified')[:,0,yi:yf,xi:xf]
cwt_qcf_curr = ReadFile(path+'column_qcf.nc','unspecified')[:,0,yi:yf,xi:xf]

theta = ReadFile(path+'theta_0.nc','theta')[:,:,yi:yf,xi:xf]
exner = ReadFile(path+'exner_0.nc','field7')[:,:,yi:yf,xi:xf]
press = ReadFile(path+'pressure_0.nc','p')[:,:,yi:yf,xi:xf]
# see eqn 4 (p. 11), large scale precipitation document
rho   = RdInv*press/theta/exner

print 'dimensions: ',lsm.shape
npts = lsm.shape[0]*lsm.shape[1]
scale = 60.0*60.0/npts

time = 0
for tt in times:
	path = '../' + tt + '/'
	pcp  = ReadFile(path+'lsrain_avg.nc','lsrain')[:,0,yi:yf,xi:xf]
	evp  = ReadFile(path+'latent_heat_flux.nc','lh')[:,0,yi:yf,xi:xf]
	hum  = ReadFile(path+'q_incr_0.nc','unspecified')[:,:,yi:yf,xi:xf]
	qcl  = ReadFile(path+'qcl_incr_0.nc','unspecified')[:,:,yi:yf,xi:xf]
	qcf  = ReadFile(path+'qcf_incr_0.nc','unspecified')[:,:,yi:yf,xi:xf]

	evp  = LvInv*evp

	cwt_wet_prev = cwt_wet_curr
	cwt_dry_prev = cwt_dry_curr
	cwt_qcl_prev = cwt_qcl_curr
	cwt_qcf_prev = cwt_qcf_curr
	cwt_wet_curr = ReadFile(path+'column_wet_mass.nc','unspecified')[:,0,yi:yf,xi:xf]
	cwt_dry_curr = ReadFile(path+'column_dry_mass.nc','unspecified')[:,0,yi:yf,xi:xf]
	cwt_qcl_curr = ReadFile(path+'column_qcl.nc','unspecified')[:,0,yi:yf,xi:xf]
	cwt_qcf_curr = ReadFile(path+'column_qcf.nc','unspecified')[:,0,yi:yf,xi:xf]
	
	if time == 0:
		hours = np.arange(11) + 1
	else:
		hours = np.arange(12)

	for hr in hours:
		if hr%3 == 0:
			hum = ReadFile(path+'q_incr_'+'%d'%hr+'.nc','unspecified')[:,:,yi:yf,xi:xf]	# vapour
			qcl = ReadFile(path+'qcl_incr_'+'%d'%hr+'.nc','unspecified')[:,:,yi:yf,xi:xf]	# liquid
			qcf = ReadFile(path+'qcf_incr_'+'%d'%hr+'.nc','unspecified')[:,:,yi:yf,xi:xf]	# ice

			theta = ReadFile(path+'theta_'+'%d'%hr+'.nc','theta')[:,:,yi:yf,xi:xf]
			exner = ReadFile(path+'exner_'+'%d'%hr+'.nc','field7')[:,:,yi:yf,xi:xf]
			press = ReadFile(path+'pressure_'+'%d'%hr+'.nc','p')[:,:,yi:yf,xi:xf]
			rho   = RdInv*press/theta/exner

		den = rho[hr%3,:,:,:]
		wat = hum[hr%3,:,:,:] + qcl[hr%3,:,:,:] + qcf[hr%3,:,:,:]
		adv = tsInv*VertInt(den*wat,hgt)

		if hr == 0:
			cwt_wet_0 = cwt_wet_prev[-1,:,:]
			cwt_dry_0 = cwt_dry_prev[-1,:,:]
			cwt_qcl_0 = cwt_qcl_prev[-1,:,:]
			cwt_qcf_0 = cwt_qcf_prev[-1,:,:]
		else:
			cwt_wet_0 = cwt_wet_curr[hr-1,:,:]
			cwt_dry_0 = cwt_dry_curr[hr-1,:,:]
			cwt_qcl_0 = cwt_qcl_curr[hr-1,:,:]
			cwt_qcf_0 = cwt_qcf_curr[hr-1,:,:]

		lhs = dtInv*(cwt_wet_curr[hr,:,:]-cwt_dry_curr[hr,:,:]) - dtInv*(cwt_wet_0[:,:]-cwt_dry_0[:,:])
		#lhs = lhs - dtInv*(cwt_qcl_curr[hr,:,:]-cwt_qcl_0[:,:])
		#lhs = lhs - dtInv*(cwt_qcf_curr[hr,:,:]-cwt_qcf_0[:,:])
		rhs = evp[hr,:,:] - pcp[hr,:,:] + adv
		res = lhs - rhs

		print 'hour: ', hr, hr%3
		print 'adv:  ', np.sum(adv)*scale
		print 'evp:  ', np.sum(evp[hr,:,:])*scale
		print 'pcp:  ', np.sum(pcp[hr,:,:])*scale
		print 'cwt:  ', np.sum(lhs)*scale
		print 'res:  ', np.sum(res)*scale
		print ''

		dCWTdt[time*12+hr] = np.sum(lhs)*scale
		E[time*12+hr]      = np.sum(evp[hr,:,:])*scale
		P[time*12+hr]      = np.sum(pcp[hr,:,:])*scale
		adv_qt[time*12+hr] = np.sum(adv)*scale
		R[time*12+hr]      = np.sum(res)*scale

	time = time + 1

np.savetxt('time.mb',T[1:])
np.savetxt('d_cwt_dt.mb',dCWTdt[1:])
np.savetxt('evap.mb',E[1:])
np.savetxt('precip.mb',P[1:])
np.savetxt('DqDt.mb',adv_qt[1:])
np.savetxt('resid.mb',R[1:])

fig = plt.figure()
ax = fig.add_subplot(111)
plt.plot(T[1:],dCWTdt[1:],'-')
plt.plot(T[1:],E[1:],'-')
plt.plot(T[1:],-1.0*P[1:],'-')
plt.plot(T[1:],adv_qt[1:],'-')
plt.plot(T[1:],R[1:],'-')
plt.plot(T[1:],np.zeros(len(T[1:])),'-')
plt.title('Moisture Budget')
plt.xlabel('Time (days)')
plt.ylabel('Moisture (mm/hr)')
plt.ylim([-1.0,+1.0])
plt.legend(('dCWT/dt','Evaporation','Precipitation','Flux','Residual'),loc=2,prop={'size':10})
pylab.savefig('moisture_budget_NoConv.png')

plt.show()
