#!/usr/bin/env python

# Generates a plot of accumulated rainfall accross the full domain and calculate rainfall amounts
# over land, sea and full domain

from scipy.interpolate import *
import matplotlib.pyplot as plt
import matplotlib.colors as cls
import numpy as np
import pylab
from Utils import *

times = ['020112','020200','020212','020300','020312','020400','020412','020500','020512','020600','020612','020700','020712','020800','020812','020900','020912']
using = [False,   True,    True,    False,   False,   True,    True,    True,    True,    False,   False,   True,    True,    False,   False,   True,    True    ]

xi = 354
xf = 390
yi = 305
yf = 357

filename = '../' + '020112' + '/Global' + '/lsm.nc'
lsm = ReadField(filename,'lsm')[0,0,:,:]
lon = ReadField(filename,'longitude')
lat = ReadField(filename,'latitude')

#precip_t = np.zeros((lsm.shape[2],lsm.shape[3]))
#dt = 1.0*60*60
#Re = 6378100.0
#dx = 2.0*np.pi*Re*(lon[-1]-lon[0])/(len(lon)-1)/360.0
#dx = dx*np.ones(len(lon))
#dy = np.zeros(len(lat))
#for ii in np.arange(len(dy))-1:
#	dy[ii] = 2.0*np.pi*Re*(lat[ii+1]-lat[ii])/360.0
#dy[-1] = dy[-2]
#area = np.outer(dy,dx)

precip_t = np.zeros((len(lat),len(lon)))
for tt in np.arange(len(times)-1):
	prevfile = '../' + times[tt]   + '/Global' + '/rain_after_step.nc'
	currfile = '../' + times[tt+1] + '/Global' + '/rain_after_step.nc'
	precip_p = ReadField(prevfile,'field1681')[0,0,:,:]
	precip_c = ReadField(currfile,'field1681')[0,0,:,:]
	precip   = (precip_c - precip_p)/12.0
	if using[tt+1]:
		precip_t = precip_t + 12.0*precip

precip_t = 3600.0*1000.0*precip_t

filename = '../020112/lsm.nc'
lsm0 = ReadField(filename,'lsm')[0,0,:,:]
lon0 = ReadField(filename,'longitude')
lat0 = ReadField(filename,'latitude')

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon0,lat0,lsm0,colors='k')
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
levs=[0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
#levs=[0.,2.,5.,10.,20.,40.,80.,120.,160.,200.]
cmap=cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm=cls.BoundaryNorm(levs,cmap.N)
co = ax.contourf(lon[xi:xf],lat[yi:yf],precip_t[yi:yf,xi:xf],levels=levs,cmap=cmap,norm=norm)
#co = ax.contourf(lon[xi:xf],lat[yi:yf],precip_t[yi:yf,xi:xf],cmap=cmap)
plt.title('Accumulated rainfall for 120 hours (mm) - (Global)')
plt.colorbar(co,orientation='vertical')
#pylab.savefig('accum_rain_120_dt15_4p00.png')

# interpolate radar mask and plot that space only
#dir_o     = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km/'
#file_o    = 'cpol_2d_rainrate_2010'
#pcp_o     = ReadField(dir_o+file_o+'0202_1200.nc','rr')
#pcp_o     = pcp_o[0,:,:]
#empty     = pcp_o[:,:]<-10
#full      = (empty+1)%2
#lon0_o    = ReadScalar(dir_o+file_o+'0201_0000.nc','radar_longitude')
#lat0_o    = ReadScalar(dir_o+file_o+'0201_0000.nc','radar_latitude')
#x         = ReadField(dir_o+file_o+'0201_0000.nc','x')
#y         = ReadField(dir_o+file_o+'0201_0000.nc','y')
#x         = 1000.0*x
#y         = 1000.0*y
#Re        = 6378100.0
#lon_o     = lon0_o + x*360.0/2.0/np.pi/Re
#lat_o     = lat0_o + y*360.0/2.0/np.pi/Re
#pts0      = np.transpose([np.tile(lon_o,len(lat_o)),np.repeat(lat_o,len(lon_o))])
#pts1      = np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
#full_r    = np.ravel(full)
#full_m    = griddata(pts0,full_r,pts1,method='nearest')
#full_m    = full_m.reshape(len(lat),len(lon))
#precip_m  = full_m*precip_t

#fig = plt.figure()
#ax = fig.add_subplot(111)
#ax.contour(lon[400:-369],lat[400:-369],lsm[400:-369,400:-369],colors='k')	# 1.33 km
#co = ax.contourf(lon[:],lat[200:360],precip[200:360,:],levels=levs,cmap=cmap,norm=norm)
#plt.title('Accumulated rainfall for 120 hours (mm) - (UM)')
#plt.colorbar(co,orientation='horizontal')
#pylab.savefig('accum_rain_120_darwin_dt15_4p00.png')
plt.show()
