#!/usr/bin/env python

import numpy as np
from scipy.io import netcdf
from scipy.interpolate import *
import matplotlib.pyplot as plt
import pylab

def ReadFile(filename,fieldname):
        print 'reading file: ', filename
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
        return field[:]

def VertInt(phi3d,hgt):
	nz    = phi3d.shape[0]
	phi2d = np.zeros((phi3d.shape[1],phi3d.shape[2]))
	phi2d = phi2d + 0.5*(hgt[1,:,:]-hgt[0,:,:])*phi3d[0,:,:]
	phi2d = phi2d + 0.5*(hgt[nz-1,:,:]-hgt[nz-2,:,:])*phi3d[nz-1,:,:]
	for ii in np.arange(nz-2)+1:
		phi2d = phi2d + 0.5*(hgt[ii+1,:,:]-hgt[ii-1,:,:])*phi3d[ii,:,:]

	return phi2d

def InterpZ( phi0, hgt0, hgt1 ):
	phi1 = np.zeros(hgt1.shape)
	for ii in np.arange(phi0.shape[1]):
		for jj in np.arange(phi0.shape[2]):
			phi1[:,ii,jj] = griddata(hgt0[:,ii,jj],phi0[:,ii,jj],hgt1[:,ii,jj],method='cubic')

	return phi1

def IntegrateZ(phi2d,hgt2d):
	nz    = phi2d.shape[0]
	phi1d = np.zeros(phi2d.shape[1])
	phi1d = phi1d + 0.5*(hgt2d[1,:]-hgt2d[0,:])*phi2d[0,:]
	phi1d = phi1d + 0.5*(hgt2d[nz-1,:]-hgt2d[nz-2,:])*phi2d[nz-1,:]
	for ii in np.arange(nz-2)+1:
		phi1d = phi1d + 0.5*(hgt2d[ii+1,:]-hgt2d[ii-1,:])*phi2d[ii,:]

	return phi1d

def CalcFlux( xi, xf, yi, yf, rho, hum, ux, uy, htt, htr ):
	dx    = 4.0e+6

	print 'calculating flux...'

	htt2 = htt[:-1,:,:]
	rho2 = rho[:-1,:,:]
	hum2 = hum[:-1,:,:]

	uxl = np.zeros((htt2.shape[0],htt2.shape[1]))
	uxr = np.zeros((htt2.shape[0],htt2.shape[1]))
	for ii in np.arange(htt.shape[1]):
		uxl[:,ii] = griddata(htr[:,ii,xi],ux[:,ii,xi],htt2[:,ii,xi],method='cubic')
		uxr[:,ii] = griddata(htr[:,ii,xf],ux[:,ii,xf],htt2[:,ii,xf],method='cubic')

	uyb = np.zeros((htt2.shape[0],htt2.shape[2]))
	uyt = np.zeros((htt2.shape[0],htt2.shape[2]))
	for ii in np.arange(htt.shape[1]):
		uyb[:,ii] = griddata(htr[:,yi,ii],0.5*(uy[:,yi,ii]+uy[:,yi+1,ii]),htt2[:,yi,ii],method='cubic')
		uyt[:,ii] = griddata(htr[:,yf,ii],0.5*(uy[:,yf,ii]+uy[:,yf+1,ii]),htt2[:,yf,ii],method='cubic')

	qul = +1.0*IntegrateZ(uxl*rho2[:,:,xi]*hum2[:,:,xi],htt2[:,:,xi])
	qur = -1.0*IntegrateZ(uxr*rho2[:,:,xf]*hum2[:,:,xf],htt2[:,:,xf])
	qub = +1.0*IntegrateZ(uyb*rho2[:,yi,:]*hum2[:,yi,:],htt2[:,yi,:])
	qut = -1.0*IntegrateZ(uyt*rho2[:,yf,:]*hum2[:,yf,:],htt2[:,yf,:])

	return (qul + qur + qub + qut)/dx

times = ['020200','020212','020300','020312','020400','020412','020500','020512','020600','020612','020700','020712','020800','020812','020900','020912']

subdir = '/4km/'
path = '../020112'+subdir
xi = +139
xf = -139
yi = +139
yf = -139
#xi = +20
#xf = -20
#yi = +20
#yf = -20

lsm = ReadFile(path+'lsm.nc','lsm')[0,0,yi:yf,xi:xf]
lon = ReadFile(path+'lsm.nc','longitude')[xi:xf]
lat = ReadFile(path+'lsm.nc','latitude')[yi:yf]
hgt = ReadFile(path+'ht_theta.nc','ht')[0,:,yi:yf,xi:xf]
htr = ReadFile(path+'ht_rho.nc','ht')[0,:,yi:yf,xi:xf]
nz  = hgt.shape[0]

print hgt.shape
print htr.shape
print 'xrange: ',lon[0],lon[-1]
print 'yrange: ',lat[0],lat[-1]

T      = np.arange(12*len(times))/24.0
dCWTdt = np.zeros(12*len(times))
E      = np.zeros(12*len(times))
P      = np.zeros(12*len(times))
adv_qt = np.zeros(12*len(times))
R      = np.zeros(12*len(times))
F      = np.zeros(12*len(times))

tsInv  = 1.0/15.0
dtInv  = 1.0/60.0/60.0
LvInv  = 1.0/(2.501e+6)
RdInv  = 1.0/287.05 # gas constant for dry air, J/kg/K

path = '../' + times[0] + subdir
cwt_wet_curr = ReadFile(path+'total_column_wet_mass_unspecified.nc','unspecified')[:,0,yi:yf,xi:xf]
cwt_dry_curr = ReadFile(path+'total_column_dry_mass_unspecified.nc','unspecified')[:,0,yi:yf,xi:xf]
cwt_qcl_curr = ReadFile(path+'total_column_qcl_unspecified.nc','unspecified')[:,0,yi:yf,xi:xf]
cwt_qcf_curr = ReadFile(path+'total_column_qcf_unspecified.nc','unspecified')[:,0,yi:yf,xi:xf]

res = np.zeros((lsm.shape))
lhs = np.zeros((lsm.shape))
rhs = np.zeros((lsm.shape))
evp = np.zeros((lsm.shape))
pcp = np.zeros((lsm.shape))

theta = ReadFile(path+'theta_0.nc','theta')[:,:,yi:yf,xi:xf]
exner = ReadFile(path+'exner_pressure_field7_0.nc','field7')[:,:,yi:yf,xi:xf]
press = ReadFile(path+'pressure_p_0.nc','p')[:,:,yi:yf,xi:xf]
# see eqn 4 (p. 11), large scale precipitation document
rho   = RdInv*press/theta/exner

DqvDt = np.zeros(12*len(times))
DqlDt = np.zeros(12*len(times))

time = 0
for tt in times:
	path = '../' + tt + subdir
	pcp_i = ReadFile(path+'precip_inst.nc','precip')[:,0,yi:yf,xi:xf]
	evp_i = ReadFile(path+'surface_latent_heat_flux_lh.nc','lh')[:,0,yi:yf,xi:xf]
	#evp_s = ReadFile(path+'evap_from_open_sea.nc','evap')[:,0,yi:yf,xi:xf]
	#evp_l = ReadFile(path+'evap_from_soil_surf_field1526.nc','field1526')[:,0,yi:yf,xi:xf]
	#evp_c = ReadFile(path+'evap_from_canopy_field1527.nc','field1527')[:,0,yi:yf,xi:xf]
	hum   = ReadFile(path+'q_incr_unspecified_0.nc','unspecified')[:,:,yi:yf,xi:xf]
	qcl   = ReadFile(path+'qcl_incr_unspecified_0.nc','unspecified')[:,:,yi:yf,xi:xf]
	qcf   = ReadFile(path+'qcf_incr_unspecified_0.nc','unspecified')[:,:,yi:yf,xi:xf]

	evp_i = LvInv*evp_i
	#mask  = evp_l < 1.0e+4
	#evp_l = mask*evp_l
	#mask  = evp_c < 1.0e+4
	#evp_c = mask*evp_c
	#evp_i = evp_s + evp_l + evp_c

	cwt_wet_prev = cwt_wet_curr
	cwt_dry_prev = cwt_dry_curr
	cwt_qcl_prev = cwt_qcl_curr
	cwt_qcf_prev = cwt_qcf_curr
	cwt_wet_curr = ReadFile(path+'total_column_wet_mass_unspecified.nc','unspecified')[:,0,yi:yf,xi:xf]
	cwt_dry_curr = ReadFile(path+'total_column_dry_mass_unspecified.nc','unspecified')[:,0,yi:yf,xi:xf]
	cwt_qcl_curr = ReadFile(path+'total_column_qcl_unspecified.nc','unspecified')[:,0,yi:yf,xi:xf]
	cwt_qcf_curr = ReadFile(path+'total_column_qcf_unspecified.nc','unspecified')[:,0,yi:yf,xi:xf]
	
	if time == 0:
		hours = np.arange(11) + 1
	else:
		hours = np.arange(12)

	#print '######## loading velocities ########'
	#if time < len(times) - 1:
	#	path2 = '../'+times[time+1]+subdir
	#	u0    = ReadFile(path +'vel_u.nc','u')[0,:nz,yi:yf,xi:xf]
	#	v0    = ReadFile(path +'vel_v.nc','v')[0,:nz,yi:yf,xi:xf]
	#	u1    = ReadFile(path2+'vel_u.nc','u')[0,:nz,yi:yf,xi:xf]
	#	v1    = ReadFile(path2+'vel_v.nc','v')[0,:nz,yi:yf,xi:xf]
	#	q0    = ReadFile(path +'specific_humidity_q.nc','q')[0,:nz,yi:yf,xi:xf]
	#	q1    = ReadFile(path2+'specific_humidity_q.nc','q')[0,:nz,yi:yf,xi:xf]
	#	print u0.shape
	#	print v0.shape
	#else:
	#	path0 = '../'+times[time-1]+subdir
	#	u0    = ReadFile(path0+'vel_u.nc','u')[0,:nz,yi:yf,xi:xf]
	#	v0    = ReadFile(path0+'vel_v.nc','v')[0,:nz,yi:yf,xi:xf]
	#	u1    = ReadFile(path +'vel_u.nc','u')[0,:nz,yi:yf,xi:xf]
	#	v1    = ReadFile(path +'vel_v.nc','v')[0,:nz,yi:yf,xi:xf]
	#	q0    = ReadFile(path0+'specific_humidity_q.nc','q')[0,:nz,yi:yf,xi:xf]
	#	q1    = ReadFile(path +'specific_humidity_q.nc','q')[0,:nz,yi:yf,xi:xf]
	#print ' '

	for hr in hours:
		if hr%3 == 0:
			hum = ReadFile(path+'q_incr_unspecified_'+'%d'%hr+'.nc','unspecified')[:,:,yi:yf,xi:xf]		# vapour
			qcl = ReadFile(path+'qcl_incr_unspecified_'+'%d'%hr+'.nc','unspecified')[:,:,yi:yf,xi:xf]	# liquid
			qcf = ReadFile(path+'qcf_incr_unspecified_'+'%d'%hr+'.nc','unspecified')[:,:,yi:yf,xi:xf]	# ice

			theta = ReadFile(path+'theta_'+'%d'%hr+'.nc','theta')[:,:,yi:yf,xi:xf]
			exner = ReadFile(path+'exner_pressure_field7_'+'%d'%hr+'.nc','field7')[:,:,yi:yf,xi:xf]
			press = ReadFile(path+'pressure_p_'+'%d'%hr+'.nc','p')[:,:,yi:yf,xi:xf]
			rho   = RdInv*press/theta/exner

		den   = rho[hr%3,:,:,:]
		adv_v = VertInt(den*hum[hr%3,:,:,:],hgt)
		adv_l = VertInt(den*qcl[hr%3,:,:,:],hgt)
		adv_i = VertInt(den*qcf[hr%3,:,:,:],hgt)
		adv   = tsInv*(adv_v + adv_l + adv_i)

		if hr == 0:
			cwt_wet_0 = cwt_wet_prev[-1,:,:]
			cwt_dry_0 = cwt_dry_prev[-1,:,:]
			cwt_qcl_0 = cwt_qcl_prev[-1,:,:]
			cwt_qcf_0 = cwt_qcf_prev[-1,:,:]
		else:
			cwt_wet_0 = cwt_wet_curr[hr-1,:,:]
			cwt_dry_0 = cwt_dry_curr[hr-1,:,:]
			cwt_qcl_0 = cwt_qcl_curr[hr-1,:,:]
			cwt_qcf_0 = cwt_qcf_curr[hr-1,:,:]

		lhs_i = dtInv*(cwt_wet_curr[hr,:,:]-cwt_dry_curr[hr,:,:]) - dtInv*(cwt_wet_0[:,:]-cwt_dry_0[:,:])
		lhs_i = lhs_i - dtInv*(cwt_qcl_curr[hr,:,:]-cwt_qcl_0[:,:])
		lhs_i = lhs_i - dtInv*(cwt_qcf_curr[hr,:,:]-cwt_qcf_0[:,:])
		rhs_i = evp_i[hr,:,:] - pcp_i[hr,:,:] + adv
		res_i = lhs_i - rhs_i
		res   = res + res_i
		lhs   = lhs + lhs_i
		rhs   = rhs + rhs_i
		evp   = evp + evp_i[hr,:,:]
		pcp   = pcp + pcp_i[hr,:,:]

		#ux    = ((12.0-hr)*u0 + hr*u1)/12.0
		#uy    = ((12.0-hr)*v0 + hr*v1)/12.0
		#qi    = ((12.0-hr)*q0 + hr*q1)/12.0
		#flx   = CalcFlux(xi,xf,yi,yf,den,qi,ux,uy,hgt,htr)

		print 'hour: ', hr, hr%3
		print 'adv:  ', np.sum(adv)
		print 'evp:  ', np.sum(evp_i[hr,:,:])
		print 'pcp:  ', np.sum(pcp_i[hr,:,:])
		print 'cwt:  ', np.sum(lhs_i)
		print 'res:  ', np.sum(res_i)
		#print 'flx:  ', np.sum(flx)
		print ''

		dCWTdt[time*12+hr] = 1.0*np.sum(lhs_i)
		E[time*12+hr]      = 1.0*np.sum(evp_i[hr,:,:])
		P[time*12+hr]      = 1.0*np.sum(pcp_i[hr,:,:])
		adv_qt[time*12+hr] = 1.0*np.sum(adv)
		R[time*12+hr]      = 1.0*np.sum(res_i)
		#F[time*12+hr]      = np.sum(flx)
		DqvDt[time*12+hr]  = 1.0*np.sum(adv_v)
		DqlDt[time*12+hr]  = 1.0*np.sum(adv_l)

	time = time + 1

R2   = R[1:]
T2   = E[1:]-P[1:]+adv_qt[1:]
err  = np.sqrt(R2*R2)/np.sqrt(T2*T2)/len(R2)
print 'error: ', err

norm = res*res/(rhs*rhs + 0.0)
norm = np.sqrt(norm)/12/len(times)

np.savetxt('time.mb',T[1:])
np.savetxt('d_cwt_dt.mb',dCWTdt[1:])
np.savetxt('evap.mb',E[1:])
np.savetxt('precip.mb',P[1:])
np.savetxt('DqDt.mb',adv_qt[1:])
np.savetxt('resid.mb',R[1:])

fig = plt.figure()
ax = fig.add_subplot(111)
plt.plot(T[1:],dCWTdt[1:],'-')
plt.plot(T[1:],E[1:],'-')
plt.plot(T[1:],-1.0*P[1:],'-')
plt.plot(T[1:],adv_qt[1:],'-')
plt.plot(T[1:],R[1:],'-')
plt.title('Moisture Budget')
plt.xlabel('Time (days)')
plt.ylabel('Moisture (mm/s)')
plt.ylim([-400,+400])
plt.legend(('dCWT/dt','Evaporation','Precipitation','Dq/Dt','Residual'),loc=0)
pylab.savefig('moisture_budget_4p00.png')

# sanity check...
dPdT = np.zeros(len(T)-2)
for ii in np.arange(len(dPdT)-2)+1:
	dPdT[ii-1] = 0.5*(P[ii+1]-P[ii-1])/60.0/60.0

fig = plt.figure()
ax = fig.add_subplot(111)
plt.plot(T[1:-1],dPdT,'-')
plt.plot(T[1:-1],DqvDt,'-')
plt.plot(T[1:-1],DqlDt,'-')
plt.xlabel('Time (days)')
ply.ylabel('Moisture/time (mm/s)')
plt.legend(('dP/dT','Dqv/Dt','DqlDt'),loc=0)

plt.show()
