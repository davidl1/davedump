#!/usr/bin/env python

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
from matplotlib import colors as cls
from matplotlib import ticker
import numpy as np
import pylab

hour = 7

hti = +1.0*np.loadtxt('ht_hour'+'%d'%(hour-1)+'_cloud.energybudget')
kei = +1.0*np.loadtxt('ke_hour'+'%d'%(hour-1)+'_cloud.energybudget')
pei = +1.0*np.loadtxt('pe_hour'+'%d'%(hour-1)+'_cloud.energybudget')
iei = +1.0*np.loadtxt('ie_hour'+'%d'%(hour-1)+'_cloud.energybudget')
mei = -1.0*np.loadtxt('me_hour'+'%d'%(hour-1)+'_cloud.energybudget')

htf = +1.0*np.loadtxt('ht_hour'+'%d'%hour+'_cloud.energybudget')
kef = +1.0*np.loadtxt('ke_hour'+'%d'%hour+'_cloud.energybudget')
pef = +1.0*np.loadtxt('pe_hour'+'%d'%hour+'_cloud.energybudget')
ief = +1.0*np.loadtxt('ie_hour'+'%d'%hour+'_cloud.energybudget')
mef = -1.0*np.loadtxt('me_hour'+'%d'%hour+'_cloud.energybudget')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot((kef-kei)/60.0/60.0,htf,'-')
plt.plot((pef-pei)/60.0/60.0,htf,'-')
plt.plot((ief-iei)/60.0/60.0,htf,'-')
plt.plot((mef-mei)/60.0/60.0,htf,'-')
plt.xlabel('W')
plt.ylabel('height (m)')
plt.xlim([-10.0,+10.0])
plt.ylim([0,30000])
plt.legend(('kinetic','potential','internal','moist'),loc=1,prop={'size':14})
plt.title('Power vertical profile (Blended), '+'%d'%hour+'pm')
pylab.savefig('power_blend_hour'+'%d'%hour+'_cloud.png')

plt.show()
