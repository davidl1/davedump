#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import pylab

e=np.loadtxt('kenergy_1d_fourier_full.txt')
k=np.loadtxt('wavenum_1d_fourier_full.txt')
n=len(e)
print n

nh=n/2
index=np.arange(n)
index[nh+1:]=np.arange(nh-1)-nh+1

e2=np.zeros(nh+1)
k2=k[:nh+1]
for ii in np.arange(n):
       ki=np.abs(index[ii])
       e2[ki]=e2[ki]+e[ii]


n4=n/4
n8=n/8
ln8dx=np.log10(k2[n8])
ln4dx=np.log10(k2[n4])
ln2dx=np.log10(k2[nh])
print 'k: ',k2[n8],'\tln(1/48x): ',ln8dx
print 'k: ',2.0*np.pi/8.0/1327.873,'\tln(1/8dx): ',np.log10(2.0*np.pi/8.0/1327.873)
print 'k: ',k2[n4],'\tln(1/4dx): ',ln4dx
print 'k: ',2.0*np.pi/4.0/1327.873,'\tln(1/4dx): ',np.log10(2.0*np.pi/4.0/1327.873)
print 'k: ',k2[nh],'\tln(1/2dx): ',ln2dx
print 'k: ',2.0*np.pi/2.0/1327.873,'\tln(1/2dx): ',np.log10(2.0*np.pi/2.0/1327.873)

# plot a least squares fit to the data
lk = np.log10(k[1:len(k2)-1])
le = np.log10(e[1:len(e2)-1])

#inds1 = np.arange(n/4-4)+4
inds1 = np.arange(n8-1-4)+4
A = np.vstack([lk[inds1],np.ones(len(lk[inds1]))]).T
m1,c1 = np.linalg.lstsq(A,le[inds1])[0]
print m1,c1

#inds2 = np.arange(3*n/4-6)+n/4
inds2 = np.arange(nh-n8-2)+n8-2
A = np.vstack([lk[inds2],np.ones(len(lk[inds2]))]).T
m2,c2 = np.linalg.lstsq(A,le[inds2])[0]
print m2,c2

plt.loglog(k2,e2,'.-')
#plt.loglog(k2[inds1],+1.0e+3*c1*np.power(k2[inds1],m1))
#plt.loglog(k2[inds2],+1.4e-1*c1*np.power(k2[inds2],m2))
plt.loglog(k2[inds1],+1.0e+3*c1*np.power(k2[inds1],m1))
plt.loglog(k2[inds2],+6.0e-3*c1*np.power(k2[inds2],m2))
plt.loglog(k2[1:nh],1.0e+2*np.power(k2[1:nh],-5.0/3.0))
#plt.loglog([k2[n4],k2[n4]],[1.0e+6,1.0e+12])
plt.loglog([k2[n8],k2[n8]],[1.0e+6,1.0e+12])
plt.loglog([k2[nh],k2[nh]],[1.0e+6,1.0e+12])
plt.xlabel('$ln(K) (m^{-1})$')
plt.ylabel('$ln(E) (m^3/s^2$')
plt.title('Kinetic energy spectra, UM')
#plt.legend(('UM spectra','$k<2\pi/4\Delta x, \mathrm{d}ln(E)/\mathrm{d}ln(K)=$'+str(m1)[:6],\
#'$k>2\pi/4\Delta x, \mathrm{d}\ln(E)/\mathrm{d}ln(K)=$'+str(m2)[:6],'-5/3 cascade'),loc=3)
plt.legend(('UM spectra','$k<2\pi/8\Delta x, \mathrm{d}ln(E)/\mathrm{d}ln(K)=$'+str(m1)[:6],\
'$k>2\pi/8\Delta x, \mathrm{d}\ln(E)/\mathrm{d}ln(K)=$'+str(m2)[:6],'-5/3 cascade'),loc=3)
pylab.savefig('ke_spectra_darwin_um.png')
plt.show()

