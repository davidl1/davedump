#!/usr/bin/env python

# Generate a series of instantaneous plots of vertically integrated cloud concentration, 
# and zonal and meridional cross sections, centered on the Tiwi islands

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
from matplotlib import colors as cls
import numpy as np
import pylab
import os
import sys

def ReadFile(filename,fieldname):
	#print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

def VertInt(phi3d,hgt):
	nz    = phi3d.shape[0]
	phi2d = np.zeros((phi3d.shape[1],phi3d.shape[2]))
	phi2d = phi2d + 0.5*(hgt[1,:,:]-hgt[0,:,:])*phi3d[0,:,:]
	phi2d = phi2d + 0.5*(hgt[nz-1,:,:]-hgt[nz-2,:,:])*phi3d[nz-1,:,:]
	for ii in np.arange(nz-2)+1:
		phi2d = phi2d + 0.5*(hgt[ii+1,:,:]-hgt[ii-1,:,:])*phi3d[ii,:,:]

	return phi2d

path     = './020200/'
lsm      = ReadFile(path+'lsm.nc','lsm')[0,0,:,:]
lon      = ReadFile(path+'lsm.nc','longitude')
lat      = ReadFile(path+'lsm.nc','latitude')
ht_rho   = ReadFile(path+'ht_rho.nc','ht')[0,:,:,:]
ht_theta = ReadFile(path+'ht_theta.nc','ht')[0,:,:,:]
orog     = ReadFile(path+'orog.nc','ht')[0,0,:,:]

slm  = (lsm+1)%2
nx   = len(lon)
ny   = len(lat)

height   = ht_theta[-1,:,:] - orog

plots = []

path  = './'
days  = ['020200','020212','020300','020312','020400','020412','020500','020512','020600','020612','020700','020712','020800','020812','020900','020912','021000','021012','021100','021112']

index = 0

xi = 498
yi = 576
ht = ht_theta[:,200,0]
#print 'lon,lat:	',lon[xi],lat[yi]
#print 'zo: 	',ht
hr = 0

for day in days:
	for hour in np.arange(12):
		if hour%3 == 0:
			ql = ReadFile(path+day+'/qcl_'+'%d'%hour+'.nc','QCL')[hour%3,:,:,:]
			qi = ReadFile(path+day+'/qcf_'+'%d'%hour+'.nc','QCF')[hour%3,:,:,:]
			qr = ReadFile(path+day+'/rain_'+'%d'%hour+'.nc','field1681')[hour%3,:,:,:]
			qg = ReadFile(path+day+'/graup_'+'%d'%hour+'.nc','field1921')[hour%3,:,:,:]

			ux = ReadFile(path+day+'/velx_'+'%d'%hour+'.nc','x-wind')[hour%3,:,:,:]
			uy = ReadFile(path+day+'/vely_'+'%d'%hour+'.nc','y-wind')[hour%3,:,:,:]
			th = ReadFile(path+day+'/theta_'+'%d'%hour+'.nc','theta')[hour%3,:,:,:]

		qz = VertInt(ql+qi+qr+qg,ht_theta)
		qz = 1000.0*qz/height

		fig = plt.figure()
		ax  = fig.add_subplot(111)
		ax.contour(lon,lat,lsm,colors='k')
		levs = [0.0,0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5,7.0,7.5,8.0]
		co = ax.contourf(lon,lat,qz,levels=levs)
		plt.title('Vertically averaged cloud concentration, '+day[2:4]+'%2d'%hr+' (g/kg)')
		plt.colorbar(co,orientation='vertical')
		filename = 'plots/cloud_'+'%.3d'%index+'.png'
		pylab.savefig(filename)
		#plt.close(fig)

		fig = plt.figure()
		ax  = fig.add_subplot(111)
		levs = [-16.0,-14.0,-12.0,-10.0,-8.0,-6.0,-4.0,-2.0,0.0,+2.0,+4.0,+6.0,+8.0,+10.0,+12.0,+14.0,+16.0]
		co = ax.contourf(lon,ht,uy[:,yi,:],levels=levs)
		ax.contour(lon,ht,qi[:,yi,:]+qg[:,yi,:],levels=[5.0e-4])
		ax.contour(lon,ht,ql[:,yi,:],levels=[5.0e-4])
		ax.contour(lon,ht,qr[:,yi,:],levels=[5.0e-4])
		#th_levs = np.linspace(400.0,1200.0,9,endpoint=True)
		th_levs = [325.0,350.0,375.0,400.0,500.0,600.0,700.0,800.0,900.0,1000.0,1100.0,1200.0]
		ci = ax.contour(lon,ht,th[:,yi,:],th_levs,colors='k')
		plt.clabel(ci,inline=1,fontsize=8)
		plt.title('Zonal cross section with y-winds, clouds and $\Theta$ contours, '+day[2:4]+'%2d'%hr)
		plt.colorbar(co,orientation='horizontal')
		filename = 'plots/zonal_'+'%.3d'%index+'.png'
		pylab.savefig(filename)
		#plt.close(fig)

		fig = plt.figure()
		ax  = fig.add_subplot(111)
		levs = [-48.0,-42.0,-36.0,-30.0,-24.0,-18.0,-12.0,-6.0,0.0,+6.0,+12.0,+18.0,+24.0,+30.0,+36.0,+42.0,+48.0]
		co = ax.contourf(lat,ht,ux[:,:,xi],levels=levs)
		ax.contour(lat,ht,qi[:,:,xi]+qg[:,:,xi],levels=[5.0e-4])
		ax.contour(lat,ht,ql[:,:,xi],levels=[5.0e-4])
		ax.contour(lat,ht,qr[:,:,xi],levels=[5.0e-4])
		#th_levs = np.linspace(400.0,1200.0,9,endpoint=True)
		th_levs = [325.0,350.0,375.0,400.0,500.0,600.0,700.0,800.0,900.0,1000.0,1100.0,1200.0]
		ci = ax.contour(lat,ht,th[:,:,xi],th_levs,colors='k')
		plt.clabel(ci,inline=1,fontsize=8)
		plt.title('Meridional cross section with x-winds, clouds and $\Theta$ contours, '+day[2:4]+'%2d'%hr)
		plt.colorbar(co,orientation='horizontal')
		filename = 'plots/meridional_'+'%.3d'%index+'.png'
		pylab.savefig(filename)
		#plt.close(fig)

		#plots.append(filename)
		index = index + 1
		hr    = hr + 1
		hr    = hr%24

#os.system("mencoder 'mf://plots/*.png' -mf type=png:fps=10 -ovc lavc -lavcopts vcodec=wmv2 -oac copy -o clouds.mpg")
#plt.show()

