#!/usr/bin/env python

import numpy as np
from Utils import *

# Darwin errors from CPOL comparison
lon_cpol  = np.loadtxt('lon_cpol.txt')
lat_cpol  = np.loadtxt('lat_cpol.txt')
full_cpol = np.loadtxt('pcp_cpol_full.txt')
land_cpol = np.loadtxt('pcp_cpol_land.txt')
sea_cpol  = np.loadtxt('pcp_cpol_sea.txt')

lon_um    = np.loadtxt('lon_um_darwin.txt')
lat_um    = np.loadtxt('lat_um_darwin.txt')
full_um   = np.loadtxt('pcp_um_darwin_full.txt')
land_um   = np.loadtxt('pcp_um_darwin_land.txt')
sea_um    = np.loadtxt('pcp_um_darwin_sea.txt')

print '#### Errors - CPOL ####'
print 'total: ',CalcError(lon_um,lat_um,full_um,lon_cpol,lat_cpol,full_cpol)
print 'land:  ',CalcError(lon_um,lat_um,land_um,lon_cpol,lat_cpol,land_cpol)
print 'sea:   ',CalcError(lon_um,lat_um,sea_um ,lon_cpol,lat_cpol,sea_cpol )

# Region errors from TRMM comparison
lon_trmm  = np.loadtxt('lon_trmm.txt')
lat_trmm  = np.loadtxt('lat_trmm.txt')
full_trmm = np.loadtxt('pcp_trmm_full.txt')
land_trmm = np.loadtxt('pcp_trmm_land.txt')
sea_trmm  = np.loadtxt('pcp_trmm_sea.txt')

lon_um    = np.loadtxt('lon_um_region.txt')
lat_um    = np.loadtxt('lat_um_region.txt')
full_um   = np.loadtxt('pcp_um_region_full.txt')
land_um   = np.loadtxt('pcp_um_region_land.txt')
sea_um    = np.loadtxt('pcp_um_region_sea.txt')

print '#### Errors - TRMM ####'
print 'total: ',CalcError(lon_um,lat_um,full_um,lon_trmm,lat_trmm,full_trmm)
print 'land:  ',CalcError(lon_um,lat_um,land_um,lon_trmm,lat_trmm,land_trmm)
print 'sea:   ',CalcError(lon_um,lat_um,sea_um ,lon_trmm,lat_trmm,sea_trmm )

# Darwin errors for WRF
lon_wrf   = np.loadtxt('lon_wrf_darwin.txt')
lat_wrf   = np.loadtxt('lat_wrf_darwin.txt')
full_wrf  = np.loadtxt('pcp_wrf_darwin_full.txt')
land_wrf  = np.loadtxt('pcp_wrf_darwin_land.txt')
sea_wrf   = np.loadtxt('pcp_wrf_darwin_sea.txt')

print '##### Errors - WRF #####'
print 'total: ',CalcError(lon_wrf,lat_wrf,full_wrf,lon_cpol,lat_cpol,full_cpol)
print 'land:  ',CalcError(lon_wrf,lat_wrf,land_wrf,lon_cpol,lat_cpol,land_cpol)
print 'sea:   ',CalcError(lon_wrf,lat_wrf,sea_wrf ,lon_cpol,lat_cpol,sea_cpol )
