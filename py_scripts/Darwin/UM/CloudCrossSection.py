#!/usr/bin/env python

# Generate a cross section in the x-z plane of hourly averaged cloud liquid and ice concentration
# accumulated over hours 15:00-18:00 inclusive (local time, UTZ+9) for day 7 (Feb. 7th 2010)

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
from matplotlib import colors as cls
import numpy as np
import pylab

def ReadFile(filename,fieldname):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

def InterpGrid(xin,yin,xout,yout,A,interpType):
	print 'interp type: ', interpType
	pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
	pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
	Ar      = np.ravel(A)
	Aor     = griddata(pts_in,Ar,pts_out,method=interpType)
	Ao      = Aor.reshape(len(yout),len(xout))
	return Ao

def GenMask(lon,lat,x,y,r,prcp,pmin):
        mask = np.zeros((len(lat),len(lon)))
        pnts = np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
        pnts[:,0] = pnts[:,0] - x
        pnts[:,1] = pnts[:,1] - y
        radi = np.sqrt(pnts[:,0]*pnts[:,0]+pnts[:,1]*pnts[:,1])
        radi = radi.reshape(len(lat),len(lon))
        mask = np.zeros(radi.shape)
        for ii in np.arange(radi.shape[0]):
                for jj in np.arange(radi.shape[1]):
                        if radi[ii,jj] < r and prcp[ii,jj] > pmin:
                                mask[ii,jj] = 1

	return mask

def GenMaskBox(lon,lat,xi,xf,yi,yf,precip,pmin):
        mask = np.zeros((len(lat),len(lon)))
        for ii in np.arange(mask.shape[0]):
                for jj in np.arange(mask.shape[1]):
			if lat[ii] > yi and lat[ii] < yf and lon[jj] > xi and lon[jj] < xf:
				#if precip[ii,jj] > pmin:
				mask[ii,jj] = 1

	return mask

def InterpZ(phi,hgt,htz,mask):
	phiz = np.zeros((len(htz),phi.shape[1],phi.shape[2]))
	for ii in np.arange(phi.shape[1]):
		for jj in np.arange(phi.shape[2]):
			if mask[ii,jj]:
				phiz[:,ii,jj]=griddata(hgt[:,ii,jj],phi[:,ii,jj],htz,method='cubic')

	return phiz

path     = '/mnt/coe0/davidl1/Darwin_15/020112/'
lsm      = ReadFile(path+'lsm.nc','lsm')[0,0,:,:]
lon      = ReadFile(path+'lsm.nc','longitude')
lat      = ReadFile(path+'lsm.nc','latitude')
ht_rho   = ReadFile(path+'ht_rho.nc','ht')[0,:,:,:]
ht_theta = ReadFile(path+'ht_theta.nc','ht')[0,:,:,:]
orog     = ReadFile(path+'orog.nc','ht')[0,0,:,:]

slm  = (lsm+1)%2
nx   = len(lon)
ny   = len(lat)

iimax = 0
jjmax = 0
htmax = -1.0e+9
for ii in np.arange(orog.shape[0]):
	for jj in np.arange(orog.shape[1]):
		if orog[ii,jj] > htmax:
			iimax = ii
			jjmax = jj
			htmax = orog[ii,jj]

z_rho   = ht_rho[:,ii,jj]
z_theta = ht_theta[:,ii,jj]

path = '/mnt/coe0/davidl1/Darwin_15_NoConv_SL02/020700/'
times = [5,6,7,8]
precip = np.zeros(lsm.shape)
rain   = ReadFile(path+'lsrain_avg.nc','lsrain')
for time in times:
	precip = precip + 60.0*60.0*rain[time,0,:,:]

#mask      = GenMask(lon,lat,131.14,-13.26,0.5,rain,0.01)
mask     = GenMaskBox(lon,lat,130.0,131.6,-12.0,-11.1,precip,10.0)

#npInv = 1.0/lsm.shape[0]/lsm.shape[1]
npInv    = 1.0/np.sum(mask)
print 'num pts: ',np.sum(mask)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon[400:-400],lat[450:-350],lsm[450:-350,400:-400],colors='k')
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
#levs = [0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
#levs = [0.,2.,5.,10.,20.,40.,80.,120.,160.,200.]
cmap = cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
#norm = cls.BoundaryNorm(levs,cmap.N)
#co   = ax.contourf(lon[xi:xf],lat[yi:yf],rain[yi:yf,xi:xf],levels=levs,cmap=cmap,norm=norm)
co   = ax.contourf(lon[400:-400],lat[450:-350],mask[450:-350,400:-400]*precip[450:-350,400:-400],cmap=cmap)
plt.colorbar(co,orientation='vertical')
plt.title('Accumulated rainfall for 4 hours (mm)')
pylab.savefig('um_hector_day7.png')

#qv = ReadFile(path+'hum_3.nc','q')[2,:,:,:]
ql = ReadFile(path+'qcl_3.nc','QCL')[2,:,:,:]
qi = ReadFile(path+'qcf_3.nc','QCF')[2,:,:,:]

#qvf = ReadFile(path+'hum_6.nc','q')[:,:,:,:]
qlf = ReadFile(path+'qcl_6.nc','QCL')[:,:,:,:]
qif = ReadFile(path+'qcf_6.nc','QCF')[:,:,:,:]
for time in np.arange(3):
	#qv = qv + qvf[time,:,:,:]
	ql = ql + qlf[time,:,:,:]
	qi = qi + qif[time,:,:,:]

# average over number of hours accumulated
#qv = 0.25*qv
ql = 0.25*ql
qi = 0.25*qi

# interpolate to consistent height levels
#qvi = InterpZ(qv,ht_theta,z_theta,mask)
qli = InterpZ(ql,ht_theta,z_theta,mask)
qii = InterpZ(qi,ht_theta,z_theta,mask)

yi = 565
print 'latitude: ',lat[yi]
#qt = qv[:,yi,:] + ql[:,yi,:] + qi[:,yi,:]
qt = ql[:,yi,:] + qi[:,yi,:]
m2 = qt > 1.0e-4
qt = m2*qt

fig = plt.figure()
ax = fig.add_subplot(111)
co   = ax.contourf(lon[450:-450],z_theta,qt[:,450:-450],cmap=cmap)
plt.colorbar(co,orientation='vertical')
plt.title('Cloud moisture fraction (kg/kg)')
pylab.savefig('um_cloud_day7.png')
plt.show()
