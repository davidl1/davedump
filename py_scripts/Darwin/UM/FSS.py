#!/usr/bin/env python

# Calculate the fractional skill score (FSS) for rainfall data against reference 
# at varying scales and precipitation intensities over the CPOL radar domain
# reference:
#	Roberts and Lean (2008) MWR: 136, 78-97

import numpy as np
from scipy.io import *
from scipy.interpolate import *
from matplotlib import pyplot as plt
import matplotlib.colors as cls
import pylab

def LoadField(filename,fieldname,isscalar):
        print 'from: ' + filename + '\tloading: ' + fieldname
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
	if isscalar:
		return field.getValue()
	else:
        	return field[:]

def InterpGrid( xin, yin, xout, yout, A ):
	pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
	pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
	Ar      = np.ravel(A)
	Aor     = griddata(pts_in,Ar,pts_out,method='cubic')
	Ao      = Aor.reshape(len(yout),len(xout))
	return Ao

def GenFSS( lon, lat, pcp_m, pcp_o, pmax, full ):
	mod = pcp_m>pmax
	obs = pcp_o>pmax

	n = np.min([len(lon),len(lat)])
	n = n/2
	n = np.arange(n)
	n = 2*n+1

	FSS = np.zeros(len(n))

	k = 0
	for ni in n:
		x = np.arange(len(lon)-ni)+ni/2
		y = np.arange(len(lat)-ni)+ni/2
		Mn = np.zeros((len(y),len(x)))
		On = np.zeros((len(y),len(x)))
		i  = 0

		nx = 0
		ny = 0
		for yi in y:
			j = 0
			for xi in x:
				xr      = xi + np.arange(ni)-ni/2
				yr      = yi + np.arange(ni)-ni/2

				if np.sum(full[yr[0]:yr[-1]+1,xr[0]:xr[-1]+1]):
					Mn[i,j] = np.sum(mod[yr[0]:yr[-1]+1,xr[0]:xr[-1]+1])
					On[i,j] = np.sum(obs[yr[0]:yr[-1]+1,xr[0]:xr[-1]+1])
					nx      = nx + 1
					ny      = ny + 1

				j = j + 1

			i = i + 1;

		MSE_ref = (np.sum(On*On) + np.sum(Mn*Mn))/nx/ny
		MSE     = np.sum((On-Mn)*(On-Mn))/nx/ny
		#MSE_ref = (np.sum(On*On) + np.sum(Mn*Mn))/len(x)/len(y)
		#MSE     = np.sum((On-Mn)*(On-Mn))/len(x)/len(y)
		FSS[k]  = 1.0 - MSE/MSE_ref

		print 'n: ',ni,'\tMSE: ',MSE,'\tref: ',MSE_ref,'\tFSS: ',FSS[k]
		k = k + 1

	return FSS

def GenF_o( pcp_o, pmax, full ):
	mask = pcp_o > pmax
	obs  = 1.0*np.sum(full*mask)
	tot  = 1.0*np.sum(full)
	return obs/tot

#lat_m = LoadField('../../Darwin_15/020112/lsm.nc','latitude',False)
#lon_m = LoadField('../../Darwin_15/020112/lsm.nc','longitude',False)
#lsm_m = LoadField('../../Darwin_15/020112/lsm.nc','lsm',False);
lat_m = LoadField('../../Darwin_15_NoConv_SL02/020400/4km/lsm.nc','latitude',False)
lon_m = LoadField('../../Darwin_15_NoConv_SL02/020400/4km/lsm.nc','longitude',False)
lsm_m = LoadField('../../Darwin_15_NoConv_SL02/020400/4km/lsm.nc','lsm',False);
lsm_m = lsm_m[0,0,:,:]

# get the radar data to determine coverage...
#dir_o  = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km/'
dir_o  = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km_2/'
file_o = 'cpol_2d_rainrate_2010'
pcp_o  = LoadField(dir_o+file_o+'0202_1200.nc','rr',False)
pcp_o  = pcp_o[0,:,:]
empty  = pcp_o[:,:]<-10
full   = (empty+1)%2
pcp_o  = full*pcp_o
lon0_o = LoadField(dir_o+file_o+'0201_0000.nc','radar_longitude',True)
lat0_o = LoadField(dir_o+file_o+'0201_0000.nc','radar_latitude',True)
x      = LoadField(dir_o+file_o+'0201_0000.nc','x',False)
y      = LoadField(dir_o+file_o+'0201_0000.nc','y',False)
x      = 1000.0*x
y      = 1000.0*y
Re     = 6378100.0
lon_o  = lon0_o + x*360.0/2.0/np.pi/Re
lat_o  = lat0_o + y*360.0/2.0/np.pi/Re
nx     = len(lon_o)
ny     = len(lat_o)

lon       = lon_o
lat       = lat_o
pts0      = np.transpose([np.tile(lon_m,len(lat_m)),np.repeat(lat_m,len(lon_m))])
pts1      = np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
full_r    = np.ravel(full)
full_m    = griddata(pts1,full_r,pts0,method='nearest')
full_m    = full_m.reshape(len(lat_m),len(lon_m))

pcp_o = np.zeros((ny,nx))
pcp_m = np.zeros((len(lat_m),len(lon_m)))

days = ['020400','020412','020500','020512','020600','020612','020700','020712','020900','020912','021100','021112']
for day in days:
	#pcp = LoadField('../'+day+'/lsrain_avg.nc','lsrain',False)
	pcp = LoadField('../'+day+'/4km/lsrain_avg.nc','lsrain',False)
	for tt in np.arange(pcp.shape[0]):
		pcp_m = pcp_m + 60.0*60.0*full_m*pcp[tt,0,:,:]

days = [4,5,6,7,9,11]
for day in days:
	for hr in np.arange(24):
		pcp = LoadField(dir_o+file_o+'02' + '%.2u'%day + '_'+ '%.2u' % hr+'00.nc','rr',False)
		#pcp_o = pcp_o + full*pcp[0,:,:]/6.0
		pcp_o = pcp_o + full*pcp[0,:,:]

# interpolate model data to observation grid
#pcp_m = InterpGrid(lon_m,lat_m,lon,lat,pcp_m)
#lsm_o = InterpGrid(lon_m,lat_m,lon,lat,lsm_m)
pcp_m_r   = np.ravel(pcp_m)
pcp_m_o   = griddata(pts0,pcp_m_r,pts1,method='cubic')
pcp_m     = pcp_m_o.reshape(ny,nx)
lsm_m_r   = np.ravel(lsm_m)
lsm_m_o   = griddata(pts0,lsm_m_r,pts1,method='nearest')
lsm_o     = lsm_m_o.reshape(ny,nx)

white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
levs=[0.,5.,10.,20.,40.,80.,160.,320.,440.]
cmap=cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm=cls.BoundaryNorm(levs,cmap.N)
#co = ax.contourf(lon,lat,precip_t,levels=levs,cmap=cmap,norm=norm)

fig=plt.figure()
ax=fig.add_subplot(111)
ax.contour(lon,lat,lsm_o,colors='k')
#co=ax.contourf(lon,lat,pcp_o,levels=levs,cmap=cmap,norm=norm)
co=ax.contourf(lon,lat,pcp_o,cmap=cmap)
plt.colorbar(co,orientation='vertical')
plt.title('Precip - CPOL')

fig=plt.figure()
ax=fig.add_subplot(111)
ax.contour(lon,lat,lsm_o,colors='k')
co=ax.contourf(lon,lat,pcp_m,levels=levs,cmap=cmap,norm=norm)
plt.colorbar(co,orientation='vertical')
plt.title('Precip - UM')
plt.show()

# binary field for threshold
n = np.min([len(lon),len(lat)])/2
pmax = np.zeros(4);
pmax[0] =  12.0
pmax[1] =  25.0
pmax[2] =  50.0
pmax[3] = 100.0
FSS = np.zeros((n,4))
f_o = np.zeros(4)
for ii in np.arange(4):
	FSS[:,ii] = GenFSS(lon,lat,pcp_m,pcp_o,pmax[ii],full)
	f_o[ii]   = GenF_o(pcp_o,pmax[ii],full)
	print 'f_',ii,': ',f_o[ii]

n   = np.arange(n)
n   = 2*n+1
dx  = Re*2.0*np.pi*(lon[-1]-lon[0])/360.0/len(lon)
dxn = dx*n/1000.0
print 'dx:',dx
plt.xlabel('Scale (km)')
plt.ylabel('FSS')
#plt.title('Fractions Skill Score (UM)')
plt.title('Fractions Skill Score (UM, $\Delta x = 4.0km$)')
plt.plot(dxn,FSS[:,0],'-',color='b')
plt.plot(dxn,FSS[:,1],'-',color='g')
plt.plot(dxn,FSS[:,2],'-',color='r')
plt.plot(dxn,FSS[:,3],'-',color='c')
plt.legend(('12mm','25mm','50mm','100mm'),loc=4)
plt.plot(dxn,f_o[0]*np.ones(len(dxn)),'--',color='b')
plt.plot(dxn,f_o[1]*np.ones(len(dxn)),'--',color='g')
plt.plot(dxn,f_o[2]*np.ones(len(dxn)),'--',color='r')
plt.plot(dxn,f_o[3]*np.ones(len(dxn)),'--',color='c')
plt.plot(dxn,0.5+0.5*f_o[0]*np.ones(len(dxn)),'-.',color='b')
plt.plot(dxn,0.5+0.5*f_o[1]*np.ones(len(dxn)),'-.',color='g')
plt.plot(dxn,0.5+0.5*f_o[2]*np.ones(len(dxn)),'-.',color='r')
plt.plot(dxn,0.5+0.5*f_o[3]*np.ones(len(dxn)),'-.',color='c')
plt.ylim([0.0,1.0])
#pylab.savefig('FSS_dt15_NoConv_SL02.png')
pylab.savefig('FSS_dt15_NoConv_SL02_4km.png')
plt.show()
