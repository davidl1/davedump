#!/usr/bin/env python

import numpy as np
from scipy.io import netcdf
from scipy.interpolate import *
import matplotlib.pyplot as plt
import pylab

def ReadFile(filename,fieldname):
        print 'reading file: ', filename
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
        return field[:]

def GenMask(lon,lat,x,y,r,prcp,pmin):
	mask = np.zeros((len(lat),len(lon)))
	pnts = np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
	pnts[:,0] = pnts[:,0] - x
	pnts[:,1] = pnts[:,1] - y
	radi = np.sqrt(pnts[:,0]*pnts[:,0]+pnts[:,1]*pnts[:,1])
	radi = radi.reshape(len(lat),len(lon))
	mask = np.zeros(radi.shape)
	for ii in np.arange(radi.shape[0]):
		for jj in np.arange(radi.shape[1]):
			if radi[ii,jj] < r and prcp[ii,jj] > pmin:
				mask[ii,jj] = 1

	return mask

def InterpZ(phi,hgt,htz,mask):
	phiz = np.zeros((len(htz),phi.shape[1],phi.shape[2]))
	for ii in np.arange(phi.shape[1]):
		for jj in np.arange(phi.shape[2]):
			if mask[ii,jj]:
				phiz[:,ii,jj]=griddata(hgt[:,ii,jj],phi[:,ii,jj],htz,method='cubic')

	return phiz

def InterpZ2(phi,htz,orog,mask):
        httop = htz[-1]
        htinv = 1.0/(htz[-1]-htz[0])
	phiz  = np.zeros((len(htz),phi.shape[1],phi.shape[2]))
	for ii in np.arange(phi.shape[1]):
		for jj in np.arange(phi.shape[2]):
			if mask[ii,jj]:
				Zi            = orog[ii,jj] + htz*(httop-orog[ii,jj])*htinv
				phiz[:,ii,jj] = griddata(Zi,phi[:,ii,jj],htz,method='cubic')

	return phiz

path = '/mnt/coe0/davidl1/Darwin_15/020112/'
lsm  = ReadFile(path+'lsm.nc','lsm')[0,0,:,:]
lon  = ReadFile(path+'lsm.nc','longitude')
lat  = ReadFile(path+'lsm.nc','latitude')
htz  = ReadFile(path+'ht_theta.nc','hybrid_ht')
#hgt  = ReadFile(path+'ht_theta.nc','ht')[0,:,:,:]
#htz  = ReadFile(path+'ht_rho.nc','hybrid_ht')
#hgt  = ReadFile(path+'ht_rho.nc','ht')[0,:,:,:]
orog  = ReadFile(path+'orog.nc','ht')[0,0,:,:]

path = '/mnt/coe0/davidl1/Darwin_15/021000/'
velz = ReadFile(path+'velz.nc','dz_dt')[0,:,:,:]
hum  = ReadFile(path+'specific_humidity_q.nc','q')[0,:,:,:] # theta levels
qcl  = ReadFile(path+'qcl_after_step.nc','QCL')[0,:,:,:]
qcf  = ReadFile(path+'qcf_after_step.nc','QCF')[0,:,:,:]
prcp = ReadFile(path+'precip_inst.nc','precip')[0,0,:,:]

mask = GenMask(lon,lat,128.5,-14.5,1.0,prcp,0.02)
#htz2 = htz[1:]
print velz.shape
print htz.shape
print orog.shape
w    = InterpZ2(velz[:-1,:,:],htz,orog,mask)
q    = InterpZ2(hum[:-1,:,:], htz,orog,mask)
ql   = InterpZ2(qcl[:-1,:,:], htz,orog,mask)
qi   = InterpZ2(qcf[:-1,:,:], htz,orog,mask)
wavg = np.zeros(len(htz))
qavg = np.zeros(len(htz))
qlav = np.zeros(len(htz))
qiav = np.zeros(len(htz))
for ii in np.arange(len(htz)):
	wavg[ii] = np.sum(w[ii,:,:])/np.sum(mask)
	qavg[ii] = np.sum(q[ii,:,:])/np.sum(mask)
	qlav[ii] = np.sum(ql[ii,:,:])/np.sum(mask)
	qiav[ii] = np.sum(qi[ii,:,:])/np.sum(mask)

# load  the 4km model
path = '/mnt/coe0/davidl1/Darwin_15/020112/4km/'
lsm4  = ReadFile(path+'lsm.nc','lsm')[0,0,:,:]
lon4  = ReadFile(path+'lsm.nc','longitude')
lat4  = ReadFile(path+'lsm.nc','latitude')
htz4  = ReadFile(path+'ht_theta.nc','hybrid_ht')
#hgt4  = ReadFile(path+'ht_theta.nc','ht')[0,:,:,:]
orog  = ReadFile(path+'orog.nc','ht')[0,0,:,:]

path = '/mnt/coe0/davidl1/Darwin_15/021000/4km/'
velz4 = ReadFile(path+'velz.nc','dz_dt')[0,:,:,:]
hum4  = ReadFile(path+'specific_humidity_q.nc','q')[0,:,:,:] # theta levels
qcl4  = ReadFile(path+'qcl_after_step.nc','QCL')[0,:,:,:]
qcf4  = ReadFile(path+'qcf_after_step.nc','QCF')[0,:,:,:]
prcp4 = ReadFile(path+'precip_inst.nc','precip')[0,0,:,:]

mask4 = GenMask(lon4,lat4,129.5,-14.9,1.0,prcp4,0.02)
w4    = InterpZ2(velz4[:-1,:,:],htz,orog,mask4)
q4    = InterpZ2(hum4[:-1,:,:], htz,orog,mask4)
ql4   = InterpZ2(qcl4[:-1,:,:], htz,orog,mask4)
qi4   = InterpZ2(qcf4[:-1,:,:], htz,orog,mask4)
wavg4 = np.zeros(len(htz))
qavg4 = np.zeros(len(htz))
qlav4 = np.zeros(len(htz))
qiav4 = np.zeros(len(htz))
for ii in np.arange(len(htz)):
	wavg4[ii] = np.sum(w4[ii,:,:])/np.sum(mask4)
	qavg4[ii] = np.sum(q4[ii,:,:])/np.sum(mask4)
	qlav4[ii] = np.sum(ql4[ii,:,:])/np.sum(mask4)
	qiav4[ii] = np.sum(qi4[ii,:,:])/np.sum(mask4)


fig = plt.figure()
ax  = fig.add_subplot(111)
ax.contour(lon,lat,lsm,colors='k')
co  = ax.contourf(lon,lat,mask*prcp)
plt.title('Storm - precip')
plt.colorbar(co,orientation='vertical')

fig = plt.figure()
ax  = fig.add_subplot(111)
ax.contour(lon,lat,lsm,colors='k')
co  = ax.contourf(lon,lat,mask*velz[0,:,:])
plt.title('Storm - vel_z')
plt.colorbar(co,orientation='vertical')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(wavg, htz,'-o')
plt.plot(wavg4,htz,'-o')
plt.xlabel('u_z (m/s)')
plt.ylabel('height (m)')
plt.legend(('$\Delta t = 1.33km$','$\Delta t = 4.00km$'),loc=0)
plt.title('Average vertical velocity inside storm')
pylab.savefig('velz_structure.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qavg, htz,'-o')
plt.plot(qavg4,htz,'-o')
plt.xlabel('q (kg/kg)')
plt.ylabel('height (m)')
plt.legend(('$\Delta t = 1.33km$','$\Delta t = 4.00km$'),loc=0)
plt.title('Average vapour content inside storm')
pylab.savefig('q_structure.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qlav, htz,'-')
plt.plot(qlav4,htz,'-')
plt.plot(qiav, htz,'-')
plt.plot(qiav4,htz,'-')
plt.xlabel('q (kg/kg)')
plt.ylabel('height (m)')
plt.legend(('Liquid: $\Delta t = 1.33km$','Liquid: $\Delta t = 4.00km$','Ice: $\Delta t = 1.33km$','Ice: $\Delta t = 4.00km$'),loc=0)
plt.title('Average moisture content inside storm')
pylab.savefig('qc_structure.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
ax.contour(lon4,lat4,lsm4,colors='k')
co  = ax.contourf(lon4,lat4,mask4*prcp4)
plt.title('Storm - precip (4km)')
plt.colorbar(co,orientation='vertical')

plt.show()
