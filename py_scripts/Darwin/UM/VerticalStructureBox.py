#!/usr/bin/env python

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
from matplotlib import colors as cls
import numpy as np
import pylab

def ReadFile(filename,fieldname):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

def GenMaskBox(lon,lat,xi,xf,yi,yf):
        mask = np.zeros((len(lat),len(lon)))
        for ii in np.arange(mask.shape[0]):
                for jj in np.arange(mask.shape[1]):
			if lat[ii] > yi and lat[ii] < yf and lon[jj] > xi and lon[jj] < xf:
				mask[ii,jj] = 1

	return mask

def GetIndices(lon,lat,xi,xf,yi,yf):
	xmin = 0
	xmax = 0
	ymin = 0
	ymax = 0

	for ii in np.arange(len(lon)):
		if lon[ii] > xi:
			xmin = ii
			break

	for ii in np.arange(len(lon)):
		if lon[ii] > xf:
			xmax = ii
			break

	for ii in np.arange(len(lat)):
		if lat[ii] > yi:
			ymin = ii
			break

	for ii in np.arange(len(lat)):
		if lat[ii] > yf:
			ymax = ii
			break

	return [xmin,xmax,ymin,ymax]


def InterpZ(phi,hgt,htz,mask):
	print 'interpolating field...'

	phiz = np.zeros((len(htz),phi.shape[1],phi.shape[2]))
	for ii in np.arange(phi.shape[1]):
		for jj in np.arange(phi.shape[2]):
			if mask[ii,jj] > 0.1:
				phiz[:,ii,jj]=griddata(hgt[:,ii,jj],phi[:,ii,jj],htz,method='cubic')

	return phiz

path     = '/mnt/coe0/davidl1/Darwin_15/020112/'
lsm      = ReadFile(path+'lsm.nc','lsm')[0,0,:,:]
lon      = ReadFile(path+'lsm.nc','longitude')
lat      = ReadFile(path+'lsm.nc','latitude')
ht_rho   = ReadFile(path+'ht_rho.nc','ht')[0,:,:,:]
ht_theta = ReadFile(path+'ht_theta.nc','ht')[0,:,:,:]
orog     = ReadFile(path+'orog.nc','ht')[0,0,:,:]

slm  = (lsm+1)%2
nx   = len(lon)
ny   = len(lat)

mask = GenMaskBox(lon,lat,130.0,131.6,-12.0,-11.1)

npInv = 1.0/np.sum(mask)

[xi,xf,yi,yf] = GetIndices(lon,lat,130.0,131.6,-12.0,-11.1)

iimax = 0
jjmax = 0
htmax = 0.0
for ii in np.arange(yf-yi)+yi:
	for jj in np.arange(xf-xi)+xi:
		if orog[ii,jj] > htmax:
			iimax = ii
			jjmax = jj
			htmax = orog[ii,jj]

print 'max indices:',iimax,jjmax,htmax
nz = ht_rho.shape[0]

z_rho   = ht_rho[:nz-1,iimax,jjmax]
z_theta = ht_theta[:nz-1,iimax,jjmax]

path  = '/mnt/coe0/davidl1/Darwin_15_NoConv_SL02_3DNoBlend/'
days  = ['020200','020300','020400','020500','020600','020700','020800']
times = [5,6,7,8]
precip = np.zeros(lsm.shape)
for day in days:
	for time in times:
		rain   = ReadFile(path+day+'/lsrain_avg.nc','lsrain')
		precip = precip + 60.0*60.0*rain[time,0,:,:]

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon,lat,lsm,colors='k')
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
#levs = [0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
#levs = [0.,2.,5.,10.,20.,40.,80.,120.,160.,200.]
cmap = cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
#norm = cls.BoundaryNorm(levs,cmap.N)
co   = ax.contourf(lon,lat,precip,cmap=cmap)
#co   = ax.contourf(lon,lat,mask,cmap=cmap)
#co   = ax.contourf(lon[400:-400],lat[450:-350],mbox[450:-350,400:-400]*precip[450:-350,400:-400],cmap=cmap)
plt.colorbar(co,orientation='vertical')
plt.title('Accumulated rainfall for 28 hours (mm)')
pylab.savefig('um_darwin.png')
plt.show()

thf  = ReadFile(path+days[0]+'/theta_3.nc','theta')[2,:,:,:]
wwz  = np.zeros(nz-1)
wuz  = np.zeros(nz-1)
wtz  = np.zeros(nz-1)
qvz  = np.zeros(nz-1)
qlz  = np.zeros(nz-1)
qiz  = np.zeros(nz-1)
qrz  = np.zeros(nz-1)
qgz  = np.zeros(nz-1)
thz  = np.zeros(nz-1)
spvz = np.zeros(nz-2)
shvz = np.zeros(nz-2)
sphz = np.zeros(nz-1)
shhz = np.zeros(nz-1)

th  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
qv  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
ql  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
qi  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
qr  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
qg  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
wu  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
ww  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
wt  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
spv = np.zeros((nz-2,thf.shape[1],thf.shape[2]))
shv = np.zeros((nz-2,thf.shape[1],thf.shape[2]))
sph = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
shh = np.zeros((nz-1,thf.shape[1],thf.shape[2]))

for day in days:
	thf  = ReadFile(path+day+'/theta_3.nc','theta')[2,:,:,:]
	uxf  = ReadFile(path+day+'/velx_3.nc','x-wind')[2,:,:,:]
	uyf  = ReadFile(path+day+'/vely_3.nc','y-wind')[2,:,:-1,:]
	uzf  = ReadFile(path+day+'/velz_3.nc','dz_dt')[2,:,:,:]
	qvf  = ReadFile(path+day+'/hum_3.nc','q')[2,:,:,:]
	qlf  = ReadFile(path+day+'/qcl_3.nc','QCL')[2,:,:,:]
	qif  = ReadFile(path+day+'/qcf_3.nc','QCF')[2,:,:,:]
	qrf  = ReadFile(path+day+'/rain_3.nc','field1681')[2,:,:,:]
	qgf  = ReadFile(path+day+'/graup_3.nc','field1921')[2,:,:,:]
	spvf = ReadFile(path+day+'/smag_p_vert_3.nc','field1020')[2,:,:,:]
	shvf = ReadFile(path+day+'/smag_h_vert_3.nc','field1021')[2,:,:,:]
	sphf = ReadFile(path+day+'/smag_p_horiz_3.nc','unspecified')[2,:,:,:]
	shhf = ReadFile(path+day+'/smag_h_horiz_3.nc','unspecified')[2,:,:,:]

	# interpolate to consistent height levels
	uxi  = InterpZ(uxf, ht_rho,  z_theta,mask)
	uyi  = InterpZ(uyf, ht_rho,  z_theta,mask)
	uzi  = InterpZ(uzf, ht_theta,z_theta,mask)
	qvi  = InterpZ(qvf, ht_theta,z_theta,mask)
	qli  = InterpZ(qlf, ht_theta,z_theta,mask)
	qii  = InterpZ(qif, ht_theta,z_theta,mask)
	qri  = InterpZ(qrf, ht_theta,z_theta,mask)
	qgi  = InterpZ(qgf, ht_theta,z_theta,mask)
	thi  = InterpZ(thf, ht_theta,z_theta,mask)
	spvi = InterpZ(spvf,ht_theta[:-1,:,:],z_theta[:-1],mask)
	shvi = InterpZ(shvf,ht_theta[:-1,:,:],z_theta[:-1],mask)
	sphi = InterpZ(sphf,ht_theta,z_theta,mask)
	shhi = InterpZ(shhf,ht_theta,z_theta,mask)

	th  = th  + thi
	qv  = qv  + qvi
	ql  = ql  + qli
	qi  = qi  + qii
	qr  = qr  + qri
	qg  = qg  + qgi
	spv = spv + spvi
	shv = shv + shvi
	sph = sph + sphi
	shh = shh + shhi

	um = np.zeros(thi.shape)
	vm = np.zeros(thi.shape)
	wm = np.zeros(thi.shape)
	tm = np.zeros(thi.shape)

	for ii in np.arange(th.shape[0]):
		um[ii,:,:] = mask*(uxi[ii,:,:] - np.sum(uxi[ii,:,:])*npInv)
		vm[ii,:,:] = mask*(uyi[ii,:,:] - np.sum(uyi[ii,:,:])*npInv)
		wm[ii,:,:] = mask*(uzi[ii,:,:] - np.sum(uzi[ii,:,:])*npInv)
		tm[ii,:,:] = mask*(thi[ii,:,:] - np.sum(thi[ii,:,:])*npInv)

	wu = wu + wm*(um + vm)
	ww = ww + wm*wm
	wt = wt + wm*tm

	thf  = ReadFile(path+day+'/theta_6.nc','theta')
	uxf  = ReadFile(path+day+'/velx_6.nc','x-wind')
	uyf  = ReadFile(path+day+'/vely_6.nc','y-wind')[:,:,:-1,:]
	uzf  = ReadFile(path+day+'/velz_6.nc','dz_dt')
	qvf  = ReadFile(path+day+'/hum_6.nc','q')
	qlf  = ReadFile(path+day+'/qcl_6.nc','QCL')
	qif  = ReadFile(path+day+'/qcf_6.nc','QCF')
	qrf  = ReadFile(path+day+'/rain_6.nc','field1681')[:,:,:,:]
	qgf  = ReadFile(path+day+'/graup_6.nc','field1921')[:,:,:,:]
	spvf = ReadFile(path+day+'/smag_p_vert_6.nc','field1020')[:,:,:,:]
	shvf = ReadFile(path+day+'/smag_h_vert_6.nc','field1021')[:,:,:,:]
	sphf = ReadFile(path+day+'/smag_p_horiz_6.nc','unspecified')[:,:,:,:]
	shhf = ReadFile(path+day+'/smag_h_horiz_6.nc','unspecified')[:,:,:,:]

	for time in np.arange(3):
		# interpolate to consistent height levels
		uxi  = InterpZ(uxf[time,:,:,:], ht_rho,  z_theta,mask)
		uyi  = InterpZ(uyf[time,:,:,:], ht_rho,  z_theta,mask)
		uzi  = InterpZ(uzf[time,:,:,:], ht_theta,z_theta,mask)
		qvi  = InterpZ(qvf[time,:,:,:], ht_theta,z_theta,mask)
		qli  = InterpZ(qlf[time,:,:,:], ht_theta,z_theta,mask)
		qii  = InterpZ(qif[time,:,:,:], ht_theta,z_theta,mask)
		qri  = InterpZ(qrf[time,:,:,:], ht_theta,z_theta,mask)
		qgi  = InterpZ(qgf[time,:,:,:], ht_theta,z_theta,mask)
		thi  = InterpZ(thf[time,:,:,:], ht_theta,z_theta,mask)
		spvi = InterpZ(spvf[time,:,:,:],ht_theta[:-1,:,:],z_theta[:-1],mask)
		shvi = InterpZ(shvf[time,:,:,:],ht_theta[:-1,:,:],z_theta[:-1],mask)
		sphi = InterpZ(sphf[time,:,:,:],ht_theta,z_theta,mask)
		shhi = InterpZ(shhf[time,:,:,:],ht_theta,z_theta,mask)

		th  = th  + thi
		qv  = qv  + qvi
		ql  = ql  + qli
		qi  = qi  + qii
		qr  = qr  + qri
		qg  = qg  + qgi
		spv = spv + spvi
		shv = shv + shvi
		sph = sph + sphi
		shh = shh + shhi

		um = np.zeros(thi.shape)
		vm = np.zeros(thi.shape)
		wm = np.zeros(thi.shape)
		tm = np.zeros(thi.shape)

		for ii in np.arange(thi.shape[0]):
			um[ii,:,:] = mask*(uxi[ii,:,:] - np.sum(uxi[ii,:,:])*npInv)
			vm[ii,:,:] = mask*(uyi[ii,:,:] - np.sum(uyi[ii,:,:])*npInv)
			wm[ii,:,:] = mask*(uzi[ii,:,:] - np.sum(uzi[ii,:,:])*npInv)
			tm[ii,:,:] = mask*(thi[ii,:,:] - np.sum(thi[ii,:,:])*npInv)

		wu = wu + wm*(um + vm)
		ww = ww + wm*wm
		wt = wt + wm*tm

for ii in np.arange(len(z_theta)):
	wwz[ii]  = np.sum(ww[ii,:,:])*npInv/len(days)/len(times)
	wuz[ii]  = np.sum(wu[ii,:,:])*npInv/len(days)/len(times)
	wtz[ii]  = np.sum(wt[ii,:,:])*npInv/len(days)/len(times)
	qvz[ii]  = np.sum(qv[ii,:,:])*npInv/len(days)/len(times)
	qlz[ii]  = np.sum(ql[ii,:,:])*npInv/len(days)/len(times)
	qiz[ii]  = np.sum(qi[ii,:,:])*npInv/len(days)/len(times)
	qrz[ii]  = np.sum(qr[ii,:,:])*npInv/len(days)/len(times)
	qgz[ii]  = np.sum(qg[ii,:,:])*npInv/len(days)/len(times)
	thz[ii]  = np.sum(th[ii,:,:])*npInv/len(days)/len(times)
	sphz[ii] = np.sum(sph[ii,:,:])*npInv/len(days)/len(times)
	shhz[ii] = np.sum(shh[ii,:,:])*npInv/len(days)/len(times)

for ii in np.arange(len(z_theta)-1):
	spvz[ii] = np.sum(spv[ii,:,:])*npInv/len(days)/len(times)
	shvz[ii] = np.sum(shv[ii,:,:])*npInv/len(days)/len(times)

np.savetxt('hgt_tiwi.box',z_theta)
np.savetxt('wwz_tiwi.box',wwz)
np.savetxt('wuz_tiwi.box',wuz)
np.savetxt('wtz_tiwi.box',wtz)
np.savetxt('thz_tiwi.box',thz)
np.savetxt('qvz_tiwi.box',qvz)
np.savetxt('qlz_tiwi.box',qlz)
np.savetxt('qiz_tiwi.box',qiz)
np.savetxt('qrz_tiwi.box',qrz)
np.savetxt('qgz_tiwi.box',qgz)
np.savetxt('spvz_tiwi.box',spvz)
np.savetxt('shvz_tiwi.box',shvz)
np.savetxt('sphz_tiwi.box',sphz)
np.savetxt('shhz_tiwi.box',shhz)

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(wuz,z_theta,'-')
plt.plot(wwz,z_theta,'-')
plt.xlabel('<$wu$> (m/s)')
plt.ylabel('height (m)')
plt.legend(('<$wu$>','<$ww$>'),loc=3,prop={'size':10})
plt.title('Perturbed momentum flux vertical profile')
pylab.savefig('wuz_darwin_box.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(wtz,z_theta,'-')
plt.xlabel('<$w\Theta$> (mK/s)')
plt.ylabel('height (m)')
plt.title('Potential temperature flux vertical profile')
pylab.savefig('wtz_darwin_box.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qvz,z_theta,'-')
plt.xlabel('<$q_{vapor}$> (kg/kg)')
plt.ylabel('height (m)')
plt.title('Water vapor vertical profile')
pylab.savefig('qvz_darwin_box.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(thz,z_theta,'-')
plt.xlabel('$<\Theta>$ (K)')
plt.ylabel('height (m)')
plt.title('Potential temperature vertical profile')
pylab.savefig('thz_darwin_box.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qlz,z_theta,'-')
plt.plot(qiz,z_theta,'-')
plt.plot(qrz,z_theta,'-')
plt.plot(qgz,z_theta,'-')
plt.xlabel('<q> (kg/kg)')
plt.legend(('cloud','ice','rain','graupel'),loc=1,prop={'size':10})
plt.ylabel('height (m)')
plt.title('Water species vertical profile')
pylab.savefig('qiz_darwin_box.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(spvz,z_theta[:-1],'-')
plt.plot(shvz,z_theta[:-1],'-')
plt.plot(sphz,z_theta,'-')
plt.plot(shhz,z_theta,'-')
plt.xlabel('viscosity')
plt.legend(('momentum-vertical','heat-vertical','momentum-horizontal','heat-horizontal'),loc=1,prop={'size':10})
plt.ylabel('height (m)')
plt.title('Smagorinsky blending coefficients')
pylab.savefig('smz_darwin_box.png')

plt.show()
