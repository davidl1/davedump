#!/usr/bin/env python

# Calculate the temporally and horizontally averaged vertical profiles within clouds (as defined by
# a critical hydrometeor concentration) over the Tiwi islands between 15.00 and 18.00 local time
# inclusive (UTZ + 9) for:
#	potential temperature 				(theta)
#	vertical momentum flux 				(w'w')
#	horiztonal momentum flux 			(w'u' + w'v')
#	potential temperature flux 			(w'theta')
#	vapor 						(q)
#	liquid 						(ql)
#	ice 						(qi)
#	rain						(qr)
#	graupel						(qg)
#	vertical mixing coefficient for momentum	(spv)
#	vertical mixing coefficient for heat		(shv)
#	horizontal mixing coefficient for momentum	(sph)
#	horizontal mixing coefficient for heat		(shh)

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
from matplotlib import colors as cls
import numpy as np
import pylab

def ReadFile(filename,fieldname):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

def GenMaskBox(lon,lat,xi,xf,yi,yf):
        mask = np.zeros((len(lat),len(lon)))
        for ii in np.arange(mask.shape[0]):
                for jj in np.arange(mask.shape[1]):
			if lat[ii] > yi and lat[ii] < yf and lon[jj] > xi and lon[jj] < xf:
				mask[ii,jj] = 1

	return mask

def GetIndices(lon,lat,xi,xf,yi,yf):
	xmin = 0
	xmax = 0
	ymin = 0
	ymax = 0

	for ii in np.arange(len(lon)):
		if lon[ii] > xi:
			xmin = ii
			break

	for ii in np.arange(len(lon)):
		if lon[ii] > xf:
			xmax = ii
			break

	for ii in np.arange(len(lat)):
		if lat[ii] > yi:
			ymin = ii
			break

	for ii in np.arange(len(lat)):
		if lat[ii] > yf:
			ymax = ii
			break

	return [xmin,xmax,ymin,ymax]


def InterpZ(phi,hgt,htz,mask):
	print 'interpolating field...'

	phiz = np.zeros((len(htz),phi.shape[1],phi.shape[2]))
	for ii in np.arange(phi.shape[1]):
		for jj in np.arange(phi.shape[2]):
			if mask[ii,jj]:
				phiz[:,ii,jj]=griddata(hgt[:,ii,jj],phi[:,ii,jj],htz,method='cubic')

	return phiz

path     = '/mnt/coe0/davidl1/Darwin_15/020112/'
lsm      = ReadFile(path+'lsm.nc','lsm')[0,0,:,:]
lon      = ReadFile(path+'lsm.nc','longitude')
lat      = ReadFile(path+'lsm.nc','latitude')
ht_rho   = ReadFile(path+'ht_rho.nc','ht')[0,:,:,:]
ht_theta = ReadFile(path+'ht_theta.nc','ht')[0,:,:,:]
orog     = ReadFile(path+'orog.nc','ht')[0,0,:,:]

slm  = (lsm+1)%2
nx   = len(lon)
ny   = len(lat)

#mbox = GenMaskBox(lon,lat,130.0,131.6,-12.0,-11.1)
[xi,xf,yi,yf] = GetIndices(lon,lat,130.0,131.6,-12.0,-11.1)

ht_rho   = ht_rho[:,yi:yf,xi:xf]
ht_theta = ht_theta[:,yi:yf,xi:xf]
orog     = orog[yi:yf,xi:xf]

print orog.shape

iimax = 0
jjmax = 0
htmax = 0.0
for ii in np.arange(orog.shape[0]):
	for jj in np.arange(orog.shape[1]):
		if orog[ii,jj] > htmax:
			iimax = ii
			jjmax = jj
			htmax = orog[ii,jj]

print 'max indices:',iimax,jjmax,htmax
nz = ht_rho.shape[0]

z_rho   = ht_rho[:nz-1,iimax,jjmax]
z_theta = ht_theta[:nz-1,iimax,jjmax]

path  = '/mnt/coe0/davidl1/Darwin_15_NoConv_SL02_3DNoBlend/'
days  = ['020200','020300','020400','020500','020600','020700','020800']
times = [5,6,7,8]
precip = np.zeros(lsm.shape)
for day in days:
	for time in times:
		rain   = ReadFile(path+day+'/lsrain_avg.nc','lsrain')
		precip = precip + 60.0*60.0*rain[time,0,:,:]

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon[xi:xf],lat[yi:yf],lsm[yi:yf,xi:xf],colors='k')
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
cmap = cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
co   = ax.contourf(lon[xi:xf],lat[yi:yf],precip[yi:yf,xi:xf],cmap=cmap)
plt.colorbar(co,orientation='vertical')
plt.title('Accumulated rainfall for 28 hours (mm)')
plt.show()

RdInv = 1.0/287.05

thf  = ReadFile(path+days[0]+'/theta_3.nc','theta')[2,:,yi:yf,xi:xf]
wwz  = np.zeros(nz-1)
wuz  = np.zeros(nz-1)
wtz  = np.zeros(nz-1)
qvz  = np.zeros(nz-1)
qlz  = np.zeros(nz-1)
qiz  = np.zeros(nz-1)
qrz  = np.zeros(nz-1)
qgz  = np.zeros(nz-1)
thz  = np.zeros(nz-1)
spvz = np.zeros(nz-2)
shvz = np.zeros(nz-2)
sphz = np.zeros(nz-1)
shhz = np.zeros(nz-1)

counter = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
th  = np.zeros(counter.shape)
qv  = np.zeros(counter.shape)
ql  = np.zeros(counter.shape)
qi  = np.zeros(counter.shape)
qr  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
qg  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
wu  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
ww  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
wt  = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
spv = np.zeros((nz-2,thf.shape[1],thf.shape[2]))
shv = np.zeros((nz-2,thf.shape[1],thf.shape[2]))
sph = np.zeros((nz-1,thf.shape[1],thf.shape[2]))
shh = np.zeros((nz-1,thf.shape[1],thf.shape[2]))

for day in days:
	thf = ReadFile(path+day+'/theta_3.nc','theta')[2,:,yi:yf,xi:xf]
	uxf = ReadFile(path+day+'/velx_3.nc','x-wind')[2,:,yi:yf,xi:xf]
	uyf = ReadFile(path+day+'/vely_3.nc','y-wind')[2,:,yi:yf,xi:xf]
	uzf = ReadFile(path+day+'/velz_3.nc','dz_dt')[2,:,yi:yf,xi:xf]
	qvf = ReadFile(path+day+'/hum_3.nc','q')[2,:,yi:yf,xi:xf]
	qlf = ReadFile(path+day+'/qcl_3.nc','QCL')[2,:,yi:yf,xi:xf]
	qif = ReadFile(path+day+'/qcf_3.nc','QCF')[2,:,yi:yf,xi:xf]
	qrf  = ReadFile(path+day+'/rain_3.nc','field1681')[2,:,yi:yf,xi:xf]
	qgf  = ReadFile(path+day+'/graup_3.nc','field1921')[2,:,yi:yf,xi:xf]
	spvf = ReadFile(path+day+'/smag_p_vert_3.nc','field1020')[2,:,yi:yf,xi:xf]
	shvf = ReadFile(path+day+'/smag_h_vert_3.nc','field1021')[2,:,yi:yf,xi:xf]
	sphf = ReadFile(path+day+'/smag_p_horiz_3.nc','unspecified')[2,:,yi:yf,xi:xf]
	shhf = ReadFile(path+day+'/smag_h_horiz_3.nc','unspecified')[2,:,yi:yf,xi:xf]

	theta = ReadFile(path+day+'/theta_3.nc','theta')[2,:,yi:yf,xi:xf]
	exner = ReadFile(path+day+'/exner_3.nc','field7')[2,:,yi:yf,xi:xf]
	press = ReadFile(path+day+'/pressure_3.nc','p')[2,:,yi:yf,xi:xf]
	rho   = RdInv*press/theta/exner
	spvf  = spvf/rho[:-1,:,:]
	shvf  = shvf/rho[:-1,:,:]

	m2 = qlf + qif + qrf + qgf > 1.0e-4
	mask = np.zeros((m2.shape[1],m2.shape[2]))
	for ii in np.arange(m2.shape[1]):
		for jj in np.arange(m2.shape[2]):
			mask[ii,jj] = np.sum(m2[:,ii,jj])

	mz = mask > 0.1
	print 'num pts: ', np.sum(mz)

	# interpolate to consistent height levels
	uxi = InterpZ(uxf,ht_rho,  z_theta,mz)
	uyi = InterpZ(uyf,ht_rho,  z_theta,mz)
	uzi = InterpZ(uzf,ht_theta,z_theta,mz)
	qvi = InterpZ(qvf,ht_theta,z_theta,mz)
	qli = InterpZ(qlf,ht_theta,z_theta,mz)
	qii = InterpZ(qif,ht_theta,z_theta,mz)
	qri = InterpZ(qrf,ht_theta,z_theta,mz)
	qgi = InterpZ(qgf,ht_theta,z_theta,mz)
	thi = InterpZ(thf,ht_theta,z_theta,mz)
	spvi = InterpZ(spvf,ht_theta[:-1,:,:],z_theta[:-1],mz)
	shvi = InterpZ(shvf,ht_theta[:-1,:,:],z_theta[:-1],mz)
	sphi = InterpZ(sphf,ht_theta,z_theta,mz)
	shhi = InterpZ(shhf,ht_theta,z_theta,mz)

	m2 = qli + qii + qri + qgi > 1.0e-4
	for ii in np.arange(th.shape[0]):
		for jj in np.arange(th.shape[1]):
			for kk in np.arange(th.shape[2]):
				if m2[ii,jj,kk]:
					counter[ii,jj,kk] = counter[ii,jj,kk] + 1.0

	th  = th  + m2*thi
	qv  = qv  + m2*qvi
	ql  = ql  + m2*qli
	qi  = qi  + m2*qii
	qr  = qr  + m2*qri
	qg  = qg  + m2*qgi
	spv = spv + m2[:-1,:,:]*spvi
	shv = shv + m2[:-1,:,:]*shvi
	sph = sph + m2*sphi
	shh = shh + m2*shhi

	um = np.zeros(thi.shape)
	vm = np.zeros(thi.shape)
	wm = np.zeros(thi.shape)
	tm = np.zeros(thi.shape)

	for ii in np.arange(m2.shape[0]):
		npts = np.sum(m2[ii,:,:])
		if npts > 0.1:
			um[ii,:,:] = m2[ii,:,:]*uxi[ii,:,:] - np.sum(m2[ii,:,:]*uxi[ii,:,:])/npts
			vm[ii,:,:] = m2[ii,:,:]*uyi[ii,:,:] - np.sum(m2[ii,:,:]*uyi[ii,:,:])/npts
			wm[ii,:,:] = m2[ii,:,:]*uzi[ii,:,:] - np.sum(m2[ii,:,:]*uzi[ii,:,:])/npts
			tm[ii,:,:] = m2[ii,:,:]*thi[ii,:,:] - np.sum(m2[ii,:,:]*thi[ii,:,:])/npts

	wu = wu + m2*wm*(um + vm)
	ww = ww + m2*wm*wm
	wt = wt + m2*wm*tm

	thf = ReadFile(path+day+'/theta_6.nc','theta')[:,:,yi:yf,xi:xf]
	uxf = ReadFile(path+day+'/velx_6.nc','x-wind')[:,:,yi:yf,xi:xf]
	uyf = ReadFile(path+day+'/vely_6.nc','y-wind')[:,:,yi:yf,xi:xf]
	uzf = ReadFile(path+day+'/velz_6.nc','dz_dt')[:,:,yi:yf,xi:xf]
	qvf = ReadFile(path+day+'/hum_6.nc','q')[:,:,yi:yf,xi:xf]
	qlf = ReadFile(path+day+'/qcl_6.nc','QCL')[:,:,yi:yf,xi:xf]
	qif = ReadFile(path+day+'/qcf_6.nc','QCF')[:,:,yi:yf,xi:xf]
	qrf  = ReadFile(path+day+'/rain_6.nc','field1681')[:,:,yi:yf,xi:xf]
	qgf  = ReadFile(path+day+'/graup_6.nc','field1921')[:,:,yi:yf,xi:xf]
	spvf = ReadFile(path+day+'/smag_p_vert_6.nc','field1020')[:,:,yi:yf,xi:xf]
	shvf = ReadFile(path+day+'/smag_h_vert_6.nc','field1021')[:,:,yi:yf,xi:xf]
	sphf = ReadFile(path+day+'/smag_p_horiz_6.nc','unspecified')[:,:,yi:yf,xi:xf]
	shhf = ReadFile(path+day+'/smag_h_horiz_6.nc','unspecified')[:,:,yi:yf,xi:xf]

	theta = ReadFile(path+day+'/theta_6.nc','theta')[:,:,yi:yf,xi:xf]
	exner = ReadFile(path+day+'/exner_6.nc','field7')[:,:,yi:yf,xi:xf]
	press = ReadFile(path+day+'/pressure_6.nc','p')[:,:,yi:yf,xi:xf]
	rho   = RdInv*press/theta/exner
	spvf  = spvf/rho[:,:-1,:,:]
	shvf  = shvf/rho[:,:-1,:,:]

	for time in np.arange(3):
		m2 = qlf[time,:,:,:] + qif[time,:,:,:] + qrf[time,:,:,:] + qgf[time,:,:,:] > 1.0e-4

		mask = np.zeros((m2.shape[1],m2.shape[2]))
		for ii in np.arange(m2.shape[1]):
			for jj in np.arange(m2.shape[2]):
				mask[ii,jj] = np.sum(m2[:,ii,jj])
	
		mz = mask > 0.1
		print 'num pts: ', np.sum(mz)

		# interpolate to consistent height levels
		thi  = InterpZ(thf[time,:,:,:], ht_theta,z_theta,mz)
		uxi  = InterpZ(uxf[time,:,:,:], ht_rho,  z_theta,mz)
		uyi  = InterpZ(uyf[time,:,:,:], ht_rho,  z_theta,mz)
		uzi  = InterpZ(uzf[time,:,:,:], ht_theta,z_theta,mz)
		qvi  = InterpZ(qvf[time,:,:,:], ht_theta,z_theta,mz)
		qli  = InterpZ(qlf[time,:,:,:], ht_theta,z_theta,mz)
		qii  = InterpZ(qif[time,:,:,:], ht_theta,z_theta,mz)
		qri  = InterpZ(qrf[time,:,:,:], ht_theta,z_theta,mz)
		qgi  = InterpZ(qgf[time,:,:,:], ht_theta,z_theta,mz)
		spvi = InterpZ(spvf[time,:,:,:],ht_theta[:-1,:,:],z_theta[:-1],mz)
		shvi = InterpZ(shvf[time,:,:,:],ht_theta[:-1,:,:],z_theta[:-1],mz)
		sphi = InterpZ(sphf[time,:,:,:],ht_theta,z_theta,mz)
		shhi = InterpZ(shhf[time,:,:,:],ht_theta,z_theta,mz)

		m2 = qli + qii + qri + qgi > 1.0e-4
		for ii in np.arange(th.shape[0]):
			for jj in np.arange(th.shape[1]):
				for kk in np.arange(th.shape[2]):
					if m2[ii,jj,kk]:
						counter[ii,jj,kk] = counter[ii,jj,kk] + 1.0

		th  = th  + m2*thi
		qv  = qv  + m2*qvi
		ql  = ql  + m2*qli
		qi  = qi  + m2*qii
		qr  = qr  + m2*qri
		qg  = qg  + m2*qgi
		spv = spv + m2[:-1,:,:]*spvi
		shv = shv + m2[:-1,:,:]*shvi
		sph = sph + m2*sphi
		shh = shh + m2*shhi

		um = np.zeros(thi.shape)
		vm = np.zeros(thi.shape)
		wm = np.zeros(thi.shape)
		tm = np.zeros(thi.shape)

		for ii in np.arange(m2.shape[0]):
			npts = np.sum(m2[ii,:,:])
			if npts > 0.1:
				um[ii,:,:] = m2[ii,:,:]*uxi[ii,:,:] - np.sum(m2[ii,:,:]*uxi[ii,:,:])/npts
				vm[ii,:,:] = m2[ii,:,:]*uyi[ii,:,:] - np.sum(m2[ii,:,:]*uyi[ii,:,:])/npts
				wm[ii,:,:] = m2[ii,:,:]*uzi[ii,:,:] - np.sum(m2[ii,:,:]*uzi[ii,:,:])/npts
				tm[ii,:,:] = m2[ii,:,:]*thi[ii,:,:] - np.sum(m2[ii,:,:]*thi[ii,:,:])/npts

		wu = wu + m2*wm*(um + vm)
		ww = ww + m2*wm*wm
		wt = wt + m2*wm*tm

for ii in np.arange(len(z_theta)):
	npts = np.sum(counter[ii,:,:])
	if npts > 0.1:
		thz[ii]  = np.sum(th[ii,:,:])/npts
		wwz[ii]  = np.sum(ww[ii,:,:])/npts
		wuz[ii]  = np.sum(wu[ii,:,:])/npts
		wtz[ii]  = np.sum(wt[ii,:,:])/npts
		qvz[ii]  = np.sum(qv[ii,:,:])/npts
		qlz[ii]  = np.sum(ql[ii,:,:])/npts
		qiz[ii]  = np.sum(qi[ii,:,:])/npts
		qrz[ii]  = np.sum(qr[ii,:,:])/npts
		qgz[ii]  = np.sum(qg[ii,:,:])/npts
		spvz[ii] = np.sum(spv[ii,:,:])/npts
		shvz[ii] = np.sum(shv[ii,:,:])/npts
		sphz[ii] = np.sum(sph[ii,:,:])/npts
		shhz[ii] = np.sum(shh[ii,:,:])/npts

np.savetxt('hgt_darwin.cloud',z_theta)
np.savetxt('wwz_darwin.cloud',wwz)
np.savetxt('wuz_darwin.cloud',wuz)
np.savetxt('wtz_darwin.cloud',wtz)
np.savetxt('thz_darwin.cloud',thz)
np.savetxt('qvz_darwin.cloud',qvz)
np.savetxt('qlz_darwin.cloud',qlz)
np.savetxt('qiz_darwin.cloud',qiz)
np.savetxt('qrz_darwin.cloud',qrz)
np.savetxt('qgz_darwin.cloud',qgz)
np.savetxt('spvz_darwin.cloud',spvz)
np.savetxt('shvz_darwin.cloud',shvz)
np.savetxt('sphz_darwin.cloud',sphz)
np.savetxt('shhz_darwin.cloud',shhz)

countz = np.zeros(len(z_theta))
for ii in np.arange(len(z_theta)):
	countz[ii] = np.sum(counter[ii,:,:])

np.savetxt('count_darwin.cloud',countz)

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(wuz,z_theta,'-')
plt.plot(wwz,z_theta,'-')
plt.xlabel('<$wu$> (m/s)')
plt.ylabel('height (m)')
plt.legend(('<$wu$>','<$ww$>'),loc=3,prop={'size':10})
plt.title('Perturbed momentum flux vertical profile')
pylab.savefig('wuz_darwin_cloud.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(wtz,z_theta,'-')
plt.xlabel('<$w\Theta$> (mK/s)')
plt.ylabel('height (m)')
plt.title('Potential temperature flux vertical profile')
pylab.savefig('wtz_darwin_cloud.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qvz,z_theta,'-')
plt.xlabel('<$q_{vapor}$> (kg/kg)')
plt.ylabel('height (m)')
plt.title('Water vapor vertical profile')
pylab.savefig('qvz_darwin_cloud.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(thz,z_theta,'-')
plt.xlabel('$<\Theta>$ (K)')
plt.ylabel('height (m)')
plt.title('Potential temperature vertical profile')
pylab.savefig('thz_darwin_cloud.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qlz,z_theta,'-')
plt.plot(qiz,z_theta,'-')
plt.xlabel('<q> (kg/kg)')
plt.legend(('liquid','ice'),loc=1,prop={'size':10})
plt.ylabel('height (m)')
plt.title('Water species vertical profile')
pylab.savefig('qiz_darwin_cloud.png')

plt.show()
