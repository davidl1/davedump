#!/usr/bin/env python

from scipy.interpolate import *
import matplotlib.pyplot as plt
import matplotlib.colors as cls
import numpy as np
import pylab
from Utils import *

def GenMask(xi,yi,xf,yf,lon,lat,dx):
	mask = np.zeros((len(lat),len(lon)))
	xmin = 0
	xmax = 0
	ymin = 0
	ymax = 0
	m    = (yf-yi)/(xf-xi)

	for ii in np.arange(len(lon)):
		x = lon[ii]
		if x < xi:
			continue
		if x > xf:
			continue

		y        = m*(x - xi) + yi
		pt       = np.abs(lat - y) < dx
		mask[:,ii] = pt 

	return mask
	

days  = ['020200','020212','020300','020312','020400','020412','020500','020512','020600','020612','020700','020712','020800','020812','020900','020912','021000','021012','021100','021112']

#filename = '../020112/4km/lsm.nc'
filename = '../020112/lsm.nc'
lsm = ReadField(filename,'lsm')[0,0,:,:]
lon = ReadField(filename,'longitude')
lat = ReadField(filename,'latitude')

xi = 130.34
yi = -11.16
xf = 131.15
yf = -11.85
mask = GenMask(xi,yi,xf,yf,lon,lat,1.0*(lon[1]-lon[0]))
npts = np.sum(mask)

hoevmuller = np.zeros((12*len(days),np.sum(mask)))
hoev_i     = np.zeros((npts))
time       = np.arange(12*len(days))*0.5/24.0
longitude  = xi + (xf-xi)*np.arange(npts)

# precip_2 is the hourly averaged dump, _1 is instantaneous
kk = 0
for day in days:
	#filename = '../' + time + '/4km' + '/precip_avg.nc'
	filename = '../' + day + '/precip_avg.nc'
	precip   = ReadField(filename,'precip')
	for hour in np.arange(12):
		pt = 0
		for jj in np.arange(len(lon)):
			for ii in np.arange(len(lat)):
				if mask[ii,jj]:
					hoev_i[pt] = precip[hour,0,ii,jj]
					pt = pt + 1

		hoevmuller[kk,:] = hoev_i
		kk = kk + 1

# rescale to mm (10^-3 m)
rho  = 1.0e+3
m2mm = 1.0e+3
dt   = 60.0*60.0
hoevmuller = (dt/rho*m2mm)*hoevmuller

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon,lat,lsm,colors='k')
co = ax.contourf(lon,lat,mask)

fig = plt.figure()
ax = fig.add_subplot(111)
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
#levs=[0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
levs=[0.,2.,5.,10.,20.,40.,80.,120.,160.,200.]
cmap=cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm=cls.BoundaryNorm(levs,cmap.N)
co = ax.contourf(longitude,time,hoevmuller,levels=levs,cmap=cmap,norm=norm)
plt.title('Rainfall over the Tiwi islands (mm)')
plt.colorbar(co,orientation='horizontal')
#pylab.savefig('accum_rain_120_dt15_1p33.png')

# interpolate radar mask and plot that space only
#dir_o     = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km/'
#file_o    = 'cpol_2d_rainrate_2010'
#pcp_o     = ReadField(dir_o+file_o+'0202_1200.nc','rr')
#pcp_o     = pcp_o[0,:,:]
#empty     = pcp_o[:,:]<-10
#full      = (empty+1)%2
#lon0_o    = ReadScalar(dir_o+file_o+'0201_0000.nc','radar_longitude')
#lat0_o    = ReadScalar(dir_o+file_o+'0201_0000.nc','radar_latitude')
#x         = ReadField(dir_o+file_o+'0201_0000.nc','x')
#y         = ReadField(dir_o+file_o+'0201_0000.nc','y')
#x         = 1000.0*x
#y         = 1000.0*y
#Re        = 6378100.0
#lon_o     = lon0_o + x*360.0/2.0/np.pi/Re
#lat_o     = lat0_o + y*360.0/2.0/np.pi/Re
#pts0      = np.transpose([np.tile(lon_o,len(lat_o)),np.repeat(lat_o,len(lon_o))])
#pts1      = np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
#full_r    = np.ravel(full)
#full_m    = griddata(pts0,full_r,pts1,method='nearest')
#full_m    = full_m.reshape(len(lat),len(lon))
#precip_m  = full_m*precip_t

#fig = plt.figure()
#ax = fig.add_subplot(111)
#ax.contour(lon[400:-369],lat[400:-369],lsm[400:-369,400:-369],colors='k')	# 1.33 km
#co = ax.contourf(lon[400:-369],lat[400:-369],precip_m[400:-369,400:-369],levels=levs,cmap=cmap,norm=norm)
#ax.contour(lon[270:-249],lat[265:-254],lsm[265:-254,270:-249],colors='k')	# 4.00 km
#co = ax.contourf(lon[270:-249],lat[265:-254],precip_m[265:-254,270:-249],levels=levs,cmap=cmap,norm=norm)
#plt.title('Accumulated rainfall for 120 hours (mm) - (UM)')
#plt.colorbar(co,orientation='horizontal')
#pylab.savefig('accum_rain_120_darwin_dt15_1p33.png')
plt.show()
