#!/usr/bin/env python

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
from matplotlib import colors as cls
import numpy as np
import pylab

def ReadFile(filename,fieldname):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

def InterpGrid(xin,yin,xout,yout,A,interpType):
	print 'interp type: ', interpType
	pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
	pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
	Ar      = np.ravel(A)
	Aor     = griddata(pts_in,Ar,pts_out,method=interpType)
	Ao      = Aor.reshape(len(yout),len(xout))
	return Ao

def GenMask(lon,lat,x,y,r,prcp,pmin):
        mask = np.zeros((len(lat),len(lon)))
        pnts = np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
        pnts[:,0] = pnts[:,0] - x
        pnts[:,1] = pnts[:,1] - y
        radi = np.sqrt(pnts[:,0]*pnts[:,0]+pnts[:,1]*pnts[:,1])
        radi = radi.reshape(len(lat),len(lon))
        mask = np.zeros(radi.shape)
        for ii in np.arange(radi.shape[0]):
                for jj in np.arange(radi.shape[1]):
                        if radi[ii,jj] < r and prcp[ii,jj] > pmin:
                                mask[ii,jj] = 1

	return mask

def GenMaskBox(lon,lat,xi,xf,yi,yf,precip,pmin):
        mask = np.zeros((len(lat),len(lon)))
        for ii in np.arange(mask.shape[0]):
                for jj in np.arange(mask.shape[1]):
			if lat[ii] > yi and lat[ii] < yf and lon[jj] > xi and lon[jj] < xf:
				if precip[ii,jj] > pmin:
					mask[ii,jj] = 1

	return mask

def InterpZ(phi,hgt,htz,mask):
	print 'interpolating field...'
	phiz = np.zeros((len(htz),phi.shape[1],phi.shape[2]))
	for ii in np.arange(phi.shape[1]):
		for jj in np.arange(phi.shape[2]):
			if mask[ii,jj]:
				phiz[:,ii,jj]=griddata(hgt[:,ii,jj],phi[:,ii,jj],htz,method='cubic')

	return phiz

path     = '/mnt/coe0/davidl1/Darwin_15/020112/'
lsm      = ReadFile(path+'lsm.nc','lsm')[0,0,:,:]
lon      = ReadFile(path+'lsm.nc','longitude')
lat      = ReadFile(path+'lsm.nc','latitude')
ht_rho   = ReadFile(path+'ht_rho.nc','ht')[0,:,:,:]
ht_theta = ReadFile(path+'ht_theta.nc','ht')[0,:,:,:]
orog     = ReadFile(path+'orog.nc','ht')[0,0,:,:]

slm  = (lsm+1)%2
nx   = len(lon)
ny   = len(lat)

iimax = 0
jjmax = 0
htmax = -1.0e+9
for ii in np.arange(orog.shape[0]):
	for jj in np.arange(orog.shape[1]):
		if orog[ii,jj] > htmax:
			iimax = ii
			jjmax = jj
			htmax = orog[ii,jj]

z_rho   = ht_rho[:,ii,jj]
z_theta = ht_theta[:,ii,jj]

path  = '/mnt/coe0/davidl1/Darwin_15_NoConv_SL02/'
days  = ['020200','020300','020400','020500','020600','020700','020800']
times = [5,6,7,8]
precip = np.zeros(lsm.shape)
for day in days:
	for time in times:
		rain   = ReadFile(path+day+'/lsrain_avg.nc','lsrain')
		precip = precip + 60.0*60.0*rain[time,0,:,:]

mask     = GenMaskBox(lon,lat,130.0,131.6,-12.0,-11.1,precip,10.0)

#npInv = 1.0/lsm.shape[0]/lsm.shape[1]
npInv    = 1.0/np.sum(mask)
print 'num pts: ',np.sum(mask)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon[400:-400],lat[450:-350],lsm[450:-350,400:-400],colors='k')
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
#levs = [0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
#levs = [0.,2.,5.,10.,20.,40.,80.,120.,160.,200.]
cmap = cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
#norm = cls.BoundaryNorm(levs,cmap.N)
#co   = ax.contourf(lon[xi:xf],lat[yi:yf],rain[yi:yf,xi:xf],levels=levs,cmap=cmap,norm=norm)
co   = ax.contourf(lon[400:-400],lat[450:-350],mask[450:-350,400:-400]*precip[450:-350,400:-400],cmap=cmap)
plt.colorbar(co,orientation='vertical')
plt.title('Accumulated rainfall for 28 hours (mm)')
pylab.savefig('um_darwin.png')
plt.show()

th = ReadFile(path+days[0]+'/theta_3.nc','theta')[2,:,:,:]
print th.shape
wwz = np.zeros(th.shape[0])
wuz = np.zeros(th.shape[0])
wtz = np.zeros(th.shape[0])
qvz = np.zeros(th.shape[0])
qlz = np.zeros(th.shape[0])
qiz = np.zeros(th.shape[0])
thz = np.zeros(th.shape[0])

for day in days:
	th = ReadFile(path+day+'/theta_3.nc','theta')[2,:,:,:]
	ux = ReadFile(path+day+'/velx_3.nc','x-wind')[2,:,:,:]
	uy = ReadFile(path+day+'/vely_3.nc','y-wind')[2,:,:,:]
	uz = ReadFile(path+day+'/velz_3.nc','dz_dt')[2,:,:,:]
	qv = ReadFile(path+day+'/hum_3.nc','q')[2,:,:,:]
	ql = ReadFile(path+day+'/qcl_3.nc','QCL')[2,:,:,:]
	qi = ReadFile(path+day+'/qcf_3.nc','QCF')[2,:,:,:]

	thf = ReadFile(path+day+'/theta_6.nc','theta')[:,:,:,:]
	uxf = ReadFile(path+day+'/velx_6.nc','x-wind')[:,:,:,:]
	uyf = ReadFile(path+day+'/vely_6.nc','y-wind')[:,:,:,:]
	uzf = ReadFile(path+day+'/velz_6.nc','dz_dt')[:,:,:,:]
	qvf = ReadFile(path+day+'/hum_6.nc','q')[:,:,:,:]
	qlf = ReadFile(path+day+'/qcl_6.nc','QCL')[:,:,:,:]
	qif = ReadFile(path+day+'/qcf_6.nc','QCF')[:,:,:,:]
	for time in np.arange(3):
		th = th + thf[time,:,:,:]
		ux = ux + uxf[time,:,:,:]
		uy = uy + uyf[time,:,:,:]
		uz = uz + uzf[time,:,:,:]
		qv = qv + qvf[time,:,:,:]
		ql = ql + qlf[time,:,:,:]
		qi = qi + qif[time,:,:,:]

	# interpolate to consistent height levels
	uxi = InterpZ(ux,ht_rho,  z_theta,  mask)
	uyi = InterpZ(uy[:,:-1,:],ht_rho,  z_theta,  mask)
	uzi = InterpZ(uz,ht_theta,z_theta,mask)
	qvi = InterpZ(qv,ht_theta,z_theta,mask)
	qli = InterpZ(ql,ht_theta,z_theta,mask)
	qii = InterpZ(qi,ht_theta,z_theta,mask)
	thi = InterpZ(th,ht_theta,z_theta,mask)

	# remove mean flow components
	thj = np.zeros(np.shape(thi))
	for ii in np.arange(len(z_rho)):
		uxi[ii,:,:] = uxi[ii,:,:] - npInv*np.sum(uxi[ii,:,:])
		uyi[ii,:,:] = uyi[ii,:,:] - npInv*np.sum(uyi[ii,:,:])
		thj[ii,:,:] = thi[ii,:,:] - npInv*np.sum(thi[ii,:,:])

	for ii in np.arange(len(z_rho)):
		wwz[ii] = wwz[ii] + npInv*np.sum(uzi[ii,:,:]*uzi[ii,:,:])
		wuz[ii] = wuz[ii] + npInv*np.sum(uzi[ii,:,:]*uxi[ii,:,:] + uzi[ii,:,:]*uyi[ii,:,:])
		wtz[ii] = wtz[ii] + npInv*np.sum(uzi[ii,:,:]*thj[ii,:,:])
		qvz[ii] = qvz[ii] + npInv*np.sum(qvi[ii,:,:])
		qlz[ii] = qlz[ii] + npInv*np.sum(qli[ii,:,:])
		qiz[ii] = qiz[ii] + npInv*np.sum(qii[ii,:,:])
		thz[ii] = thz[ii] + npInv*np.sum(thi[ii,:,:])

wwz = wwz/len(times)/len(days)
wuz = wuz/len(times)/len(days)
wtz = wtz/len(times)/len(days)
qvz = qvz/len(times)/len(days)
qlz = qlz/len(times)/len(days)
qiz = qiz/len(times)/len(days)
thz = thz/len(times)/len(days)
	
fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(wuz,z_theta,'-')
plt.plot(wwz,z_theta,'-')
plt.xlabel('<$wu$> (m/s)')
plt.ylabel('height (m)')
plt.legend(('<$wu$>','<$ww$>'),loc=3,prop={'size':10})
plt.title('Perturbed momentum flux vertical profile')
pylab.savefig('wuz_darwin.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(wtz,z_theta,'-')
plt.xlabel('<$w\Theta$> (mK/s)')
plt.ylabel('height (m)')
plt.title('Potential temperature flux vertical profile')
pylab.savefig('wtz_darwin.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qvz,z_theta,'-')
plt.xlabel('<$q_{vapor}$> (kg/kg)')
plt.ylabel('height (m)')
plt.title('Water vapor vertical profile')
pylab.savefig('qvz_darwin.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(thz,z_theta,'-')
plt.xlabel('$<\Theta>$ (K)')
plt.ylabel('height (m)')
plt.title('Potential temperature vertical profile')
pylab.savefig('thz_darwin.png')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(qlz,z_theta,'-')
plt.plot(qiz,z_theta,'-')
plt.xlabel('<q> (kg/kg)')
plt.legend(('liquid','ice'),loc=1,prop={'size':10})
plt.ylabel('height (m)')
plt.title('Water species vertical profile')
pylab.savefig('qiz_darwin.png')

plt.show()

np.savetxt('hgt_darwin.txt',z_theta)
np.savetxt('wwz_darwin.txt',wwz)
np.savetxt('wuz_darwin.txt',wuz)
np.savetxt('wtz_darwin.txt',wtz)
np.savetxt('thz_darwin.txt',thz)
np.savetxt('qvz_darwin.txt',qvz)
np.savetxt('qlz_darwin.txt',qlz)
np.savetxt('qiz_darwin.txt',qiz)
