#!/usr/bin/env python

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
from matplotlib import colors as cls
import numpy as np
import pylab

def ReadFile(filename,fieldname):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

def GetIndices(lon,lat,xi,xf,yi,yf):
	xmin = 0
	xmax = 0
	ymin = 0
	ymax = 0

	for ii in np.arange(len(lon)):
		if lon[ii] > xi:
			xmin = ii
			break

	for ii in np.arange(len(lon)):
		if lon[ii] > xf:
			xmax = ii
			break

	for ii in np.arange(len(lat)):
		if lat[ii] > yi:
			ymin = ii
			break

	for ii in np.arange(len(lat)):
		if lat[ii] > yf:
			ymax = ii
			break

	return [xmin,xmax,ymin,ymax]


def InterpZ(phi,hgt,htz):
	print 'interpolating field...'

	phiz = np.zeros((len(htz),phi.shape[1],phi.shape[2]))
	for ii in np.arange(phi.shape[1]):
		for jj in np.arange(phi.shape[2]):
			phiz[:,ii,jj]=griddata(hgt[:,ii,jj],phi[:,ii,jj],htz,method='cubic')

	return phiz

path     = '/mnt/coe0/davidl1/Darwin_15/020112/'
lsm      = ReadFile(path+'lsm.nc','lsm')[0,0,:,:]
lon      = ReadFile(path+'lsm.nc','longitude')
lat      = ReadFile(path+'lsm.nc','latitude')
ht_rho   = ReadFile(path+'ht_rho.nc','ht')[0,:,:,:]
ht_theta = ReadFile(path+'ht_theta.nc','ht')[0,:,:,:]
orog     = ReadFile(path+'orog.nc','ht')[0,0,:,:]

slm  = (lsm+1)%2
nx   = len(lon)
ny   = len(lat)

[xi,xf,yi,yf] = GetIndices(lon,lat,130.0,131.6,-12.0,-11.1)

#ht_rho   = ht_rho[:,yi:yf,xi:xf]
#ht_theta = ht_theta[:,yi:yf,xi:xf]
#orog     = orog[yi:yf,xi:xf]
#lon      = lon[xi:xf]
#lat      = lat[yi:yf]
#lsm      = lsm[yi:yf,xi:xf]

print orog.shape

iimax = 0
jjmax = 0
htmax = 0.0
for ii in np.arange(orog.shape[0]):
	for jj in np.arange(orog.shape[1]):
		if orog[ii,jj] > htmax:
			iimax = ii
			jjmax = jj
			htmax = orog[ii,jj]

print 'max indices:',iimax,jjmax,htmax
nz = ht_rho.shape[0]

z_rho   = ht_rho[:nz-1,iimax,jjmax]
z_theta = ht_theta[:nz-1,iimax,jjmax]

path  = '/mnt/coe0/davidl1/Darwin_15_NoConv_SL02_3DNoBlend/'

day   = '020700'
hour  = 3
hrmod = hour%3
hrdiv = hour-hrmod

#velx  = ReadFile(path+day+'/velx_'+'%d'%hrdiv+'.nc','x-wind')[hrmod,:,yi:yf,xi:xf]
#vely  = ReadFile(path+day+'/vely_'+'%d'%hrdiv+'.nc','y-wind')[hrmod,:,yi:yf,xi:xf]
#velz  = ReadFile(path+day+'/velz_'+'%d'%hrdiv+'.nc','dz_dt')[hrmod,:,yi:yf,xi:xf]
velx  = ReadFile(path+day+'/velx_'+'%d'%hrdiv+'.nc','x-wind')[hrmod,:,20:-20,20:-20]
vely  = ReadFile(path+day+'/vely_'+'%d'%hrdiv+'.nc','y-wind')[hrmod,:,20:-20,20:-20]
velz  = ReadFile(path+day+'/velz_'+'%d'%hrdiv+'.nc','dz_dt')[hrmod,:,20:-20,20:-20]

dudx = np.zeros((velx.shape[0],velx.shape[1],velx.shape[2]-2))
dvdx = np.zeros((vely.shape[0],vely.shape[1],vely.shape[2]-2))
dwdx = np.zeros((velz.shape[0],velz.shape[1],velz.shape[2]-2))

dxInv = 0.5/1330.0
for ii in np.arange(dudx.shape[0]):
	for jj in np.arange(dudx.shape[1]):
		for kk in np.arange(dudx.shape[2]):
			dudx[ii,jj,kk] = (velx[ii,jj,kk+2] - velx[ii,jj,kk])*dxInv
			dvdx[ii,jj,kk] = (vely[ii,jj,kk+2] - vely[ii,jj,kk])*dxInv

for ii in np.arange(dwdx.shape[0]):
	for jj in np.arange(dwdx.shape[1]):
		for kk in np.arange(dwdx.shape[2]):
			dwdx[ii,jj,kk] = (velz[ii,jj,kk+2] - velz[ii,jj,kk])*dxInv

print 'ux:',np.min(dudx),np.max(dudx)
print 'vx:',np.min(dvdx),np.max(dvdx)
print 'wx:',np.min(dwdx),np.max(dwdx)

dudy = np.zeros((velx.shape[0],velx.shape[1]-2,velx.shape[2]))
dvdy = np.zeros((vely.shape[0],vely.shape[1]-2,vely.shape[2]))
dwdy = np.zeros((velz.shape[0],velz.shape[1]-2,velz.shape[2]))

dyInv = 0.5/1330.0
for ii in np.arange(dudy.shape[0]):
	for jj in np.arange(dudy.shape[1]):
		for kk in np.arange(dudy.shape[2]):
			dudy[ii,jj,kk] = (velx[ii,jj+2,kk] - velx[ii,jj,kk])*dyInv
			dvdy[ii,jj,kk] = (vely[ii,jj+2,kk] - vely[ii,jj,kk])*dyInv

for ii in np.arange(dwdy.shape[0]):
	for jj in np.arange(dwdy.shape[1]):
		for kk in np.arange(dwdy.shape[2]):
			dwdy[ii,jj,kk] = (velz[ii,jj+2,kk] - velz[ii,jj,kk])*dyInv

print 'uy:',np.min(dudy),np.max(dudy)
print 'vy:',np.min(dvdy),np.max(dvdy)
print 'wy:',np.min(dwdy),np.max(dwdy)

dudz = np.zeros((velx.shape[0]-2,velx.shape[1],velx.shape[2]))
dvdz = np.zeros((vely.shape[0]-2,vely.shape[1],vely.shape[2]))
dwdz = np.zeros((velz.shape[0]-2,velz.shape[1],velz.shape[2]))

for ii in np.arange(dudz.shape[0]):
	for jj in np.arange(dudz.shape[1]):
		for kk in np.arange(dudz.shape[2]):
			dudz[ii,jj,kk] = (velx[ii+2,jj,kk] - velx[ii,jj,kk])/(ht_rho[ii+2,jj,kk] - ht_rho[ii,jj,kk])
			dvdz[ii,jj,kk] = (vely[ii+2,jj,kk] - vely[ii,jj,kk])/(ht_rho[ii+2,jj,kk] - ht_rho[ii,jj,kk])

for ii in np.arange(dwdz.shape[0]):
	for jj in np.arange(dwdz.shape[1]):
		for kk in np.arange(dwdz.shape[2]):
			dwdz[ii,jj,kk] = (velz[ii+2,jj,kk] - velz[ii,jj,kk])/(ht_theta[ii+2,jj,kk] - ht_theta[ii,jj,kk])

print 'uz:',np.min(dudz),np.max(dudz)
print 'vz:',np.min(dvdz),np.max(dvdz)
print 'wz:',np.min(dwdz),np.max(dwdz)

levs = [200.0,400.0,600.0,800.0,1000.0,1200.0,1400.0,1600.0,1800.0,2000.0,2500.0,3000.0,4000.0,5000.0,6000.0]
dt   = 15.0

# interpolate to consistent height levels
dudzi  = dt*InterpZ(dudz, ht_rho[1:-1,:,:],  levs)
dvdzi  = dt*InterpZ(dvdz, ht_rho[1:-1,:,:],  levs)
dwdzi  = dt*InterpZ(dwdz, ht_theta[1:-1,:,:],levs)

for ii in np.arange(len(levs)):
	print 'generating plots at level: ',levs[ii]
	fig = plt.figure()
	ax  = fig.add_subplot(111)
	ax.contour(lon,lat,lsm,colors='k')
	co = ax.contourf(lon,lat,dudz[ii,:,:])
	plt.title('du/dz, '+'%2d'%ii)
	plt.colorbar(co,orientation='vertical')
	filename = 'plots/dudz_'+'%.2d'%ii+'.png'
	pylab.savefig(filename)	 

	fig = plt.figure()
	ax  = fig.add_subplot(111)
	ax.contour(lon,lat,lsm,colors='k')
	co = ax.contourf(lon,lat,dvdz[ii,:,:])
	plt.title('dv/dz, '+'%2d'%ii)
	plt.colorbar(co,orientation='vertical')
	filename = 'plots/dvdz_'+'%.2d'%ii+'.png'
	pylab.savefig(filename)	 

	fig = plt.figure()
	ax  = fig.add_subplot(111)
	ax.contour(lon,lat,lsm,colors='k')
	co = ax.contourf(lon,lat,dwdz[ii,:,:])
	plt.title('dw/dz, '+'%2d'%ii)
	plt.colorbar(co,orientation='vertical')
	filename = 'plots/cdwdz_'+'%.2d'%ii+'.png'
	pylab.savefig(filename)	 

