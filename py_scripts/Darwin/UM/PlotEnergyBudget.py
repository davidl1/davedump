#!/usr/bin/env python

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
from matplotlib import colors as cls
from matplotlib import ticker
import numpy as np
import pylab
import sys

hour = sys.argv[1]
print 'hour: ',hour

htf = +1.0*np.loadtxt('ht_hour'+hour+'_cloud.energybudget')
kef = +1.0*np.loadtxt('ke_hour'+hour+'_cloud.energybudget')
pef = +1.0*np.loadtxt('pe_hour'+hour+'_cloud.energybudget')
ief = +1.0*np.loadtxt('ie_hour'+hour+'_cloud.energybudget')
mef = -1.0*np.loadtxt('me_hour'+hour+'_cloud.energybudget')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(kef/60.0/60.0,htf,'-')
plt.plot(pef/60.0/60.0,htf,'-')
plt.plot(ief/60.0/60.0,htf,'-')
plt.plot(mef/60.0/60.0,htf,'-')
plt.xlabel('J')
plt.ylabel('height (m)')
plt.ylim([0,30000])
plt.legend(('kinetic','potential','internal','moist'),loc=1,prop={'size':14})
plt.title('Energy vertical profile (Blended), '+hour+'pm')
pylab.savefig('energy_blend_hour'+hour+'_cloud.png')

plt.show()
