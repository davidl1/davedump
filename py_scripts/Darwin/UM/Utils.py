from scipy.io import netcdf
from scipy.interpolate import griddata
import numpy as np

def ReadField(filename,fieldname):
	print 'from: ',filename,'\treading: ',fieldname
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

def ReadScalar(filename,varname):
	print 'from: ',filename,'\treading: ',varname
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[varname]
	infile.close()
	return field.getValue()

def InterpGrid(xin,yin,xout,yout,A,interpType):
	print 'interp type: ',interpType
	pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
	pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
	Ar      = np.ravel(A)
	Aor     = griddata(pts_in,Ar,pts_out,method=interpType)
	Ao      = Aor.reshape(len(yout),len(xout))
	return Ao

def CalcError(xm,ym,phim,xo,yo,phio):
	phim2 = InterpGrid(xm,ym,xo,yo,phim,'cubic')
	#err   = 0.0
	#norm  = 0.0
	#for ii in np.arange(len(yo)):
	#	for jj in np.arange(len(xo)):
	#		err  = err  + (phio[ii,jj]-phim2[ii,jj])*(phio[ii,jj]-phim2[ii,jj])
	#		norm = norm + phio[ii,jj]*phio[ii,jj]
	err  = (phio-phim2)*(phio-phim2)
	norm = phio*phio

	return np.sum(err)/np.sum(norm)

