#!/usr/bin/env python

# Generate the daily averaged diurnal cycle over the CPOL radar domain for land and sea
# NB: only use days for which CPOL data exists, and for which the precipitation is driven
# by diurnal and not large scale forcing

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
import numpy as np
import pylab

def ReadFile(filename,fieldname,isscalar):
        print 'reading file: ', filename
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
        if isscalar:
                return field.getValue()
        else:
                return field[:]

times = ['020200','020212','020400','020412','020500','020512','020700','020712','020900','020912']

filename = '../' + '020112' + '/lsm.nc'
lsm = ReadFile(filename,'lsm',False)
lon = ReadFile(filename,'longitude',False)
lat = ReadFile(filename,'latitude',False)
lsm = lsm[0,0,:,:]
nx  = len(lon)
ny  = len(lat)

# get the radar data to determine coverage...
dir_cpol  = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km/'
file_cpol = 'cpol_2d_rainrate_2010'
pcp_cpol  = ReadFile(dir_cpol+file_cpol+'0202_0000.nc','rr',False)
empty     = pcp_cpol[0,:,:]<-10
full      = (empty+1)%2
lon0_cpol = ReadFile(dir_cpol+file_cpol+'0201_0000.nc','radar_longitude',True)
lat0_cpol = ReadFile(dir_cpol+file_cpol+'0201_0000.nc','radar_latitude',True)
x         = ReadFile(dir_cpol+file_cpol+'0201_0000.nc','x',False)
y         = ReadFile(dir_cpol+file_cpol+'0201_0000.nc','y',False)
x         = 1000.0*x
y         = 1000.0*y
Re        = 6378100.0
lon_cpol  = lon0_cpol + x*360.0/2.0/np.pi/Re
lat_cpol  = lat0_cpol + y*360.0/2.0/np.pi/Re
# interpolate
pts0      = np.transpose([np.tile(lon_cpol,len(lat_cpol)),np.repeat(lat_cpol,len(lon_cpol))])
pts1      = np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
full      = np.ravel(full)
full      = griddata(pts0,full,pts1,method='nearest')
full      = full.reshape(ny,nx)

slm = (lsm+1)%2
lsm = full*lsm
slm = full*slm
nl  = np.sum(lsm)
ns  = np.sum(slm)

lrain = np.zeros(24)
srain = np.zeros(24)

hour = 0
for tt in np.arange(len(times)):
	hour = hour%24
	filename = '../' + times[tt] + '/precip.nc'
	precip   = ReadFile(filename,'precip',False)
	for hr in np.arange(precip.shape[0]):
		#rescale to mm
		rain = 60.0*60.0*precip[hr,0,:,:]

		lrain[hour+hr] = lrain[hour+hr] + np.sum(rain*lsm)
		srain[hour+hr] = srain[hour+hr] + np.sum(rain*slm)
			
	hour = hour + 12

avg_lrain = lrain/5/nl
avg_srain = srain/5/ns

fig=plt.figure()
ax=fig.add_subplot(111)
time=np.arange(24)
avg_lrain_2=np.zeros(24)
avg_srain_2=np.zeros(24)
GMT=9
for ii in np.arange(24):
	ind=(ii-GMT)%24
	avg_lrain_2[ii]=avg_lrain[ind]
	avg_srain_2[ii]=avg_srain[ind]

plt.plot(time,avg_lrain_2,'-o',time,avg_srain_2,'-o')
plt.legend(('land','sea'),loc=0)
plt.xlabel('local time (hrs)')
plt.ylabel('precipitation rate (mm)')
plt.xlim([0,23])
plt.title('precipitation vs. local time for land and sea (UM)')
pylab.savefig('avg_diurnal_cycle_um.png')
plt.show()
