#!/usr/bin/env python

# Calculate the mesoscale KE spectrum for high resolution nested model
# Generate from 1D east-west spectral averaged over latitude and height 
# (3-9km), excluding first 15 horizontal grid points in each dimension
#
# Process is:
#	1) Interpolate velocity fields to regular points in z
#	2) Remove the linear trend in the zonal (x) dimension using least
#	   squares
#	3) Filter out 10% of points on the edges in the zonal dimension 
#	   using a cosine bell filter
#	4) Perform the FFT in x
#	5) Calculate the KE of the data in x-fourier, y-real, z-real space
#	6) Average the KE in y and z dimensions
#	7) Rescale by the length of the domain in x
#
# Reference:
#	Skamarock, W. C. (2004) Evaluating Mesoscale NWP Models Using 
#	Kinetic Energy Spectra, Mon. Wea. Rev. 132, 3019-3032

import math
import numpy as np
from scipy.io import *
from scipy.fftpack import *
from scipy.interpolate import *
from matplotlib import pyplot as plt
import pylab

def ReadFile(filename,fieldname):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

def RemoveTrend(A,X):
	Y   = np.vstack([X,np.ones(len(X))]).T
	m,c = np.linalg.lstsq(Y,A)[0]
	L   = m*X+c
	return A-L

def FilterEdges(A,X):
	n       = X.shape[0]
	m       = n/10
	L       = 0.5-0.5*np.cos(np.pi*(X[:m]-X[0])/(X[m-1]-X[0]))
	R       = L[::-1] 
	B       = np.ones(n)
        B[:m]   = L
        B[n-m:] = R
	return B*A

def AvgFourTrans(A,z,mz,lon,lat,ht,orog):
	nx = A.shape[2]
	ny = A.shape[1]
	nz = A.shape[0]

	B     = np.zeros((mz,ny,nx))
	Z     = np.linspace(z[0],z[1],mz)
	httop = ht[-1]
	htinv = 1.0/(ht[-1]-ht[0])

	# interpolate to regular points in z
	print 'interpolating to regular points in z...'
	for ii in np.arange(ny):
		if ii%10 == 0:
			print ii,' of ',ny
		for jj in np.arange(nx):
			Zi         = orog[ii,jj] + ht*(httop-orog[ii,jj])*htinv
			B[:,ii,jj] = griddata(Zi,A[:,ii,jj],Z,method='cubic')


	# remove the linear trend, filter the edges and perform the fft
	C = np.zeros((mz,ny,nx),dtype=complex)
	for ii in np.arange(mz):
		for jj in np.arange(ny):
			c          = FilterEdges(RemoveTrend(B[ii,jj,:],lon),lon)
			#C[ii,jj,:] = rfft(c)
			C[ii,jj,:] = fft(c)

	return C

def AvgFourTrans2(A,z,mz,lon,lat,ht):
	nx = A.shape[2]
	ny = A.shape[1]
	nz = A.shape[0]

	B     = np.zeros((mz,ny,nx))
	Z     = np.linspace(z[0],z[1],mz)

	# interpolate to regular points in z
	print 'interpolating to regular points in z...'
	for ii in np.arange(ny):
		if ii%10 == 0:
			print ii,' of ',ny
		for jj in np.arange(nx):
			B[:,ii,jj] = griddata(ht[:,ii,jj],A[:,ii,jj],Z,method='cubic')


	# remove the linear trend, filter the edges and perform the fft
	C = np.zeros((mz,ny,nx),dtype=complex)
	for ii in np.arange(mz):
		for jj in np.arange(ny):
			c          = FilterEdges(RemoveTrend(B[ii,jj,:],lon),lon)
			#C[ii,jj,:] = rfft(c)
			C[ii,jj,:] = fft(c)

	return C

def Downscale1D(A):
	nx   = len(A)/2
	inds = 2*np.arange(nx)
	B    = A[inds]
	return B

def Downscale2D(A):
	print 'downscaing...',A.shape
	nx = A.shape[2]/2
	ny = A.shape[1]/2
	nz = A.shape[0]

	B = np.zeros((nz,ny,nx))

	for ii in np.arange(ny):
		for jj in np.arange(nx):
			B[:,ii,jj] = A[:,2*ii,2*jj]

	return B

velx = ReadFile('../020400/velx.nc','u')
vely = ReadFile('../020400/vely.nc','v')
velz = ReadFile('../020400/velz.nc','dz_dt')
lon  = ReadFile('../020400/velx.nc','longitude')
lat  = ReadFile('../020400/velx.nc','latitude')
ht   = ReadFile('../020112/ht_rho.nc','ht')

nx0 = len(lon)
ny0 = len(lat)
nz0 = ht.shape[1]
print nx0,ny0,nz0

xinds = 116+np.arange(nx0-231)
yinds = 116+np.arange(ny0-231)

xi = xinds[0]
xf = xinds[-1]
yi = yinds[0]
yf = yinds[-1]
velx = velx[0,:,yi:yf,xi:xf]
vely = vely[0,:,yi:yf,xi:xf]
velz = velz[0,:,yi:yf,xi:xf]
lon  = lon[xi:xf]
lat  = lat[yi:yf]
ht   = ht[0,:,yi:yf,xi:xf]

print velx.shape
print ht.shape

# downscale...
#lon  = Downscale1D(lon)
#lat  = Downscale1D(lat)
#velx = Downscale2D(velx)
#vely = Downscale2D(vely)
#velz = Downscale2D(velz)
#ht   = Downscale2D(ht)

x = [lon[0],lon[-1]]
y = [lat[0],lat[-1]]
z = [4000.0,9000.0]

nx1 = len(lon)
ny1 = len(lat)
nz1 = 20
print nx1,ny1

Re=6378100.0
dx=2.0*np.pi*Re*(x[1]-x[0])/360.0/nx1
#L=nx1*dx
#k=2.0*np.pi*8.0/L
print 'dx: ',dx
print 'x range: ',x[0],'\t',x[1]
print 'y range: ',y[0],'\t',y[1]
print nx1,ny1
#print '  k:       ',k
#print '  l:       ',L*k/2.0/np.pi
#print 'ln(k):     ',np.log10(k)

#average the vertical velocity to cell centers
print 'averaging vertical velocity to height levels...'
velz2 = np.zeros((velx.shape))
for jj in np.arange(ny1):
        for kk in np.arange(nx1):
                for ii in np.arange(nz0):
                        velz2[ii,jj,kk] = 0.5*(velz[ii,jj,kk] + velz[ii+1,jj,kk])

velz = velz2

Re              = 6378100.0
wavenum         = np.arange(nx1)
nxh             = nx1/2
wavenum[nxh+1:] = np.arange(nxh-1)-nxh+1
wavenum         = wavenum*2.0*np.pi/(2.0*np.pi*Re*(x[1]-x[0])/360.0)

U  = AvgFourTrans2(velx,z,nz1,lon,lat,ht)
V  = AvgFourTrans2(vely,z,nz1,lon,lat,ht)
W  = AvgFourTrans2(velz,z,nz1,lon,lat,ht)
KE = 0.5*(U.real*U.real + U.imag*U.imag + V.real*V.real + V.imag*V.imag + W.real*W.real + W.imag*W.imag)

# average in y and z
kez = np.zeros((ny1,nx1))
for ii in np.arange(ny1):
        for jj in np.arange(nx1):
                kez[ii,jj] = np.sum(KE[:,ii,jj])/nz1

keyz = np.zeros((nx1))
for ii in np.arange(nx1):
        keyz[ii] = np.sum(kez[:,ii])/ny1

kenergy = 2.0*np.pi*Re*(x[1]-x[0])/360.0*keyz

np.savetxt('wavenum_1d_fourier_full.txt',wavenum)
np.savetxt('kenergy_1d_fourier_full.txt',kenergy)

# plot a least squares fit to the data
#lk = np.log10(wavenum[1:len(wavenum)])
#le = np.log10(kenergy[1:len(kenergy)])
#A = np.vstack([lk,np.ones(len(lk))]).T
#m,c = np.linalg.lstsq(A,le)[0]
#print 'least squares fit... ',m,c
#print -5.0/3.0
#plt.plot(lk,le,'.',lk,m*lk+c,lk,-5.0/3.0*lk+c)
#plt.xlabel('$ln(||K||)$')
#plt.ylabel('$ln(E)$')
#plt.title('Vertically averaged 1D kinetic energy spectra')
#plt.legend(('UM spectra','Least squares fit','-5/3 cascade'),loc=3)

nh = nx1/2
k  = np.arange(nx1)
k[nh+1:] = np.arange(nh-1)-nh+1
plt.loglog(k[1:nh+1],kenergy[1:nh+1])
plt.loglog(k[1:nh+1],kenergy[1]*np.power(wavenum[1:nh+1],-5.0/3.0))
plt.loglog(k[nh+1:],kenergy[nh+1:])
plt.loglog([k[nh+1],k[nh+1]],[1.0e+5,1.0e+14])
#pylab.savefig('ke_spectra.png')
plt.show()
