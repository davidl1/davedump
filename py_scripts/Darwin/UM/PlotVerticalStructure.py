#!/usr/bin/env python

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
from matplotlib import colors as cls
from matplotlib import ticker
import numpy as np
import pylab

ht = np.loadtxt('ht_hour7_cloud.energybudget')
ke = np.loadtxt('ke_hour7_cloud.energybudget')
pe = np.loadtxt('pe_hour7_cloud.energybudget')
ie = np.loadtxt('ie_hour7_cloud.energybudget')
me = np.loadtxt('me_hour7_cloud.energybudget')

fig = plt.figure()
ax  = fig.add_subplot(111)
plt.plot(ke,ht,'-')
plt.plot(pe,ht,'-')
plt.plot(ie,ht,'-')
plt.plot(me,ht,'-')
plt.xlabel('J')
plt.ylabel('height (m)')
plt.ylim([0,30000])
plt.legend(('kinetic','potential','internal','moist'),loc=1,prop={'size':14})
plt.title('Energy budget vertical profile (Blended)')
pylab.savefig('energy_blend_hour7_cloud.png')

plt.show()
