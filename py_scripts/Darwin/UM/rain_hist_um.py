#!/usr/bin/env python

from scipy.io import *
from scipy.interpolate import *
import matplotlib.pyplot as plt
import numpy as np
import pylab

# Calculate a histogram of rainfall intensity for land, sea and full area for the CPOL radar domain
# NB: only use days for which CPOL data exists, and for which the precipitation is driven
# by diurnal and not large scale forcing

def ReadFile(filename,fieldname,isscalar):
	print 'reading file: ', filename
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	if isscalar:
		return field.getValue()
	else:
		return field[:]

times = ['020200','020212','020400','020412','020500','020512','020700','020712','020900','020912']

filename = '../' + '020112' + '/lsm.nc'
lsm = ReadFile(filename,'lsm',False)
lon = ReadFile(filename,'longitude',False)
lat = ReadFile(filename,'latitude',False)
lsm = lsm[0,0,:,:]
nx  = len(lon)
ny  = len(lat)

# get the radar data to determine coverage...
dir_cpol  = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km/'
file_cpol = 'cpol_2d_rainrate_2010'
pcp_cpol  = ReadFile(dir_cpol+file_cpol+'0202_0000.nc','rr',False)
empty     = pcp_cpol[0,:,:]<-10
full      = (empty+1)%2
lon0_cpol = ReadFile(dir_cpol+file_cpol+'0201_0000.nc','radar_longitude',True)
lat0_cpol = ReadFile(dir_cpol+file_cpol+'0201_0000.nc','radar_latitude',True)
x         = ReadFile(dir_cpol+file_cpol+'0201_0000.nc','x',False)
y         = ReadFile(dir_cpol+file_cpol+'0201_0000.nc','y',False)
x         = 1000.0*x
y         = 1000.0*y
Re        = 6378100.0
lon_cpol  = lon0_cpol + x*360.0/2.0/np.pi/Re
lat_cpol  = lat0_cpol + y*360.0/2.0/np.pi/Re
# interpolate
pts0      = np.transpose([np.tile(lon_cpol,len(lat_cpol)),np.repeat(lat_cpol,len(lon_cpol))])
pts1      = np.transpose([np.tile(lon,len(lat)),np.repeat(lat,len(lon))])
full      = np.ravel(full)
full      = griddata(pts0,full,pts1,method='nearest')
full      = full.reshape(ny,nx)

slm = (lsm+1)%2
nl  = np.sum(full*lsm)
ns  = np.sum(full*slm)

prange = 1
nbins  = 250

shist = np.zeros(nbins)
lhist = np.zeros(nbins)
thist = np.zeros(nbins)

full      = full.flatten()
for tt in np.arange(len(times)):
	filename = '../' + times[tt] + '/precip.nc'
	precip   = ReadFile(filename,'precip',False)
	rain     = np.zeros((ny,nx))
	lsm      = lsm.flatten()
	for hr in np.arange(precip.shape[0]):	
		#rescale to mm
		rain = 60.0*60.0*precip[hr,0,:,:]

		rain2 = rain.flatten()
		rain2 = rain2/prange
		for ii in np.arange(nx*ny):
			# apply the mask for the cpol domain
			if full[ii]:
				# increment the bin to which the grid point rainfall corresponds
				rbin = np.int(rain2[ii])
				thist[rbin] = thist[rbin] + 1
				if lsm[ii]:
					lhist[rbin] = lhist[rbin] + 1
				else:
					shist[rbin] = shist[rbin] + 1

lhist = 100*lhist/5/24/nl
shist = 100*shist/5/24/ns
thist = 100*thist/5/24/np.sum(full)

print np.sum(lhist)
print np.sum(shist)
print np.sum(thist)

fig=plt.figure()		
ax=fig.add_subplot(111)
rfall = prange*np.arange(nbins)
plt.semilogy(rfall,lhist,'-o',rfall,shist,'-o',rfall,thist,'-o')
plt.legend(('land','sea','total'),loc=0)
plt.xlabel('precipitation (mm)')
plt.ylabel('% of domain area')
plt.title('Rainfall Histogram (UM)')
pylab.savefig('rain_histogram.png')
plt.show()
