#!/usr/bin/env python

# skill score for rainfall based on structure, ampliture and location
# reference:
#	Wernli, Paulat, Hagen and Frei (2008) MWR, 136. 4470-4487

import numpy as np
from matplotlib import pyplot as plt
from scipy.io import netcdf
from scipy.interpolate import *

def LoadField(filename,fieldname):
        print 'from: ' + filename + '\tloading: ' + fieldname
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
        return field[:]

def ReadVariable(filename,fieldname):
	print 'from: ' + filename + '\tloading: ' + fieldname
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field.getValue()

def SearchFeature(A,C,S,xi,yi,Amin):
	if xi > 0:
		if A[yi,xi-1] > Amin and S[yi,xi-1]==0:
			S[yi,xi-1] = 1
			C[yi,xi-1] = 1
			SearchFeature(A,C,S,xi-1,yi,Amin)

	if xi < A.shape[1]-1:
		if A[yi,xi+1] > Amin and S[yi,xi+1]==0:
			S[yi,xi+1] = 1
			C[yi,xi+1] = 1
			SearchFeature(A,C,S,xi+1,yi,Amin)

	if yi > 0:
		if A[yi-1,xi] > Amin and S[yi-1,xi]==0:
			S[yi-1,xi] = 1
			C[yi-1,xi] = 1
			SearchFeature(A,C,S,xi,yi-1,Amin)

	if yi < A.shape[0]-1:
		if A[yi+1,xi] > Amin and S[yi+1,xi]==0:
			S[yi+1,xi] = 1
			C[yi+1,xi] = 1
			SearchFeature(A,C,S,xi,yi+1,Amin)

	if xi > 0 and yi > 0:
		if A[yi-1,xi-1] > Amin and S[yi-1,xi-1]==0:
			S[yi-1,xi-1] = 1
			C[yi-1,xi-1] = 1
			SearchFeature(A,C,S,xi-1,yi-1,Amin)

	if xi < A.shape[1]-1 and yi > 0:
		if A[yi-1,xi+1] > Amin and S[yi-1,xi+1]==0:
			S[yi-1,xi+1] = 1
			C[yi-1,xi+1] = 1
			SearchFeature(A,C,S,xi+1,yi-1,Amin)

	if xi > 0 and yi < A.shape[0]-1:
		if A[yi+1,xi-1] > Amin and S[yi+1,xi-1]==0:
			S[yi+1,xi-1] = 1
			C[yi+1,xi-1] = 1
			SearchFeature(A,C,S,xi-1,yi+1,Amin)

	if xi < A.shape[1]-1 and yi < A.shape[0]-1:
		if A[yi+1,xi+1] > Amin and S[yi+1,xi+1]==0:
			S[yi+1,xi+1] = 1
			C[yi+1,xi+1] = 1
			SearchFeature(A,C,S,xi+1,yi+1,Amin)

def FindFeature(A,Amin):
	xi = 0
	yi = 0
	Amax = 0.0
	for ii in np.arange(A.shape[0]):
		for jj in np.arange(A.shape[1]):
			if A[ii,jj] > Amax:
				xi = jj
				yi = ii
				Amax = A[ii,jj]

	S = np.zeros(A.shape)
	C = np.zeros(A.shape)
	S[yi,xi] = 1
	C[yi,xi] = 1

	SearchFeature(A,C,S,xi,yi,Amin)
	return C

def Avg(A):
	return np.sum(A)/A.shape[0]/A.shape[1]

def COM(lon,lat,A):
	Ax=np.zeros(len(lon))
	for ii in np.arange(len(lon)):
		Ax[ii] = np.sum(A[:,ii])/len(lat)

	x = np.sum(lon*Ax)/np.sum(Ax)

	Ay=np.zeros(len(lat))
	for ii in np.arange(len(lat)):
		Ay[ii] = np.sum(A[ii,:])/len(lon)

	y = np.sum(lat*Ay)/np.sum(Ay)

	return [x,y]

def ScaleVol(A):
	return np.sum(A)/np.max(A)

lat_m = LoadField('../020212/precip.nc','latitude')
lon_m = LoadField('../020212/precip.nc','longitude')
pcp_m = LoadField('../020212/precip.nc','precip')
pcp_m = 60.0*60.0*pcp_m[0,0,:,:]

dir_o  = '/mnt/coe0/davidl1/Darwin_50/CPOL/Feb10_150_2.5km/'
file_o = 'cpol_2d_rainrate_2010'
lat0_o = ReadVariable(dir_o+file_o+'0202_1200.nc','radar_latitude')
lon0_o = ReadVariable(dir_o+file_o+'0202_1200.nc','radar_longitude')
x      = LoadField(dir_o+file_o+'0201_0000.nc','x')
y      = LoadField(dir_o+file_o+'0201_0000.nc','y')
x      = 1000.0*x
y      = 1000.0*y
Re     = 6378100.0
lon_o  = lon0_o + x*360.0/2.0/np.pi/Re
lat_o  = lat0_o + y*360.0/2.0/np.pi/Re
pcp_o  = LoadField(dir_o+file_o+'0202_1200.nc','rr')
pcp_o  = pcp_o[0,:,:]
empty  = pcp_o[:,:]<-10
full   = (empty+1)%2
pcp_o  = full*pcp_o

lsm    = LoadField('../020112/lsm.nc','lsm');
lsm    = lsm[0,0,:,:]

# interpolate the radar mask to the model points
pts0   = np.transpose([np.tile(lon_o,len(lat_o)),np.repeat(lat_o,len(lon_o))])
pts1   = np.transpose([np.tile(lon_m,len(lat_m)),np.repeat(lat_m,len(lon_m))])
full_r = np.ravel(full)
full_m = griddata(pts0,full_r,pts1,method='nearest')
full_m = full_m.reshape(len(lat_m),len(lon_m))

pcp_m  = full_m*pcp_m

cloud_o = FindFeature(pcp_o,0.8*np.max(pcp_o))
cloud_m = FindFeature(pcp_m,0.8*np.max(pcp_o))

pcp_c_m = cloud_m*pcp_m
pcp_c_o = cloud_o*pcp_o

print lon_m.shape
print lat_m.shape
print lsm.shape

xi = 400
xf = -369

fig=plt.figure()
ax=fig.add_subplot(111)
ax.contour(lon_m[xi:xf],lat_m[xi:xf],lsm[xi:xf,xi:xf],colors='k')
co=ax.contourf(lon_o,lat_o,pcp_c_o)
plt.colorbar(co,orientation='vertical')
plt.title('feature - CPOL')

fig=plt.figure()
ax=fig.add_subplot(111)
ax.contour(lon_m[xi:xf],lat_m[xi:xf],lsm[xi:xf,xi:xf],colors='k')
co=ax.contourf(lon_m[xi:xf],lat_m[xi:xf],pcp_c_m[xi:xf,xi:xf])
plt.colorbar(co,orientation='vertical')
plt.title('feature - UM')
plt.show()

# amplitude
A = 2.0*(Avg(pcp_c_m) - Avg(pcp_c_o))/(Avg(pcp_c_m) + Avg(pcp_c_o))

# center of mass
[xo,yo] = COM(lon_o,lat_o,pcp_c_o)
[xm,ym] = COM(lon_m,lat_m,pcp_c_m)

L = np.sqrt((xo-xm)*(xo-xm) + (yo-ym)*(yo-ym))/\
    np.sqrt((lon_o[-1]-lon_o[0])*(lon_o[-1]-lon_o[0]) + (lat_o[-1]-lat_o[0])*(lat_o[-1]-lat_o[0]))

S = 2.0*(ScaleVol(pcp_c_m) - ScaleVol(pcp_c_o))/(ScaleVol(pcp_c_m) + ScaleVol(pcp_c_o))

print 'S: ',S
print 'A: ',A
print 'L: ',L
