#!/usr/bin/env python

#module load pythonlib/ScientificPython

from Scientific.IO.NetCDF import NetCDFFile
import matplotlib.pyplot as plt
import numpy as np
import pylab
from Utils import *

land_data = LoadField( 'LandSeaMask.nc', 'lsm' )
lat_data  = LoadField( 'LandSeaMask.nc', 'latitude' )
lon_data  = LoadField( 'LandSeaMask.nc', 'longitude' )

time_data = LoadField( 'Precip1_00.nc', 't' )
t = 24.0*time_data
precip_data = LoadField( 'Precip1_00.nc', 'precip' )
print precip_data.shape

nx=len(lon_data)
ny=len(lat_data)

nSteps=precip_data.shape[0]
nStepsPerPeriod=1728
dt=1.0*24.0*60.0*60.0/nStepsPerPeriod
print dt

time=np.zeros(nSteps*nx)
land=np.zeros(nSteps*nx)
sea=np.zeros(nSteps*nx)

#calculate the rain over land and sea - weighted by the latitude for different grid sizes
Re=6371000.0
for ii in np.arange(nSteps):
	print ii
	time[ii*nx:(ii+1)*nx]=t[ii]+lon_data/360
	for jj in np.arange(nx):
		lArea=0.0
		sArea=0.0
		for kk in 1+np.arange(ny-2):
			li=0.5*(lat_data[kk-1]+lat_data[kk])
			lf=0.5*(lat_data[kk]+lat_data[kk+1])
			yi=Re*np.cos(np.pi*li/180)
			yf=Re*np.cos(np.pi*lf/180)
			dy=abs(yf-yi)
			if land_data[0,0,kk,jj] == 1:
				lArea=lArea+dy
				land[ii*nx+jj]=land[ii*nx+jj]+precip_data[ii,0,kk,jj]*dy
			else:
				sArea=sArea+dy
				sea[ii*nx+jj]=sea[ii*nx+jj]+precip_data[ii,0,kk,jj]*dy
		land[ii*nx+jj]=land[ii*nx+jj]/lArea
		sea[ii*nx+jj]=sea[ii*nx+jj]/sArea

#plot the fields
fig=plt.figure()
ax=fig.add_subplot(111)
plt.plot(time,land,'o',time,sea,'o')
plt.legend(('land','sea'),loc=0)
plt.xlabel('local time (hrs)')
plt.ylabel('precipitation rate (kg/m^2/s)')
plt.title('precipitation vs. local time for land and sea')
pylab.savefig('precip_v_time.png')
plt.show()
