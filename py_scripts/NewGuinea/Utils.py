#!/usr/bin/env python

from Scientific.IO.NetCDF import NetCDFFile

def LoadField( filename, fieldname ):
	print 'from: ' + filename + '\treading: ' + fieldname
	filep = NetCDFFile( filename, 'r' )
	field = filep.variables[fieldname]
	return field.getValue()
	filep.close()
