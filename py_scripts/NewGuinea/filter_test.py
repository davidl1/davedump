#!/usr/bin/env python

#filter using the second order commutative scheme of vasilyev et al, 98
#to load scientific libraries on raijin: 'model load pythonlib/ScientificPython'

import numpy as np
import scipy.interpolate as interp
from matplotlib import pyplot as plt
import pylab

def forward_map(x0,l):
	return np.arctanh(x0/(l+0.001*l))

def backward_map(x1,l):
	return (l+0.001*l)*np.tanh(x1)

def filter2(Y1):
	n1=len(Y1)
	Y1f=np.zeros(n1)
	for ii in np.arange(n1-2)+1:
		Y1f[ii]=0.25*Y1[ii-1]+0.5*Y1[ii]+0.5*Y1[ii+1]

	Y1f[0]=0.875*Y1[0]+0.375*Y1[1]-0.375*Y1[2]+0.125*Y1[3]
	Y1f[n1-1]=0.875*Y1[n1-1]+0.375*Y1[n1-2]-0.375*Y1[n1-3]+0.125*Y1[n1-4]

	return Y1f

def filter4(Y1):
	n1=len(Y1)
	Y1f=np.zeros(n1)
	for ii in np.arange(n1-4)+2:
		Y1f[ii]=-0.0625*Y1[ii-2]+0.25*Y1[ii-1]+0.625*Y1[ii]+0.25*Y1[ii+1]-0.0625*Y1[ii+2]

	Y1f[0]=0.875*Y1[0]+0.375*Y1[1]-0.375*Y1[2]+0.125*Y1[3]
	Y1f[1]=0.125*Y1[0]+0.625*Y1[1]+0.375*Y1[2]-0.125*Y1[3]
	Y1f[n1-1]=0.875*Y1[n1-1]+0.375*Y1[n1-2]-0.375*Y1[n1-3]+0.125*Y1[n1-4]
	Y1f[n1-2]=0.125*Y1[n1-1]+0.625*Y1[n1-2]+0.375*Y1[n1-3]-0.125*Y1[n1-4]

	return Y1f
	

n=256
l=np.pi
X=l*np.cos(np.pi*np.arange(n+1)/n)
Y=3*np.cos(5.5*X) + 2.5*np.cos(8.5*X) + 4*np.sin(11*X) - np.cos(14.5*X) + 2*np.sin(19*X)
#plt.plot(X,Y)

#map to regular points in eta-space and interpolate to those points
n1=200
Eta0=forward_map(X,l)
Eta1=np.linspace(min(Eta0),max(Eta0),num=n1)
X1=backward_map(Eta1,l)
Y1=interp.griddata(X,Y,X1,method='cubic')
#plt.plot(X,Y,X1,Y1,'o')

plt.plot(X,Y,X1,Y1,X1,filter2(Y1),X1,filter4(Y1))
pylab.rcParams.update({'legend.fontsize':10,'legend.linewidth':1})
plt.legend(('orig. at cgl pts','orig. at filter pts.','filtered 2nd ord.','filtered 4th ord.'),loc=0)
plt.show()
