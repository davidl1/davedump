#!/usr/bin/env python

# calculate the fractional skill score for rainfall data against reference 
# at varying scales
# reference:
#	Roberts and Lean (2008) MWR: 136, 78-97

import numpy as np
from scipy.io import *
from scipy.interpolate import *
from matplotlib import pyplot as plt
import pylab
from ChebTrans import *

def LoadField(filename,fieldname):
        print 'from: ' + filename + '\tloading: ' + fieldname
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
        return field[:]

def InterpGrid( xin, yin, xout, yout, A, interpType ):
	print 'interpolating, nx: ', len(xout), '\tny: ', len(yout), '\ttype: ', interpType
	pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
	pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
	Ar      = np.ravel(A)
	Aor     = griddata(pts_in,Ar,pts_out,method=interpType)
	Ao      = Aor.reshape(len(yout),len(xout))
	return Ao

lat_m = LoadField('../020212/precip_2_020212.nc','latitude')
lon_m = LoadField('../020212/precip_2_020212.nc','longitude')
pcp_m = np.zeros((len(lat_m),len(lon_m)))
times = ['020200','020212','020300','020312','020400','020412','020500','020512']
for time in times:
	pcp = LoadField('../'+time+'/precip_2_'+time+'.nc','precip')
	for tt in np.arange(pcp.shape[0]):
		pcp_m = pcp_m + 60.0*60.0*pcp[tt,0,:,:]

lat_o = LoadField('../TRMM/3B42.20100202.12.7.nc','latitude')
lon_o = LoadField('../TRMM/3B42.20100202.12.7.nc','longitude')
pcp_o = np.zeros((len(lat_o),len(lon_o)))
for day in np.arange(4)+2:
	for time in 3*np.arange(8):
		pcp   = LoadField('../TRMM/3B42.201002'+'%.2u'%day+'.'+'%.2u'%time+'.7.nc','pcp')
		mask  = pcp[0,:,:]>0.0
		pcp_o = pcp_o + 3*mask*pcp[0,:,:]

xinds = np.arange(106)+1228
yinds = np.arange(84)+127
lon_o = lon_o[xinds]
lat_o = lat_o[yinds]
pcp_o = pcp_o[yinds[0]:yinds[-1]+1,xinds[0]:xinds[-1]+1]

lsm_m = LoadField('../020112/lsm_020112.nc','lsm');
lsm_m = lsm_m[0,0,:,:]

# interpolate model data to observation grid
xhalf   = 0.5*(lon_o[-2]-lon_o[1])
yhalf   = 0.5*(lat_o[-2]-lat_o[1])
xorig   = 0.5*(lon_o[-2]+lon_o[1])
yorig   = 0.5*(lat_o[-2]+lat_o[1])
nx      = len(lon_o)-1
ny      = len(lat_o)-1
lon     = xorig + xhalf*np.cos(np.pi*np.arange(nx+1)/nx)
lat     = yorig + yhalf*np.cos(np.pi*np.arange(ny+1)/ny)
pcp_m_c = InterpGrid(lon_m,lat_m,lon,lat,pcp_m,'cubic')
pcp_o_c = InterpGrid(lon_o,lat_o,lon,lat,pcp_o,'cubic')
lsm_o   = InterpGrid(lon_m,lat_m,lon_o,lat_o,lsm_m,'nearest')
lsm_c   = InterpGrid(lon_m,lat_m,lon,lat,lsm_m,'nearest')

fig=plt.figure()
ax=fig.add_subplot(111)
ax.contour(lon_o,lat_o,lsm_o,colors='k')
co=ax.contourf(lon_o,lat_o,pcp_o)
plt.colorbar(co,orientation='horizontal')
plt.title('precip - TRMM')

fig=plt.figure()
ax=fig.add_subplot(111)
ax.contour(lon_m,lat_m,lsm_m,colors='k')
co=ax.contourf(lon_m,lat_m,pcp_m)
plt.colorbar(co,orientation='horizontal')
plt.title('precip - UM')

fig=plt.figure()
ax=fig.add_subplot(111)
ax.contour(lon,lat,lsm_c,colors='k')
co=ax.contourf(lon,lat,pcp_o_c)
plt.colorbar(co,orientation='horizontal')
plt.title('precip - TRMM (cheb)')

fig=plt.figure()
ax=fig.add_subplot(111)
ax.contour(lon,lat,lsm_c,colors='k')
co=ax.contourf(lon,lat,pcp_m_c)
plt.colorbar(co,orientation='horizontal')
plt.title('precip - UM (cheb)')

# transform
pcp_m_cheb = InvChebTrans2D(pcp_m_c)
pcp_o_cheb = InvChebTrans2D(pcp_o_c)

# normalise...
m_max = np.max(pcp_m_cheb)
m_min = np.min(pcp_m_cheb)
if np.abs(m_min) > m_max:
	m_max = np.abs(m_min)

pcp_m_cheb = pcp_m_cheb/m_max

o_max = np.max(pcp_o_cheb)
o_min = np.min(pcp_o_cheb)
if np.abs(o_min) > o_max:
	o_max = np.abs(o_min)

pcp_o_cheb = pcp_o_cheb/o_max

Re = 6378100.0
xhalf = xhalf*2.0*np.pi*Re/360.0
yhalf = yhalf*2.0*np.pi*Re/360.0
print xhalf,yhalf

xlambda     = np.zeros(nx+1)
xlambda[0]  = 2*xhalf
xlambda[1:] = xhalf/(np.arange(nx)+1)
ylambda     = np.zeros(ny+1)
ylambda[0]  = 2*yhalf
ylambda[1:] = yhalf/(np.arange(ny)+1)

plt.figure()
ax=fig.add_subplot(111)
cm=plt.pcolormesh(pcp_m_cheb,vmin=-1.0,vmax=+1.0)
plt.xlim([0,105])
plt.ylim([0,83])
plt.colorbar(cm,orientation='horizontal')
plt.title('Chebyshev Transform, UM')
pylab.savefig('rain_cheb_um.png')

plt.figure()
ax=fig.add_subplot(111)
cm=plt.pcolormesh(pcp_o_cheb,vmin=-1.0,vmax=+1.0)
plt.xlim([0,105])
plt.ylim([0,83])
plt.colorbar(cm,orientation='horizontal')
plt.title('Chebyshev Transform, TRMM')
pylab.savefig('rain_cheb_trmm.png')

plt.figure()
ax=fig.add_subplot(111)
cm=plt.pcolormesh(np.abs(pcp_o_cheb)-np.abs(pcp_m_cheb))
plt.xlim([0,105])
plt.ylim([0,83])
plt.colorbar(cm,orientation='horizontal')
plt.title('Chebyshev Transform Error (|TRMM|-|UM|)')
pylab.savefig('rain_cheb_diff.png')

plt.show()
