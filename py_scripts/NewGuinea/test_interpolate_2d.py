#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from ChebTrans import *
from Interp3D import *

def f(x,y):
	#return np.outer(np.cos(5*np.pi*y),np.cos(4*np.pi*x))
	return 1.0*np.outer(x*y,y) + 3.0*np.outer(0.5*(x-1)/np.cosh(y),2*np.cos(2*np.pi*x)*y)

def Interp2D(Z0,X0,Y0,X1,Y1,ord):
	nx0 = len(X0)
	ny0 = len(Y0)
	nx1 = len(X1)
	ny1 = len(Y1)

	Zy = np.zeros((ny1,nx0))
        for ii in np.arange(ny1):
		for jj in np.arange(nx0):
			Ai = Z0[:,jj]
			Zy[ii,jj] = Interp1D(Ai,Y0,Y1[ii],ord)

	Zx = np.zeros((ny1,nx1))
	for ii in np.arange(ny1):
		for jj in np.arange(nx1):
			Ai = Zy[ii,:]
			Zx[ii,jj] = Interp1D(Ai,X0,X1[jj],ord)

	return Zx

nx0 = 200
ny0 = 200
lx0 = 2.0
ly0 = 2.0
X0 = lx0*(1.0*np.arange(nx0+1)/nx0-0.5)
Y0 = ly0*(1.0*np.arange(ny0+1)/ny0-0.5)
Z0 = f(X0,Y0)

nx1 = 128
ny1 = 128
lx1 = 0.5*lx0
ly1 = 0.5*ly0
X1 = -0.5*lx1*np.cos(np.pi*np.arange(nx1+1)/nx1)
Y1 = -0.5*ly1*np.cos(np.pi*np.arange(ny1+1)/ny1)

ord = 9

Z1 = np.zeros((ny1+1,nx1+1))
Z1 = Interp2D(Z0,X0,Y0,X1,Y1,ord)

fig = plt.figure()
ax = fig.add_subplot(111)
co = ax.contourf(X0[50:151],Y0[50:151],Z0[50:151,50:151],20,linewidth=0.05)
plt.title('original field')
plt.colorbar(co)

fig = plt.figure()
ax = fig.add_subplot(111)
co = ax.contourf(X1,Y1,Z1,20,linewidth=0.05)
plt.title('interpolated field')
plt.colorbar(co)

C0=np.zeros(nx0+1)
for ii in np.arange(nx0+1):
	C0[ii] = Z0[ii,ii]
C1=np.zeros(nx1+1)
for ii in np.arange(nx1+1):
	C1[ii] = Z1[ii,ii]
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(X0[50:151],C0[50:151],X1,C1,'o')
plt.legend(('orig','interp'),loc=0)
plt.title('xy-crosssection')

print X0[50],X1[0]
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(Y0[50:151],Z0[50:151,50],Y1,Z1[:,0],'o')
plt.legend(('orig','interp'),loc=0)
plt.title('x-crosssection')
plt.show()
