#!/usr/bin/env python

#module load pythonlib/ScientificPython

from Scientific.IO.NetCDF import NetCDFFile
import matplotlib.pyplot as plt
import numpy as np
import pylab
from Utils import *

times = ['020112','020200','020212','020300','020312','020400','020412','020500','020512']

filename = '../' + times[0] + '/lsm_' + times[0] + '.nc'
lsm = LoadField(filename,'lsm')
lon = LoadField(filename,'longitude')
lat = LoadField(filename,'latitude')

nx=len(lon)
ny=len(lat)

times = times[1:]

filename = '../' + times[0] + '/precip_2_' + times[0] + '.nc'
ltime = LoadField(filename,'t')

#xi  = 75
#xf  = nx-75
#lon = lon[xi:xf]
#lsm = lsm[0,0,:,xi:xf]
#slm = (lsm+1)%2
#nx  = len(lon)
#dx  = nx/9

xi    = 33
xf    = nx-179
yf    = ny-53
lon   = lon[xi:xf]
lat   = lat[0:yf]
lsm   = lsm[0,0,0:yf,xi:xf]
slm   = (lsm+1)%2
nx    = len(lon)
ny    = len(lat)
dx    = np.zeros(7)+nx/7
dx[6] = dx[6]+nx%7
tspan  = 24.0*(lon[-1]-lon[0])/360.0
print 'time span: ',tspan
print 'long span: ',lon[0],'\t',lon[-1]
print 'lat span:  ',lat[0],'\t',lat[-1]

nl  = np.zeros(nx)
ns  = np.zeros(nx)
for ii in np.arange(nx):
	nl[ii] = np.sum(lsm[:,ii])
	ns[ii] = np.sum(slm[:,ii])

# break the time series up into 15 minute segments
lrain = np.zeros(4*24)
srain = np.zeros(4*24)
larea = np.zeros(4*24)
sarea = np.zeros(4*24)

hour = 0
for tt in np.arange(len(times)):
	hour = hour%24
	filename = '../' + times[tt] + '/precip_2_' + times[tt] + '.nc'
	precip   = LoadField(filename,'precip')
	for hr in np.arange(precip.shape[0]):
		#rescale to mm
		rain = 60.0*60.0*precip[hr,0,0:yf,xi:xf]
		lr   = np.zeros(7)
		sr   = np.zeros(7)
		la   = np.zeros(7)
		sa   = np.zeros(7)
		for ii in np.arange(7):
			for jj in np.arange(dx[ii]) + ii*dx[0]:
				lr[ii] = lr[ii] + np.sum(rain[:,jj]*lsm[:,jj])
				sr[ii] = sr[ii] + np.sum(rain[:,jj]*slm[:,jj])
				la[ii] = la[ii] + nl[jj]
				sa[ii] = sa[ii] + ns[jj]

		inds = np.arange(7) + 4*(hr+1 + hour)
		inds = inds%(4*24)
		lrain[inds] = lrain[inds] + lr
		srain[inds] = srain[inds] + sr
		larea[inds] = larea[inds] + la
		sarea[inds] = sarea[inds] + sa
			
	hour = hour + 12

avg_lrain = np.zeros(24)
avg_srain = np.zeros(24)
for ii in np.arange(24):
	inds = np.arange(4) + 4*ii
	avg_lrain[ii] = np.sum(lrain[inds]/larea[inds])/4
	avg_srain[ii] = np.sum(srain[inds]/sarea[inds])/4

fig=plt.figure()
ax=fig.add_subplot(111)
#plt.plot(0.2*np.arange(5*24),lrain/larea,'-o',0.2*np.arange(5*24),srain/sarea,'-o')
time=np.arange(24)
avg_lrain_2=np.zeros(24)
avg_srain_2=np.zeros(24)
GMT=9
for ii in np.arange(24):
	ind=(ii-GMT)%24
	avg_lrain_2[ii]=avg_lrain[ind]
	avg_srain_2[ii]=avg_srain[ind]

plt.plot(time,avg_lrain_2,'-o',time,avg_srain_2,'-o')
plt.legend(('land','sea'),loc=0)
plt.xlabel('local time (hrs)')
plt.ylabel('precipitation rate (mm)')
plt.xlim([0,23])
plt.title('precipitation vs. local time for land and sea (UM)')
pylab.savefig('avg_diurnal_cycle.png')
plt.show()
