#!/usr/bin/env python

#module load pythonlib/ScientificPython

import numpy as np
from scipy.io import netcdf
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import pylab

def ReadFile(filename,fieldname):
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
	return field[:]

def InterpGrid(xin,yin,xout,yout,A,interpType):
        print 'interp type: ',interpType
        pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
        pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
        Ar      = np.ravel(A)
        Aor     = griddata(pts_in,Ar,pts_out,method=interpType)
        Ao      = Aor.reshape(len(yout),len(xout))
        return Ao

directory = '/scratch/hassimme/WRF/New_Guinea/d03/PRCP/'
filename  = 'prcp_d03_2010-02-'
timedump  = ['02_01:00:00','02_02:00:00','02_03:00:00','02_04:00:00','02_05:00:00','02_06:00:00',
             '02_07:00:00','02_08:00:00','02_09:00:00','02_10:00:00','02_11:00:00','02_12:00:00',
             '02_13:00:00','02_14:00:00','02_15:00:00','02_16:00:00','02_17:00:00','02_18:00:00',
             '02_19:00:00','02_20:00:00','02_21:00:00','02_22:00:00','02_23:00:00','03_00:00:00',
             '03_01:00:00','03_02:00:00','03_03:00:00','03_04:00:00','03_05:00:00','03_06:00:00',
             '03_07:00:00','03_08:00:00','03_09:00:00','03_10:00:00','03_11:00:00','03_12:00:00',
             '03_13:00:00','03_14:00:00','03_15:00:00','03_16:00:00','03_17:00:00','03_18:00:00',
             '03_19:00:00','03_20:00:00','03_21:00:00','03_22:00:00','03_23:00:00','04_00:00:00',
             '04_01:00:00','04_02:00:00','04_03:00:00','04_04:00:00','04_05:00:00','04_06:00:00',
             '04_07:00:00','04_08:00:00','04_09:00:00','04_10:00:00','04_11:00:00','04_12:00:00',
             '04_13:00:00','04_14:00:00','04_15:00:00','04_16:00:00','04_17:00:00','04_18:00:00',
             '04_19:00:00','04_20:00:00','04_21:00:00','04_22:00:00','04_23:00:00','05_00:00:00',
             '05_01:00:00','05_02:00:00','05_03:00:00','05_04:00:00','05_05:00:00','05_06:00:00',
             '05_07:00:00','05_08:00:00','05_09:00:00','05_10:00:00','05_11:00:00','05_12:00:00',
             '05_13:00:00','05_14:00:00','05_15:00:00','05_16:00:00','05_17:00:00','05_18:00:00',
             '05_19:00:00','05_20:00:00','05_21:00:00','05_22:00:00','05_23:00:00','06_00:00:00']

lat  = ReadFile(directory+'XLAT_d03_NewGuinea.nc','XLAT')
lon  = ReadFile(directory+'XLONG_d03_NewGuinea.nc','XLONG')
hgt  = ReadFile(directory+'TERRAIN_HGT_d03_NewGuinea.nc','HGT')
lsm  = ReadFile(directory+'LANDMASK_d03_NewGuinea.nc','LANDMASK')

lat = lat[0,:,0]
lon = lon[0,0,:]
hgt = hgt[0,:,:]
lsm = lsm[0,:,:]

xi  = 10
xf  = len(lon)-xi
yi  = 71
yf  = len(lat)-1
lon = lon[xi:xf]
lat = lat[yi:yf]
lsm = lsm[yi:yf,xi:xf]
slm = (lsm+1)%2
nx  = len(lon)
ny  = len(lat)
print 'long span (degs): ',lon[0],'\t',lon[-1]
print 'lat  span (degs): ',lat[0],'\t',lat[-1]

nl  = np.zeros(nx)
ns  = np.zeros(nx)
for ii in np.arange(nx):
	nl[ii] = np.sum(lsm[:,ii])
	ns[ii] = np.sum(slm[:,ii])
nl = np.sum(nl)
ns = np.sum(ns)

nbins  = 400
prange = 1

shist = np.zeros(nbins)
lhist = np.zeros(nbins)
thist = np.zeros(nbins)

precip_c0  = ReadFile(directory+filename+'02_00:00:00','RAINC')
precip_nc0 = ReadFile(directory+filename+'02_00:00:00','RAINNC')

lon_trmm   = np.loadtxt('lon_newguinea_trmm.txt')
lat_trmm   = np.loadtxt('lat_newguinea_trmm.txt')
#lon_trmm   = lon_trmm[:-2]
#lat_trmm   = lat_trmm[4:]
lsm_trmm   = InterpGrid(lon,lat,lon_trmm,lat_trmm,lsm,'nearest')
lsm_trmm   = lsm_trmm.flatten()
slm_trmm   = (lsm_trmm+1)%2
nl_trmm    = np.sum(lsm_trmm)
ns_trmm    = np.sum(slm_trmm)

print 'lon (wrf):  ',lon[0],lon[-1]
print 'lon (trmm): ',lon_trmm[0],lon_trmm[-1]
print 'lat (wrf):  ',lat[0],lat[-1]
print 'lat (trmm): ',lat_trmm[0],lat_trmm[-1]

#for tt in np.arange(len(times)):
rain = np.zeros((ny,nx))
for tt in np.arange(96):
	print 'step: ',tt
	precip_c1  = ReadFile(directory+filename+timedump[tt],'RAINC')
	precip_nc1 = ReadFile(directory+filename+timedump[tt],'RAINNC')

	rain       = rain + precip_c1[0,yi:yf,xi:xf] + precip_nc1[0,yi:yf,xi:xf] - precip_c0[0,yi:yf,xi:xf] - precip_nc0[0,yi:yf,xi:xf]

	if tt%3 == 0:
		#rain2 = rain.flatten()
		#rain2 = rain2/prange/3
		#lsm   = lsm.flatten()
		#for ii in np.arange(nx*ny):
		#	rbin = np.int(rain2[ii])
		#	thist[rbin] = thist[rbin] + 1
		#	if lsm[ii]:
		#		lhist[rbin] = lhist[rbin] + 1
		#	else:
		#		shist[rbin] = shist[rbin] + 1

		#rain  = np.zeros((ny,nx))

		rain_t = InterpGrid(lon,lat,lon_trmm,lat_trmm,rain,'cubic')
		rain2  = rain_t.flatten()
		rain2  = rain2/prange/3
		for ii in np.arange(len(rain2)):
			rbin = np.int(rain2[ii])
			thist[rbin] = thist[rbin] + 1
			if lsm_trmm[ii]:
				lhist[rbin] = lhist[rbin] + 1
			else:
				shist[rbin] = shist[rbin] + 1

		rain  = np.zeros((ny,nx))

	precip_c0  = precip_c1
	precip_nc0 = precip_nc1

#lhist = 100*lhist/32/nl
#shist = 100*shist/32/ns
#thist = 100*thist/32/(nx*ny)
lhist = 100*lhist/32/nl_trmm
shist = 100*shist/32/ns_trmm
thist = 100*thist/32/(len(lon_trmm)*len(lat_trmm))

print np.sum(lhist)
print np.sum(shist)
print np.sum(thist)

np.savetxt('rain_hist_l_wrf_dscale.txt',lhist)
np.savetxt('rain_hist_s_wrf_dscale.txt',shist)
np.savetxt('rain_hist_t_wrf_dscale.txt',thist)

fig=plt.figure()		
ax=fig.add_subplot(111)
rfall = prange*np.arange(nbins)
plt.semilogy(rfall,thist,'-o',rfall,lhist,'-o',rfall,shist,'-o')
plt.legend(('total','land','sea'),loc=0)
plt.xlabel('precipitation (mm)')
plt.ylabel('% of domain area')
plt.title('rainfall histogram (WRF)')
pylab.savefig('rain_histogram_wrf_dscale.png')
plt.show()
