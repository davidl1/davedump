#!/usr/bin/env python

# Calculate the mesoscale KE spectrum for high resolution nested model
# Generate from 1D east-west spectral averaged over latitude and height 
# (3-9km), excluding first 15 horizontal grid points in each dimension
#
# Reference:
#	Skamarock, W. C. (2004) Evaluating Mesoscale NWP Models Using 
#	Kinetic Energy Spectra, Mon. Wea. Rev. 132, 3019-3032

import math
import numpy as np
from scipy.fftpack import *
from scipy.interpolate import *
from scipy.io import *
from matplotlib import pyplot as plt
import pylab
from Interp3D import *

def LoadField(filename,fieldname):
	print 'from: ' + filename + '\tloading: ' + fieldname
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	m      = field.scale_factor
	c      = field.add_offset
	infile.close()
	return m*field[:]+c

def RemoveTrend(A,X):
	Y   = np.vstack([X,np.ones(len(X))]).T
	m,c = np.linalg.lstsq(Y,A)[0]
	L   = m*X+c
	return A-L

def FilterEdges(A,X):
	n       = X.shape[0]
	m       = n/10
	L       = 0.5-0.5*np.cos(np.pi*(X[:m]-X[0])/(X[m-1]-X[0]))
	R       = L[::-1] 
	B       = np.ones(n)
        B[:m]   = L
        B[n-m:] = R
	return B*A

def AvgFourTrans(A,z,mz,lon,lat,ht):
	nx = A.shape[2]
	ny = A.shape[1]
	nz = A.shape[0]

	B  = np.zeros((mz,ny,nx))
	Z  = np.linspace(z[0],z[1],mz)

	# interpolate to regular points in z
	print 'interpolating to regular points in z...'
	for ii in np.arange(ny):
		if ii%10 == 0:
			print ii,' of ',ny
		for jj in np.arange(nx):
			B[:,ii,jj] = griddata(ht[:,ii,jj],A[:,ii,jj],Z,method='cubic')

	# remove the linear trend, filter the edges and perform the fft
	C = np.zeros((mz,ny,nx),dtype=complex)
	for ii in np.arange(mz):
		for jj in np.arange(ny):
			b          = RemoveTrend(B[ii,jj,:],lon)
			c          = FilterEdges(b,lon)
			C[ii,jj,:] = fft(c)

	return C

lat  = LoadField('XLAT_2010_020400.nc','XLAT')
lon  = LoadField('XLONG_2010_020400.nc','XLONG')
velx = LoadField('U_2010_020400.nc','U')
vely = LoadField('V_2010_020400.nc','V')
velz = LoadField('W_2010_020400.nc','W')
phi  = LoadField('PH_2010_020400.nc','PH')
phi0 = LoadField('PHB_2010_020400.nc','PHB')
orog = LoadField('HGT_2010_020400.nc','HGT')

lat = lat[0,:,0]
lon = lon[0,0,:]
nx0 = len(lon)
ny0 = len(lat)
nz0 = len(velx[0,:,0,0])

print len(lon)
print len(lat)

xinds = 71+np.arange(nx0-141)
yinds = 147+np.arange(ny0-293)

xi = xinds[0]
xf = xinds[-1]
yi = yinds[0]
yf = yinds[-1]
velx = velx[0,:,yi:yf,xi:xf]
vely = vely[0,:,yi:yf,xi:xf]
velz = velz[0,:,yi:yf,xi:xf]
phi  = phi[0,:,yi:yf,xi:xf]
phi0 = phi0[0,:,yi:yf,xi:xf]
orog = orog[:,yi:yf,xi:xf]
lon  = lon[xi:xf]
lat  = lat[yi:yf]

# downscale...
#velx = Downscale2D(velx)
#vely = Downscale2D(vely)
#velz = Downscale2D(velz)
#phi  = Downscale2D(phi)
#phi0 = Downscale2D(phi0)
#orog = Downscale2D(orog)
#lon  = Downscale1D(lon)
#lat  = Downscale1D(lat)

orog = orog[0,:,:]
x = [lon[0],lon[-1]]
y = [lat[0],lat[-1]]
z = [5000.0,9000.0]

nx1 = len(lon)
ny1 = len(lat)
nz1 = 16

#calculate the heights, densities and areas
Rd   = 287.05 #specific has constant for dry air
gInv = 1.0/9.8
hgt  = np.zeros((phi.shape))
for ii in np.arange(phi.shape[0]):
	for jj in np.arange(phi.shape[1]):
		for kk in np.arange(phi.shape[2]):
        		hgt[ii,jj,kk] = (phi[ii,jj,kk] + phi0[ii,jj,kk])*gInv

hgt2 = np.zeros(velx.shape)
for jj in np.arange(ny1):
	for kk in np.arange(nx1):
		for ii in np.arange(nz0):
			hgt2[ii,jj,kk] = 0.5*(hgt[ii,jj,kk] + hgt[ii+1,jj,kk])

Re              = 6378100.0
nxh             = nx1/2
wavenum         = np.arange(nx1)
wavenum[nxh+1:] = np.arange(nxh-1)-nxh+1
wavenum         = wavenum*2.0*np.pi/(2.0*np.pi*Re*(x[1]-x[0])/360.0)

U       = AvgFourTrans(velx,z,nz1,lon,lat,hgt2)
V       = AvgFourTrans(vely,z,nz1,lon,lat,hgt2)
W       = AvgFourTrans(velz,z,nz1,lon,lat,hgt)
KE      = 0.5*(U.real*U.real + U.imag*U.imag + V.real*V.real + V.imag*V.imag + W.real*W.real + W.imag*W.imag)

# average in y and z
kez = np.zeros((ny1,nx1))
for ii in np.arange(ny1):
	for jj in np.arange(nx1):
		kez[ii,jj]=np.sum(KE[:,ii,jj])/nz1

keyz = np.zeros((nx1))
for ii in np.arange(nx1):
	keyz[ii]=np.sum(kez[:,ii])/ny1

kenergy = 2.0*np.pi*Re*(x[1]-x[0])/360.0*keyz

np.savetxt('wavenum_1d_fourier_wrf.txt',wavenum)
np.savetxt('kenergy_1d_fourier_wrf.txt',kenergy)

# plot a least squares fit to the data
lk = np.log10(wavenum[1:len(wavenum)])
le = np.log10(kenergy[1:len(kenergy)])
A = np.vstack([lk,np.ones(len(lk))]).T
m,c = np.linalg.lstsq(A,le)[0]
print 'least squares fit... ',m,c
print -5.0/3.0
plt.plot(lk,le,'.',lk,m*lk+c,lk,-5.0/3.0*lk+c)
plt.xlabel('$ln(||K||)$')
plt.ylabel('$ln(E)$')
plt.title('Vertically averaged 1D kinetic energy spectra')
plt.legend(('WRF spectra','Least squares fit','-5/3 cascade'),loc=3)
pylab.savefig('ke_spectra_96.png')
plt.show()
