#!/usr/bin/env python

import numpy as np
from scipy.io import netcdf
import matplotlib.pyplot as plt
import pylab

def ReadFile(filename,fieldname):
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
        return field[:]

directory = '/scratch/hassimme/WRF/New_Guinea/d03/PRCP/'
filename  = 'prcp_d03_2010-02-'
timedump  = ['02_01:00:00','02_02:00:00','02_03:00:00','02_04:00:00','02_05:00:00','02_06:00:00',
	     '02_07:00:00','02_08:00:00','02_09:00:00','02_10:00:00','02_11:00:00','02_12:00:00',
	     '02_13:00:00','02_14:00:00','02_15:00:00','02_16:00:00','02_17:00:00','02_18:00:00',
	     '02_19:00:00','02_20:00:00','02_21:00:00','02_22:00:00','02_23:00:00','03_00:00:00',
	     '03_01:00:00','03_02:00:00','03_03:00:00','03_04:00:00','03_05:00:00','03_06:00:00',
	     '03_07:00:00','03_08:00:00','03_09:00:00','03_10:00:00','03_11:00:00','03_12:00:00',
	     '03_13:00:00','03_14:00:00','03_15:00:00','03_16:00:00','03_17:00:00','03_18:00:00',
	     '03_19:00:00','03_20:00:00','03_21:00:00','03_22:00:00','03_23:00:00','04_00:00:00',
	     '04_01:00:00','04_02:00:00','04_03:00:00','04_04:00:00','04_05:00:00','04_06:00:00',
	     '04_07:00:00','04_08:00:00','04_09:00:00','04_10:00:00','04_11:00:00','04_12:00:00',
	     '04_13:00:00','04_14:00:00','04_15:00:00','04_16:00:00','04_17:00:00','04_18:00:00',
	     '04_19:00:00','04_20:00:00','04_21:00:00','04_22:00:00','04_23:00:00','05_00:00:00',
	     '05_01:00:00','05_02:00:00','05_03:00:00','05_04:00:00','05_05:00:00','05_06:00:00',
	     '05_07:00:00','05_08:00:00','05_09:00:00','05_10:00:00','05_11:00:00','05_12:00:00',
	     '05_13:00:00','05_14:00:00','05_15:00:00','05_16:00:00','05_17:00:00','05_18:00:00',
	     '05_19:00:00','05_20:00:00','05_21:00:00','05_22:00:00','05_23:00:00','06_00:00:00']

lat = ReadFile(directory+'XLAT_d03_NewGuinea.nc','XLAT')
lon = ReadFile(directory+'XLONG_d03_NewGuinea.nc','XLONG')
hgt = ReadFile(directory+'TERRAIN_HGT_d03_NewGuinea.nc','HGT')
lsm = ReadFile(directory+'LANDMASK_d03_NewGuinea.nc','LANDMASK')

lat = lat[0,:,0]
lon = lon[0,0,:]
hgt = hgt[0,:,:]
lsm = lsm[0,:,:]

xi  = 10
xf  = len(lon)-xi
yi  = 71
yf  = len(lat)-1
lon = lon[xi:xf]
lat = lat[yi:yf]
lsm = lsm[yi:yf,xi:xf]
slm = (lsm+1)%2
nx  = len(lon)
ny  = len(lat)
dx  = nx/7
tspan  = 24.0*(lon[-1]-lon[0])/360.0
print 'time span (hrs):  ',tspan
print 'long span (degs): ',lon[0],'\t',lon[-1]
print 'lat  span (degs): ',lat[0],'\t',lat[-1]
print 'nx: ',nx,'\tdx: ',dx,'\tremainder: ',nx%7

nl  = np.zeros(nx)
ns  = np.zeros(nx)
for ii in np.arange(nx):
	nl[ii] = np.sum(lsm[:,ii])
	ns[ii] = np.sum(slm[:,ii])

# break the time series up into 15 minute segments
lrain = np.zeros(4*24)
srain = np.zeros(4*24)
larea = np.zeros(4*24)
sarea = np.zeros(4*24)

precip_c0  = ReadFile(directory+filename+'02_00:00:00','RAINC')
precip_nc0 = ReadFile(directory+filename+'02_00:00:00','RAINNC')
for hr in np.arange(4*24):
	hour = (hr+1)%24

	print 'loading fields for hour: ',hr
	precip_c1  = ReadFile(directory+filename+timedump[hr],'RAINC')
	precip_nc1 = ReadFile(directory+filename+timedump[hr],'RAINNC')
	rain       = precip_c1[0,yi:yf,xi:xf] + precip_nc1[0,yi:yf,xi:xf] - precip_c0[0,yi:yf,xi:xf] - precip_nc0[0,yi:yf,xi:xf]

	lr   = np.zeros(7)
	sr   = np.zeros(7)
	la   = np.zeros(7)
	sa   = np.zeros(7)
	for ii in np.arange(7):
		for jj in np.arange(dx) + ii*dx:
			lr[ii] = lr[ii] + np.sum(rain[:,jj]*lsm[:,jj])
			sr[ii] = sr[ii] + np.sum(rain[:,jj]*slm[:,jj])
			la[ii] = la[ii] + nl[jj]
			sa[ii] = sa[ii] + ns[jj]

	inds = np.arange(7) + 4*hour
	inds = inds%(4*24)
	lrain[inds] = lrain[inds] + lr
	srain[inds] = srain[inds] + sr
	larea[inds] = larea[inds] + la
	sarea[inds] = sarea[inds] + sa

	precip_c0  = precip_c1
	precip_nc0 = precip_nc1
			
avg_lrain = np.zeros(24)
avg_srain = np.zeros(24)
for ii in np.arange(24):
	inds = np.arange(4) + 4*ii
	avg_lrain[ii] = np.sum(lrain[inds]/larea[inds])/4
	avg_srain[ii] = np.sum(srain[inds]/sarea[inds])/4

fig=plt.figure()
ax=fig.add_subplot(111)
time=np.arange(24)
avg_lrain_2=np.zeros(24)
avg_srain_2=np.zeros(24)
GMT=9
for ii in np.arange(24):
	ind=(ii-GMT)%24
	avg_lrain_2[ii]=avg_lrain[ind]
	avg_srain_2[ii]=avg_srain[ind]

plt.plot(time,avg_lrain_2,'-o',time,avg_srain_2,'-o')
plt.legend(('land','sea'),loc=0)
plt.xlabel('local time (hrs)')
plt.ylabel('precipitation rate (mm)')
plt.xlim([0,23])
plt.title('precipitation vs. local time for land and sea (WRF)')
pylab.savefig('avg_diurnal_cycle_wrf.png')
plt.show()
