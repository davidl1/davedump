#!/usr/bin/env python

# Calculate the mesoscale KE spectrum for high resolution nested model
# Generate from 1D east-west spectral averaged over latitude and height 
# (3-9km), excluding first 15 horizontal grid points in each dimension
#
# Reference:
#	Skamarock, W. C. (2004) Evaluating Mesoscale NWP Models Using 
#	Kinetic Energy Spectra, Mon. Wea. Rev. 132, 3019-3032

import math
import numpy as np
from matplotlib import pyplot as plt
import pylab
from Utils import *
from ChebTrans import *
from Interp3D import *

def Flipit(A):
	nx = A.shape[1]
	ny = A.shape[0]
	Bx = np.zeros((ny,nx))
	By = np.zeros((ny,nx))

	for ii in np.arange(ny):
		Bx[ii,:] = A[ii,::-1]

	for ii in np.arange(nx):
		By[:,ii] = Bx[::-1,ii]

	return By

velx = LoadField('../020212/velx_020212.nc','u')
vely = LoadField('../020212/vely_020212.nc','v')
velz = LoadField('../020212/velz_020212.nc','dz_dt')
rho  = LoadField('../020212/rho_020212.nc','unspecified')
orog = LoadField('../020212/orog.nc','ht')

lon = LoadField('../020212/rho_020212.nc','longitude')
lat = LoadField('../020212/rho_020212.nc','latitude')
ht  = LoadField('../020212/rho_020212.nc','hybrid_ht')

nx0 = len(lon)
ny0 = len(lat)
nz0 = len(ht)

xinds = 432+np.arange(nx0-863)
yinds = 132+np.arange(ny0-263)

xi   = xinds[0]
xf   = xinds[-1]
yi   = yinds[0]
yf   = yinds[-1]
velx = velx[0,:,yi:yf,xi:xf]
vely = vely[0,:,yi:yf,xi:xf]
velz = velz[0,:,yi:yf,xi:xf]
rho  = rho[0,:,yi:yf,xi:xf]
orog = orog[0,:,yi:yf,xi:xf]
lon  = lon[xi:xf]
lat  = lat[yi:yf]

# downscale...
lon  = Downscale1D(lon)
lat  = Downscale1D(lat)
velx = Downscale2D(velx)
vely = Downscale2D(vely)
velz = Downscale2D(velz)
rho  = Downscale2D(rho)
orog = Downscale2D(orog)
# downscale...
lon  = Downscale1D(lon)
lat  = Downscale1D(lat)
velx = Downscale2D(velx)
vely = Downscale2D(vely)
velz = Downscale2D(velz)
rho  = Downscale2D(rho)
orog = Downscale2D(orog)

nx0 = len(lon)
ny0 = len(lat)
nz0 = len(ht)

orog = orog[0,:,:]
x = [lon[1],lon[-2]]
y = [lat[1],lat[-2]]
z = [5000.0,9000.0] #should start at 3000.0 meters, but New Guinea is too mountainous...

nht = 0
for hti in ht:
	if hti > z[0] and hti < z[1]:
		nht = nht + 1
print 'nht: ',nht

nx1 = 256
ny1 = 256 #math.trunc(nx1*(y[1]-y[0])/(x[1]-x[0]))
nz1 = 8

order = 5

#TODO: chech the other fields to see if they're staggered and if so remap to cell centers

#average the vertical velocity to cell centers
print 'averaging vertical velocity to height levels...'
velz2 = np.zeros((velx.shape))
for jj in np.arange(ny0):
	for kk in np.arange(nx0):
		for ii in np.arange(nz0):
			velz2[ii,jj,kk] = 0.5*(velz[ii,jj,kk] + velz[ii+1,jj,kk])

velz = velz2
print velz.shape

#remove the r^2 term from the density
Re = 6335437.0
H1 = ht[-1]
print 'removing r^2 term from the density field and multiply by the volume...'
for yi in np.arange(ny0):
	for xi in np.arange(nx0):
		R = orog[yi,xi] + ht*(H1-orog[yi,xi]) + Re
		Z = orog[yi,xi] + ht*(H1-orog[yi,xi])
		V = np.zeros(nz0)
		for zi in 1+np.arange(nz0-2):
			V[zi] = 0.5*(Z[zi+1]-Z[zi-1])
		V[0]  = 0.5*(Z[1]-Z[0])
		V[-1] = 0.5*(Z[-1]-Z[-2])
		rho[:,yi,xi] = rho[:,yi,xi]*V*1334.3*1334.3/R/R
		#rho[:,yi,xi] = rho[:,yi,xi]/R/R
print '\t...done.'

#plot KE at lat/lon coords...
Pz=Interp3Dto2D(rho ,z,nz1,lon,lat,ht,orog)
Uz=Interp3Dto2D(velx,z,nz1,lon,lat,ht,orog)
Vz=Interp3Dto2D(vely,z,nz1,lon,lat,ht,orog)
Wz=Interp3Dto2D(velz,z,nz1,lon,lat,ht,orog)
KE1=0.5*Pz*(Uz*Uz+Vz*Vz+Wz*Wz)
fig=plt.figure()
ax=fig.add_subplot(111)
co=ax.contourf(lon,lat,KE1)
plt.title('orig')
plt.colorbar(co)

P=InterpAndFlatten(rho, x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,0)
U=InterpAndFlatten(velx,x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,0)
V=InterpAndFlatten(vely,x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,0)
W=InterpAndFlatten(velz,x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,0)
xorig=0.5*(x[1]+x[0])
yorig=0.5*(y[1]+y[0])
xhalf=0.5*(x[1]-x[0])
yhalf=0.5*(y[1]-y[0])
X=xorig-xhalf*np.cos(np.pi*np.arange(nx1+1)/nx1)
Y=yorig-yhalf*np.cos(np.pi*np.arange(ny1+1)/ny1)
KE2=0.5*P*(U*U+V*V+W*W)
fig=plt.figure()
ax=fig.add_subplot(111)
co=ax.contourf(X,Y,KE2)
plt.title('cheb')
plt.colorbar(co)
plt.show()

#P = InterpBox(rho, x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,order)
#U = InterpBox(velx,x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,order)
#V = InterpBox(vely,x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,order)
#W = InterpBox(velz,x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,order)

#da = 1334.3*1334.3
#KEu = FlattenKEz(P,U,z,nz1,da)
#KEv = FlattenKEz(P,V,z,nz1,da)
#KEw = FlattenKEz(P,W,z,nz1,da)

#P = Flipit(P)
#U = Flipit(U)
#V = Flipit(V)
#W = Flipit(W)

KEu = 0.5*P*U*U
KEv = 0.5*P*V*V
KEw = 0.5*P*W*W

print 'KEu min: ',np.min(np.min(KEu))
print 'KEv min: ',np.min(np.min(KEv))
print 'KEw min: ',np.min(np.min(KEw))

#TODO see clearx and heijst, 00.
#average before calcualting 1d spectra, and flip data so it goes from +L->-L
KEuc = ChebTrans2D(KEu)
KEvc = ChebTrans2D(KEv)
KEwc = ChebTrans2D(KEw)

print 'KEuc min: ',np.min(np.min(KEuc))
print 'KEvc min: ',np.min(np.min(KEvc))
print 'KEwc min: ',np.min(np.min(KEwc))

kenergy = np.zeros((ny1+1)*(nx1+1))
wavenum = np.zeros((ny1+1)*(nx1+1))
ki = 0
for ii in np.arange(ny1+1):
	for jj in np.arange(nx1+1):
		wavenum[ki] = np.sqrt(ii*ii + jj*jj)
		kenergy[ki] = KEuc[ii,jj] + KEvc[ii,jj] + KEwc[ii,jj]
		ki = ki+1

xh = 0.5*(x[1]-x[0])
wavenum = wavenum/xh

np.savetxt('wavenum_2d_256x256.txt',wavenum)
np.savetxt('kenergy_2d_256x256.txt',kenergy)

#plt.loglog(wavenum,kenergy,'.')
#plt.xlabel('wavenumber, k m^{-1}')
#plt.ylabel('kinetic energy, kg.m^2/s^2')
#plt.title('Kinetic energy spectra, 2D Chebyshev')
#plt.show()
#pylab.savefig('ke_spec.png')
