#!/usr/bin/env python

#module load pythonlib/ScientificPython

from Scientific.IO.NetCDF import NetCDFFile
import matplotlib.pyplot as plt
import numpy as np
import pylab
from Utils import *

times = ['020112','020200','020212','020300','020312','020400','020412','020500','020512']

filename = '../' + times[0] + '/lsm_' + times[0] + '.nc'
lsm = LoadField(filename,'lsm')
lon = LoadField(filename,'longitude')
lat = LoadField(filename,'latitude')

nx=len(lon)
ny=len(lat)

times = times[1:]

xi    = 33
xf    = nx-179
yf    = ny-53
lon   = lon[xi:xf]
lat   = lat[0:yf]
lsm   = lsm[0,0,0:yf,xi:xf]
slm   = (lsm+1)%2
nx    = len(lon)
ny    = len(lat)
print 'long span: ',lon[0],'\t',lon[-1]
print 'lat span:  ',lat[0],'\t',lat[-1]

nl  = np.zeros(nx)
ns  = np.zeros(nx)
for ii in np.arange(nx):
	nl[ii] = np.sum(lsm[:,ii])
	ns[ii] = np.sum(slm[:,ii])
nl = np.sum(nl)
ns = np.sum(ns)

prange = 1
nbins  = 400

shist = np.zeros(nbins)
lhist = np.zeros(nbins)
thist = np.zeros(nbins)

#for tt in np.arange(len(times)):
for tt in np.arange(8):
	filename = '../' + times[tt] + '/precip_2_' + times[tt] + '.nc'
	precip   = LoadField(filename,'precip')
	rain     = np.zeros((ny,nx))
	for hr in np.arange(precip.shape[0]):	
		print 'step: ',hr
		#rescale to mm
		rain = rain + 60.0*60.0*precip[hr,0,0:yf,xi:xf]

		if hr%3==0:
			rain2 = rain.flatten()
			rain2 = rain2/prange/3
			lsm   = lsm.flatten()
			for ii in np.arange(nx*ny):
				rbin = np.int(rain2[ii])
				thist[rbin] = thist[rbin] + 1
				if lsm[ii]:
					lhist[rbin] = lhist[rbin] + 1
				else:
					shist[rbin] = shist[rbin] + 1

			rain  = np.zeros((ny,nx))

lhist = 100*lhist/4/8/nl
shist = 100*shist/4/8/ns
thist = 100*thist/4/8/(nx*ny)

np.savetxt('rain_hist_l.txt',lhist)
np.savetxt('rain_hist_s.txt',shist)
np.savetxt('rain_hist_t.txt',thist)

fig=plt.figure()		
ax=fig.add_subplot(111)
rfall = prange*np.arange(nbins)
plt.semilogy(rfall,lhist,'-o',rfall,shist,'-o')
plt.legend(('land','sea'),loc=0)
plt.xlabel('precipitation (mm)')
plt.ylabel('% of domain area')
plt.title('rainfall histogram')
pylab.savefig('rain_histogram.png')
plt.show()
