#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import pylab

k=np.loadtxt('wavenum_2d_64x64.txt')
e=np.loadtxt('kenergy_2d_64x64.txt')

inds = np.argsort(k)
k = k[inds]
e = e[inds]

plt.loglog(k,e,'.')
plt.loglog(k[1:len(k)],np.power(e[0],0.92)*np.power(k[1:len(k)],-5.0/3.0))
plt.show()
pylab.savefig('k_v_e.png')

# plot a least squares fit to the data
print np.min(e)
e  = np.abs(e)
lk = np.log10(k[1:])
le = np.log10(e[1:])

A = np.vstack([lk,np.ones(len(lk))]).T
m,c = np.linalg.lstsq(A,le)[0]
print m,c
print -5.0/3.0
plt.plot(lk,le,'.',lk,m*lk+c,lk,-5.0/3.0*lk+c)
#plt.plot(lk,le,'.',lk,-5.0/3.0*lk+2.0e+1)
plt.xlabel('$ln(||K||)$')
plt.ylabel('$ln(E)$')
plt.title('Vertically averaged 2D kinetic energy spectra')
plt.legend(('UM spectra','Least squares fit','-5/3 cascade'),loc=3)
pylab.savefig('ke_spectra_512.png')
plt.show()

