#!/usr/bin/env python

#to load libraries on raijin:
#module load pythonlib/ScientificPython

from Scientific.IO.NetCDF import NetCDFFile
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import griddata
import pylab
from ChebTrans import *

#extract the netCDF file
precip=NetCDFFile('PrecipMonthly8410.nc')
p=precip.variables['precip']
p_data=p.getValue()

lat=precip.variables['latitude']
lon=precip.variables['longitude']
lat_data=lat.getValue()
lon_data=lon.getValue()

lsm=NetCDFFile('LandSeaMask.nc')
l=lsm.variables['lsm']
l_data=l.getValue()

#set the chebyshev-gauss-lobarro points
Lx=max(lon_data)-min(lon_data)
Ly=max(lat_data)-min(lon_data)
Nx=192
Ny=128
nx=np.arange(Nx+1)
ny=np.arange(Ny+1)
x=0.5*Lx*(1.0-np.cos(np.pi*nx/Nx))
y=-Ly*np.cos(np.pi*ny/Ny)

#TODO: map the lat-lon coords (in degrees) to spatial coodinates before interpolating to the cgl coords
#interpolate from lat-lon coords to cgl coords
pts0=np.transpose([np.tile(lon_data,len(lat_data)),np.repeat(lat_data,len(lon_data))])
pts1=np.transpose([np.tile(x,Ny+1),np.repeat(y,Nx+1)])
pd2=np.ravel(p_data[0,0])
p2=griddata(pts0,pd2,pts1,method='cubic')
p_data2=p2.reshape(Ny+1,Nx+1)

#plot the fields
fig=plt.figure()
ax=fig.add_subplot(111)
co=ax.contourf(lon_data,lat_data,p_data[0,0,:,:])
ax.contour(lon_data,lat_data,l_data[0,0,:,:])
plt.xlabel('longitude, $^\circ$ (degrees)')
plt.ylabel('latitude, $^\circ$ (degrees)')
plt.title('precipitation rate (daily mean), $\mathrm{kg\cdot m}^{-2}\mathrm{s}^{-1}$ - original form UM')
plt.colorbar(co,orientation='horizontal')
pylab.savefig('old.png')

fig=plt.figure()
ax=fig.add_subplot(111)
cn=ax.contourf(x,y,p_data2)
ax.contour(lon_data,lat_data,l_data[0,0,:,:])
plt.xlabel('longitude, $^\circ$ (degrees)')
plt.ylabel('latitude, $^\circ$ (degrees)')
plt.title('precipitation rate (daily mean), $\mathrm{kg\cdot m}^{-2}\mathrm{s}^{-1}$ - interpolated to CGL points')
plt.colorbar(cn,orientation='horizontal')
pylab.savefig('new.png')

#now do the transform
C=InvChebTrans2D(p_data2)

fig=plt.figure()
ax=fig.add_subplot(111)
plt.pcolormesh(nx[0:32],ny[0:32],C[0:32,0:32])
plt.xlabel('$k_x$')
plt.ylabel('$k_y$')
plt.title('precipitation rate (daily mean) - Chebyshev modes')
plt.colorbar(format='%.2e')
pylab.savefig('cheb.png')

#test by transforming back again
D=ChebTrans2D(C)

fig=plt.figure()
ax=fig.add_subplot(111)
cd=ax.contourf(x[::-1],y[::-1],D)
ax.contour(lon_data,lat_data,l_data[0,0,:,:])
plt.xlabel('longitude, $^\circ$ (degrees)')
plt.ylabel('latitude, $^\circ$ (degrees)')
plt.title('precipitation rate (daily mean), $\mathrm{kg\cdot m}^{-2}\mathrm{s}^{-1}$ - reconstructed')
plt.colorbar(cd,orientation='horizontal')
pylab.savefig('back.png')
plt.show()
