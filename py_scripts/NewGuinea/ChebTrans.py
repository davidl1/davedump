import numpy as np
from scipy.fftpack import *

def Tn(n,m):
	M=np.arange(m+1)
	X=np.cos(np.pi*M/m)
	return np.cos(n*np.arccos(X))

def Quad(n):
	w = np.zeros(n+1);
	v = np.ones(n-1);
	if n%2 == 0:
		w[0] = w[n] = 1.0/(n*n-1)
		nn=np.arange(n-1)+1
		for k in np.arange(n/2-2)+1:
			v = v - 2.0*np.cos(2.0*k*np.pi*nn/n)/(4*k*k-1)

		v = v - np.cos(n*np.pi*nn/n)/(n*n-1)
	else:
		print 'ERROR: have not implemented...'

	w[1:n] = 2*v/n

	return w

def Integrate(A,l):
	n = len(A)-1
	w = Quad(n)
	return l*np.inner(w,A)

def ChebTrans(A):
	n=A.shape[0]
	K=n-1-np.arange(n-1)
	B=np.hstack((A[0:n-1],A[K]))
	C=ifft(B)
	return np.hstack((C[0],2*C[1:n-1],C[n])).real

def ChebTrans2D(A):
	nx=A.shape[1]
	ny=A.shape[0]
	Cx=np.zeros((ny,nx))
	Ct=np.zeros((nx,ny))

	for ii in np.arange(ny):
		Cx[ii,:]=ChebTrans(A[ii,:])

	Cy=Cx.T
	for ii in np.arange(nx):
		Ct[ii,:]=ChebTrans(Cy[ii,:])

	return Ct.T

#TODO: test this!!!
def ChebTrans3D(A):
	nx=A.shape[2]
	ny=A.shape[1]
	nz=A.shape[0]

	C=np.zeros((nz,ny,nx))
	Cxy=np.zeros((nz,ny,nx))
	for ii in np.arange(nz):
		Cxy[ii,:,:]=ChebTrans2D(A[ii,:,:])

	for jj in np.arange(ny):
		for kk in np.arange(nx):
			C[:,jj,kk]=ChebTrans(Cxy[:,jj,kk])

	return C
		

def InvChebTrans(A):
	n=len(A)
	k=n-1-np.arange(n-1)
	V=np.hstack((A[0:n-1],A[k]))
	F=ifft(V)
	return n/2*np.hstack((F[0].real,2*F[1:n-1].real,F[n-1].real))

def InvChebTrans2D(A):
	nx=A.shape[1]
	ny=A.shape[0]
	Cx=np.zeros((ny,nx))
	Ct=np.zeros((nx,ny))

	for ii in np.arange(ny):
		Cx[ii,:]=InvChebTrans(A[ii,::-1])

	Cy=Cx.T
	for ii in np.arange(nx):
		Ct[ii,:]=InvChebTrans(Cy[ii,::-1])

	return Ct.T

def ChebTrans2D_old(A):
	nx=A.shape[1]
	ny=A.shape[0]
	B=np.zeros((ny,nx))
	for jj in np.arange(ny):
		for kk in np.arange(nx):
			B=B+A[jj,kk]*np.outer(Tn(jj,ny-1),Tn(kk,nx-1))

	return B

def GenOuterProduct3D(Tz,Ty,Tx):
	nx=len(Tx)
	ny=len(Ty)
	nz=len(Tz)

	A=np.zeros((nz,ny,nx))

	for ii in np.arange(nz):
		for jj in np.arange(ny):
			for kk in np.arange(nx):
				A[ii,jj,kk]=Tz[ii]*Ty[jj]*Tx[kk]

	return A

def ChebTrans3D_old(A):
	nx=A.shape[2]
	ny=A.shape[1]
	nz=A.shape[0]
	B=np.zeros((nz,ny,nx))
	for ii in np.arange(nz):
		for jj in np.arange(ny):
			for kk in np.arange(nx):
				T=GenOuterProduct3D(Tn(ii,nz-1),Tn(jj,ny-1),Tn(nx,kk-1))
				B=B+A[ii,jj,kk]*T

	return B
