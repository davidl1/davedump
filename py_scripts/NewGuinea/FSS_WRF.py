#!/usr/bin/env python

# calculate the fractional skill score for rainfall data against reference 
# at varying scales
# reference:
#	Roberts and Lean (2008) MWR: 136, 78-97

import numpy as np
from scipy.io import *
from scipy.interpolate import *
from matplotlib import pyplot as plt
import pylab

def LoadField(filename,fieldname):
        print 'from: ' + filename + '\tloading: ' + fieldname
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
        return field[:]

def InterpGrid( xin, yin, xout, yout, A ):
	pts_in  = np.transpose([np.tile(xin,len(yin)),np.repeat(yin,len(xin))])
	pts_out = np.transpose([np.tile(xout,len(yout)),np.repeat(yout,len(xout))])
	Ar      = np.ravel(A)
	Aor     = griddata(pts_in,Ar,pts_out,method='cubic')
	Ao      = Aor.reshape(len(yout),len(xout))
	return Ao

def GenFSS( lon, lat, pcp_m, pcp_o, pmax ):
	mod = pcp_m>pmax
	obs = pcp_o>pmax

	n = np.min([len(lon),len(lat)])
	n = n/2
	n = np.arange(n)
	n = 2*n+1

	FSS     = np.zeros(len(n))

	k = 0
	for ni in n:
		x = np.arange(len(lon)-ni)+ni/2
		y = np.arange(len(lat)-ni)+ni/2
		Mn = np.zeros((len(y),len(x)))
		On = np.zeros((len(y),len(x)))
		i = 0
		for yi in y:
			j = 0
			for xi in x:
				xr = xi + np.arange(ni)-ni/2
				yr = yi + np.arange(ni)-ni/2
				Mn[i,j] = np.sum(mod[yr[0]:yr[-1]+1,xr[0]:xr[-1]+1])
				On[i,j] = np.sum(obs[yr[0]:yr[-1]+1,xr[0]:xr[-1]+1])
				j = j + 1

			i = i + 1;

		MSE_ref = (np.sum(On*On) + np.sum(Mn*Mn))/len(x)/len(y)
		MSE     = np.sum((On-Mn)*(On-Mn))/len(x)/len(y)
		FSS[k]  = 1.0 - MSE/MSE_ref
	
		print 'n: ',ni,'\tMSE: ',MSE,'\tref: ',MSE_ref,'\tFSS: ',FSS[k]
		k = k + 1

	return FSS

xinds = np.arange(106)+1228
yinds = np.arange(88)+123

lat_m    = LoadField('/scratch/hassimme/WRF/New_Guinea/d03/PRCP/XLAT_d03_NewGuinea.nc','XLAT')
lon_m    = LoadField('/scratch/hassimme/WRF/New_Guinea/d03/PRCP/XLONG_d03_NewGuinea.nc','XLONG')
ht_m     = LoadField('/scratch/hassimme/WRF/New_Guinea/d03/PRCP/LANDMASK_d03_NewGuinea.nc','LANDMASK')
rainc    = LoadField('/scratch/hassimme/WRF/New_Guinea/d03/PRCP/prcp_d03_2010-02-02_12:00:00','RAINC')
rainnc   = LoadField('/scratch/hassimme/WRF/New_Guinea/d03/PRCP/prcp_d03_2010-02-02_12:00:00','RAINNC')
rainc_p  = LoadField('/scratch/hassimme/WRF/New_Guinea/d03/PRCP/prcp_d03_2010-02-02_11:00:00','RAINC')
rainnc_p = LoadField('/scratch/hassimme/WRF/New_Guinea/d03/PRCP/prcp_d03_2010-02-02_11:00:00','RAINNC')
pcp_m    = rainc + rainnc - rainc_p - rainnc_p
pcp_m    = pcp_m[0,:,:]
lat_m    = lat_m[0,:,0]
lon_m    = lon_m[0,0,:]

lat_o = LoadField('/data/cdgs1/hassimme/POSTDOC/TRMM/3B42/Ver7/3B42.20100202.12.7.nc','latitude')
lon_o = LoadField('/data/cdgs1/hassimme/POSTDOC/TRMM/3B42/Ver7/3B42.20100202.12.7.nc','longitude')
pcp_o = LoadField('/data/cdgs1/hassimme/POSTDOC/TRMM/3B42/Ver7/3B42.20100202.12.7.nc','pcp')
lon_o = lon_o[xinds]
lat_o = lat_o[yinds]
pcp_o = pcp_o[0,yinds[0]:yinds[-1]+1,xinds[0]:xinds[-1]+1]

# interpolate model data to observation grid
lon = lon_o
lat = lat_o
print lon.shape,lat.shape
pcp_m = InterpGrid(lon_m,lat_m,lon,lat,pcp_m)
ht_o  = InterpGrid(lon_m,lat_m,lon,lat,ht_m)

fig=plt.figure()
ax=fig.add_subplot(111)
ax.contour(lon,lat,ht_o,colors='k')
co=ax.contourf(lon,lat,pcp_o)
plt.colorbar(co,orientation='horizontal')
plt.title('precip - TRMM')

fig=plt.figure()
ax=fig.add_subplot(111)
ax.contour(lon,lat,ht_o,colors='k')
co=ax.contourf(lon,lat,pcp_m)
plt.colorbar(co,orientation='horizontal')
plt.title('precip - WRF')
plt.show()

# binary field for threshold
n = np.min([len(lon),len(lat)])/2
pmax = np.zeros(6)
pmax[0] =  4.0
pmax[1] =  8.0
pmax[2] = 12.0
pmax[3] = 16.0
pmax[4] = 20.0
pmax[5] = 24.0
FSS = np.zeros((n,6))
for ii in np.arange(6):
	FSS[:,ii] = GenFSS(lon,lat,pcp_m,pcp_o,pmax[ii])

n   = np.arange(n)
n   = 2*n+1
dx  = 6371000.0*2.0*np.pi*(lon[-1]-lon[0])/360.0/len(lon)
dxn = dx*n/1000.0
print 'dx:',dx

np.savetxt('FSS_scale.txt',dx*n)
np.savetxt('FSS_skill_WRF.txt',FSS)

plt.xlabel('Scale (km)')
plt.ylabel('FSS')
plt.title('Fractions Skill Score (FSS)')
plt.plot(dxn,FSS[:,0],'-o')
plt.plot(dxn,FSS[:,1],'-o')
plt.plot(dxn,FSS[:,2],'-o')
plt.plot(dxn,FSS[:,3],'-o')
plt.plot(dxn,FSS[:,4],'-o')
plt.plot(dxn,FSS[:,5],'-o')
plt.legend(('4mm','8mm','12mm','16mm','20mm','24mm'),loc=2)
pylab.savefig('FSS_WRF.png')
plt.show()
