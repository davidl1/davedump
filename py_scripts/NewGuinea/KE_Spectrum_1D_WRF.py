#!/usr/bin/env python

# Calculate the mesoscale KE spectrum for high resolution nested model
# Generate from 1D east-west spectral averaged over latitude and height 
# (3-9km), excluding first 15 horizontal grid points in each dimension
#
# Reference:
#	Skamarock, W. C. (2004) Evaluating Mesoscale NWP Models Using 
#	Kinetic Energy Spectra, Mon. Wea. Rev. 132, 3019-3032

import math
import numpy as np
from scipy.io import netcdf
from scipy.fftpack import *
from matplotlib import pyplot as plt
import pylab
from Interp3D import *

def LoadField(filename,fieldname):
	print 'from: ' + filename + '\tloading: ' + fieldname
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        m      = field.scale_factor
        c      = field.add_offset
        infile.close()
        return m*field[:]+c

def Interp3Dto2D_WRF(A,z,nz,lon,lat,Z):
        print 'interpolating to 2d regular field...'

        #Zi = np.linspace(z[0],z[1],nz+1)
        #Zi = Zi[0:nz] + 0.5*(Zi[1]-Zi[0])
	Zi = 7000.0

        nlon  = len(lon)
        nlat  = len(lat)

        B = np.zeros((nlat,nlon))
        for jj in np.arange(nlat):
                if jj%10 == 0:
                        print `jj` + ' of ' + `nlat`
                for kk in np.arange(nlon):
                        Bz       = griddata(Z[:,jj,kk],A[:,jj,kk],Zi,method='cubic')
                        #B[jj,kk] = np.sum(Bz)/nz
                        B[jj,kk] = Bz

        return B

def RemoveTrend(A,X):
	nx = len(X)
	B  = np.zeros(nx)

	m  = (A[-1]-A[0])/(X[-1]-X[0])
	c  = A[-1] - m*X[-1]
	for ii in np.arange(nx):
		B[ii] = A[ii] - (m*X[ii]+c)

	return B

lat = LoadField('XLAT_2010_020400.nc','XLAT')
lon = LoadField('XLONG_2010_020400.nc','XLONG')

velx = LoadField('U_2010_020400.nc','U')
vely = LoadField('V_2010_020400.nc','V')
velz = LoadField('W_2010_020400.nc','W')
phi  = LoadField('PH_2010_020400.nc','PH')
phi0 = LoadField('PHB_2010_020400.nc','PHB')
temp = LoadField('T_2010_020400.nc','T')
pres = LoadField('P_2010_020400.nc','P')
preb = LoadField('PB_2010_020400.nc','PB')
orog = LoadField('HGT_2010_020400.nc','HGT')

lat = lat[0,:,0]
lon = lon[0,0,:]
nx0 = len(lon)
ny0 = len(lat)
nz0 = velx.shape[1]
print nx0,ny0,nz0

xinds = 327+np.arange(nx0-653)
yinds = 147+np.arange(ny0-293)

xi = xinds[0]
xf = xinds[-1]
yi = yinds[0]
yf = yinds[-1]
velx = velx[0,:,yi:yf,xi:xf]
vely = vely[0,:,yi:yf,xi:xf]
velz = velz[0,:,yi:yf,xi:xf]
phi  = phi[0,:,yi:yf,xi:xf]
phi0 = phi0[0,:,yi:yf,xi:xf]
temp = temp[0,:,yi:yf,xi:xf]
pres = pres[0,:,yi:yf,xi:xf]
preb = preb[0,:,yi:yf,xi:xf]
orog = orog[:,yi:yf,xi:xf]
lon  = lon[xi:xf]
lat  = lat[yi:yf]

# downscale...
velx = Downscale2D(velx)
vely = Downscale2D(vely)
velz = Downscale2D(velz)
phi  = Downscale2D(phi)
phi0 = Downscale2D(phi0)
temp = Downscale2D(temp)
pres = Downscale2D(pres)
preb = Downscale2D(preb)
orog = Downscale2D(orog)
lon  = Downscale1D(lon)
lat  = Downscale1D(lat)
# downscale...
velx = Downscale2D(velx)
vely = Downscale2D(vely)
velz = Downscale2D(velz)
phi  = Downscale2D(phi)
phi0 = Downscale2D(phi0)
temp = Downscale2D(temp)
pres = Downscale2D(pres)
preb = Downscale2D(preb)
orog = Downscale2D(orog)
lon  = Downscale1D(lon)
lat  = Downscale1D(lat)
# downscale...
velx = Downscale2D(velx)
vely = Downscale2D(vely)
velz = Downscale2D(velz)
phi  = Downscale2D(phi)
phi0 = Downscale2D(phi0)
temp = Downscale2D(temp)
pres = Downscale2D(pres)
preb = Downscale2D(preb)
orog = Downscale2D(orog)
lon  = Downscale1D(lon)
lat  = Downscale1D(lat)
# downscale...
velx = Downscale2D(velx)
vely = Downscale2D(vely)
velz = Downscale2D(velz)
phi  = Downscale2D(phi)
phi0 = Downscale2D(phi0)
temp = Downscale2D(temp)
pres = Downscale2D(pres)
preb = Downscale2D(preb)
orog = Downscale2D(orog)
lon  = Downscale1D(lon)
lat  = Downscale1D(lat)

orog = orog[0,:,:]
x = [lon[0],lon[-1]]
y = [lat[0],lat[-1]]
z = [5000.0,9000.0]

nx1 = len(lon)
ny1 = len(lat)
nz1 = 16
print nx1,ny1

#average the vertical velocity to cell centers
print 'averaging vertical velocity and potentials to height levels...'
phi2  = np.zeros((nz0,ny1,nx1))
phi02 = np.zeros((nz0,ny1,nx1))
velz2 = np.zeros((nz0,ny1,nx1))
for jj in np.arange(ny1):
        for kk in np.arange(nx1):
                for ii in np.arange(nz0):
			phi2[ii,jj,kk]  = 0.5*(phi[ii,jj,kk] + phi[ii+1,jj,kk])
			phi02[ii,jj,kk] = 0.5*(phi0[ii,jj,kk] + phi0[ii+1,jj,kk])
                        velz2[ii,jj,kk] = 0.5*(velz[ii,jj,kk] + velz[ii+1,jj,kk])

#calculate the heights, densities and areas
Rd = 287.05 #specific has constant for dry air
hgt = np.zeros((nz0,ny1,nx1))
rho = np.zeros((nz0,ny1,nx1))
for ii in np.arange(nz0):
	for jj in np.arange(ny1):
		for kk in np.arange(nx1):
			hgt[ii,jj,kk] = (phi2[ii,jj,kk] + phi02[ii,jj,kk])/9.8
			rho[ii,jj,kk] = (pres[ii,jj,kk] + preb[ii,jj,kk])/temp[ii,jj,kk]/Rd

U  = Interp3Dto2D_WRF(velx,z,nz1,lon,lat,hgt)
V  = Interp3Dto2D_WRF(vely,z,nz1,lon,lat,hgt)
W  = Interp3Dto2D_WRF(velz2,z,nz1,lon,lat,hgt)
P  = Interp3Dto2D_WRF(rho, z,nz1,lon,lat,hgt)

#scaling by the volume - assume regular points in all dimensions
dx  = (x[1]-x[0])/(nx1-1)
dy  = (y[1]-y[0])/(ny1-1)
dz  = (z[1]-z[0])/1
P   = dx*dy*dz*P

#flatten the fields
Ux = np.zeros(nx1)
Vx = np.zeros(nx1)
Wx = np.zeros(nx1)
Px = np.zeros(nx1)
Kx = np.zeros(nx1)
for ii in np.arange(nx1):
	Ux[ii] = np.sum(U[:,ii])/ny1
	Vx[ii] = np.sum(V[:,ii])/ny1
	Wx[ii] = np.sum(W[:,ii])/ny1
	Px[ii] = np.sum(P[:,ii])/ny1

Kx = 0.5*Px*(Ux*Ux + Vx*Vx + Wx*Wx)

#remove the linear trend
m = (Kx[-1]-Kx[0])/(x[1]-x[0])
c = Kx[-1] - m*lat[-1]
print 'removing linear trend... ',m,c
for ii in np.arange(nx1):
	Kx[ii] = Kx[ii] - (m*lon[ii]+c)

Khat = fft(Kx)

#Ux = RemoveTrend(Ux,lon)
#Vx = RemoveTrend(Vx,lon)
#Wx = RemoveTrend(Wx,lon)
#Px = RemoveTrend(Px,lon)
#UF = fft(Ux)
#VF = fft(Vx)
#WF = fft(Wx)
#PF = fft(Px)
#kenergy = 0.5*np.sqrt(PF.real*PF.real + PF.imag*PF.imag)*(UF.real*UF.real + UF.imag*UF.imag + 
#                                                          VF.real*VF.real + VF.imag*VF.imag + 
#                                                          WF.real*WF.real + WF.imag*WF.imag)

wavenum = np.arange(nx1)
wavenum = (x[1]-x[0])/(2.0*np.pi)*wavenum

kenergy = Khat.real*Khat.real + Khat.imag*Khat.imag
kenergy = np.sqrt(kenergy)

np.savetxt('wavenum_1d_fourier.txt',wavenum)
np.savetxt('kenergy_1d_fourier.txt',kenergy)

#plt.loglog(wavenum,kenergy,'.')
#plt.xlabel('wavenumber, k m^{-1}')
#plt.ylabel('kinetic energy, kg.m^2/s^2')
#plt.title('kinetic energy spectra')
#plt.show()
#pylab.savefig('ke_spec.png')

# plot a least squares fit to the data
lk = np.log10(wavenum[1:len(wavenum)])
le = np.log10(kenergy[1:len(kenergy)])
A = np.vstack([lk,np.ones(len(lk))]).T
m,c = np.linalg.lstsq(A,le)[0]
print 'least squares fit... ',m,c
print -5.0/3.0
plt.plot(lk,le,'.',lk,m*lk+c,lk,-5.0/3.0*lk+c)
plt.xlabel('$ln(||K||)$')
plt.ylabel('$ln(E)$')
plt.title('Vertically averaged 1D kinetic energy spectra')
plt.legend(('WRF spectra','Least squares fit','-5/3 cascade'),loc=3)
pylab.savefig('ke_spectra_wrf.png')
plt.show()
