#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import pylab 

time  = np.loadtxt('time.txt')
land  = np.loadtxt('land.txt')
sea   = np.loadtxt('sea.txt')
aland = np.loadtxt('land_area.txt')
asea  = np.loadtxt('sea_area.txt')

nt = len(time)
ns = nt/8

time = time/24.0
dt   = 1.0*60.0*60.0
land = dt*land
sea  = dt*sea

area  = 1334.3*1334.3
nland = np.round(aland/area)
nsea  = np.round(asea/area)

avgland = np.zeros(nt)
avgsea  = np.zeros(nt)
for ii in np.arange(nt):
	if nland[ii] > 1.0e-4:
		avgland[ii] = land[ii]/nland[ii]
	if nsea[ii] > 1.0e-4:
		avgsea[ii] = sea[ii]/nsea[ii]

fig=plt.figure()
ax=fig.add_subplot(111)
#plt.plot(time,avgland,'.',time,avgsea,'.')
plt.plot(time,land,'.',time,sea,'.')
plt.legend(('land','sea'),loc=0)
plt.xlabel('local time since midnight Feb. 2$^\mathrm{nd}$, (days)')
#plt.ylabel('average precipitation (mm)')
plt.ylabel('precipitation (mm)')
plt.title('Precipitation vs. local time for land and sea')
#pylab.savefig('avg_precip_v_time_96.png')
pylab.savefig('precip_v_time_96.png')
plt.show()
