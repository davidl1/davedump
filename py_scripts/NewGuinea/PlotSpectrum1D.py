#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import pylab

e=np.loadtxt('kenergy_1d_fourier.txt')
k=np.loadtxt('wavenum_1d_fourier.txt')
n=len(e)
#k=np.arange(n)
#k[n/2:]=-k[n/2:0:-1]
#k=np.abs(k)

print len(k)

n2=512
ln4dx=np.log10(k[n2])
print 'k: ',k[n2],'\tln(1/4dx): ',ln4dx
print 'k: ',2.0*np.pi/4.0/1333.687,'\tln(1/4dx): ',np.log10(2.0*np.pi/4.0/1333.687)
#print 'k: ',2.0*np.pi/4.0/3997.1646,'\tln(1/4dx): ',np.log10(2.0*np.pi/4.0/3997.1646)

# plot a least squares fit to the data
lk = np.log10(k[1:len(k)-1])
#e  = np.abs(e)
le = np.log10(e[1:len(e)-1])

#inds1 = np.arange(n/4-4)+4
inds1 = np.arange(n2-1-4)+4
A = np.vstack([lk[inds1],np.ones(len(lk[inds1]))]).T
m1,c1 = np.linalg.lstsq(A,le[inds1])[0]
print m1,c1

#inds2 = np.arange(3*n/4-6)+n/4
inds2 = np.arange(n-n2-2)+n2-2
A = np.vstack([lk[inds2],np.ones(len(lk[inds2]))]).T
m2,c2 = np.linalg.lstsq(A,le[inds2])[0]
print m2,c2

#plt.plot(lk,le,'.-',lk[inds1],m1*lk[inds1]+c1,lk[inds2],m2*lk[inds2]+c2,lk,-5.0/3.0*lk+0.5*c1,[ln8dx,ln8dx],[5,14])
#plt.xlabel('$ln(K) (m^{-1})$')
#plt.ylabel('$ln(E) (m^3/s^2)$')
#plt.title('Kinetic energy spectra, UM')
#plt.legend(('UM spectra','$k<2\pi/4\Delta x, \mathrm{d}ln(E)/\mathrm{d}ln(K)=$'+str(m1)[:6],\
#'$k>2\pi/4\Delta x, \mathrm{d}\ln(E)/\mathrm{d}ln(K)=$'+str(m2)[:6],'-5/3 cascade'),loc=3)
#pylab.savefig('ke_spectra_fourier_um.png')
#plt.show()

plt.loglog(k[:-1],e[:-1],'.-')
plt.loglog(k[inds1],+1.2e+4*c1*np.power(k[inds1],m1))
plt.loglog(k[inds2],-1.0e-3*c2*np.power(k[inds2],m2))
plt.loglog(k[1:],1.0e+2*np.power(k[1:],-5.0/3.0))
plt.loglog([k[n2],k[n2]],[1.0e+5,1.0e+14])
plt.xlabel('$ln(K) (m^{-1})$')
plt.ylabel('$ln(E) (m^3/s^2)$')
plt.title('Kinetic energy spectra, UM')
plt.legend(('UM spectra','$k<2\pi/4\Delta x, \mathrm{d}ln(E)/\mathrm{d}ln(K)=$'+str(m1)[:6],\
'$k>2\pi/4\Delta x, \mathrm{d}\ln(E)/\mathrm{d}ln(K)=$'+str(m2)[:6],'-5/3 cascade'),loc=3)
pylab.savefig('ke_spectra_fourier_um.png')
plt.show()

