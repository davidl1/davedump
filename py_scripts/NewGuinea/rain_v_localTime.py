#!/usr/bin/env python

#module load pythonlib/ScientificPython

from Scientific.IO.NetCDF import NetCDFFile
import matplotlib.pyplot as plt
import numpy as np
import pylab

#extract the netCDF files
lsm=NetCDFFile('LandSeaMask.nc')
land=lsm.variables['lsm']
lat=lsm.variables['latitude']
lon=lsm.variables['longitude']
land_data=land.getValue()
lat_data=lat.getValue()
lon_data=lon.getValue()

T=[]
t=np.zeros(10)
for ii in np.arange(10):
	file='Theta0' + str(ii) + '.nc'
	print file
	tt=NetCDFFile(file)
	time=tt.variables['t']
	t[ii]=24*time.getValue()
	theta=tt.variables['theta']
	theta_data=theta.getValue()
	T.append(theta_data[0,0,:,:])

nx=len(lon_data)
ny=len(lat_data)
print lat_data

time=np.zeros(10*nx)
land=np.zeros(10*nx)
sea=np.zeros(10*nx)

#calculate the rain over land and sea - weighted by the latitude for different grid sizes
for ii in np.arange(10):
	time[ii*nx:(ii+1)*nx]=ii+lon_data/360
	Ti=T[ii]
	for jj in np.arange(nx):
		nl=0
		ns=0
		for kk in np.arange(ny):
			if land_data[0,0,kk,jj] == 1:
				nl=nl+1
				land[ii*nx+jj]=land[ii*nx+jj]+Ti[kk,jj]*np.cos(np.pi*lat_data[kk]/180)
			else:
				ns=ns+1
				sea[ii*nx+jj]=sea[ii*nx+jj]+Ti[kk,jj]*np.cos(np.pi*lat_data[kk]/180)
		land[ii*nx+jj]=land[ii*nx+jj]/nl
		sea[ii*nx+jj]=sea[ii*nx+jj]/ns

#plot the fields
fig=plt.figure()
ax=fig.add_subplot(111)
plt.plot(time[0:3*nx],land[0:3*nx],time[0:3*nx],sea[0:3*nx])
plt.legend(('land','sea'),loc=0)
plt.xlabel('local time (hrs)')
plt.ylabel('potential temperature')
plt.title('potential temperature vs. local time for land and sea')
pylab.savefig('ptemp_v_time.png')
plt.show()
