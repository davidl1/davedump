#!/usr/bin/env python

from Scientific.IO.NetCDF import NetCDFFile
import matplotlib.pyplot as plt
import matplotlib.colors as cls
import numpy as np
import pylab
from Utils import *

#times = ['020112','020200','020212','020300','020312','020400','020412','020500','020512']
times = ['020200','020212','020300','020312','020400','020412','020500','020512']
#times = ['020200','020212']

filename = '../' + '020112' + '/lsm_' + '020112' + '.nc'
lsm = LoadField(filename,'lsm')
lon = LoadField(filename,'longitude')
lat = LoadField(filename,'latitude')

filename = '../' + '020112' + '/ht_' + '020112' + '.nc'
ht = LoadField(filename,'ht')

precip_t = np.zeros((lsm.shape[2],lsm.shape[3]))
dt = 1.0*60*60
Re = 6371000.0
dx = 2.0*np.pi*Re*(lon[-1]-lon[0])/(len(lon)-1)/360.0
dx = dx*np.ones(len(lon))
dy = np.zeros(len(lat))
for ii in np.arange(len(dy))-1:
	dy[ii] = 2.0*np.pi*Re*(lat[ii+1]-lat[ii])/360.0
dy[-1] = dy[-2]
area = np.outer(dy,dx)

# precip_2 is the hourly averaged dump, _1 is instantaneous
for time in times:
	filename = '../' + time + '/precip_2_' + time + '.nc'
	precip_i = LoadField(filename,'precip')
	for time_i in np.arange(precip_i.shape[0]):
		precip_t = precip_t + precip_i[time_i,0,:,:]

# rescale to mm (10^-3 m)
rho = 1.0e+3
m2mm = 1.0e+3
precip_t = (dt/rho*m2mm)*precip_t

# calculate averages
lsm = lsm[0,0,:,:]
slm = (lsm+1)%2
totland = sum(sum(lsm*area))
totsea  = sum(sum(slm*area))
totlandrain = sum(sum(lsm*area*precip_t))
totsearain  = sum(sum(slm*area*precip_t))
print 'avg. precip. land:  ', totlandrain/totland
print 'avg. precip. sea:   ', totsearain/totsea
print 'avg. precip. total: ', sum(sum(precip_t*area))/sum(sum(area))

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon,lat,lsm,colors='k')
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
#levs=[0.,2.,5.,12.,25.,50.,100.,150.,200.,400.]
levs=[0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
cmap=cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm=cls.BoundaryNorm(levs,cmap.N)
co = ax.contourf(lon,lat,precip_t,levels=levs,cmap=cmap,norm=norm)
plt.title('Accumulated rainfall for 96 hours (mm) - (UM)')
plt.colorbar(co,orientation='horizontal')
pylab.savefig('accum_rain_96.png')
plt.show()

#save fields to file
np.savetxt('lat_96_um.txt',lat)
np.savetxt('lon_96_um.txt',lon)
np.savetxt('pcp_96_um.txt',precip_t)
