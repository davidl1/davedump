#!/usr/bin/env python

# Calculate the mesoscale KE spectrum for high resolution nested model
# Generate from 1D east-west spectral averaged over latitude and height 
# (3-9km), excluding first 15 horizontal grid points in each dimension
#
# Reference:
#	Skamarock, W. C. (2004) Evaluating Mesoscale NWP Models Using 
#	Kinetic Energy Spectra, Mon. Wea. Rev. 132, 3019-3032

import math
import numpy as np
from scipy.fftpack import *
from scipy.interpolate import *
from matplotlib import pyplot as plt
import pylab
from Utils import *
from Interp3D import *

def RemoveTrend(A,X):
	Y   = np.vstack([X,np.ones(len(X))]).T
	m,c = np.linalg.lstsq(Y,A)[0]
	L   = m*X+c
	return A-L

def FilterEdges(A,X):
	n       = X.shape[0]
	m       = n/10
	L       = 0.5-0.5*np.cos(np.pi*(X[:m]-X[0])/(X[m-1]-X[0]))
	R       = L[::-1] 
	B       = np.ones(n)
        B[:m]   = L
        B[n-m:] = R
	return B*A

def AvgFourTrans(A,z,mz,lon,lat,ht,orog):
	nx = A.shape[2]
	ny = A.shape[1]
	nz = A.shape[0]

	B     = np.zeros((mz,ny,nx))
	Z     = np.linspace(z[0],z[1],mz)
	httop = ht[-1]
	htinv = 1.0/(ht[-1]-ht[0])

	# interpolate to regular points in z
	print 'interpolating to regular points in z...'
	for ii in np.arange(ny):
		if ii%10 == 0:
			print ii,' of ',ny
		for jj in np.arange(nx):
			Zi         = orog[ii,jj] + ht*(httop-orog[ii,jj])*htinv
			B[:,ii,jj] = griddata(Zi,A[:,ii,jj],Z,method='cubic')


	# remove the linear trend, filter the edges and perform the fft
	C = np.zeros((mz,ny,nx),dtype=complex)
	for ii in np.arange(mz):
		for jj in np.arange(ny):
			c          = FilterEdges(RemoveTrend(B[ii,jj,:],lon),lon)
			C[ii,jj,:] = fft(c)

	return C

def AvgFourTrans2(A,z,mz,lon,lat,ht):
	nx = A.shape[2]
	ny = A.shape[1]
	nz = A.shape[0]

	B     = np.zeros((mz,ny,nx))
	Z     = np.linspace(z[0],z[1],mz)

	# interpolate to regular points in z
	print 'interpolating to regular points in z...'
	for ii in np.arange(ny):
		if ii%10 == 0:
			print ii,' of ',ny
		for jj in np.arange(nx):
			B[:,ii,jj] = griddata(ht[:,ii,jj],A[:,ii,jj],Z,method='cubic')


	# remove the linear trend, filter the edges and perform the fft
	C = np.zeros((mz,ny,nx),dtype=complex)
	for ii in np.arange(mz):
		for jj in np.arange(ny):
			c          = FilterEdges(RemoveTrend(B[ii,jj,:],lon),lon)
			C[ii,jj,:] = fft(c)

	return C

velx = LoadField('../020212/velx_020212.nc','u')
vely = LoadField('../020212/vely_020212.nc','v')
velz = LoadField('../020212/velz_020212.nc','dz_dt')
orog = LoadField('../020212/orog.nc','ht')
lon = LoadField('../020212/rho_020212.nc','longitude')
lat = LoadField('../020212/rho_020212.nc','latitude')
ht  = LoadField('../020212/rho_020212.nc','hybrid_ht')
ht2 = LoadField('ht_rho.nc','ht')
#velx = LoadField('../020212/u_020212_4km.nc','u')
#vely = LoadField('../020212/v_020212_4km.nc','v')
#velz = LoadField('../020212/dz_dt_020212_4km.nc','dz_dt')
#orog = LoadField('../020212/ht_020212_4km.nc','ht')
#lon =  LoadField('../020212/ht_020212_4km.nc','longitude')
#lat =  LoadField('../020212/ht_020212_4km.nc','latitude')
#ht  =  LoadField('../020212/u_020212_4km.nc','hybrid_ht')

nx0 = len(lon)
ny0 = len(lat)
nz0 = len(ht)
print nx0,ny0

xinds = 176+np.arange(nx0-351)
yinds = 132+np.arange(ny0-263)
#xinds = 258+np.arange(nx0-515)
#yinds = 194+np.arange(ny0-387)

xi = xinds[0]
xf = xinds[-1]
yi = yinds[0]
yf = yinds[-1]
velx = velx[0,:,yi:yf,xi:xf]
vely = vely[0,:,yi:yf,xi:xf]
velz = velz[0,:,yi:yf,xi:xf]
orog = orog[0,:,yi:yf,xi:xf]
lon  = lon[xi:xf]
lat  = lat[yi:yf]
ht2  = ht2[0,:,yi:yf,xi:xf]

print velx.shape
print ht2.shape

# downscale...
#lon  = Downscale1D(lon)
#lat  = Downscale1D(lat)
#velx = Downscale2D(velx)
#vely = Downscale2D(vely)
#velz = Downscale2D(velz)
#orog = Downscale2D(orog)
#ht2  = Downscale2D(ht2)

orog = orog[0,:,:]
x = [lon[0],lon[-1]]
y = [lat[0],lat[-1]]
z = [5000.0,9000.0]

nx1 = len(lon)
ny1 = len(lat)
nz1 = 16
print nx1,ny1

Re=6378100.0
dx=2.0*np.pi*Re*(x[1]-x[0])/360.0/nx1
#L=nx1*dx
#k=2.0*np.pi*8.0/L
print 'dx: ',dx
print 'x range: ',x[0],'\t',x[1]
print 'y range: ',y[0],'\t',y[1]
print nx1,ny1
#print '  k:       ',k
#print '  l:       ',L*k/2.0/np.pi
#print 'ln(k):     ',np.log10(k)

#average the vertical velocity to cell centers
print 'averaging vertical velocity to height levels...'
velz2 = np.zeros((velx.shape))
for jj in np.arange(ny1):
        for kk in np.arange(nx1):
                for ii in np.arange(nz0):
                        velz2[ii,jj,kk] = 0.5*(velz[ii,jj,kk] + velz[ii+1,jj,kk])

velz = velz2

Re              = 6378100.0
nxh             = nx1/2
wavenum         = np.arange(nx1)
wavenum[nxh+1:] = np.arange(nxh-1)-nxh+1
wavenum         = wavenum*2.0*np.pi/(2.0*np.pi*Re*(x[1]-x[0])/360.0)

#U  = AvgFourTrans(velx,z,nz1,lon,lat,ht,orog)
#V  = AvgFourTrans(vely,z,nz1,lon,lat,ht,orog)
#W  = AvgFourTrans(velz,z,nz1,lon,lat,ht,orog)
U  = AvgFourTrans2(velx,z,nz1,lon,lat,ht2)
V  = AvgFourTrans2(vely,z,nz1,lon,lat,ht2)
W  = AvgFourTrans2(velz,z,nz1,lon,lat,ht2)
KE = 0.5*(U.real*U.real + U.imag*U.imag + V.real*V.real + V.imag*V.imag + W.real*W.real + W.imag*W.imag)

# average in y and z
kez = np.zeros((ny1,nx1))
for ii in np.arange(ny1):
        for jj in np.arange(nx1):
                kez[ii,jj] = np.sum(KE[:,ii,jj])/nz1

keyz = np.zeros((nx1))
for ii in np.arange(nx1):
        keyz[ii] = np.sum(kez[:,ii])/ny1

kenergy = 2.0*np.pi*Re*(x[1]-x[0])/360.0*keyz

np.savetxt('wavenum_1d_fourier_downscale.txt',wavenum)
np.savetxt('kenergy_1d_fourier_downscale.txt',kenergy)

# plot a least squares fit to the data
lk = np.log10(wavenum[1:len(wavenum)])
le = np.log10(kenergy[1:len(kenergy)])
A = np.vstack([lk,np.ones(len(lk))]).T
m,c = np.linalg.lstsq(A,le)[0]
print 'least squares fit... ',m,c
print -5.0/3.0
plt.plot(lk,le,'.',lk,m*lk+c,lk,-5.0/3.0*lk+c)
plt.xlabel('$ln(||K||)$')
plt.ylabel('$ln(E)$')
plt.title('Vertically averaged 1D kinetic energy spectra')
plt.legend(('UM spectra','Least squares fit','-5/3 cascade'),loc=3)
pylab.savefig('ke_spectra.png')
plt.show()
