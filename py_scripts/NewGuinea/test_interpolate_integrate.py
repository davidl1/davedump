#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from ChebTrans import *
from Interp3D import *

def f(x,r):
	return np.sqrt(r*r-x*x)

r = 2.0
n0 = 10
X0 = np.arange(n0+1)
X0 = r*2.0*(X0-0.5*n0)/n0
Y0 = f(X0,r)

n1 = 8
X1 = -r*np.cos(np.pi*np.arange(n1+1)/n1)
Y1 = np.zeros(n1+1)

ord = 3

for imin in np.arange(n1+1):
	if X1[imin] < X0[ord/2]:
		Y1[imin] = f(X1[imin],r)
	else:
		break

for imax in np.arange(n1+1):
	if X1[n1-imax] > X0[n0-ord/2]:
		Y1[n1-imax] = f(X1[n1-imax],r)
	else:
		break

for ii in np.arange(n1-imax-imin+1)+imin:
	Y1[ii] = Interp1D(Y0,X0,X1[ii],ord)

a1=Integrate(Y1,r)

print a1, 0.5*np.pi*r*r

plt.plot(X0,Y0,'-o',X1,Y1,'-o')
plt.show()
