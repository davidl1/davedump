#!/usr/bin/env python

# Calculate the mesoscale KE spectrum for high resolution nested model
# Generate from 1D east-west spectral averaged over latitude and height 
# (3-9km), excluding first 15 horizontal grid points in each dimension
#
# Reference:
#	Skamarock, W. C. (2004) Evaluating Mesoscale NWP Models Using 
#	Kinetic Energy Spectra, Mon. Wea. Rev. 132, 3019-3032

import math
import numpy as np
from matplotlib import pyplot as plt
import pylab
from Utils import *
from ChebTrans import *
from Interp3D import *

velx = LoadField('../020212/velx_020212.nc','u')
vely = LoadField('../020212/vely_020212.nc','v')
velz = LoadField('../020212/velz_020212.nc','dz_dt')
rho  = LoadField('../020212/rho_020212.nc','unspecified')
orog = LoadField('../020212/orog.nc','ht')

lon = LoadField('../020212/rho_020212.nc','longitude')
lat = LoadField('../020212/rho_020212.nc','latitude')
ht  = LoadField('../020212/rho_020212.nc','hybrid_ht')

nx0 = len(lon)
ny0 = len(lat)
nz0 = len(ht)

xinds = 432+np.arange(nx0-863)
yinds = 132+np.arange(ny0-263)

xi = xinds[0]
xf = xinds[-1]
yi = yinds[0]
yf = yinds[-1]
velx = velx[0,:,yi:yf,xi:xf]
vely = vely[0,:,yi:yf,xi:xf]
velz = velz[0,:,yi:yf,xi:xf]
rho  = rho[0,:,yi:yf,xi:xf]
orog = orog[0,:,yi:yf,xi:xf]
lon  = lon[xi:xf]
lat  = lat[yi:yf]

# downscale...
lon  = Downscale1D(lon)
lat  = Downscale1D(lat)
velx = Downscale2D(velx)
vely = Downscale2D(vely)
velz = Downscale2D(velz)
rho  = Downscale2D(rho)
orog = Downscale2D(orog)

nx0 = len(lon)
ny0 = len(lat)
nz0 = len(ht)

orog = orog[0,:,:]
x = [lon[1],lon[-2]]
y = [lat[1],lat[-2]]

Re    = 6371000.0
xhalf = 0.5*(x[1]-x[0])*2.0*np.pi*Re/360.0
yhalf = 0.5*(y[1]-y[0])*2.0*np.pi*Re/360.0
print 'scale: ',xhalf,'\t',yhalf
z = [5000.0,9000.0] #should start at 3000.0 meters, but New Guinea is too mountainous...

nx1 = 512
ny1 = 512 #math.trunc(nx1*(y[1]-y[0])/(x[1]-x[0]))
nz1 = 8

order = 5

#TODO: chech the other fields to see if they're staggered and if so remap to cell centers

#average the vertical velocity to cell centers
print 'averaging vertical velocity to height levels...'
velz2 = np.zeros((velx.shape))
for jj in np.arange(ny0):
	for kk in np.arange(nx0):
		for ii in np.arange(nz0):
			velz2[ii,jj,kk] = 0.5*(velz[ii,jj,kk] + velz[ii+1,jj,kk])

velz = velz2
print velz.shape

#remove the r^2 term from the density
Re = 6335437.0
H1 = ht[-1]
print 'removing r^2 term from the density field...'
for yi in np.arange(ny0):
	for xi in np.arange(nx0):
		R = orog[yi,xi] + ht*(H1-orog[yi,xi]) + Re
		#rho[:,yi,xi] = rho[:,yi,xi]/R/R
                Z = orog[yi,xi] + ht*(H1-orog[yi,xi])
                Vz = np.zeros(nz0)
                for zi in 1+np.arange(nz0-2):
                        Vz[zi] = 0.5*(Z[zi+1]-Z[zi-1])
                Vz[0]  = 0.5*(Z[1]-Z[0])
                Vz[-1] = 0.5*(Z[-1]-Z[-2])
                rho[:,yi,xi] = rho[:,yi,xi]*Vz*1334.3*1334.3/R/R

print '\t...done.'

#U = InterpBox(velx,x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,order)
#V = InterpBox(vely,x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,order)
#W = InterpBox(velz,x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,order)
#P = InterpBox(rho, x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,order)
U = InterpAndFlatten(velx,x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,0)
V = InterpAndFlatten(vely,x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,0)
W = InterpAndFlatten(velz,x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,0)
#P = InterpAndFlatten(rho, x,y,z,nx1,ny1,nz1,lon,lat,ht,orog,0)

#da = 1334.3*1334.3
#KEu = FlattenKEz(P,U,z,nz1,da)
#KEv = FlattenKEz(P,V,z,nz1,da)
#KEw = FlattenKEz(P,W,z,nz1,da)

#KEu = 0.5*P*U*U
#KEv = 0.5*P*V*V
#KEw = 0.5*P*W*W

#KEux = FlattenKEyz(KEu,y)
#KEvx = FlattenKEyz(KEv,y)
#KEwx = FlattenKEyz(KEw,y)

#KEuc = ChebTrans(KEux)
#KEvc = ChebTrans(KEvx)
#KEwc = ChebTrans(KEwx)

#kenergy = np.zeros(nx1+1)
#wavenum = np.zeros(nx1+1)
#for ii in np.arange(nx1+1):
#	wavenum[ii] = ii
#	kenergy[ii] = 0.5*(KEuc[ii] + KEvc[ii] + KEwc[ii])
#
#xh = 0.5*(x[1]-x[0])
#wavenum = wavenum/xh

#KE  = 0.5*P*(U*U+V*V+W*W)
UC = ChebTrans2D(U)
VC = ChebTrans2D(V)
WC = ChebTrans2D(W)
#PC = ChebTrans2D(P)

#KEC = 0.5*PC*(UC*UC+VC*VC+WC*WC)
KEC = 0.5*(UC*UC+VC*VC+WC*WC)
#KEC = ChebTrans2D(KE)
kenergy = np.zeros(nx1+1)
wavenum = np.arange(nx1+1)/xhalf
for ii in np.arange(nx1+1):
	kenergy[ii] = np.sum(KEC[:,ii])/(ny1+1)
	#kenergy[ii] = Integrate(KEC[:,ii],1)

np.savetxt('wavenum_1d_512.txt',wavenum)
np.savetxt('kenergy_1d_512.txt',kenergy)

plt.loglog(wavenum,kenergy,'.')
plt.xlabel('wavenumber, k m^{-1}')
plt.ylabel('kinetic energy, kg.m^2/s^2')
plt.title('Kinetic energy spectra, 1D Chebyshev')
plt.show()
pylab.savefig('ke_spec.png')
