#!/usr/bin/env python

#module load pythonlib/ScientificPython

from Scientific.IO.NetCDF import NetCDFFile
import matplotlib.pyplot as plt
import numpy as np
import pylab
from Utils import *

times = ['020112','020200','020212','020300','020312','020400','020412','020500','020512']

filename = '../' + times[0] + '/lsm_' + times[0] + '.nc'
lsm = LoadField(filename,'lsm')
lon = LoadField(filename,'longitude')
lat = LoadField(filename,'latitude')

nx=len(lon)
ny=len(lat)

times = times[1:]

filename = '../' + times[0] + '/precip_1_' + times[0] + '.nc'
ltime = LoadField(filename,'t')

nl     = len(ltime)
nt     = len(times)
nsteps = nx*nl*nt
time   = np.zeros(nsteps)
land   = np.zeros(nsteps)
sea    = np.zeros(nsteps)
aland  = np.zeros(nsteps)
asea   = np.zeros(nsteps)

#calculate the rain over land and sea - weighted by the latitude for different grid sizes
Re=6371000.0
dx = 2.0*np.pi*Re*(lon[-1]-lon[0])/(nx-1)/360.0
dy = np.zeros(ny)
for kk in 1+np.arange(ny-2):
	li = 0.5*(lat[kk-1] + lat[kk])
	lf = 0.5*(lat[kk] + lat[kk+1])
	#yi = Re*np.cos(np.pi*li/180)
	#yf = Re*np.cos(np.pi*lf/180)
	#dy[kk] = abs(yf - yi)
	dy[kk] = 2.0*np.pi*Re*(lf-li)/360.0

for tt in np.arange(nt):
	print 'reading: ' + times[tt]
	filename = '../' + times[tt] + '/precip_1_' + times[tt] + '.nc'
	precip = LoadField(filename,'precip')
	for ii in np.arange(nl):
		print '   time: ' + `ii`
		step_i = tt*nl*nx + ii*nx
		time[step_i:step_i+nx] = tt*nl + ii + lon/360
		for jj in np.arange(nx):
			larea = 0.0
			sarea = 0.0
			lrain = 0.0
			srain = 0.0
			rain = precip[ii,0,:,jj]
			for kk in 1+np.arange(ny-2):
				if lsm[0,0,kk,jj] == 1:
					larea = larea + dx*dy[kk]
					#lrain = lrain + rain[kk]*dy[kk]
					lrain = lrain + rain[kk]
				else:
					sarea = sarea + dx*dy[kk]
					#srain = srain + rain[kk]*dy[kk]
					srain = srain + rain[kk]

			land[step_i+jj]  = lrain
			sea[step_i+jj]   = srain
			aland[step_i+jj] = larea
			asea[step_i+jj]  = sarea
			#if larea > 1.0e-4:
			#	land[step_i+jj] = lrain/larea
			#if sarea > 1.0e-4:
			#	sea[step_i+jj] = srain/sarea
		
#save and plot the fields
np.savetxt('time.txt',time)
np.savetxt('land.txt',land)
np.savetxt('sea.txt',sea)
np.savetxt('land_area.txt',aland)
np.savetxt('sea_area.txt',asea)

fig=plt.figure()
ax=fig.add_subplot(111)
plt.plot(time,land,'.',time,sea,'.')
plt.legend(('land','sea'),loc=0)
plt.xlabel('local time (hrs)')
plt.ylabel('precipitation rate (kg/m^2/s)')
plt.title('precipitation vs. local time for land and sea')
pylab.savefig('precip_v_time.png')
plt.show()
