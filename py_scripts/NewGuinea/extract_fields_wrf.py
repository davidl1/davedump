#!/usr/bin/env python

import numpy as np
from scipy.io import netcdf
from matplotlib import pyplot as plt
from matplotlib import colors as cls
import pylab

def ReadFile(filename,fieldname):
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	#m      = field.scale_factor
	#c      = field.add_offset
	#infile.close()
	#return m*field[:,:,:]+c
	return field[:]

def WriteFile(filename,fieldname,field):
	nx = field.shape[1]
	ny = field.shape[0]
	outfile = netcdf.netcdf_file(filename,'w')
	outfile.createDimension(fieldname,ny*nx)
	data    = outfile.createVariable(fieldname,'i',(fieldname,))
	field1d = field.reshape(-1)
	data[:] = field1d
	outfile.close()

def FlipField(field):
	nx = field.shape[1]
	ny = field.shape[0]
	field2=np.zeros((ny,nx))
	field3=np.zeros((ny,nx))
	for ii in np.arange(len(x)):
		field2[:,ii] = field[::-1,ii]
	for ii in np.arange(len(y)):
		field3[ii,:] = field2[ii,::-1]
	return field3

directory = '/scratch/hassimme/WRF/New_Guinea/d03/PRCP/'
filename  = 'prcp_d03_2010-02-'
timedump  = ['02_01:00:00','02_02:00:00','02_03:00:00','02_04:00:00','02_05:00:00','02_06:00:00',
	     '02_07:00:00','02_08:00:00','02_09:00:00','02_10:00:00','02_11:00:00','02_12:00:00',
	     '02_13:00:00','02_14:00:00','02_15:00:00','02_16:00:00','02_17:00:00','02_18:00:00',
	     '02_19:00:00','02_20:00:00','02_21:00:00','02_22:00:00','02_23:00:00','03_00:00:00',
	     '03_01:00:00','03_02:00:00','03_03:00:00','03_04:00:00','03_05:00:00','03_06:00:00',
	     '03_07:00:00','03_08:00:00','03_09:00:00','03_10:00:00','03_11:00:00','03_12:00:00',
	     '03_13:00:00','03_14:00:00','03_15:00:00','03_16:00:00','03_17:00:00','03_18:00:00',
	     '03_19:00:00','03_20:00:00','03_21:00:00','03_22:00:00','03_23:00:00','04_00:00:00',
	     '04_01:00:00','04_02:00:00','04_03:00:00','04_04:00:00','04_05:00:00','04_06:00:00',
	     '04_07:00:00','04_08:00:00','04_09:00:00','04_10:00:00','04_11:00:00','04_12:00:00',
	     '04_13:00:00','04_14:00:00','04_15:00:00','04_16:00:00','04_17:00:00','04_18:00:00',
	     '04_19:00:00','04_20:00:00','04_21:00:00','04_22:00:00','04_23:00:00','05_00:00:00',
             '05_01:00:00','05_02:00:00','05_03:00:00','05_04:00:00','05_05:00:00','05_06:00:00',
	     '05_07:00:00','05_08:00:00','05_09:00:00','05_10:00:00','05_11:00:00','05_12:00:00',
	     '05_13:00:00','05_14:00:00','05_15:00:00','05_16:00:00','05_17:00:00','05_18:00:00',
	     '05_19:00:00','05_20:00:00','05_21:00:00','05_22:00:00','05_23:00:00','06_00:00:00']

lat  = ReadFile(directory+'XLAT_d03_NewGuinea.nc','XLAT')
lon  = ReadFile(directory+'XLONG_d03_NewGuinea.nc','XLONG')
hgt  = ReadFile(directory+'TERRAIN_HGT_d03_NewGuinea.nc','HGT')
lsm  = ReadFile(directory+'LANDMASK_d03_NewGuinea.nc','LANDMASK')

lat  = lat[0,:,:]
lon  = lon[0,:,:]
hgt  = hgt[0,:,:]
lsm  = lsm[0,:,:]
ny   = hgt.shape[0]
nx   = hgt.shape[1]

totrain = np.zeros((ny,nx))
#for time in timedump:
#	print 'reading file: ' + time
#	rainc   = ReadFile(directory+filename+time,'RAINC')
#	rainnc  = ReadFile(directory+filename+time,'RAINNC')
#	totrain = totrain + rainnc[0,:,:]
rain_c0  = ReadFile(directory+filename+timedump[0],'RAINC')
rain_nc0 = ReadFile(directory+filename+timedump[0],'RAINNC')
rain_c1  = ReadFile(directory+filename+timedump[-1],'RAINC')
rain_nc1 = ReadFile(directory+filename+timedump[-1],'RAINNC')
totrain  = rain_c1[0,:,:] + rain_nc1[0,:,:] - rain_c0[0,:,:] - rain_nc0[0,:,:]

WriteFile('rain_020300.nc','TOTRAIN',totrain)
WriteFile('height.nc','HGT',hgt)

# write the geometry
outfile  = netcdf.netcdf_file('geometry.nc','w')
outfile.createDimension('ny',1)
outfile.createDimension('nx',1)
outfile.createDimension('lon',nx)
outfile.createDimension('lat',ny)
nb       = outfile.createVariable('ny','i',('ny',))
na       = outfile.createVariable('nx','i',('nx',))
x        = outfile.createVariable('lon','i',('lon',))
y        = outfile.createVariable('lat','i',('lat',))
nb       = ny
na       = nx
x        = lon
y        = lat
outfile.close()

lon = lon[0,:]
lat = lat[:,0]

Re=6361000.0
dx=2*np.pi*Re*(lon[-1]-lon[0])/(len(lon)-1)/360.0
dy=2*np.pi*Re*(lat[-1]-lat[0])/(len(lat)-1)/360.0
print dx,dy

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon,lat,lsm,colors='k')
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
levs=[0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
cmap=cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm=cls.BoundaryNorm(levs,cmap.N)
co = ax.contourf(lon,lat,totrain,levels=levs,cmap=cmap,norm=norm)
#co = ax.contourf(lon,lat,totrain)
plt.colorbar(co,orientation='horizontal')
plt.title('Accumulated rainfall for 96 hours (mm) - WRF')
pylab.savefig('accum_rain_96_wrf.png')
plt.show()

#save fields to file
np.savetxt('lat_96_wrf.txt',lat)
np.savetxt('lon_96_wrf.txt',lon)
np.savetxt('pcp_96_wrf.txt',totrain)
