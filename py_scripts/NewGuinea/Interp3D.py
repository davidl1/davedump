#!/usr/bin/env python

import numpy as np
from scipy.interpolate import *
from ChebTrans import *

def GetIndices(xi,xj,n):
	nx = len(xi)
	for ii in np.arange(nx):
		if xi[ii] > xj:
			break

	i0 = ii-n/2-1
	if i0 < 0:
		i0 = 0
	if i0 > nx-n-1:
		i0 = nx-n-1
	inds = np.arange(n+1)+i0
	return inds

def Interp1D(yi,xi,xj,n):
	inds = GetIndices(xi,xj,n)

	li = np.ones(n+1)
	yj = 0.0

	for ii in np.arange(n+1):
		for jj in np.arange(n+1):
			if inds[ii] == inds[jj]:
				continue

			li[ii] = li[ii]*(xj-xi[inds[jj]])/(xi[inds[ii]]-xi[inds[jj]])

		yj = yj + yi[inds[ii]]*li[ii]

	return yj

def InterpBox(A,x,y,z,nx,ny,nz,lon,lat,ht,Orog,order):
	print 'interpolating to 3d chebyshev-gauss-lobatto points...'
	xhalf = 0.5*(x[1]-x[0])
	yhalf = 0.5*(y[1]-y[0])
	zhalf = 0.5*(z[1]-z[0])

	xorig = 0.5*(x[1]+x[0])
	yorig = 0.5*(y[1]+y[0])
	zorig = 0.5*(z[1]+z[0])

	X = xorig-xhalf*np.cos(np.pi*np.arange(nx+1)/nx)
	Y = yorig-yhalf*np.cos(np.pi*np.arange(ny+1)/ny)
	Z = zorig-zhalf*np.cos(np.pi*np.arange(nz+1)/nz)

	nlon  = len(lon)
	nlat  = len(lat)
	httop = ht[-1]
	htinv = 1.0/(ht[-1]-ht[0])

	# interpolate to z coords
	Bz = np.zeros((nz+1,nlat,nlon))
	for jj in np.arange(nlat):
		if jj%10 == 0:
			print `jj` + ' of ' + `nlat`
		for kk in np.arange(nlon):
			# TODO: fix rescale the z coods by the orogrophy
			#zi = Orog[jj,kk] + ht*(httop-Orog[jj,kk])
			zi = Orog[jj,kk] + ht*(httop-Orog[jj,kk])*htinv
			#for ii in np.arange(nz+1):
			#	Ai = A[:,jj,kk]
			#	Bz[ii,jj,kk] = Interp1D(Ai,zi,Z[ii],order)
			#	if Bz[ii,jj,kk] < 0.0:
			#		print 'rho lt 0: ',Bz[ii,jj,kk],'\tz: ',Z[ii],'\tdims: ',z
			#		print Ai
			Bz[:,jj,kk] = griddata(zi,A[:,jj,kk],Z,method='cubic')
	
	# interpolate to y coords
	By = np.zeros((nz+1,ny+1,nlon))
	for kk in np.arange(nlon):
		if kk%10 == 0:
			print `kk` + ' of ' + `nlon`
		for ii in np.arange(nz+1):
			#for jj in np.arange(ny+1):
			#	Ai = Bz[ii,:,kk]
			#	By[ii,jj,kk] = Interp1D(Ai,lat,Y[jj],order)
			#	if By[ii,jj,kk] < 0.0:
			#		print 'rho lt 0: ',By[ii,jj,kk],'\ty: ',Y[jj],'\tdims: ',y
			#		print Ai
			By[ii,:,kk] = griddata(lat,Bz[ii,:,kk],Y,method='cubic')

	Bx = np.zeros((nz+1,ny+1,nx+1))
	for ii in np.arange(nz+1):
		if ii%4 == 0:
			print `ii` + ' of ' + `nz+1`
		for jj in np.arange(ny+1):
			#for kk in np.arange(nx+1):
			#	Ai = By[ii,jj,:]
			#	Bx[ii,jj,kk] = Interp1D(Ai,lon,X[kk],order)
			#	if Bx[ii,jj,kk] < 0.0:
			#		print 'rho lt 0: ',Bx[ii,jj,kk],'\tx: ',X[kk],'\tdims: ',x
			#		print Ai
			Bx[ii,jj,:] = griddata(lon,By[ii,jj,:],X,method='cubic')

	print '         ...done.'
	return Bx

def FlattenKEz(P,U,z,nz,da):
	nx = U.shape[2]
	ny = U.shape[1]
	zh = 0.5*(z[1]-z[0])
	zo = 0.5*(z[1]+z[0])

	Z = zo-zh*np.cos(np.pi*np.arange(nz+1)/nz)
	V = np.zeros(nz+1)
	for ii in 1+np.arange(nz-1):
		V[ii] = 0.5*(Z[ii+1]+Z[ii-1])
	V[0]  = 0.5*(Z[1]-Z[0])
	V[nz] = 0.5*(Z[nz]-Z[nz-1])
	V = da*V

	KE  = np.zeros((ny,nx))
	KEz = np.zeros((ny,nx))

	for ii in np.arange(ny):
		for jj in np.arange(nx):
			Uz  = U[:,ii,jj]
			Pz  = P[:,ii,jj]*V
			#KEz = np.inner(Uz,Uz)
			#KEz = np.inner(Pz,KEz)
			KEz = Pz*Uz*Uz
			KE[ii,jj] = Integrate(KEz,zh)

	return 0.5*KE

def FlattenKEyz(KEz,y):
	nx = KEz.shape[1]
	yh = 0.5*(y[1]-y[0])

	KE = np.zeros(nx)

	for ii in np.arange(nx):
		KE[ii] = Integrate(KEz[:,ii],yh)

	return KE

def Interp3Dto2D(A,z,nz,lon,lat,ht,Orog):
	print 'interpolating to 2d regular field...'

	#zhalf = 0.5*(z[1]-z[0])
	#zorig = 0.5*(z[1]+z[0])
	#Z = zorig-zhalf*np.cos(np.pi*np.arange(nz+1)/nz)
	Z     = np.linspace(z[0],z[1],nz+1)
	Z     = Z[0:nz] + 0.5*(Z[1]-Z[0])

	nlon  = len(lon)
	nlat  = len(lat)
	httop = ht[-1]
	htinv = 1.0/(ht[-1]-ht[0])

	# interpolate to z coords
	#Bz = np.zeros((nz+1,nlat,nlon))
	B = np.zeros((nlat,nlon))
	for jj in np.arange(nlat):
		if jj%10 == 0:
			print `jj` + ' of ' + `nlat`
		for kk in np.arange(nlon):
			zi = Orog[jj,kk] + ht*(httop-Orog[jj,kk])*htinv
			
			#for ii in np.arange(nz+1):
				#Ai = A[:,jj,kk]
				#Bz[ii,jj,kk] = Interp1D(Ai,zi,Z[ii],order)
			Bz       = griddata(zi,A[:,jj,kk],Z,method='cubic')
			B[jj,kk] = np.sum(Bz)/nz
	
	#B = np.zeros((nlat,nlon))
	#for ii in np.arange(nlat):
	#	for jj in np.arange(nlon):
	#		B[ii,jj] = Integrate(Bz[:,ii,jj],zhalf)

	return B

def Downscale1D(A):
	nx   = len(A)/2
	inds = 2*np.arange(nx)
	B    = A[inds]
	return B

def Downscale2D(A):
	print 'downscaing...',A.shape
	nx = A.shape[2]/2
	ny = A.shape[1]/2
	nz = A.shape[0]

	B = np.zeros((nz,ny,nx))

	for ii in np.arange(ny):
		for jj in np.arange(nx):
			B[:,ii,jj] = A[:,2*ii,2*jj]

	return B

def InterpAndFlatten(A,x,y,z,nx,ny,nz,lon,lat,ht,Orog,byvol):
	print 'interpolating to 3d chebyshev-gauss-lobatto points...'
	xhalf = 0.5*(x[1]-x[0])
	yhalf = 0.5*(y[1]-y[0])
	zhalf = 0.5*(z[1]-z[0])

	xorig = 0.5*(x[1]+x[0])
	yorig = 0.5*(y[1]+y[0])
	zorig = 0.5*(z[1]+z[0])

	X = xorig-xhalf*np.cos(np.pi*np.arange(nx+1)/nx)
	Y = yorig-yhalf*np.cos(np.pi*np.arange(ny+1)/ny)
	#Z = zorig-zhalf*np.cos(np.pi*np.arange(nz+1)/nz)
	Z = np.linspace(z[0],z[1],nz+1)
	Z = Z[0:nz] + 0.5*(Z[1]-Z[0])

	nlon  = len(lon)
	nlat  = len(lat)
	httop = ht[-1]
	htinv = 1.0/(ht[-1]-ht[0])

	#da = 1334.4*1334.3
	#V = np.zeros(nz+1)
	#for ii in 1+np.arange(nz-1):
	#	V[ii] = 0.5*(Z[ii+1]-Z[ii-1])
	#V[0]  = 0.5*(Z[1]-Z[0])
	#V[nz] = 0.5*(Z[nz]-Z[nz-1])
	#V = da*V

	# interpolate to z coords and integrate
	Bz = np.zeros((nlat,nlon))
	for jj in np.arange(nlat):
		if jj%10 == 0:
			print `jj` + ' of ' + `nlat`
		for kk in np.arange(nlon):
			zi        = Orog[jj,kk] + ht*(httop-Orog[jj,kk])*htinv
			Ai        = A[:,jj,kk]
			bz        = griddata(zi,Ai,Z,method='cubic')
			if byvol:
				bz = bz*V
			#Bz[jj,kk] = Integrate(bz,zhalf)
			Bz[jj,kk] = np.sum(bz)/nz
	
	# interpolate to y coords
	print 'interp y...'
	By = np.zeros((ny+1,nlon))
	for kk in np.arange(nlon):
		Bi       = Bz[:,kk]
		By[:,kk] = griddata(lat,Bi,Y,method='cubic')

	# interpolate to x coords
	print 'interp z...'
	Bx = np.zeros((ny+1,nx+1))
	for jj in np.arange(ny+1):
		Bi       = By[jj,:]
		Bx[jj,:] = griddata(lon,Bi,X,method='cubic')

	print '         ...done.'
	return Bx

