#!/usr/bin/env python

import numpy as np
from scipy.io import netcdf
from matplotlib import pyplot as plt
from matplotlib import colors as cls
import pylab

def ReadFile(filename,fieldname):
	infile = netcdf.netcdf_file(filename,'r')
	field  = infile.variables[fieldname]
	infile.close()
	return field[:]

directory = '/data/cdgs1/hassimme/POSTDOC/TRMM/3B42/Ver7/'
filename  = '3B42.'
timedump = ['20100206.00.7.nc','20100202.03.7.nc','20100202.06.7.nc','20100202.09.7.nc',
            '20100202.12.7.nc','20100202.15.7.nc','20100202.18.7.nc','20100202.21.7.nc',
            '20100203.00.7.nc','20100203.03.7.nc','20100203.06.7.nc','20100203.09.7.nc',
            '20100203.12.7.nc','20100203.15.7.nc','20100203.18.7.nc','20100203.21.7.nc',
            '20100204.00.7.nc','20100204.03.7.nc','20100204.06.7.nc','20100204.09.7.nc',
            '20100204.12.7.nc','20100204.15.7.nc','20100204.18.7.nc','20100204.21.7.nc',
            '20100205.00.7.nc','20100205.03.7.nc','20100205.06.7.nc','20100205.09.7.nc',
            '20100205.12.7.nc','20100205.15.7.nc','20100205.18.7.nc','20100205.21.7.nc']

lat  = ReadFile(directory+filename+timedump[0],'latitude')
lon  = ReadFile(directory+filename+timedump[0],'longitude')
#hgt  = ReadFile(directory+'TERRAIN_HGT_d03_NewGuinea.nc','HGT')
#lsm  = ReadFile(directory+'LANDMASK_d03_NewGuinea.nc','LANDMASK')

#hgt  = hgt[0,:,:]
#lsm  = lsm[0,:,:]
#print lat[80:220]
#print lon[1220:1340]
xinds = np.arange(107)+1227
yinds = np.arange(88)+123
lon   = lon[xinds]
lat   = lat[yinds]
ny    = len(lat)
nx    = len(lon)
print nx,ny

#get the land sea mask
lat2 = ReadFile(directory+'XLAT_d03_NewGuinea.nc','XLAT')
lon2 = ReadFile(directory+'XLONG_d03_NewGuinea.nc','XLONG')
hgt2 = ReadFile(directory+'TERRAIN_HGT_d03_NewGuinea.nc','HGT')

totrain = np.zeros((ny,nx))
for time in timedump:
	print 'reading file: ' + time
	rain    = ReadFile(directory+filename+time,'pcp')
	rain2   = rain[0,yinds[0]:yinds[-1]+1,xinds[0]:xinds[-1]+1]
	# replace missing values with 0
	missing_value = 0
	for ii in np.arange(rain2.shape[0]):
		for jj in np.arange(rain2.shape[1]):
			if rain2[ii,jj] > -1.0e-4:
				totrain[ii,jj] = totrain[ii,jj] + 3.0*rain2[ii,jj]
			else:
				missing_value = 1

	if missing_value:
		print 'missing value!'

Re=6361000.0
dx=2*np.pi*Re*(lon[-1]-lon[0])/(len(lon)-1)/360.0
dy=2*np.pi*Re*(lat[-1]-lat[0])/(len(lat)-1)/360.0
print dx,dy

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contour(lon2[0,0,:],lat2[0,:,0],hgt2[0,:,:],colors='k')
#co = ax.contourf(lon,lat,totrain)
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
levs=[0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
cmap=cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm=cls.BoundaryNorm(levs,cmap.N)
co = ax.contourf(lon,lat,totrain,levels=levs,cmap=cmap,norm=norm)

plt.colorbar(co,orientation='horizontal')
plt.title('Accumulated rainfall for 96 hours - TRMM')
pylab.savefig('accum_rain_96_trmm.png')
plt.show()

#save to file
np.savetxt('lat_96_trmm.txt',lat)
np.savetxt('lon_96_trmm.txt',lon)
np.savetxt('pcp_96_trmm.txt',totrain)
