#!/usr/bin/env python

#module load pythonlib/ScientificPython

import numpy as np
from scipy.io import netcdf
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import pylab

def ReadFile(filename,fieldname):
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
        infile.close()
	return field[:]

directory = '/data/cdgs1/hassimme/POSTDOC/TRMM/3B42/Ver7/'
filename  = '3B42.'
timedump = ['20100206.00.7.nc','20100202.03.7.nc','20100202.06.7.nc','20100202.09.7.nc',
            '20100202.12.7.nc','20100202.15.7.nc','20100202.18.7.nc','20100202.21.7.nc',
            '20100203.00.7.nc','20100203.03.7.nc','20100203.06.7.nc','20100203.09.7.nc',
            '20100203.12.7.nc','20100203.15.7.nc','20100203.18.7.nc','20100203.21.7.nc',
            '20100204.00.7.nc','20100204.03.7.nc','20100204.06.7.nc','20100204.09.7.nc',
            '20100204.12.7.nc','20100204.15.7.nc','20100204.18.7.nc','20100204.21.7.nc',
            '20100205.00.7.nc','20100205.03.7.nc','20100205.06.7.nc','20100205.09.7.nc',
            '20100205.12.7.nc','20100205.15.7.nc','20100205.18.7.nc','20100205.21.7.nc']

lat  = ReadFile(directory+filename+timedump[0],'latitude')
lon  = ReadFile(directory+filename+timedump[0],'longitude')
lat0 = ReadFile(directory+'XLAT_d03_NewGuinea.nc','XLAT')
lon0 = ReadFile(directory+'XLONG_d03_NewGuinea.nc','XLONG')
lsm0 = ReadFile(directory+'LANDMASK_d03_NewGuinea.nc','LANDMASK')
lat0 = lat0[0,:,0]
lon0 = lon0[0,0,:]
lsm0 = lsm0[0,:,:]

#xinds = np.arange(106)+1228
#yinds = np.arange(88)+123
xinds = np.arange(105)+1228
yinds = np.arange(84)+127
lon   = lon[xinds]
lat   = lat[yinds]
ny    = len(lat)
nx    = len(lon)
print 'long span (degs): ',lon[0],'\t',lon[-1]
print 'lat  span (degs): ',lat[0],'\t',lat[-1]

# interpolate the land sea mask
pts0=np.transpose([np.tile(lon0,len(lat0)),np.repeat(lat0,len(lon0))])
pts1=np.transpose([np.tile(lon,ny),np.repeat(lat,nx)])
lsmr=np.ravel(lsm0)
lsm1=griddata(pts0,lsmr,pts1,method='nearest')
lsm =lsm1.reshape(ny,nx)
slm = (lsm+1)%2

nl  = np.zeros(nx)
ns  = np.zeros(nx)
for ii in np.arange(nx):
	nl[ii] = np.sum(lsm[:,ii])
	ns[ii] = np.sum(slm[:,ii])
nl = np.sum(nl)
ns = np.sum(ns)

prange = 1
nbins  = 400

shist = np.zeros(nbins)
lhist = np.zeros(nbins)
thist = np.zeros(nbins)

#for tt in np.arange(len(times)):
for tt in np.arange(32):
        print 'loading fields for hour: ',3*tt
        precip = ReadFile(directory+filename+timedump[tt],'pcp')
        pcp0   = precip[0,yinds[0]:yinds[-1]+1,xinds[0]:xinds[-1]+1]

        # replace any missing values with 0
        pcp = np.zeros((ny,nx))
        for ii in np.arange(ny):
                for jj in np.arange(nx):
                        if pcp0[ii,jj] > -1.0e-4:
                                pcp[ii,jj] = 1.0*pcp0[ii,jj]

	pcp = pcp.flatten()
	pcp = pcp/prange
	lsm = lsm.flatten()
	for ii in np.arange(nx*ny):
		rbin = np.int(pcp[ii])
		thist[rbin] = thist[rbin] + 1
		if lsm[ii]:
			lhist[rbin] = lhist[rbin] + 1
		else:
			shist[rbin] = shist[rbin] + 1


#lhist = 100*lhist/32/nl
#shist = 100*shist/32/ns
#thist = 100*thist/32/(nx*ny)
lhist = 100*lhist/4/8/nl
shist = 100*shist/4/8/ns
thist = 100*thist/4/8/(nx*ny)
print np.sum(lhist)
print np.sum(shist)
print np.sum(thist)

np.savetxt('lon_newguinea_trmm.txt',lon)
np.savetxt('lat_newguinea_trmm.txt',lat)
np.savetxt('rain_hist_l_trmm.txt',lhist)
np.savetxt('rain_hist_s_trmm.txt',shist)
np.savetxt('rain_hist_t_trmm.txt',thist)

fig=plt.figure()		
ax=fig.add_subplot(111)
rfall = prange*np.arange(nbins)
plt.semilogy(rfall,thist,'-o',rfall,lhist,'-o',rfall,shist,'-o')
plt.legend(('total','land','sea'),loc=0)
plt.xlabel('precipitation (mm)')
plt.ylabel('% of domain area')
plt.title('rainfall histogram (TRMM)')
pylab.savefig('rain_histogram_trmm_dayavg.png')
plt.show()
