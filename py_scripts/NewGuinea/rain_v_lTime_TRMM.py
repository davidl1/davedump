#!/usr/bin/env python

import numpy as np
from scipy.io import netcdf
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import pylab

def ReadFile(filename,fieldname):
        infile = netcdf.netcdf_file(filename,'r')
        field  = infile.variables[fieldname]
	infile.close()
        return field[:]

directory = '/data/cdgs1/hassimme/POSTDOC/TRMM/3B42/Ver7/'
filename  = '3B42.'
timedump = ['20100206.00.7.nc','20100202.03.7.nc','20100202.06.7.nc','20100202.09.7.nc',
            '20100202.12.7.nc','20100202.15.7.nc','20100202.18.7.nc','20100202.21.7.nc',
            '20100203.00.7.nc','20100203.03.7.nc','20100203.06.7.nc','20100203.09.7.nc',
            '20100203.12.7.nc','20100203.15.7.nc','20100203.18.7.nc','20100203.21.7.nc',
            '20100204.00.7.nc','20100204.03.7.nc','20100204.06.7.nc','20100204.09.7.nc',
            '20100204.12.7.nc','20100204.15.7.nc','20100204.18.7.nc','20100204.21.7.nc',
            '20100205.00.7.nc','20100205.03.7.nc','20100205.06.7.nc','20100205.09.7.nc',
            '20100205.12.7.nc','20100205.15.7.nc','20100205.18.7.nc','20100205.21.7.nc']

lat  = ReadFile(directory+filename+timedump[0],'latitude')
lon  = ReadFile(directory+filename+timedump[0],'longitude')
lat0 = ReadFile(directory+'XLAT_d03_NewGuinea.nc','XLAT')
lon0 = ReadFile(directory+'XLONG_d03_NewGuinea.nc','XLONG')
lsm0 = ReadFile(directory+'LANDMASK_d03_NewGuinea.nc','LANDMASK')
lat0 = lat0[0,:,0]
lon0 = lon0[0,0,:]
lsm0 = lsm0[0,:,:]

xinds = np.arange(106)+1228
yinds = np.arange(88)+123
lon   = lon[xinds]
lat   = lat[yinds]
ny    = len(lat)
nx    = len(lon)
dx     = np.ones(7)
dx     = np.int(nx/7)*dx
dx[-1] = dx[-1]+nx%7
print dx
tspan = 24.0*(lon[-1]-lon[0])/360.0
print 'time span (hrs):  ',tspan
print 'long span (degs): ',lon[0],'\t',lon[-1]
print 'lat  span (degs): ',lat[0],'\t',lat[-1]

# interpolate the land sea mask
pts0=np.transpose([np.tile(lon0,len(lat0)),np.repeat(lat0,len(lon0))])
pts1=np.transpose([np.tile(lon,ny),np.repeat(lat,nx)])
lsmr=np.ravel(lsm0)
lsm1=griddata(pts0,lsmr,pts1,method='nearest')
lsm =lsm1.reshape(ny,nx)
slm = (lsm+1)%2

# plot the land sea masks
fig=plt.figure()
ax=fig.add_subplot(111)
cd=ax.contour(lon0,lat0,lsm0,colors='k')
ax.contourf(lon,lat,lsm)
plt.show()

nl  = np.zeros(nx)
ns  = np.zeros(nx)
for ii in np.arange(nx):
	nl[ii] = np.sum(lsm[:,ii])
	ns[ii] = np.sum(slm[:,ii])

# break the time series up into 15 minute segments
lrain = np.zeros(4*8)
srain = np.zeros(4*8)
larea = np.zeros(4*8)
sarea = np.zeros(4*8)

for hr in np.arange(4*8):
	hour = hr%8

	print 'loading fields for hour: ',3*hr
	precip = ReadFile(directory+filename+timedump[hr],'pcp')
	pcp0   = precip[0,yinds[0]:yinds[-1]+1,xinds[0]:xinds[-1]+1]

	# replace any missing values with 0
	pcp = np.zeros((ny,nx))
	for ii in np.arange(ny):
		for jj in np.arange(nx):
			if pcp0[ii,jj] > -1.0e-4:
				pcp[ii,jj] = 1.0*pcp0[ii,jj]

	lr   = np.zeros(7)
	sr   = np.zeros(7)
	la   = np.zeros(7)
	sa   = np.zeros(7)
	for ii in np.arange(7):
		for jj in np.arange(dx[ii]) + ii*dx[0]:
			lr[ii] = lr[ii] + np.sum(pcp[:,jj]*lsm[:,jj])
			sr[ii] = sr[ii] + np.sum(pcp[:,jj]*slm[:,jj])
			la[ii] = la[ii] + nl[jj]
			sa[ii] = sa[ii] + ns[jj]

	inds = np.arange(7) + 4*hour
	inds = inds%(4*8)
	lrain[inds] = lrain[inds] + lr
	srain[inds] = srain[inds] + sr
	larea[inds] = larea[inds] + la
	sarea[inds] = sarea[inds] + sa

avg_lrain = np.zeros(8)
avg_srain = np.zeros(8)
for ii in np.arange(8):
	inds = np.arange(4) + 4*ii
	avg_lrain[ii] = np.sum(lrain[inds]/larea[inds])/4
	avg_srain[ii] = np.sum(srain[inds]/sarea[inds])/4

fig=plt.figure()
ax=fig.add_subplot(111)
time=3*np.arange(8)
avg_lrain_2=np.zeros(8)
avg_srain_2=np.zeros(8)
GMT=9
for ii in np.arange(8):
	ind=(ii-GMT/3)%8
	avg_lrain_2[ii]=avg_lrain[ind]
	avg_srain_2[ii]=avg_srain[ind]

plt.plot(time,avg_lrain_2,'-o',time,avg_srain_2,'-o')
plt.legend(('land','sea'),loc=0)
plt.xlabel('local time (hrs)')
plt.ylabel('precipitation rate (mm)')
plt.xlim([0,21])
plt.title('precipitation vs. local time for land and sea (TRMM)')
pylab.savefig('avg_diurnal_cycle_trmm.png')
plt.show()
