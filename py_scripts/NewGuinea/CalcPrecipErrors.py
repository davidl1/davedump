#!/usr/bin/env python

import numpy as np
from scipy.interpolate import *

from matplotlib import pyplot as plt
from matplotlib import colors as cls
import pylab

lat_w=np.loadtxt('lat_96_wrf.txt')
lon_w=np.loadtxt('lon_96_wrf.txt')
pcp_w=np.loadtxt('pcp_96_wrf.txt')
lat_u=np.loadtxt('lat_96_um.txt')
lon_u=np.loadtxt('lon_96_um.txt')
pcp_u=np.loadtxt('pcp_96_um.txt')
lat_t=np.loadtxt('lat_96_trmm.txt')
lon_t=np.loadtxt('lon_96_trmm.txt')
pcp_t=np.loadtxt('pcp_96_trmm.txt')

Xu=np.zeros((len(lat_u)*len(lon_u),2))
kk=0
for ii in np.arange(len(lat_u)):
	for jj in np.arange(len(lon_u)):
		Xu[kk][0]=lon_u[jj]
		Xu[kk][1]=lat_u[ii]
		kk=kk+1

Xw=np.zeros((len(lat_w)*len(lon_w),2))
kk=0
for ii in np.arange(len(lat_w)):
	for jj in np.arange(len(lon_w)):
		Xw[kk][0]=lon_w[jj]
		Xw[kk][1]=lat_w[ii]
		kk=kk+1

Xt=np.zeros((len(lat_t)*len(lon_t),2))
kk=0
for ii in np.arange(len(lat_t)):
	for jj in np.arange(len(lon_t)):
		Xt[kk][0]=lon_t[jj]
		Xt[kk][1]=lat_t[ii]
		kk=kk+1

Pu=np.ravel(pcp_u)
Pw=np.ravel(pcp_w)

pu=griddata(Xu,Pu,Xt,method='cubic')
pw=griddata(Xw,Pw,Xt,method='cubic')
pu=pu.reshape(len(lat_t),len(lon_t))
pw=pw.reshape(len(lat_t),len(lon_t))

print pu.shape
print pw.shape
print pcp_t.shape

err_u=0.
err_w=0.
tsq=0.
for ii in 5+np.arange(len(lat_t)-10):
	for jj in 5+np.arange(len(lon_t)-10):
		err_u=err_u+(pu[ii,jj])*(pu[ii,jj])
		err_w=err_w+(pw[ii,jj])*(pw[ii,jj])
		err_u = err_u + (pu[ii,jj]-pcp_t[ii,jj])*(pu[ii,jj]-pcp_t[ii,jj])
		err_w = err_w + (pw[ii,jj]-pcp_t[ii,jj])*(pw[ii,jj]-pcp_t[ii,jj])
		tsq   = tsq   + pcp_t[ii,jj]*pcp_t[ii,jj]

#err_u=(pu-pcp_t)*(pu-pcp_t)
#err_w=(pw-pcp_t)*(pw-pcp_t)
#tsq  =pcp_t*pcp_t

print 'um err:       ',np.sqrt(err_u)
print 'wrf err:      ',np.sqrt(err_w)
print 'sum_trmm:     ',np.sqrt(tsq)
print 'norm um err:  ',np.sqrt(err_u)/np.sqrt(tsq)
print 'norm wrf err: ',np.sqrt(err_w)/np.sqrt(tsq)

fig = plt.figure()
ax = fig.add_subplot(111)
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
levs=[0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
cmap=cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm=cls.BoundaryNorm(levs,cmap.N)
co = ax.contourf(lon_t,lat_t,pw,levels=levs,cmap=cmap,norm=norm)
plt.colorbar(co,orientation='WRF')
plt.title('UM-orig')

fig = plt.figure()
ax = fig.add_subplot(111)
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
levs=[0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
cmap=cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm=cls.BoundaryNorm(levs,cmap.N)
co = ax.contourf(lon_t,lat_t,pu,levels=levs,cmap=cmap,norm=norm)
plt.colorbar(co,orientation='horizontal')
plt.title('UM-interp')

fig = plt.figure()
ax = fig.add_subplot(111)
white      = [1.0,1.0,1.0]
cyan       = [0.0,0.8,1.0]
blue       = [0.0,0.4,1.0]
green      = [0.0,0.5,0.0]
lightgreen = [0.7,1.0,0.3]
yellow     = [1.0,0.9,0.0]
orange     = [1.0,0.5,0.0]
red        = [1.0,0.0,0.0]
darkred    = [0.5,0.0,0.0]
levs=[0.,5.,10.,25.,50.,100.,200.,300.,400.,800.]
cmap=cls.ListedColormap([white,cyan,blue,green,lightgreen,yellow,orange,red,darkred])
norm=cls.BoundaryNorm(levs,cmap.N)
co = ax.contourf(lon_t,lat_t,pcp_t,levels=levs,cmap=cmap,norm=norm)
plt.colorbar(co,orientation='horizontal')
plt.title('TRMM')

plt.show()
